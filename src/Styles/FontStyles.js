import {Platform, StyleSheet} from 'react-native';

const FontStyles = StyleSheet.create({
  // Segoe_Reg: {fontFamily: 'Roboto-Regular'},
  Segoe_Reg: {fontFamily: 'Segoe UI'},
  // Segoe_Bold: {fontFamily: 'Roboto-Bold'},
  Segoe_Bold: {
    fontFamily: Platform.OS == 'ios' ? 'Roboto-Bold' : 'Segoe UI Bold',
  },
});

export default FontStyles;
