import {StyleSheet} from 'react-native';
import Colors from '../Constants/Colors';
import Responsive from '../Constants/Responsive';

const SpStyles = StyleSheet.create({
  shadow: {
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.41,
    shadowRadius: 9.11,
    elevation: 14,
  },
  shadowLight: {
    shadowColor: 'rgba(0, 0, 0, 0.9)',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  shadowLight3: {
    shadowColor: 'rgba(0, 0, 0, 0.9)',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  shadowLight2: {
    shadowColor: 'rgba(0, 0, 0, 0.9)',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 1.24,
    elevation: 5,
  },
  FDR_ALC: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  FDR_ALC_JCS: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  FDR_ALS_JCS: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  FDR_ALC_JCS_W100: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  FDR_ALC_JCS_W92_self: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: Responsive.widthPx(92),
    alignSelf: 'center',
  },
  flex1: {flex: 1},
  flexGrow1: {flexGrow: 1},
  height0: {height: 0},
  bgColorWhite: {
    backgroundColor: Colors.white,
  },
  FDR_WRAP: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  FDR_Wr100: {
    flexDirection: 'row',
    width: Responsive.widthPx(100),
  },
  ALC_JCC: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SpStyles;
