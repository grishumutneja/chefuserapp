import {requestLocationPermission} from '../Components/Permissions/LocationPermission';
import {
  getUserInfoList,
  guestAccount,
} from '../Core/Redux/Slices/GetUserInfoSlice';
import {setAccessToken} from './GlobalFunction';

const {
  default: AsyncStorage,
} = require('@react-native-async-storage/async-storage');
const {
  getHomeBannerCall,
  getDeliveryAddressCall,
  getSocialMediaApiCall,
  getUserInfoCall,
  getRestHomeBannerCall,
  userAllTransactionApiCall,
} = require('../Core/Config/ApiCalls');
const {default: AsKey} = require('./AsKey');

const CallInitialApi = async ({dispatch}) => {
  requestLocationPermission(dispatch);
  AsyncStorage.getItem(AsKey.userLoginData)
    .then(JSON.parse)
    .then(async response => {
      const loginDataCont = {
        isLogin: true,
        loginData: response,
      };
      dispatch(getUserInfoList(loginDataCont));
      getHomeBannerCall(dispatch);
      getDeliveryAddressCall({
        dispatch: dispatch,
        userId: response.user_id,
      });
      userAllTransactionApiCall({dispatch, userId: response.user_id});
      getSocialMediaApiCall({dispatch});
      getUserInfoCall({
        dispatch: dispatch,
        userId: response.user_id,
      });
    });

  AsyncStorage.getItem(AsKey.isGuestAccount)
    .then(JSON.parse)
    .then(res => {
      if (res == true) {
        dispatch(guestAccount(true));
      } else {
        dispatch(guestAccount(false));
      }
    });

  AsyncStorage.getItem(AsKey.userLocation)
    .then(JSON.parse)
    .then(res => {
      if (res) {
        getRestHomeBannerCall({dispatch: dispatch, location: res});
      }
    })
    .catch(err => console.warn('err', err));
};

export default CallInitialApi;
