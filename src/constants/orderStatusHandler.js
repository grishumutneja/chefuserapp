export const orderStatusHandler = order_status => {
  const orderStatus =
    order_status == 'pending'
      ? 'Waiting'
      : order_status == 'confirmed'
      ? 'Pending'
      : order_status == 'preparing'
      ? 'Preparing'
      : order_status == 'ready_to_dispatch'
      ? 'Ready to dispatch'
      : order_status == 'dispatched'
      ? 'Out for delivery'
      : order_status == 'completed'
      ? 'Delivered'
      : order_status == 'completed'
      ? 'Delivered'
      : order_status !== 'cancelled_by_customer_before_confirmed' ||
        'cancelled_by_customer_after_confirmed' ||
        'cancelled_by_customer_during_prepare' ||
        'cancelled_by_customer_after_disptch'
      ? 'Cancelled'
      : order_status == 'cancelled_by_vendor'
      ? 'Cancelled'
      : order_status == 'completed'
      ? 'Delivered'
      : '';
  return orderStatus;
};
