import AsyncStorage from '@react-native-async-storage/async-storage';
import {Variables} from './Variable';
import instance from '../Core/Config/apiConfig';

export const setAccessToken = async payload => {
  instance.defaults.headers.common['Authorization'] = 'Bearer ' + payload;
  // instance.defaults.headers.common['Authorization'] = 'Bearer ' + payload;
  await saveData(Variables.accessToken, payload);
};

export const saveData = async (key, data) => {
  await AsyncStorage.setItem(key, data);
};
