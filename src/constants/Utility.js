// import toast from '../Components/Toast/toast';
import Toast from 'react-native-toast-message';
import Responsive from './Responsive';

const deepClone = val => {
  return JSON.parse(JSON.stringify(val));
};

const showToast = message => {
  // Toast.danger({message: message, duration: 2000});
  Toast.show({
    type: 'tomatoToast',
    text1: message,
    bottomOffset: Responsive.widthPx(20),
    position: 'bottom',
    visibilityTime: 1200,
  });
};

const isValid = (value, isEmail = false) => {
  if (isEmail) {
    const reg = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/;
    return !value.trim() || !reg.test(value.trim());
  }
  return value === null || value === undefined || !value.trim();
};

const Utility = {
  deepClone,
  isValid,
  showToast,
};

export default Utility;
