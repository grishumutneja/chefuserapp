const Animations = {
  process_failed: require('../Assets/Animations/process_failed.json'),
  random: require('../Assets/Animations/random.json'),
  imageLoading: require('../Assets/Animations/imageLoading.json'),
  cong: require('../Assets/Animations/34.json'),
  suc: require('../Assets/Animations/12.json'),
  waitPayment: require('../Assets/Animations/waitPayment.json'),
  happyFace: require('../Assets/Animations/happyFace.json'),
  check_mark: require('../Assets/Animations/check_mark.json'),
  saving_money: require('../Assets/Animations/saving_money.json'),
  freeDelivery: require('../Assets/Animations/freeDelivery.json'),
  freeDelivery2: require('../Assets/Animations/freeDelivery2.json'),
  freeDelivery3: require('../Assets/Animations/freeDelivery3.json'),
};

export default Animations;
