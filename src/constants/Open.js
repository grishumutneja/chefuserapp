import {Linking, Platform} from 'react-native';

const message = ' ';
let whatsAppMsg = 'Hey, I need help!';

const anyUrl = url => {
  url &&
    Linking.openURL(url).catch(err => {
      alert('Failed to Open');
    });
};

const whatsapp = clientMobile1 => {
  const WHATS_APP_URL = `whatsapp://send?text=${whatsAppMsg}&phone=91${clientMobile1}`;
  Linking.openURL(WHATS_APP_URL).catch(err => {
    alert('Make sure WhatsApp installed on your device');
  });
};

const phoneCall = mobileNumber => {
  let phoneNumber = '';
  if (Platform.OS === 'android') {
    phoneNumber = `tel:${mobileNumber}`;
  } else {
    phoneNumber = `telprompt:${mobileNumber}`;
  }
  const PHONE_CALL_URL = `${phoneNumber}`;
  Linking.openURL(PHONE_CALL_URL).catch(err => {
    console.error('Failed opening page because: ', err);
    alert('Failed to open page' + err);
  });
};

const email = emailAddress => {
  const mailSubject = '365 Touch';
  const mailDescription = 'Created with 365 Touch Digital Post';
  const MAIL_URL = `mailto:${emailAddress}?subject=${mailSubject}&body=${mailDescription}`;
  Linking.openURL(MAIL_URL).catch(err => {
    console.error('Failed opening page because: ', err);
    alert('Failed to open page');
  });
};

const Open = {anyUrl, whatsapp, phoneCall, email};

export default Open;
