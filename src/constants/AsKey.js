const AsKey = {
  userLocation: 'userLocation',
  userToken: 'userToken',
  userLoginData: 'userLoginData',
  fcmToken: 'fcmToken',

  screenStart: 'screenStart',
  screenEnd: 'screenEnd',
  appStart: 'appStart',
  cartData: 'cartData2',
  isGuestAccount: 'isGuestAccount',
};

export default AsKey;
