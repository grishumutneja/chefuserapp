import React, {useEffect, useRef, useState} from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import TextStyles from '../Styles/TextStyles';
import Colors from './Colors';

const OtpInputPart = ({setOtpCode}) => {
  const ref1 = useRef();
  const ref2 = useRef();
  const ref3 = useRef();
  const ref4 = useRef();
  const [letter1, setLetter1] = useState();
  const [letter2, setLetter2] = useState();
  const [letter3, setLetter3] = useState();

  useEffect(() => {
    ref1.current.focus();
  }, []);

  return (
    <View style={{flexDirection: 'row'}}>
      <TextInput
        style={styles.boxStyle}
        maxLength={1}
        ref={ref1}
        keyboardType={'number-pad'}
        onChangeText={text => {
          if (text.length === 1) {
            ref2.current.focus();
            const myOTP = letter1;
            setOtpCode(myOTP);
            setLetter1(text);
          }
        }}
      />
      <TextInput
        style={styles.boxStyle}
        maxLength={1}
        ref={ref2}
        keyboardType={'number-pad'}
        onChangeText={text => {
          setLetter2(text);
          if (text.length === 1) {
            const myOTP = letter1 + letter2;
            setOtpCode(myOTP);
            ref3.current.focus();
          } else {
            const myOTP = letter1;
            setOtpCode(myOTP);
            text.length === 0 && ref1.current.focus();
          }
        }}
        onKeyPress={({nativeEvent}, text) => {
          if (nativeEvent.key === 'Backspace') {
            ref1.current.focus();
          }
        }}
      />
      <TextInput
        style={styles.boxStyle}
        maxLength={1}
        ref={ref3}
        keyboardType={'number-pad'}
        onChangeText={text => {
          setLetter3(text);
          if (text.length === 1) {
            const myOTP = letter1 + letter2 + letter3;
            setOtpCode(myOTP);
            ref4.current.focus();
          } else {
            const myOTP = letter1 + letter2;
            setOtpCode(myOTP);
            text.length === 0 && ref2.current.focus();
          }
        }}
        onKeyPress={({nativeEvent}) => {
          if (nativeEvent.key === 'Backspace') {
            ref2.current.focus();
          }
        }}
      />
      <TextInput
        style={styles.boxStyle}
        maxLength={1}
        keyboardType={'number-pad'}
        ref={ref4}
        onChangeText={text => {
          if (text.length === 1) {
            // ref1.current.focus();
            const myOTP = letter1 + letter2 + letter3 + text;
            setOtpCode(myOTP);
          } else {
            const myOTP = letter1 + letter2 + letter3;
            setOtpCode(myOTP);
            text.length === 0 && ref3.current.focus();
          }
        }}
        onKeyPress={({nativeEvent}) => {
          if (nativeEvent.key === 'Backspace') {
            ref3.current.focus();
          }
        }}
      />
    </View>
  );
};
export default OtpInputPart;

const styles = StyleSheet.create({
  boxStyle: {
    width: 42,
    height: 42,
    borderWidth: 0.8,
    borderColor: Colors.darkGray,
    backgroundColor: Colors.lightGray,
    textAlign: 'center',
    marginRight: 10,
    borderRadius: 10,
    ...TextStyles.black_S_16_700,
  },
});
