const {default: MyData} = require('./MyData');

export const getLocationByLatLong = ({
  position,
  setCityName,
  setLocationName,
}) => {
  fetch(
    'https://maps.googleapis.com/maps/api/geocode/json?address=' +
      position?.lat +
      ',' +
      position?.lng +
      '&key=' +
      MyData.googleMapApiKey,
  )
    .then(response => response.json())
    .then(responseJson => {
      let LocationName = responseJson?.results[0]?.formatted_address;
      setLocationName && setLocationName(LocationName);
      if (responseJson.status === 'OK') {
        if (responseJson.results.length > 8) {
          if (responseJson.results[5].address_components.length > 4) {
            let str = `${responseJson.results[5].address_components[1].long_name}, ${responseJson.results[5].address_components[3].long_name}, ${responseJson.results[5].address_components[4].long_name}`;
            setCityName && setCityName(str);
          } else {
            let str = `${responseJson.results[5].address_components[1].long_name}, ${responseJson.results[5].address_components[2].long_name}, ${responseJson.results[5].address_components[3].long_name}`;
            setCityName && setCityName(str);
          }
        }
        if (responseJson.results.length == 6) {
          let str = `${responseJson.results[1].address_components[0].long_name}, ${responseJson.results[1].address_components[2].long_name}, ${responseJson.results[1].address_components[3].long_name}`;
          setCityName && setCityName(str);
        }

        if (responseJson.results.length == 7) {
          let str = `${responseJson.results[1].address_components[0].long_name}, ${responseJson.results[1].address_components[2].long_name}, ${responseJson.results[1].address_components[3].long_name}`;
          setCityName && setCityName(str);
        }
        if (responseJson.results.length == 8) {
          let str = `${responseJson.results[5].address_components[0].long_name}, ${responseJson.results[5].address_components[1].long_name}, ${responseJson.results[5].address_components[2].long_name}`;
          setCityName && setCityName(str);
        }
      }
    });
};
