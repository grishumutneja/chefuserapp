import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import {Image} from 'react-native';
import Images from '../../Constants/Images';

const NoInternetScreen = () => {
  return (
    <View style={styles.container}>
      <Image source={Images.noWifi} style={styles.img} resizeMode="contain" />
      <Text style={styles.title}>Oops, No Internet Connection</Text>
      <Text style={styles.text}>
        Make sure wifi or cellular data is turned on and then try again.
      </Text>
    </View>
  );
};

export default NoInternetScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 150,
    backgroundColor: 'white',
  },
  img: {width: 150, height: 150, alignSelf: 'center'},
  title: {
    marginTop: 24,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '700',
    color: 'black',
  },
  text: {
    marginTop: 24,
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
    marginHorizontal: 30,
    color: 'black',
  },
});
