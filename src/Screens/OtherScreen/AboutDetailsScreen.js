import {View, Text, SafeAreaView, FlatList, ScrollView} from 'react-native';
import React from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import {useRoute} from '@react-navigation/native';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';

const AboutDetailsScreen = () => {
  const route = useRoute();
  const headerTitle = route?.params?.headerTitle;
  const data = route?.params?.data;

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={headerTitle} backVisible={true} />
        <ScrollView>
          <View
            style={{
              padding: Responsive.widthPx(4),
            }}>
            <Text style={[TextStyles.black_S_16_400, {lineHeight: 22}]}>
              {data}
            </Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default AboutDetailsScreen;
