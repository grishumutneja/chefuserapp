import {
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Images from '../../Constants/Images';
import RedBorderButton from '../../Components/Buttons/RedBorderButton';
import TextStyles from '../../Styles/TextStyles';
import SpStyles from '../../Styles/SpStyles';
import RedButton from '../../Components/Buttons/RedButton';
import {AddDeliveryAddressModal} from '../../Components/Modals/AddDeliveryAddressModal';
import SearchLocationModal from '../../Components/Modals/SearchLocationModal';
import {useSelector} from 'react-redux';
import Geolocation from 'react-native-geolocation-service';
import {getLocationByLatLong} from '../../Constants/GetLocationByLatLong';
import {useNavigation, useRoute} from '@react-navigation/native';
import {UpdateLocationModal} from '../../Components/Modals/UpdateLocationModal';
import Screens from '../../Core/Stack/Screens';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';

const MapsScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const searchRef = useRef(null);
  const {locationData} = useSelector(state => state.location);
  const [location, setLocation] = useState({
    lat: 22.719568591882783,
    lng: 75.85772573947906,
  });
  const [locationName, setLocationName] = useState();
  const [cityName, setCityName] = useState();
  const [searchLocModalVisible, setSearchLocModalVisible] = useState(false);
  const [updateModalVisible, setUpdateModalVisible] = useState(false);
  const [addDeliveryVisible, setAddDeliveryVisible] = useState(false);
  const [isSearched, setIsSearched] = useState(false);
  const [ischange, setChange] = useState(false);
  const isUpdate = route?.params?.isUpdate;
  const UpdateAddress = route?.params?.UpdateAddress;
  const isFromCart = route?.params?.isFromCart;
  const [fullAddress, setFullAddress] = useState(UpdateAddress?.fullAddress);
  const [landmark, setLandmark] = useState(UpdateAddress?.landmark);
  const [contact, setContact] = useState(UpdateAddress?.contact);
  const [isPrimary, setIsPrimary] = useState(UpdateAddress?.isPrimary);
  const [defaultType, setDefaultType] = useState(UpdateAddress?.defaultType);
  const [addressId, setAddressId] = useState(UpdateAddress?.addressId);

  const [hide, setHide] = useState(true);
  const UpdateAddress2 = {
    setDefaultType: setDefaultType,
    defaultType: defaultType,
    setFullAddress: setFullAddress,
    fullAddress: fullAddress,
    setLandmark: setLandmark,
    landmark: landmark,
    setContact: setContact,
    contact: contact,
    setIsPrimary: setIsPrimary,
    isPrimary: isPrimary,
    setLocation: setLocation,
    location: location,
    setLocationName: setLocationName,
    locationName: locationName,
    setAddressId: setAddressId,
    addressId: addressId,
  };

  useEffect(() => {
    setTimeout(() => {
      currentLocationHandler();
    }, 1100);
  }, []);

  const currentLocationHandler = () => {
    setHide(true);
    Geolocation.getCurrentPosition(
      position => {
        if (position) {
          const locationData = {
            lat: position?.coords?.latitude,
            lng: position?.coords?.longitude,
          };
          getLocationByLatLong({
            setLocationName: setLocationName,
            position: locationData,
            setCityName: setCityName,
          });
          setLocation(locationData);
          setTimeout(() => {
            setHide(false);
          }, 600);
        }
      },
      error => {
        console.warn('map error: ', error);
        console.warn(error.code, error.message);
        setTimeout(() => {
          setHide(false);
        }, 600);
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
    // setHide(false);
  };

  useEffect(() => {
    location &&
      getLocationByLatLong({
        setLocationName: ischange
          ? !isSearched && setLocationName
          : !locationName && !isSearched && setLocationName,
        position: location,
        setCityName: setCityName,
      });
  }, [location]);

  const renderSearch = () => {
    return (
      <TouchableOpacity
        style={CmnStyles.mapScreenSearchCont}
        onPress={() => {
          setSearchLocModalVisible(true);
          searchRef?.current?.focus();
        }}>
        <Image
          source={Images.location}
          style={CmnStyles.locationIconMap}
          resizeMode="contain"
        />
        <Text style={CmnStyles.mapScreenSearchText}>
          Search For Area, Street Name
        </Text>
      </TouchableOpacity>
    );
  };

  console.log('location map', location);

  const renderBottom = () => {
    return (
      <View style={CmnStyles.mapScreenBottomContMain}>
        <RedBorderButton
          text={'Use Current Location'}
          otherStyle={CmnStyles.mapScreenLocationBtn}
          otherTextStyle={TextStyles.red_17_700}
          onPress={() => {
            currentLocationHandler();
          }}
        />
        <View style={CmnStyles.mapScreenBottomCont}>
          <Image
            source={Images.location}
            style={CmnStyles.locationIconMapBig}
            resizeMode="contain"
          />
          <View style={SpStyles.flex1}>
            <Text style={CmnStyles.mapScreenDarkText}>{locationName}</Text>
            <Text style={CmnStyles.mapScreenText}>{cityName}</Text>
          </View>
        </View>
        {hide == true ? (
          <RedButton
            text={''}
            otherStyle={{
              marginBottom: Responsive.widthPx(6),
              marginTop: Responsive.widthPx(6),
              backgroundColor: Colors.white,
            }}
          />
        ) : (
          <RedButton
            text={'Continue'}
            otherStyle={CmnStyles.MapScreenContBtn}
            onPress={() => {
              if (isUpdate) {
                setUpdateModalVisible(true);
                UpdateAddress2.locationName = locationName;
                // UpdateAddress2.setFullAddress = setFullAddress;
                // UpdateAddress2.fullAddress = fullAddress =
                //   UpdateAddress.fullAddress;
              } else {
                setAddDeliveryVisible(true);
              }
            }}
          />
        )}
        {/* )} */}
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack text={'Select from Map'} backVisible={true} />
          {hide ? (
            <View
              style={{
                height: Responsive.heightPx(86),
                width: Responsive.widthPx(100),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator size={'large'} color={Colors.red} />
            </View>
          ) : (
            <View style={CmnStyles.mapStyleTrack}>
              {location && (
                <MapView
                  provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                  style={CmnStyles.mapStyleTrack}
                  onRegionChangeComplete={res => {
                    console.log('res onRegionChangeComplete==>', res);
                    const newLocation = {
                      lat: res?.latitude,
                      lng: res?.longitude,
                    };
                    const refLocation = {
                      lat: 48.77451823596422,
                      lng: -1.8263999372720727,
                    };
                    JSON.stringify(newLocation) !==
                      JSON.stringify(refLocation) && setLocation(newLocation);
                    setChange(true);
                    setTimeout(() => {
                      setChange(false);
                    }, 1000);
                  }}
                  region={{
                    latitude:
                      Platform.OS == 'ios'
                        ? location?.lat
                        : parseFloat(location?.lat),
                    longitude:
                      Platform.OS == 'ios'
                        ? location?.lng
                        : parseFloat(location?.lng),
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                  }}>
                  <Marker
                    coordinate={{
                      latitude:
                        Platform.OS == 'ios'
                          ? location?.lat
                          : parseFloat(location?.lat),
                      longitude:
                        Platform.OS == 'ios'
                          ? location?.lng
                          : parseFloat(location?.lng),
                      latitudeDelta: 0.015,
                      longitudeDelta: 0.0121,
                    }}
                    title={'Your Location'}
                  />
                </MapView>
              )}
            </View>
          )}
          {!hide && renderSearch()}
          {!hide && renderBottom()}
          <AddDeliveryAddressModal
            modalVisible={addDeliveryVisible}
            onPressCancel={() => {
              setAddDeliveryVisible(false);
            }}
            setAddDeliveryVisible={setAddDeliveryVisible}
            location={location}
            locationName={locationName}
            isFromCart={isFromCart}
          />
          <SearchLocationModal
            searchRef={searchRef}
            visible={searchLocModalVisible}
            onPressCancel={() => {
              setSearchLocModalVisible(false);
            }}
            setLocation={setLocation}
            setSearchLocModalVisible={setSearchLocModalVisible}
            setLocationName={setLocationName}
            setIsSearched={setIsSearched}
          />
          <UpdateLocationModal
            modalVisible={updateModalVisible}
            onPressCancel={() => {
              setUpdateModalVisible(false);
              navigation.navigate(Screens.SavedAddress);
            }}
            UpdateAddress={UpdateAddress2}
            setUpdateModalVisible={setUpdateModalVisible}
            location={location}
          />
        </View>
      </SafeAreaView>
    </View>
  );
};

export default MapsScreen;
