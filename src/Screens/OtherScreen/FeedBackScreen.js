import {View, SafeAreaView, ScrollView} from 'react-native';
import React, {useState} from 'react';
import _ from 'lodash';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import ProfileInput from '../../Components/Inputs/ProfileInput';
import Responsive from '../../Constants/Responsive';
import RedButton from '../../Components/Buttons/RedButton';
import Utility from '../../Constants/Utility';
import {useSelector} from 'react-redux';
import {
  guestAccountHandler,
  saveFeedbackApiCall,
} from '../../Core/Config/ApiCalls';
import {useNavigation} from '@react-navigation/native';

const FeedBackScreen = () => {
  const navigation = useNavigation();
  const [subject, setSubject] = useState();
  const [description, setDescription] = useState();
  const {getUserInfoData, isGuestAccount} = useSelector(
    state => state.userInfo,
  );

  const submitHandler = () => {
    if (!_.trim(subject)) {
      return Utility.showToast('Please enter subject');
    } else if (!_.trim(description)) {
      return Utility.showToast('Please enter description');
    } else {
      let body = {
        subject: subject,
        description: description,
      };
      saveFeedbackApiCall({body, navigation});
    }
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={'Feedback'} backVisible={true} />
        <ScrollView
          style={[CmnStyles.bodyBg]}
          automaticallyAdjustKeyboardInsets
          keyboardShouldPersistTaps="always">
          <View
            style={[CmnStyles.bodyBg, {paddingBottom: Responsive.widthPx(4)}]}>
            <ProfileInput
              title="Name"
              placeholder="Name"
              value={getUserInfoData?.name}
              editActive={false}
            />
            <ProfileInput
              title="Email"
              placeholder="Enter Email"
              value={getUserInfoData?.email}
              editActive={false}
            />
            <ProfileInput
              title="Mobile Number"
              placeholder="Enter Mobile Number"
              value={getUserInfoData?.alternative_number}
              editActive={false}
            />
            <ProfileInput
              title="Subject"
              placeholder="Enter Subject"
              value={subject}
              setValue={setSubject}
              editActive={true}
            />
            <ProfileInput
              title="Description"
              placeholder="Enter Description"
              value={description}
              setValue={setDescription}
              editActive={true}
            />
            <RedButton
              text={'Submit'}
              onPress={() => {
                if (isGuestAccount) {
                  guestAccountHandler({navigation});
                } else {
                  submitHandler();
                }
              }}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default FeedBackScreen;
