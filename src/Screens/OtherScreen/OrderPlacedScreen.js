import React, {useEffect} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import LottieView from 'lottie-react-native';
import Animations from '../../Constants/Animations';
import TextStyles from '../../Styles/TextStyles';
import CmnStyles from '../../Styles/CmnStyles';
import RedButton from '../../Components/Buttons/RedButton';
import {useNavigation, useRoute} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import {
  orderHistoryApiCall,
  removeEmptyCartApiCall,
  userAllTransactionApiCall,
} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';
import {addDriverRate} from '../../Core/Redux/Slices/DriverRateSlice';

const OrderPlacedScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route = useRoute();
  const {getUserInfoData} = useSelector(state => state.userInfo);
  const orderId = route?.params?.orderId;
  const {rateGivenCount} = useSelector(state => state.driverRate);

  useEffect(() => {
    let body = {
      user_id: getUserInfoData?.id,
    };
    removeEmptyCartApiCall({body, dispatch});
    const orderBody = {
      order_for: 'restaurant',
      offset: 0,
    };
    userAllTransactionApiCall({userId: getUserInfoData?.id, dispatch});
    orderHistoryApiCall({dispatch, body: orderBody});
    dispatch(addDriverRate(rateGivenCount + 1));
    setTimeout(() => {
      navigation.replace(Screens.TrackOrder, {
        orderId: orderId,
        isFromPlaced: true,
      });
    }, 3000);
  }, []);

  return (
    <SafeAreaView style={CmnStyles.bodyBg}>
      <View style={styles.container}>
        <LottieView
          source={Animations.check_mark}
          // source={Animations.process_failed}
          style={styles.loti}
          autoPlay={true}
          loop={false}
        />
        <Text style={styles.text}>Order Placed Successfully</Text>
        <RedButton
          text={'Track Order'}
          otherStyle={styles.btn}
          otherTextStyle={TextStyles.white_S_21_700}
          onPress={() => {
            navigation.replace(Screens.TrackOrder, {orderId: orderId});
          }}
        />
      </View>
    </SafeAreaView>
  );
};

export default OrderPlacedScreen;

const styles = StyleSheet.create({
  container: {
    ...CmnStyles.bodyBg,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 100,
  },
  loti: {height: 200, width: 200},
  text: {
    ...TextStyles.darkGray_S_24_700,
  },
  btn: {
    width: '80%',
    paddingHorizontal: 50,
    paddingVertical: 10,
    height: 48,
  },
});
