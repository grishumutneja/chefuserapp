import {useNavigation} from '@react-navigation/native';
import {useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  TextInput,
  SafeAreaView,
  Alert,
} from 'react-native';
import Images from '../../Constants/Images';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import RedButton from '../../Components/Buttons/RedButton';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import {deleteUserApiCall} from '../../Core/Config/ApiCalls';

const deleteData = [
  {text: "I don't want to use ChefLab anymore"},
  {text: "I'm using different account"},
  {text: "I'm worried about my privacy"},
  {text: "You're sending me too many emails/notifications"},
  {text: 'The app is not working properly'},
  {text: 'Other'},
];

const DeleteScreen = () => {
  const navigation = useNavigation();
  const [activeOther, setActiveOther] = useState(false);
  const [reviewText, setReviewText] = useState();

  const RenderDeleteData = ({item, index}) => {
    return (
      <TouchableOpacity
        style={CmnStyles.deleteDataRenderCont}
        onPress={() => {
          item?.text == 'Other'
            ? setActiveOther(true)
            : deleteHandler(item.text);
        }}>
        <Text style={TextStyles.black_S_14_400}>{item.text}</Text>
        <TouchableOpacity style={{width: 16, height: 16, marginRight: 10}}>
          <Image
            style={CmnStyles.forwardIcon}
            source={Images.back}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  const deleteHandler = myReviewText => {
    const body = {delete_reason: myReviewText};
    Alert.alert(
      'Hold On..!',
      'Are you sure want to Delete Account?',
      [
        {
          text: 'Cancel',
          onPress: () => {
            return null;
          },
        },
        {
          text: 'Confirm',
          onPress: () => deleteUserApiCall({body, navigation}),
        },
      ],
      {cancelable: false},
    );
  };

  const renderOther = () => {
    return (
      <View style={CmnStyles.deleteScreenOtherCont}>
        <Text style={TextStyles.black_S_18_700}>
          Anything else you'd like to add?
        </Text>
        <TextInput
          placeholder="Share your feedback for us? (Optional)"
          onChangeText={text => setReviewText(text)}
          style={CmnStyles.deleteScreenInput}
          numberOfLines={4}
          multiline={true}
        />
        <RedButton
          text={'Submit & Delete'}
          onPress={() => {
            deleteHandler(reviewText);
          }}
          otherStyle={{width: 180}}
        />
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack
          backVisible={true}
          text={'Delete Account'}
          downText={'Why would you like to delete your account?'}
          onPressBack={() => {
            activeOther ? setActiveOther(false) : navigation.goBack();
          }}
        />
        {!activeOther ? (
          <FlatList
            data={deleteData}
            renderItem={RenderDeleteData}
            style={{marginTop: 20}}
          />
        ) : (
          renderOther()
        )}
      </SafeAreaView>
    </View>
  );
};

export default DeleteScreen;
