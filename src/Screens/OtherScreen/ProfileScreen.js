import {
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import _ from 'lodash';
import React, {useEffect, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';
import ProfileInput from '../../Components/Inputs/ProfileInput';
import RedButton from '../../Components/Buttons/RedButton';
import TextButton from '../../Components/Buttons/TextButton';
import {CameraModal} from '../../Components/Modals/CameraModal';
import requestCameraPermission from '../../Components/CameraActivity/CameraPermission';
import onCameraPressHandler from '../../Components/CameraActivity/onCameraPressHandler';
import onGalleryPressHandler from '../../Components/CameraActivity/onGalleryPressHandler';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch, useSelector} from 'react-redux';
import Utility from '../../Constants/Utility';
import {getUserInfoCall} from '../../Core/Config/ApiCalls';
import {BASE_URL, UPDATE_USER_INFO} from '../../Core/Config/EndPoint';
import {resetCart} from '../../Core/Redux/Slices/RestaurantsSlices/CartSlice';
import {resetDeliveryAddress} from '../../Core/Redux/Slices/DeliveryAddressSlice';
import AsKey from '../../Constants/AsKey';
import {resetGetUserInfoList} from '../../Core/Redux/Slices/GetUserInfoSlice';
import {resetLocation} from '../../Core/Redux/Slices/CommonSlices/LocationSlice';

const ProfileScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [email, setEmail] = useState();
  const [moNumber, setMoNumber] = useState();
  const [altMobNumber, setAltMobNumber] = useState();
  const [editActive, setEditActive] = useState(false);
  const [isCameraModalVisible, setIsCameraModalVisible] = useState(false);
  const [proImg, setProImg] = useState();
  const {getUserInfoData, loginData, isGuestAccount} = useSelector(
    state => state.userInfo,
  );

  useEffect(() => {
    requestCameraPermission();
    setFirstName(getUserInfoData?.name);
    setLastName(getUserInfoData?.surname);
    setEmail(getUserInfoData?.email);
    setMoNumber(loginData?.mobile);
    setAltMobNumber(getUserInfoData?.alternative_number);
    setProImg(getUserInfoData?.image);
  }, []);

  const options = {
    setLogoImage: setProImg,
    setCameraModalVisible: setIsCameraModalVisible,
  };

  const editHandler = () => {
    if (!_.trim(firstName)) {
      return Utility.showToast('Please Enter First Name');
    } else if (!_.trim(lastName)) {
      return Utility.showToast('Please Enter Last Name');
    } else if (!_.trim(email)) {
      return Utility.showToast('Please Enter Email Id');
    } else if (Utility.isValid(email, true)) {
      return Utility.showToast('Please Enter Valid Email Id');
    } else if (!_.trim(moNumber)) {
      return Utility.showToast('Please Enter Mobile Number');
    } else if (moNumber?.length !== 10) {
      return Utility.showToast('Please Enter Valid Mobile Number');
    } else if (!_.trim(altMobNumber)) {
      return Utility.showToast('Please Enter Alternative Mobile Number');
    } else if (altMobNumber?.length !== 10) {
      return Utility.showToast('Please Enter Valid Alternative Mobile Number');
    } else if (!_.trim(proImg)) {
      return Utility.showToast('Please Upload Profile Picture');
    } else {
      const token = loginData.token;
      const formData = new FormData();
      formData.append('name', firstName);
      formData.append('lastname', lastName);
      formData.append('email', email);
      formData.append('alternative_number', altMobNumber);
      formData.append('image', {
        uri: proImg,
        name: 'profile_pic',
        type: 'image/jpeg',
      });
      const myUrl = BASE_URL + UPDATE_USER_INFO;
      fetch(myUrl, {
        method: 'post',
        body: formData,
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'multipart/form-data',
        },
      })
        .then(res => {
          if (res.status) {
            getUserInfoCall({dispatch});
            Utility.showToast('Details Updated Successfully');
            setEditActive(false);
          }
        })
        .catch(err => console.warn(err));
    }
  };

  const logOutHandler = () => {
    AsyncStorage.clear();
    AsyncStorage.removeItem(AsKey.userLocation);
    AsyncStorage.removeItem(AsKey.userLoginData);
    AsyncStorage.removeItem(AsKey.isGuestAccount);
    dispatch(resetCart());
    dispatch(resetDeliveryAddress());
    dispatch(resetLocation());
    dispatch(resetGetUserInfoList());
    AsyncStorage.removeItem(AsKey.userLocation);
    navigation.reset({routes: [{name: Screens.Login}]});
  };

  const deleteAccountHandler = () => {
    navigation.navigate(Screens.Delete);
  };

  const renderProImg = () => {
    return (
      <>
        {editActive ? (
          <TouchableOpacity
            style={CmnStyles.proFileImgBox}
            onPress={() => {
              setIsCameraModalVisible(true);
            }}>
            <Image
              source={{uri: proImg}}
              style={CmnStyles.proScreenProImg}
              resizeMode="contain"
            />
            <View style={CmnStyles.proScreenCameraBox}>
              <Image
                source={Images.camera}
                style={CmnStyles.proScreenCamera}
                resizeMode="contain"
              />
            </View>
          </TouchableOpacity>
        ) : (
          <View style={CmnStyles.proFileImgBox}>
            <Image
              source={{uri: proImg}}
              style={CmnStyles.proScreenProImg}
              resizeMode="contain"
            />
          </View>
        )}
      </>
    );
  };

  return (
    // <KeyboardAvoidingView behavior="padding" style={{flex: 1}}>
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack
          rightImg={
            isGuestAccount ? null : editActive ? Images.close : Images.edit
          }
          text={'View Profile'}
          backVisible={true}
          onPressRightImg={() => {
            setEditActive(!editActive);
          }}
        />
        <ScrollView
          style={[CmnStyles.bodyBg]}
          automaticallyAdjustKeyboardInsets
          keyboardShouldPersistTaps="always">
          <View
            style={[CmnStyles.bodyBg, {paddingBottom: Responsive.widthPx(4)}]}>
            {renderProImg()}
            <ProfileInput
              title="First Name"
              placeholder="Enter Name"
              value={firstName}
              setValue={setFirstName}
              editActive={editActive}
            />
            <ProfileInput
              title="Last Name"
              placeholder="Enter Name"
              value={lastName}
              setValue={setLastName}
              editActive={editActive}
            />
            <ProfileInput
              title="Email"
              placeholder="Enter Email"
              value={email}
              setValue={setEmail}
              editActive={editActive}
            />
            <ProfileInput
              title="Mobile Number"
              placeholder="Enter Mobile Number"
              value={moNumber}
              setValue={setMoNumber}
              editActive={false}
              nonEditActive={editActive}
            />
            <ProfileInput
              title="Alternative Mobile Number"
              placeholder="Enter Alternative Mobile Number"
              value={altMobNumber}
              setValue={setAltMobNumber}
              editActive={editActive}
            />
            <RedButton
              text={editActive ? 'Update' : 'Logout'}
              onPress={() => {
                if (editActive) {
                  editHandler();
                } else {
                  logOutHandler();
                }
              }}
            />
            {!isGuestAccount && !editActive && (
              <TextButton
                text={'Did you want to delete your account?'}
                onPress={() => {
                  deleteAccountHandler();
                }}
              />
            )}
            <CameraModal
              modalVisible={isCameraModalVisible}
              onPressCancel={() => setIsCameraModalVisible(false)}
              onPressCamera={() => {
                onCameraPressHandler(options);
              }}
              onPressGallery={() => {
                onGalleryPressHandler(options);
              }}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
    // </KeyboardAvoidingView>
  );
};

export default ProfileScreen;
