import {View, SafeAreaView, Alert} from 'react-native';
import React, {useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import DeliveryRateCard from '../../Components/Cards/DeliveryRateCard';
import Utility from '../../Constants/Utility';
import {
  getDriverRateDataApiCall,
  saveDriverRateDataApiCall,
} from '../../Core/Config/ApiCalls';
import {useNavigation, useRoute} from '@react-navigation/native';
import {addDriverRate} from '../../Core/Redux/Slices/DriverRateSlice';
import {useDispatch, useSelector} from 'react-redux';

const DriverRattingScreen = () => {
  const route = useRoute();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [starRate, setStarRate] = useState(0);
  const [suggestion, setSuggestion] = useState();
  const [deliveryData, setDeliveryData] = useState();
  const orderId = route?.params?.orderId;
  const {rateGivenCount} = useSelector(state => state.driverRate);

  useState(() => {
    const body = {
      order_id: orderId,
    };
    getDriverRateDataApiCall({body}).then(res => {
      setDeliveryData(res);
    });
  }, []);

  const submitHandler = () => {
    if (starRate < 1) {
      Utility.showToast('Click on Star, Give Star Ratting to Vender');
    } else {
      const body = {
        rider_id: deliveryData?.rider_id,
        rating: starRate,
        review: suggestion?.length > 0 ? suggestion : '',
        order_id: deliveryData?.order_id,
      };
      saveDriverRateDataApiCall({body, navigation});
      dispatch(addDriverRate(rateGivenCount + 1));
    }
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={'Rate Your Delivery'} backVisible={true} />
        <DeliveryRateCard
          text={'Rate Your Delivery By'}
          name={deliveryData?.driver_name}
          setStarRate={setStarRate}
          starRate={starRate}
          isDriverRate={true}
          setSuggestion={setSuggestion}
          suggestion={suggestion}
          onPress={() => {
            submitHandler();
          }}
          imgUri={deliveryData?.driver_image}
        />
      </SafeAreaView>
    </View>
  );
};

export default DriverRattingScreen;
