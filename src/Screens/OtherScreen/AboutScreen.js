import {View, Text, SafeAreaView} from 'react-native';
import React, {useEffect} from 'react';
import DeviceInfo from 'react-native-device-info';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import TextCard from '../../Components/Cards/TextCard';
import Responsive from '../../Constants/Responsive';
import FollowOnCard from '../../Components/Cards/FollowOnCard';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import {
  aboutUsApiCall,
  cancellationPolicyApiCall,
  privacyPolicyApiCall,
  termsAndConditionsApiCall,
} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';

const AboutScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {
    aboutUsData,
    termsAndConditionData,
    privacyPolicyData,
    cancellationPolicyData,
  } = useSelector(state => state.about);
  const versionCode = DeviceInfo.getVersion();

  useEffect(() => {
    aboutUsData == null && aboutUsApiCall({dispatch});
    termsAndConditionData == null && termsAndConditionsApiCall({dispatch});
    privacyPolicyData == null && privacyPolicyApiCall({dispatch});
    cancellationPolicyData == null && cancellationPolicyApiCall({dispatch});
  }, []);

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={'About ChefLab'} backVisible={true} />
        <View style={{paddingTop: Responsive.widthPx(3)}}>
          <TextCard
            text={'About Us'}
            onPress={() => {
              navigation.navigate(Screens.AboutDetails, {
                headerTitle: 'About Us',
                data: aboutUsData,
              });
            }}
          />
          <TextCard
            text={'Terms and Conditions'}
            onPress={() => {
              navigation.navigate(Screens.AboutDetails, {
                headerTitle: 'Terms and Conditions',
                data: termsAndConditionData,
              });
            }}
          />
          <TextCard
            text={'Privacy Policy'}
            onPress={() => {
              navigation.navigate(Screens.AboutDetails, {
                headerTitle: 'Privacy Policy',
                data: privacyPolicyData,
              });
            }}
          />
          <TextCard
            text={'Refund & Cancellation Policy'}
            onPress={() => {
              navigation.navigate(Screens.AboutDetails, {
                headerTitle: 'Refund & Cancellation Policy',
                data: cancellationPolicyData,
              });
            }}
          />
          <TextCard
            text={`FAQ's`}
            onPress={() => {
              navigation.navigate(Screens.Faqs);
            }}
          />
          <FollowOnCard otherStyle={{marginTop: Responsive.widthPx(6)}} />
        </View>
        <Text style={CmnStyles.versionTxt}>App Version {versionCode}</Text>
      </SafeAreaView>
    </View>
  );
};

export default AboutScreen;
