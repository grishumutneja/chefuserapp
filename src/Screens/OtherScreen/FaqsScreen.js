import {
  FlatList,
  Image,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import {useDispatch, useSelector} from 'react-redux';
import {getFaqsApiCall} from '../../Core/Config/ApiCalls';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';

const FaqsScreen = () => {
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const {getFaqsData} = useSelector(state => state.about);

  useEffect(() => {
    getFaqsData == null && getFaqsApiCall({dispatch});
  }, []);

  useEffect(() => {
    const tempData = getFaqsData?.map(item => {
      return {
        ...item,
        expanded: false,
      };
    });
    setData(tempData);
  }, [getFaqsData]);

  const updateCouponArray = (i, index) => {
    let _a = data?.map(item => {
      let temp = Object.assign({}, item);
      if (i?.id == temp.id) {
        temp.expanded = !temp.expanded;
      } else {
        temp.expanded = false;
      }
      return temp;
    });
    setData(_a);
  };

  const renderCouponItem = ({item, index}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={CmnStyles.faqsScreenBoxCont}
        onPress={() => {
          updateCouponArray(item, index);
        }}>
        <View style={SpStyles.FDR_ALC_JCS}>
          <Text style={CmnStyles.faqQuestion}>{item?.faq_question}</Text>
          <Image
            source={Images.plusIcon}
            style={CmnStyles.faqPlusIcon}
            resizeMode="contain"
          />
        </View>
        {item.expanded ? (
          <Text style={CmnStyles.faqScreenText}>{item?.faq_answer}</Text>
        ) : null}
      </TouchableOpacity>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={"FAQ's"} backVisible={true} />
        <FlatList data={data} extraData={data} renderItem={renderCouponItem} />
      </SafeAreaView>
    </View>
  );
};

export default FaqsScreen;
