import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
  Alert,
  BackHandler,
  AppState,
  RefreshControl,
  Platform,
} from 'react-native';
import React, {memo, useCallback, useEffect, useRef, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import TextStyles from '../../Styles/TextStyles';
import Responsive from '../../Constants/Responsive';
import OrderCard from '../../Components/Cards/OrderCard';
import SpStyles from '../../Styles/SpStyles';
import Colors from '../../Constants/Colors';
import DeliveryInstruction from '../../Components/Cards/DeliveryInstruction';
import Images from '../../Constants/Images';
import TextPriceCard from '../../Components/Cards/TextPriceCard';
import DashedPart from '../../Components/Cards/DashedPart';
import RedButton from '../../Components/Buttons/RedButton';
import {
  cancellationPolicyApiCall,
  createOrderApiCall,
  createOrderIdForRazorPay,
  createTempOrderApiCall,
  getCartListApiCall,
  getPromoCodeDetailApiCall,
  getResDetailPageApiCall,
  getTempOrderDetailApiCall,
  makeOrderRefundApiCall,
  razorPayCheckOutFunction,
  updateCartApiCall,
} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';
import {TaxModal} from '../../Components/Modals/TaxModal';
import {PromoCodeModal} from '../../Components/Modals/PromoCodeModal';
import {SuccessModal} from '../../Components/Modals/SuccessModal';
import {DeliveryInstructionModal} from '../../Components/Modals/DeliveryInstructionModal';
import {
  applyPromoCode,
  applyWallet,
  grossAmountDispatch,
} from '../../Core/Redux/Slices/RestaurantsSlices/CartSlice';
import Screens from '../../Core/Stack/Screens';
import {useIsFocused, useNavigation, useRoute} from '@react-navigation/native';
import Utility from '../../Constants/Utility';
import EmptyCartCard from '../../Components/Cards/EmptyCartCard';
import CartSkelton from '../../Components/Loader/CartSkelton';
import {PayMentMadeWaitModal} from '../../Components/Modals/PayMentMadeWaitModal';
import MyData from '../../Constants/MyData';
import {PaymentWaitModal} from '../../Components/Modals/PaymentWaitModal';
import {decode, encode} from 'js-base64';
import {CartSelectAddress} from '../../Components/Modals/CartSelectAddress';
import OfferPriceCard from '../../Components/Cards/OfferPriceCard';
import CartBottomCard from '../../Components/Cards/CartBottomCard';
import {notificationListener} from '../../Core/fireBase/notificationService';
import LocalNotification from '../../Core/fireBase/LocalNotification';
import {removeItemInCart} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantDetailsSlice';
import DeliveryChargeCard from '../../Components/Cards/DeliveryChargeCard';

const deliveryIns = [
  {
    icon: Images.avoid_bell,
    title: 'Avoid ringing bell',
    selected: false,
  },
  {
    icon: Images.leave_at_door,
    title: 'Leave at door',
    selected: false,
  },
  {
    icon: Images.direction,
    title: 'Directions to reach',
    selected: false,
  },
  {
    icon: Images.avoid_calling,
    title: 'Avoid calling',
    selected: false,
  },
];

const CartScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [chefMsg, setChefMsg] = useState();
  const [sendCutlery, setSendCutlery] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState();
  const [walletAmountApply, setWalletAmountApply] = useState();
  const {getUserInfoData, loginData} = useSelector(state => state.userInfo);
  const {itemsInCart, cart_id, proceedCartRefreshCount, clearResItem} =
    useSelector(state => state.restaurantDetails);
  const {
    cartListData,
    totalPrice,
    taxPrice,
    promoCodeList,
    payAmount,
    totalAmount,
    cartItems,
    isLoading,
    cartPassData,
    offerValue,
  } = useSelector(state => state.cart);
  const route = useRoute();
  const isFocused = useIsFocused();
  const [taxModalVisible, setTaxModalVisible] = useState(false);
  const [promoCodeModal, setPromoCodeModal] = useState(false);
  const [discountAmount, setDiscountAmount] = useState();
  const [appliedCode, setAppliedCode] = useState();
  const [successModalVisible, setSuccessModalVisible] = useState(false);
  const [deliveryInstructionModal, setDeliveryInstructionModal] =
    useState(false);
  const [deliveryInstruction, setDeliveryInstruction] = useState();
  const [deliveryCharge, setDeliveryCharge] = useState(0);
  const [isDeliveryChargeApply, setIsDeliveryChargeApply] = useState(false);
  const [locationData, setLocationDataActive] = useState(false);
  const [avoidBell, setAvoidBell] = useState(false);
  const [leaveDoor, setLeaveDoor] = useState(false);
  const [avoidCall, setAvoidCall] = useState(false);
  const [walletAmount, setWalletAmount] = useState();
  const [toPayAmount, setToPayAmount] = useState(0);
  const [selectDelModalVisible, setSelectDelModalVisible] = useState(false);
  const {cancellationPolicyData} = useSelector(state => state.about);
  const [loading, setLoading] = useState(true);
  const [couponId, setCouponId] = useState();
  const [grossAmount, setGrossAmount] = useState();
  const [paymentWaitModalVisible, setPaymentWaitModalVisible] = useState(false);
  const [paymentModalVisible, setPaymentModalVisible] = useState(false);
  const [waitTimeComplete, setWaitTimeComplete] = useState(false);
  const isFromResDetail = route?.params?.isFromResDetail;
  const [apiCallTime, setApiCallTime] = useState(0);
  const [tempOrderId, setTempOrderId] = useState();
  const appState = useRef(AppState.currentState);
  const isFromAdd = route.params?.isFromAdd;
  const updateLocation = route.params?.updateLocation;
  const {deliveryAddressData} = useSelector(state => state.deliveryAddress);
  const [appStatus, setAppStatus] = useState(false);
  const [fromRes, setFromRes] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      dispatch(removeItemInCart([]));
    }, 2000);
  }, [isFocused]);

  useEffect(() => {
    if (locationData) {
      const cartBody = {
        user_id: loginData?.user_id,
        lat: locationData.lat,
        lng: locationData.long,
        discount_amount: discountAmount,
      };
      getCartListApiCall({
        body: cartBody,
        dispatch,
        isRechargeDone: true,
      }).then(res => {
        setSelectDelModalVisible(false);
      });
    }
  }, [discountAmount]);

  useEffect(() => {
    if (deliveryCharge >= 0 && cartListData?.free_delivery == '1') {
      if (
        parseFloat(cartListData?.minimum_order_amount) -
          parseFloat(totalPrice) <=
        0
      ) {
        setDeliveryCharge(locationData ? cartListData?.delivery_charge : 0);
      } else {
        setDeliveryCharge(cartListData?.delivery_charge_2);
      }
    }
  }, [deliveryCharge, cartListData, totalPrice, locationData]);
  useEffect(() => {
    if (isFromAdd) {
      // setTimeout(() => {
      setLocationDataActive(updateLocation);
      const cartBody = {
        user_id: loginData?.user_id,
        lat: updateLocation.lat,
        lng: updateLocation.long,
      };
      console.log('get cart');
      getCartListApiCall({
        body: cartBody,
        dispatch,
        isRechargeDone: true,
      }).then(res => {
        setSelectDelModalVisible(false);
      });
      //   deliveryAddressData?.map((item, index) => {
      //     if (item?.primary_key == 1) {
      //       // setDefaultAddress(index);
      //       setLocationDataActive(item);
      // const cartBody = {
      //   user_id: loginData?.user_id,
      //   lat: item.lat,
      //   lng: item.long,
      // };
      // getCartListApiCall({
      //   body: cartBody,
      //   dispatch,
      //   isRechargeDone: true,
      // }).then(res => {
      //   setSelectDelModalVisible(false);
      // });
      //     }
      //   });
      // }, 1000);
    }
  }, [isFromAdd, updateLocation]);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        // console.log('App has come to the foreground!');
        setAppStatus('active');
      } else {
        // console.log('background Cart');
        setAppStatus('inactive');
        const body = {
          cart_id: cartListData?.cart_id,
          user_id: getUserInfoData?.id,
          vendor_id: cartListData?.vendor?.vendor_id,
          products: cartPassData,
        };
        if (isFromResDetail || fromRes) {
          setFromRes(true);
          const resBody = {
            vendor_id: cartListData?.vendor?.vendor_id,
          };
          getResDetailPageApiCall({
            body: resBody,
            dispatch: dispatch,
          });
        }
        if (cartPassData) {
          console.log('cartPassData in the use effect');
          updateCartApiCall({body, dispatch}).then(res => {
            console.log('first,res', res);
            if (res) {
              console.log('cartPassData useEffect ====>', cartPassData);
              console.log(
                'discountAmount useEffect Apoply USe',
                discountAmount,
              );

              setTimeout(() => {
                if (cartPassData && discountAmount) {
                  setDiscountAmount(parseFloat(discountAmount));
                  console.log('applyPromoCode In side call', discountAmount);
                  dispatch(
                    applyPromoCode({discount: parseFloat(discountAmount)}),
                  );
                  // dispatch(grossAmountDispatch(payAmount));
                }
              }, 1000);

              console.log('walletAmountApply', walletAmountApply);
              console.log('cartPassData', cartPassData);

              setTimeout(() => {
                if (cartPassData && walletAmountApply) {
                  console.log('walletAmountApply In', walletAmountApply);
                  setWalletAmountApply(walletAmountApply);
                  dispatch(grossAmountDispatch(payAmount));
                }
              }, 1500);
            }
          });
        } else {
          console.log('cartPassData false', cartPassData);
        }
      }
      appState.current = nextAppState;
    });

    return () => {
      subscription.remove();
    };
  }, [cartPassData, discountAmount, walletAmountApply, payAmount]);

  useEffect(() => {
    const backAction = () => {
      console.log(
        'back button pressed',
        'isFromResDetail==' + isFromResDetail + '   fromRes' + fromRes,
      );
      if (cartListData) {
        if (cartPassData !== null) {
          console.log('isFromResDetail this');
          isFromResDetail || fromRes ? cartFunction() : navigation.goBack();
        } else {
          console.log('isFromResDetail jkljkljkljk');
          if (isFromResDetail || fromRes) {
            const resBody = {
              vendor_id: cartListData?.vendor?.vendor_id,
            };
            getResDetailPageApiCall({
              body: resBody,
              dispatch: dispatch,
            });
            setTimeout(() => {
              navigation.navigate(Screens.RestaurantDetail, {
                resData: cartListData?.vendor,
                isRefresh: true,
                isBackBtnPressed: true,
              });
            }, 1000);
          } else {
            console.log('isFromResDetail none');

            navigation.goBack();
          }
        }
      } else {
        if (isFromResDetail || fromRes) {
          navigation.reset({routes: [{name: Screens.BottomStack}]});
        } else {
          navigation.goBack();
        }
      }
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [cartPassData, cartListData, isFromResDetail, fromRes]);

  useEffect(() => {
    if (waitTimeComplete) {
      navigation.replace(Screens.OrderPlaced, {
        orderId: tempOrderId,
      });
    }
  }, []);

  useEffect(() => {
    setTimeout(() => {
      if (paymentModalVisible) {
        setApiCallTime(apiCallTime + 1);
        getOrderStatusHandler();
      }
    }, 5000);
  }, [apiCallTime, paymentModalVisible, isFocused, appStatus]);

  const getOrderStatusHandler = () => {
    const body = {temporary_transaction_id: tempOrderId};
    getTempOrderDetailApiCall({body: body})
      .then(tempRes => {
        setLoading(true);
        if (tempRes?.statusCode == '402') {
          setPaymentModalVisible(true);
        } else if (tempRes?.statusCode == '405') {
          Alert.alert(
            'Opps...!',
            'Vendor is not available, your will get refund soon.',
          );
        } else {
          setPaymentModalVisible(false);
          navigation.replace(Screens.OrderPlaced, {
            orderId: tempRes?.order_id,
          });
          notificationListener();
          const notifyData = {
            title: 'Wohoo !     ',
            // title: 'Congratulations...!',
            message:
              'Your order is placed successfully. It will be delivered to you soon.',
          };
          LocalNotification(notifyData);
        }
      })
      .catch(err => {
        console.warn('err', err);
        setLoading(false);
      });
  };

  useEffect(() => {
    console.log('cartListData useEffect call ===>', cartListData);
    cartListData && dispatch(applyPromoCode({discount: parseFloat(0)}));
  }, []);

  useEffect(() => {
    cancellationPolicyData == null && cancellationPolicyApiCall({dispatch});
  }, []);

  useEffect(() => {
    if (!global.btoa) {
      global.btoa = encode;
    }
    if (!global.atob) {
      global.atob = decode;
    }
  }, []);

  useEffect(() => {
    const am = (
      parseFloat(totalAmount?.toFixed(2)) + parseFloat(deliveryCharge)
    ).toFixed(2);
    setGrossAmount(am);
  }, [totalAmount, deliveryCharge]);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(isLoading);
    }, 500);
  }, [isLoading, isFocused]);

  useEffect(() => {
    const body = {
      user_id: getUserInfoData?.id,
    };
    if (!cartListData) {
      getCartListApiCall({body, dispatch});
    }
  }, []);

  useEffect(() => {
    if (walletAmountApply) {
      setWalletAmountApply();
      dispatch(applyWallet(-walletAmountApply));
    }
  }, [deliveryCharge]);

  useEffect(() => {
    const am =
      // payAmount + deliveryCharge;
      parseFloat(payAmount?.toFixed(2)) + parseFloat(deliveryCharge);
    setToPayAmount(am);
  }, [payAmount, deliveryCharge]);

  useEffect(() => {
    const amount = walletAmountApply
      ? (cartListData?.wallet_amount - walletAmountApply).toFixed(2)
      : cartListData?.wallet_amount;
    setWalletAmount(amount);
  }, [cartListData, walletAmountApply]);

  useEffect(() => {
    if (cartListData) {
      if (locationData) {
        setDeliveryCharge(cartListData?.delivery_charge);
        setIsDeliveryChargeApply(true);
      } else {
        setDeliveryCharge(locationData ? cartListData?.delivery_charge : 0);
        setIsDeliveryChargeApply(false);
      }
    }
  }, [cartListData, locationData]);

  useEffect(() => {
    const promoBody = {
      vendor_id: cartListData?.vendor?.vendor_id,
    };
    !promoCodeList &&
      cartListData?.vendor?.vendor_id &&
      getPromoCodeDetailApiCall({body: promoBody, dispatch});
  }, [cartListData]);

  const onRefresh = useCallback(() => {
    setIsRefreshing(true);
    const body = {
      cart_id: cartListData?.cart_id,
      user_id: getUserInfoData?.id,
      vendor_id: cartListData?.vendor?.vendor_id,
      products: cartPassData,
    };
    if (cartPassData) {
      updateCartApiCall({body, dispatch});
    }
    setTimeout(() => {
      const cartBody = {
        user_id: getUserInfoData?.id,
        lat: locationData?.lat,
        lng: locationData?.lng,
      };
      getCartListApiCall({
        body: cartBody,
        dispatch,
        isRechargeDone: false,
      }).then(res => {
        setDiscountAmount();
        setAppliedCode();
        setWalletAmountApply();
        setChefMsg();
        dispatch(applyPromoCode({discount: 0}));
        setAvoidBell(false);
        setLeaveDoor(false);
        setAvoidCall(false);
        setDeliveryInstruction();
        setIsRefreshing(false);
        setSendCutlery(false);
      });
    }, 2000);
  }, [cartPassData]);

  const cartFunction = backPressed => {
    const body = {
      cart_id: cartListData?.cart_id,
      user_id: getUserInfoData?.id,
      vendor_id: cartListData?.vendor?.vendor_id,
      products: cartPassData,
    };
    updateCartApiCall({body, dispatch})
      .then(res => {
        const resBody = {
          vendor_id: cartListData?.vendor?.vendor_id,
        };
        backPressed &&
          getResDetailPageApiCall({
            body: resBody,
            dispatch: dispatch,
          });
        setTimeout(() => {
          navigation.navigate(Screens.RestaurantDetail, {
            resData: cartListData?.vendor,
            isRefresh: true,
            isBackBtnPressed: backPressed,
          });
        }, 1000);
      })
      .catch(err => {
        console.warn('err', err);
      });
  };

  const updateDatQty = () => {
    const body = {
      cart_id: cartListData?.cart_id,
      user_id: getUserInfoData?.id,
      vendor_id: cartListData?.vendor?.vendor_id,
      products: cartPassData,
    };
    if (cartPassData) {
      updateCartApiCall({body, dispatch}).then(res => {});
    }
  };

  const locationDataClear = () => {
    setLocationDataActive(false);
  };
  const renderOrder = () => {
    return (
      <View key={cartItems}>
        <Text style={CmnStyles.cartScreenOrderTitle}>Your Order</Text>
        <FlatList
          data={cartItems}
          renderItem={({item, index}) => {
            return (
              <>
                {item?.product_qty > 0 && (
                  <OrderCard
                    item={item}
                    index={index}
                    locationDataClear={locationDataClear}
                    setDiscountAmount={setDiscountAmount}
                    setAppliedCode={setAppliedCode}
                    setWalletAmountApply={setWalletAmountApply}
                  />
                )}
              </>
            );
          }}
          scrollEnabled={false}
        />
      </View>
    );
  };

  const renderAddMore = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <TouchableOpacity
          style={CmnStyles.cartScreenAddMoreCont}
          onPress={() => {
            const body = {
              cart_id: cartListData?.cart_id,
              user_id: getUserInfoData?.id,
              vendor_id: cartListData?.vendor?.vendor_id,
              products: cartPassData,
            };
            if (cartPassData) {
              updateCartApiCall({body, dispatch}).then(res => {});
            }
            setTimeout(() => {
              setDiscountAmount();
              dispatch(applyPromoCode({discount: 0}));
              setWalletAmountApply();
              setAppliedCode();
              navigation.navigate(Screens.RestaurantDetail, {
                resData: cartListData?.vendor,
                isRefresh: true,
              });
            }, 1000);
          }}>
          <Text style={TextStyles.blue_S_14_400}>+ Add more items</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const renderAnyMsgChef = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <TouchableOpacity style={CmnStyles.cartScreenAddMoreCont}>
          <TextInput
            placeholder="Any message for Chef ?"
            placeholderTextColor={Colors.darkGray}
            style={[TextStyles.black_S_16_400, {paddingBottom: 0}]}
            value={chefMsg}
            onChangeText={text => {
              setChefMsg(text);
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const renderCutlery = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <TouchableOpacity
          style={CmnStyles.cartScreenAddMoreCont}
          onPress={() => {
            setSendCutlery(!sendCutlery);
          }}>
          <Text style={TextStyles.darkGray_S_14_500}>Send Cutlery</Text>
          <Image
            source={sendCutlery ? Images.checked : Images.unchecked}
            style={CmnStyles.cartScreenCheckImg}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    );
  };

  const renderEnterPromoCode = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <TouchableOpacity
          style={CmnStyles.cartScreenAddMoreCont}
          onPress={() => {
            setPromoCodeModal(true);
          }}>
          <View style={SpStyles.FDR_ALC_JCS}>
            <Image
              source={Images.discount}
              style={CmnStyles.cartScreenPromoImg}
            />
            <Text style={CmnStyles.cartScreenPromoText} numberOfLines={1}>
              {appliedCode
                ? appliedCode + ' code applied'
                : 'Select/Enter Promo Code'}
            </Text>
          </View>
          <Image
            source={Images.back}
            style={CmnStyles.cartScreenPromoFrdImg}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    );
  };

  const renderFreeDelivery = () => {
    if (cartListData.delivery_charge != 0) {
      return <View></View>;
    } else {
      return (
        <View style={CmnStyles.freeDeliveryCont2}>
          <TouchableOpacity
            style={CmnStyles.freeDeliveryCont}
            onPress={() => {
              setPromoCodeModal(true);
            }}>
            <View style={SpStyles.FDR_ALC_JCS}>
              <Image
                source={Images.offer}
                style={[
                  CmnStyles.cartScreenPromoImg,
                  {tintColor: Colors.greenPureVeg},
                ]}
              />
              <Text style={CmnStyles.freeDeliveryTitle}>
                ₹ {parseFloat(cartListData?.delivery_charge_2).toFixed(0)}{' '}
                Saved!{' '}
                <Text style={CmnStyles.freeDeliveryText}>
                  with FREE delivery
                </Text>
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  };

  const renderDeliveryInstruction = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <View style={CmnStyles.cartScreenDelInsCont}>
          <Text style={TextStyles.darkGray_S_15_400}>
            Delivery Instructions
          </Text>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <DeliveryInstruction
              item={deliveryIns[0]}
              onClick={() => {
                setAvoidBell(!avoidBell);
              }}
              selected={avoidBell}
            />
            <DeliveryInstruction
              item={deliveryIns[1]}
              onClick={() => {
                setLeaveDoor(!leaveDoor);
              }}
              selected={leaveDoor}
            />
            <DeliveryInstruction
              item={deliveryIns[2]}
              onClick={() => {
                setDeliveryInstructionModal(true);
              }}
              selected={deliveryInstruction}
            />
            <DeliveryInstruction
              item={deliveryIns[3]}
              onClick={() => {
                setAvoidCall(!avoidCall);
              }}
              selected={avoidCall}
            />
          </ScrollView>
          {deliveryInstruction && (
            <Text style={CmnStyles.deliveryInstruction}>
              {deliveryInstruction}
            </Text>
          )}
        </View>
      </View>
    );
  };

  const renderTips = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <View style={CmnStyles.cartScreenDelInsCont}>
          <Text style={TextStyles.darkGray_S_15_400}>Tips :</Text>
          <Text style={CmnStyles.tipText}>
            We don't accept tips, we are truly yours 😍
          </Text>
        </View>
      </View>
    );
  };

  const renderWalletAmount = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <View style={SpStyles.flex1}>
          <View style={CmnStyles.cartScreenAddMoreCont}>
            <Text style={TextStyles.darkGray_S_14_700}>Wallet Amount</Text>
            <View style={SpStyles.FDR_ALC}>
              <Text style={TextStyles.darkGray_S_15_700}>₹ {walletAmount}</Text>
              <TouchableOpacity
                onPress={() => {
                  if (walletAmountApply) {
                    setWalletAmountApply();
                    dispatch(applyWallet(-walletAmountApply));
                  } else {
                    if (walletAmount != 0) {
                      if (cartListData?.wallet_amount < toPayAmount) {
                        setWalletAmountApply(cartListData?.wallet_amount);
                        dispatch(applyWallet(cartListData?.wallet_amount));
                      } else {
                        setWalletAmountApply(toPayAmount);
                        dispatch(applyWallet(toPayAmount));
                      }
                    }
                  }
                }}>
                <Image
                  source={walletAmountApply ? Images.checked : Images.unchecked}
                  style={CmnStyles.cartScreenWalletCheckImg}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
          </View>
          <Text style={CmnStyles.cartScreenWalletText}>
            If you want to use your wallet amount in this order, please tick the
            box.
          </Text>
        </View>
      </View>
    );
  };

  const renderPayBox = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <View style={CmnStyles.cartScreenDelInsCont}>
          <TextPriceCard item={'Item Total'} price={totalPrice?.toFixed(2)} />
          {discountAmount >= 0 && (
            <TextPriceCard
              item={'Promo Code Applied'}
              price={discountAmount == 0 ? 0 : discountAmount?.toFixed(2)}
              removeVisible={true}
              onPressRemove={async () => {
                setDiscountAmount();
                setAppliedCode();
                setWalletAmountApply();
                dispatch(applyPromoCode({discount: 0}));
                // updateDatQty()
              }}
            />
          )}
          <TextPriceCard
            item={'Taxes and Fee ⓘ'}
            price={taxPrice?.toFixed(2)}
            onPress={() => {
              setTaxModalVisible(true);
            }}
          />
          {isDeliveryChargeApply && (
            <DeliveryChargeCard
              item={'Delivery Charges'}
              price={deliveryCharge?.toFixed(2)}
              isFree={cartListData?.free_delivery == 1}
              deliveryCharges={cartListData.delivery_charge}
              mimumDiscount={
                discountAmount != undefined ? discountAmount?.toFixed(2) : 0
              }
              minimumTOtamAMount={totalPrice?.toFixed(2)}
              minimumAmountForCharges={parseFloat(
                cartListData?.minimum_order_amount,
              ).toFixed(2)}
              actualDeliveryCharge={parseFloat(
                cartListData?.delivery_charge_2,
              ).toFixed(2)}
              minimum_order_amount={(
                parseFloat(cartListData?.minimum_order_amount) -
                parseFloat(totalPrice)
              ).toFixed(2)}
            />
          )}
          <TextPriceCard
            item={'Total Amount'}
            price={grossAmount}
            otherTextStyle={{color: Colors.blue_0638ff}}
          />
          {walletAmountApply && (
            <TextPriceCard
              item={'Wallet amount used'}
              price={walletAmountApply?.toFixed(2)}
              removeVisible={true}
              onPressRemove={() => {
                setWalletAmountApply();
                dispatch(applyWallet(-walletAmountApply));
              }}
            />
          )}
          <DashedPart />
          <TextPriceCard
            item={'To Pay'}
            price={toPayAmount?.toFixed(2)}
            otherTextStyle={TextStyles.darkGray_S_18_700}
          />
        </View>
      </View>
    );
  };

  const renderCancellationPolicy = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <View style={{marginRight: Responsive.widthPx(-4)}}>
          <View style={CmnStyles.cartScreenAddMoreCont}>
            <Text style={TextStyles.darkGray_S_14_700}>
              Cancellation Policy
            </Text>
          </View>
          <Text style={CmnStyles.cartScreenWalletText}>
            If you cancel within 30 second of placing your order, 100% amount
            will be refunded. No refund for cancellation made after 30 seconds.
          </Text>
          <TouchableOpacity>
            <Text
              onPress={() => {
                navigation.navigate(Screens.AboutDetails, {
                  headerTitle: 'Refund & Cancellation Policy',
                  data: cancellationPolicyData,
                });
              }}
              style={CmnStyles.cancellationPolicyText}>
              Read Cancellation Policy
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const createOrderHandler = () => {
    const body = {
      cart_id: cartListData?.cart_id,
      user_id: getUserInfoData?.id,
      vendor_id: cartListData?.vendor?.vendor_id,
      products: cartPassData,
    };
    if (walletAmountApply) {
      let grossAmountInt = parseFloat(grossAmount)?.toFixed(0);
      let walletInt = walletAmountApply?.toFixed(0);
      if (grossAmountInt == walletInt) {
        callCreateOrderApi(null);
      } else {
        console.log('cartPassData', cartPassData);
        if (cartPassData) {
          setPaymentWaitModalVisible(true);
          // updateCartApiCall({body, dispatch}).then(res => {
          //   setPaymentWaitModalVisible(true);
          //   console.log('discountAmount kjkjkjkk', discountAmount);
          //   if (discountAmount) {
          //     dispatch(applyPromoCode({discount: parseFloat(discountAmount)}));
          //     // dispatch(grossAmountDispatch(payAmount));
          //   }
          //   console.log('walletAmountApply after', walletAmountApply);
          //   if (walletAmountApply) {
          //     dispatch(grossAmountDispatch(payAmount));
          //   }
          // });
        } else {
          setPaymentWaitModalVisible(true);
        }
      }
    } else {
      if (cartPassData) {
        setPaymentWaitModalVisible(true);
        // updateCartApiCall({body, dispatch}).then(res => {
        //   setPaymentWaitModalVisible(true);
        //   if (discountAmount) {
        //     setTimeout(() => {
        //       dispatch(applyPromoCode({discount: parseFloat(discountAmount)}));
        //     }, 1000);
        //     // dispatch(grossAmountDispatch(payAmount));
        //   }
        //   console.log('walletAmountApply', walletAmountApply);
        //   if (walletAmountApply) {
        //     setTimeout(() => {
        //       dispatch(grossAmountDispatch(payAmount));
        //     }, 1000);
        //   }
        // });
      } else {
        setPaymentWaitModalVisible(true);
      }
    }
  };

  const cartBodyHandler = transaction_id => {
    let productInCart = [];
    cartItems?.map((item, index) => {
      let variant = [];
      item?.variants?.map((itm, ind) => {
        let newObj = Object.assign({});
        if (itm?.added == true) {
          newObj.variant_id = itm?.variant_id;
          newObj.variant_qty = itm?.qty;
          newObj.variant_price =
            itm?.offer_id !== 0 ? itm?.after_offer_price : itm?.variant_price;
          newObj.variant_name = itm?.variant_name;
          newObj.added = true;
          variant?.push(newObj);
        }
        item.variants = variant;
      });
      let Addon = [];
      item?.addons?.map((itm, ind) => {
        let newObj = Object.assign({});
        if (itm?.added == true) {
          newObj.addon_id = itm?.addon_id;
          newObj.addon_qty = itm?.addon_qty;
          newObj.addon_name = itm?.addon_name;
          newObj.addon_price = itm?.addon_price;
          // newObj.added = itm?.added;
          Addon?.push(newObj);
        }
        item.addons = Addon;
      });
      item.product_price =
        item?.offer_id != 0 ? item?.after_offer_price : item?.product_price;
      item?.product_qty > 0 && productInCart.push(item);
    });

    let body = {
      user_id: getUserInfoData?.id,
      vendor_id: cartListData?.vendor?.vendor_id,
      customer_name: getUserInfoData?.name,
      delivery_address: locationData?.house_no,
      reach: locationData?.reach,
      mobile_number: locationData?.contact_no,
      total_amount: totalPrice + '', //sum of all amounts
      gross_amount: grossAmount + '', //after tax deduction
      net_amount: grossAmount + '', //after discount deduction
      discount_amount: discountAmount ? discountAmount : 0,
      coupon_id: couponId,
      payment_type: 'Online',
      payment_status: 'paid',
      transaction_id: transaction_id,
      payment_string: transaction_id,
      city: 'Indore',
      delivery_charge: deliveryCharge,
      send_cutlery: sendCutlery ? 1 : 0,
      avoid_ring_bell: avoidBell ? 1 : 0,
      leave_at_door: leaveDoor ? 1 : 0,
      direction_to_reach: deliveryInstruction ? 1 : 0,
      avoid_calling: avoidCall ? 1 : 0,
      direction_instruction: deliveryInstruction || '',
      pincode: '456002',
      lat: locationData?.lat + '',
      long: locationData?.long + '',
      wallet_cut: walletAmountApply,
      gateway_response: transaction_id,
      gateway_amount: grossAmount,
      platform_charges: cartListData?.platform_charges,
      tex: taxPrice - parseFloat(cartListData?.platform_charges),
      wallet_apply: walletAmountApply > 0 ? '1' : '0',
      products: productInCart,
      chef_message: chefMsg,
    };
    return body;
  };

  console.log('toPayAmount Cart', toPayAmount);

  const paymentHandler = transaction_id => {
    const body = cartBodyHandler(transaction_id);
    console.log('toPayAmount paymentHandler', toPayAmount);
    const newAmount = parseFloat(toPayAmount).toFixed(2) / 100;
    const newAmount2 = newAmount.toFixed(4);

    const razorPayAmount = (newAmount2 * 10000).toFixed(0);
    setLoading(true);
    body &&
      createTempOrderApiCall({body})
        .then(res => {
          createOrderIdForRazorPay({
            amount: razorPayAmount,
            tempOrderId: res?.transactionId,
            transaction_for: 'order',
          })
            .then(resp => {
              const razorPayBody = {
                description: res?.transactionId,
                currency: 'INR',
                key: MyData.razorPayKey, //Real key
                amount: razorPayAmount,
                order_id: resp?.data?.id,
                prefill: {
                  email: loginData?.email,
                  contact: loginData?.mobile,
                  name: getUserInfoData?.name,
                },
              };
              const cartBody = {
                user_id: getUserInfoData?.id,
                lat: locationData?.lat,
                long: locationData?.lng,
              };
              razorPayCheckOutFunction({
                body: razorPayBody,
                cartBody,
                dispatch,
                setLoading,
              })
                .then(response => {
                  const tempOrderBody = {
                    temporary_transaction_id: res?.transactionId,
                  };
                  setTempOrderId(res?.transactionId);
                  if (Platform.OS == 'android') {
                    setPaymentModalVisible(true);
                  }
                  getTempOrderDetailApiCall({body: tempOrderBody})
                    .then(tempRes => {
                      setLoading(true);
                      if (Platform.OS == 'ios') {
                        setPaymentModalVisible(false);
                        navigation.replace(Screens.OrderPlaced, {
                          orderId: tempRes?.order_id,
                        });
                      } else {
                        if (tempRes?.statusCode == '402') {
                          setPaymentModalVisible(true);
                        } else if (tempRes?.statusCode == '405') {
                          Alert.alert(
                            'Opps...!',
                            'Vendor is not available, your will get refund soon.',
                          );
                        } else {
                          setPaymentModalVisible(false);
                          navigation.replace(Screens.OrderPlaced, {
                            orderId: tempRes?.order_id,
                          });
                          notificationListener();
                          const notifyData = {
                            title: 'Wohoo !     ',
                            // title: 'Congratulations...!',
                            message:
                              'Your order is placed successfully. It will be delivered to you soon.',
                          };
                          LocalNotification(notifyData);
                        }
                      }
                    })
                    .catch(err => {
                      console.warn('err', err);
                      setLoading(false);
                    });
                })
                .catch(err => {
                  setDiscountAmount();
                  setAppliedCode();
                  setWalletAmountApply();
                  dispatch(applyPromoCode({discount: 0}));
                  setPaymentModalVisible(false);
                  setLoading(false);
                  onRefresh();
                  console.warn('err myyy razorPayCheckOutFunction', err);
                });
            })
            .catch(error => {
              console.warn('error getTempOrderDetailApiCall', error);
              setLoading(false);
            });
        })
        .catch(err => {
          console.warn('err', err?.error);
          setLoading(false);
          if (err?.error == 'Vendor not available') {
            const refundBody = {amount: err?.gateway_amount};
            makeOrderRefundApiCall({body: refundBody});
          }
        });
  };

  console.log('discountAmount', discountAmount);

  const callCreateOrderApi = async transaction_id => {
    const body = cartBodyHandler(transaction_id);
    body && createOrderApiCall({body, dispatch, navigation});
  };

  const renderDelivery = () => {
    return (
      <View style={CmnStyles.orderCardCont}>
        <View>
          <View style={CmnStyles.cartScreenAddMoreCont2}>
            <View style={SpStyles.FDR_ALC_JCS}>
              <Image
                source={Images.location}
                style={CmnStyles.locationImgCart}
                resizeMode="contain"
              />
              <Text style={TextStyles.darkGray_S_16_700}>Delivery Address</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                setSelectDelModalVisible(true);
              }}
              style={{padding: 4, paddingRight: 0}}>
              <Text style={TextStyles.blue_S_13_400}>
                Select / Change Address
              </Text>
            </TouchableOpacity>
          </View>
          <View style={CmnStyles.cartScreenDeliveryBox}>
            {locationData && (
              <View>
                <Text style={TextStyles.black_S_15_700}>
                  {locationData?.address_type == '1'
                    ? 'Home'
                    : locationData?.address_type == '2'
                    ? 'Work'
                    : locationData.address_type == '3'
                    ? 'Others'
                    : null}
                </Text>
                <Text
                  style={[
                    TextStyles.black_S_12_400,
                    {marginVertical: Responsive.widthPx(0.6)},
                  ]}>
                  {locationData?.house_no}
                </Text>
                <Text style={TextStyles.darkGray_S_12_500}>
                  Nearest Landmark: {locationData?.reach}
                </Text>
              </View>
            )}
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack
          text={'Your Cart'}
          backVisible={true}
          downText={cartListData?.vendor?.name}
          onPressBack={() => {
            if (cartListData) {
              if (cartPassData !== null) {
                isFromResDetail || fromRes
                  ? cartFunction()
                  : navigation.goBack();
              } else {
                if (isFromResDetail || fromRes) {
                  const resBody = {
                    vendor_id: cartListData?.vendor?.vendor_id,
                  };
                  getResDetailPageApiCall({
                    body: resBody,
                    dispatch: dispatch,
                  });
                  setTimeout(() => {
                    navigation.navigate(Screens.RestaurantDetail, {
                      resData: cartListData?.vendor,
                      isRefresh: true,
                      isBackBtnPressed: true,
                    });
                  }, 1000);
                } else {
                  navigation.goBack();
                }
              }
            } else {
              if (isFromResDetail || fromRes) {
                navigation.reset({routes: [{name: Screens.BottomStack}]});
              } else {
                navigation.goBack();
              }
            }
          }}
        />
        {cartListData ? (
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
            }>
            {renderOrder()}
            {renderAddMore()}
            {renderAnyMsgChef()}
            {renderCutlery()}
            {renderEnterPromoCode()}
            {renderDeliveryInstruction()}
            {/* {renderTips()} */}
            {renderWalletAmount()}
            {cartListData?.free_delivery == 1 &&
              locationData &&
              parseFloat(cartListData?.minimum_order_amount) -
                parseFloat(totalPrice) <=
                0 &&
              renderFreeDelivery()}
            {renderPayBox()}
            {renderCancellationPolicy()}
            {/* {renderDelivery()} */}

            {/* <OfferPriceCard />
            <RedButton
              text={'Proceed to Pay'}
              otherStyle={CmnStyles.proceedToPayBtn}
              onPress={() => {
                if (!locationData) {
                  Utility.showToast('Please Select Delivery Address');
                } else {
                  createOrderHandler();
                }
              }}
            /> */}
            <TaxModal
              modalVisible={taxModalVisible}
              tax={taxPrice}
              platFormFee={Number(cartListData?.platform_charges)}
              onPressCancel={() => {
                setTaxModalVisible(false);
              }}
            />
            {promoCodeModal && (
              <PromoCodeModal
                modalVisible={promoCodeModal}
                onPressCancel={() => {
                  setPromoCodeModal(false);
                }}
                promoCodeList={promoCodeList?.vendor}
                setDiscountAmount={setDiscountAmount}
                totalPrice={totalPrice}
                setPromoCodeModal={setPromoCodeModal}
                setAppliedCode={setAppliedCode}
                appliedCode={appliedCode}
                setCouponId={setCouponId}
                discountAmount={discountAmount}
                successCall={() => {
                  setSuccessModalVisible(true);
                  setTimeout(() => {
                    setSuccessModalVisible(false);
                  }, 3000);
                }}
                setWalletAmountApply={setWalletAmountApply}
                walletAmountApply={walletAmountApply}
              />
            )}
            <SuccessModal
              appliedCode={appliedCode}
              discountAmount={discountAmount}
              modalVisible={successModalVisible}
            />
            {deliveryInstructionModal && (
              <DeliveryInstructionModal
                modalVisible={deliveryInstructionModal}
                onPressCancel={() => {
                  setDeliveryInstructionModal(false);
                }}
                setDeliveryInstruction={setDeliveryInstruction}
                deliveryInstruction={deliveryInstruction}
                setModalVisible={setDeliveryInstructionModal}
              />
            )}
            <CartSelectAddress
              modalVisible={selectDelModalVisible}
              onPressCancel={() => {
                setSelectDelModalVisible(false);
              }}
              onPressAddAddress={() => {
                navigation.navigate(Screens.Maps, {isFromCart: true});
                setSelectDelModalVisible(false);
              }}
              setSelectDelModalVisible={setSelectDelModalVisible}
              setLocationData={setLocationDataActive}
              discountAmount={discountAmount}
            />
            <View
              style={{
                height:
                  offerValue > 0
                    ? Responsive.widthPx(55)
                    : Responsive.widthPx(
                        cartListData?.free_delivery == '1' ? 52 : 28,
                      ),
              }}
            />
            <PayMentMadeWaitModal
              modalVisible={paymentWaitModalVisible}
              onPressCancel={() => {
                if (cartPassData) {
                  const body = {
                    cart_id: cartListData?.cart_id,
                    user_id: getUserInfoData?.id,
                    vendor_id: cartListData?.vendor?.vendor_id,
                    products: cartPassData,
                  };
                  console.log('body', body);
                  updateCartApiCall({body, dispatch}).then(res => {
                    setPaymentWaitModalVisible(false);
                    setDiscountAmount();
                    setWalletAmountApply();
                    setAppliedCode();
                    dispatch(applyPromoCode({discount: 0}));
                  });
                } else {
                  setDiscountAmount();
                  setWalletAmountApply();
                  setAppliedCode();
                  dispatch(applyPromoCode({discount: 0}));
                  setPaymentWaitModalVisible(false);
                }
              }}
              onPressContinue={() => {
                paymentHandler(null);
                setPaymentWaitModalVisible(false);
              }}
            />
            {paymentModalVisible && (
              <PaymentWaitModal
                modalVisible={paymentModalVisible}
                setWaitTimeComplete={setWaitTimeComplete}
              />
            )}
          </ScrollView>
        ) : (
          <EmptyCartCard />
        )}
        {cartListData && (
          <CartBottomCard
            onPressPlaceOrder={() => {
              if (!locationData) {
                Utility.showToast('Please Select Delivery Address');
              } else {
                createOrderHandler();
              }
            }}
            payAmount={toPayAmount?.toFixed(2)}
            onPressAddress={() => {
              setSelectDelModalVisible(true);
              updateDatQty();
            }}
            // updateDatQty={updateDatQty()}
            address={locationData?.house_no}
            discountAmount={discountAmount}
            deliveryChargeSave={cartListData?.delivery_charge}
            deliverySaveAmount={
              locationData &&
              cartListData?.free_delivery == '1' &&
              parseFloat(cartListData?.minimum_order_amount) -
                parseFloat(totalPrice) <=
                0
                ? parseFloat(cartListData?.delivery_charge_2)
                : 0
            }
          />
        )}

        <CartSkelton visible={loading} />
      </SafeAreaView>
    </View>
  );
};

export default memo(CartScreen);
