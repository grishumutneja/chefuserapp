import {View, Text, SafeAreaView, FlatList} from 'react-native';
import React, {memo, useEffect, useState} from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import TitleRestaurant from '../../Components/Inputs/TitleRestaurant';
import Responsive from '../../Constants/Responsive';
import RestaurantCard from '../../Components/Cards/RestaurantCard';
import {useDispatch, useSelector} from 'react-redux';
import {
  getFoodForYouRestaurantsApiCall,
  getRestaurantFoodCategoriesApiCall,
} from '../../Core/Config/ApiCalls';
import {useNavigation, useRoute} from '@react-navigation/native';
import FooterRender from '../../Components/Cards/FooterRender';
import SearchBox from '../../Components/Inputs/SearchBox';
import FilterButton from '../../Components/Buttons/FilterButton';
import SpStyles from '../../Styles/SpStyles';
import {ResListFilterModal} from '../../Components/Modals/ResListFilterModal';
import Screens from '../../Core/Stack/Screens';
import CatSkelton from '../../Components/Loader/CatSkelton';

const RestaurantListScreen = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {locationData} = useSelector(state => state.location);
  const {foodForYouRestaurantData, isFoodCategoriesDataComplete} = useSelector(
    state => state.foodForYou,
  );
  const restaurantData = route?.params?.restaurantData;
  const type = route?.params?.type;
  const [offset, setOffset] = useState(0);
  const [loading, setLoading] = useState();
  const [pageLoading, setPageLoading] = useState(false);
  const [filterModalVisible, setFilterModalVisible] = useState(false);

  useEffect(() => {
    type == 'Choice'
      ? getFoodForYouRestaurantsApiCallHandler(0)
      : getRestaurantFoodCategoriesApiCallHandler(0);
  }, []);

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const getFoodForYouRestaurantsApiCallHandler = myOffset => {
    const body = {
      cuisines_id: restaurantData?.id,
      lat: locationData?.lat,
      lng: locationData?.lng,
      vendor_offset: myOffset,
      vendor_limit: 10,
    };

    getFoodForYouRestaurantsApiCall({body, dispatch, setLoading});
  };

  const getRestaurantFoodCategoriesApiCallHandler = myOffset => {
    const body = {
      category_id: restaurantData?.id,
      lat: locationData?.lat,
      lng: locationData?.lng,
      vendor_offset: myOffset,
      vendor_limit: 10,
    };

    getRestaurantFoodCategoriesApiCall({body, dispatch, setLoading});
  };

  console.log(
    'foodForYouRestaurantData?.length',
    foodForYouRestaurantData?.length,
  );

  const renderRestaurantExplore = () => {
    return (
      <View>
        <SearchBox
          isFixed={true}
          onPress={() => {
            navigation.navigate(Screens.SearchResDish);
          }}
        />
        <View style={SpStyles.FDR_ALC_JCS}>
          <TitleRestaurant
            titleBig={`Restaurants`}
            otherStyle={{marginBottom: Responsive.widthPx(1)}}
          />
          <FilterButton
            onPress={() => {
              setFilterModalVisible(true);
            }}
          />
        </View>
        <FlatList
          removeClippedSubviews={false}
          data={foodForYouRestaurantData}
          extraData={foodForYouRestaurantData}
          renderItem={({item, index}) => {
            return (
              <RestaurantCard item={item} index={index} isResList={true} />
            );
          }}
          contentContainerStyle={{
            marginHorizontal: Responsive.widthPx(2),
            marginBottom: Responsive.widthPx(20),
            paddingBottom: Responsive.widthPx(30),
          }}
          showsHorizontalScrollIndicator={false}
          onScroll={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              if (!isFoodCategoriesDataComplete) {
                if (!pageLoading) {
                  setOffset(offset + 10);
                  setPageLoading(true);
                  type == 'Choice'
                    ? getFoodForYouRestaurantsApiCallHandler(offset + 10)
                    : getRestaurantFoodCategoriesApiCallHandler(offset + 10);
                }
              }
            } else {
              setPageLoading(false);
            }
          }}
          ListFooterComponent={() => {
            return (
              <FooterRender
                isComplete={isFoodCategoriesDataComplete}
                pageLoading={pageLoading}
              />
            );
          }}
        />
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={restaurantData?.name} backVisible={true} />

        {loading ? <CatSkelton /> : renderRestaurantExplore()}
        <ResListFilterModal
          modalVisible={filterModalVisible}
          onPressCancel={() => {
            setFilterModalVisible(false);
          }}
          type={type}
          setFilterModalVisible={setFilterModalVisible}
          locationData={locationData}
          cuisines_id={restaurantData?.id}
          setOffset={setOffset}
          setLoading={setLoading}
        />
      </SafeAreaView>
    </View>
  );
};

export default memo(RestaurantListScreen);
