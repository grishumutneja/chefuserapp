import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  Platform,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import TextStyles from '../../Styles/TextStyles';
import messaging from '@react-native-firebase/messaging';
import RedButton from '../../Components/Buttons/RedButton';
import MapView, {
  AnimatedRegion,
  Marker,
  PROVIDER_GOOGLE,
} from 'react-native-maps';
import database from '@react-native-firebase/database';
import MapViewDirections from 'react-native-maps-directions';
import MyData from '../../Constants/MyData';
import OrderDetailTrackCard from '../../Components/Cards/OrderDetailTrackCard';
import FooterSliderPart from '../../Components/Cards/FooterSliderPart';
import {
  getOrderDetailsApiCall,
  getRestTrackBannerCall,
  pendingOrderRatingApiCall,
  reOrderApiCall,
} from '../../Core/Config/ApiCalls';
import {useNavigation, useRoute} from '@react-navigation/native';
import OrderDetailCard from '../../Components/Cards/OrderDetailCard';
import {useDispatch, useSelector} from 'react-redux';
import Screens from '../../Core/Stack/Screens';
import {ViewOrderModal} from '../../Components/Modals/ViewOrderModal';
import Open from '../../Constants/Open';
import SpStyles from '../../Styles/SpStyles';
import Responsive from '../../Constants/Responsive';
import TrackSkelton from '../../Components/Loader/TrackSkelton';
import {orderHistoryApiCall} from '../../Core/Config/ApiCalls';

const ASPECT_RATIO = Responsive.widthPx(100) / Responsive.heightPx(100);
const LATITUDE_DELTA = 0.9222;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const TrackOrderScreen = () => {
  const route = useRoute();
  const mapRef = useRef();
  const markerRef = useRef();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const {locationData} = useSelector(state => state.location);
  const {restTrackBannerData} = useSelector(state => state.restHomeBanners);
  const [viewOrderVisible, setViewOrderVisible] = useState(false);
  const [isDelivered, setIsDelivered] = useState(false);
  const [orderData, setOrderData] = useState();
  const [orderStatus, setOrderStatus] = useState();
  const destination = {latitude: 22.7196, longitude: 75.8577};
  const orderId = route?.params?.orderId;
  const isFromPlaced = route?.params?.isFromPlaced;
  const [deliveryLocation, setDeliveryLocation] = useState();
  const [vendorLocation, setVendorLocation] = useState();
  const [riderHeading, setRiderHeading] = useState(0);
  const [deliverOTP, setDeliverOTP] = useState();
  const [remainTime, setRemainTime] = useState(30);
  const [riderLocation, setRiderLocation] = useState(
    new AnimatedRegion({
      latitude: 0,
      longitude: 0,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    }),
  );
  const [notData, setNotData] = useState();
  const [driverId, setDriverId] = useState();
  const [mapRegion, setMapRegion] = useState({
    latitude:
      Platform.OS == 'ios' ? 22.72087744368 : parseFloat(22.72087744368),
    longitude:
      Platform.OS == 'ios' ? 75.857141520828 : parseFloat(75.857141520828),
    latitudeDelta: 0.005,
    longitudeDelta: 0.005,
  });
  const [trackCall, setTrackCall] = useState(0);
  const {notificationData} = useSelector(state => state.notification);
  const [driverData, setDriverData] = useState();

  useEffect(() => {
    const backAction = () => {
      navigation.navigate(Screens.OrderHistory);
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      let data = JSON.parse(remoteMessage.data?.data);

      if (data?.data != null && data?.data != undefined) {
        setNotData(data?.data);
      } else {
        if (data) {
          setNotData(data);
        }
      }
    });
    return unsubscribe;
  }, []);

  useEffect(() => {
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      let data = JSON.parse(remoteMessage.data?.data);
      if (data?.data != null && data?.data != undefined) {
        setNotData(data?.data);
      } else {
        if (data) {
          setNotData(data);
        }
      }
    });
  }, []);

  useEffect(() => {
    const body = {
      order_for: 'restaurant',
      offset: 0,
    };
    if (notData) {
      orderHistoryApiCall({dispatch, body: body});
    }
  }, [notData]);

  useEffect(() => {
    if (notData?.id == orderId) {
      setOrderStatus(notData?.order_status);
      setDriverData(notData);
      setDeliverOTP(notData?.deliver_otp);
    } else {
      setDriverData(orderData);
      setOrderStatus(orderData?.order_status);
      setDeliverOTP(orderData?.deliver_otp);
    }
  }, [orderData, notData]);

  useEffect(() => {
    if (orderStatus == 'completed') {
      setTimeout(() => {
        pendingOrderRatingApiCall({body: null})
          .then(res => {
            if (res?.pending_driver_review == null) {
            } else {
              if (orderId == res?.pending_driver_review?.id) {
                navigation.navigate(Screens.DriverRatting, {orderId: orderId});
              }
            }
          })
          .catch(err => {
            console.warn('err pendingOrderRatingApiCall', err);
          });
      }, 2000);
    }
  }, [orderStatus]);

  useEffect(() => {
    orderStatus == 'completed' && setIsDelivered(true);
    let body = {
      lat: locationData?.lat,
      lng: locationData?.lng,
      for: 'order_traking',
    };
    !restTrackBannerData && getRestTrackBannerCall({body, dispatch});
  }, [orderData, orderStatus]);

  useEffect(() => {
    const body = {
      order_id: orderId,
    };
    setLoading(true);
    getOrderDetailsApiCall({body})
      .then(res => {
        setOrderData(res);
        setLoading(false);
        const location = {
          latitude: parseFloat(res?.lat),
          longitude: parseFloat(res?.long),
        };
        const venLocation = {
          latitude: parseFloat(res?.vendor_lat),
          longitude: parseFloat(res?.vendor_long),
        };
        setDeliveryLocation(location);
        setVendorLocation(venLocation);
        trackDriverPosition(res?.accepted_driver_id);
        setDriverId(res?.accepted_driver_id);
        setMapRegion({
          latitude: Platform.OS === 'ios' ? res?.lat : parseFloat(res?.lat),
          longitude: Platform.OS === 'ios' ? res?.long : parseFloat(res?.long),
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        });
      })
      .catch(err => {
        console.warn('err', err);
        setLoading(false);
      });
    return () => {
      stopDriverTrack();
    };
  }, []);

  const repeatHandler = () => {
    let body = {
      order_id: orderData?.id,
    };
    const myBody = {
      user_id: orderData?.user_id,
    };
    reOrderApiCall({body, myBody, dispatch})
      .then(res => {
        navigation.navigate(Screens.Cart);
      })
      .catch(err => console.warn('err repeatHandler', err));
  };

  const animate = (latitude, longitude) => {
    const newCoord = {
      latitude,
      longitude,
    };
    if (Platform.OS == 'android') {
      if (markerRef.current) {
        markerRef.current.animateMarkerToCoordinate(newCoord, 7000);
      }
    }
  };

  const moveToCurrent = () => {
    if (mapRegion.latitude && mapRegion.longitude) {
      mapRef?.current?.animateToRegion({
        latitude:
          Platform.OS == 'android'
            ? parseFloat(mapRegion.latitude)
            : mapRegion.latitude,
        longitude:
          Platform.OS == 'android'
            ? parseFloat(mapRegion.longitude)
            : mapRegion.longitude,
        latitudeDelta: 0,
        longitudeDelta: 0,
      });
    }
  };

  const moveToCurrent2 = () => {
    mapRef?.current?.animateToRegion({
      latitude: vendorLocation.latitude,
      longitude: vendorLocation.longitude,
      latitudeDelta: 0,
      longitudeDelta: 0,
    });
  };

  const trackDriverPosition = async id => {
    if (id === undefined || id === null || id === '') {
      return;
    }
    database()
      .ref(`${MyData.realTimeLocationKey}/${id}`)
      .on('value', snapshot => {
        let position = snapshot.val();
        const number = trackCall + 1;
        setTrackCall(number);
        const posLat =
          Platform.OS === 'ios' ? position?.lat : parseFloat(position?.lat);
        const posLng =
          Platform.OS === 'ios' ? position?.long : parseFloat(position?.long);
        if (position) {
          setRiderHeading(parseFloat(position?.heading) || 0);
          if (number % 10 == 0) {
            setMapRegion({
              latitude: posLat,
              longitude: posLng,
              latitudeDelta: 0.005,
              longitudeDelta: 0.005,
            });
          } else if (number < 3) {
            setMapRegion({
              latitude: posLat,
              longitude: posLng,
              latitudeDelta: 0.005,
              longitudeDelta: 0.005,
            });
          }

          animate(posLat, posLng);
          setRiderLocation(
            new AnimatedRegion({
              latitude: posLat,
              longitude: posLng,
              latitudeDelta: 0.005,
              longitudeDelta: 0.005,
            }),
          );
        }
      });
  };

  const stopDriverTrack = () => {
    database()
      .ref(`locations/${driverId}`)
      .off('value', snapshot => {});
  };

  const renderButtons = () => {
    return (
      <View style={CmnStyles.trackScreenBtnCont}>
        <RedButton
          text={'Download Invoice'}
          otherTextStyle={TextStyles.white_S_15_700}
          otherStyle={CmnStyles.trackScreenRedBtn}
          onPress={() => {
            Open.anyUrl(orderData?.pdf_url);
          }}
        />
        <RedButton
          text={'Repeat Order'}
          otherTextStyle={TextStyles.white_S_15_700}
          otherStyle={CmnStyles.trackScreenRedBtn}
          onPress={() => {
            repeatHandler();
          }}
        />
      </View>
    );
  };

  console.log('vendorLocation', vendorLocation);

  const renderMapView = () => {
    return (
      <View style={SpStyles.flex1}>
        <MapView
          ref={mapRef}
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={CmnStyles.mapStyleTrack}
          region={mapRegion}
          minZoomLevel={0} // default => 0
          maxZoomLevel={20}>
          <MapViewDirections
            origin={deliveryLocation ? deliveryLocation : destination}
            destination={vendorLocation ? vendorLocation : destination}
            apikey={MyData.googleMapApiKey}
            strokeWidth={3}
            strokeColor={'#222222'}
            optimizeWaypoints={true}
          />
          <Marker.Animated ref={markerRef} coordinate={riderLocation}>
            <Image
              source={Images.riderBike}
              style={[
                CmnStyles.box45,
                {
                  transform: [{rotate: `${riderHeading}deg`}],
                },
              ]}
              resizeMode="contain"
            />
          </Marker.Animated>
          <Marker
            coordinate={deliveryLocation ? deliveryLocation : destination}>
            <Image
              source={Images.locationHome}
              style={CmnStyles.box30}
              resizeMode="contain"
            />
          </Marker>
          <Marker coordinate={vendorLocation ? vendorLocation : destination}>
            <Image
              source={Images.locationRestaurant}
              style={CmnStyles.box30}
              resizeMode="contain"
            />
          </Marker>
        </MapView>
        <TouchableOpacity
          onPress={() => moveToCurrent()}
          style={CmnStyles.currentLocationTrack}>
          <Image
            source={Images.currentLocation}
            style={CmnStyles.box30}
            resizeMode="contain"
          />
        </TouchableOpacity>
        {orderStatus == 'dispatched' && (
          <View style={CmnStyles.trackOtpBox}>
            <Text style={TextStyles.black_S_14_400}>Deliver OTP</Text>
            <Text style={TextStyles.black_S_15_700}>{deliverOTP}</Text>
          </View>
        )}
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack
            text={'Track Order'}
            backVisible={true}
            onPressBack={() => {
              navigation.navigate(Screens.OrderHistory);
            }}
            rightImg={Images.blueMessage}
            rightImgOtherStyle={CmnStyles.blueMsgIcon}
            onPressRightImg={() => {
              Open.whatsapp('918962689850');
            }}
          />
          {loading ? (
            <TrackSkelton visible={loading} />
          ) : (
            <ScrollView
              style={CmnStyles.bodyBg}
              scrollEnabled={isDelivered ? true : false}>
              <OrderDetailTrackCard
                isDelivered={isDelivered}
                orderData={orderData}
                orderStatus={orderStatus}
                setViewOrderVisible={setViewOrderVisible}
              />
              <ViewOrderModal
                modalVisible={viewOrderVisible}
                onPressCancel={() => {
                  setViewOrderVisible(false);
                }}
                orderData={orderData}
              />
              {isDelivered ? (
                <>
                  <Text style={CmnStyles.trackScreenOrderTitle}>
                    Order Details
                  </Text>
                  <OrderDetailCard orderData={orderData} />
                  {renderButtons()}
                </>
              ) : (
                <>
                  {vendorLocation &&
                    deliveryLocation &&
                    mapRegion?.latitude &&
                    mapRegion?.longitude &&
                    renderMapView()}
                </>
              )}
              {!isDelivered && restTrackBannerData && (
                <FooterSliderPart
                  data={restTrackBannerData}
                  orderStatus={orderStatus}
                  driverData={driverData}
                  isFromPlaced={isFromPlaced}
                  remainTime={remainTime}
                  setRemainTime={setRemainTime}
                  setOrderStatus={setOrderStatus}
                />
              )}
            </ScrollView>
          )}
        </View>
      </SafeAreaView>
    </View>
  );
};

export default TrackOrderScreen;
