import {View, SafeAreaView} from 'react-native';
import React, {useEffect} from 'react';
import _ from 'lodash';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import TopTapBar from '../../Core/Stack/TopTapBar';
import FavDishes from '../../Components/BarComponents/FavDishes';
import FavRestaurants from '../../Components/BarComponents/FavRestaurants';
import {useDispatch, useSelector} from 'react-redux';
import {
  getAllFavDishesApiCall,
  getAllFavRestaurantsApiCall,
} from '../../Core/Config/ApiCalls';

const Tab = createMaterialTopTabNavigator();

const FavoritesScreen = () => {
  const dispatch = useDispatch();
  const {getAllFavRestaurantData, locationWhileList} = useSelector(
    state => state.favoritesRestaurants,
  );
  const {getAllFavDishesData} = useSelector(state => state.favoritesDishes);
  const {locationData} = useSelector(state => state.location);

  useEffect(() => {
    const favBody = {
      lat: locationData?.lat,
      lng: locationData?.lng,
    };
    getAllFavDishesApiCall({dispatch: dispatch, body: favBody});
    if (!_.isEqual(favBody, locationWhileList)) {
      getAllFavRestaurantsApiCall({dispatch: dispatch, body: favBody});
    }
  }, []);

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={'Your Favorites'} backVisible={true} />
        <Tab.Navigator tabBar={props => <TopTapBar {...props} />}>
          <Tab.Screen name="Restaurants">
            {props => (
              <FavRestaurants data={getAllFavRestaurantData} {...props} />
            )}
          </Tab.Screen>
          <Tab.Screen name="Dishes">
            {props => <FavDishes data={getAllFavDishesData} {...props} />}
          </Tab.Screen>

          {/* <Tab.Screen
            name="Dishes"
            component={props => (
              <FavDishes data={getAllFavDishesData} {...props} />
            )}
          /> */}
        </Tab.Navigator>
      </SafeAreaView>
    </View>
  );
};

export default FavoritesScreen;
