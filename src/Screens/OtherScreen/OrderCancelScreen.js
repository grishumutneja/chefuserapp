import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import LottieView from 'lottie-react-native';
import Animations from '../../Constants/Animations';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Responsive from '../../Constants/Responsive';
import Screens from '../../Core/Stack/Screens';
import {
  orderHistoryApiCall,
  userAllTransactionApiCall,
} from '../../Core/Config/ApiCalls';
import {addDriverRate} from '../../Core/Redux/Slices/DriverRateSlice';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import RedButton from '../../Components/Buttons/RedButton';

const OrderCancelScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {getUserInfoData} = useSelector(state => state.userInfo);
  const {rateGivenCount} = useSelector(state => state.driverRate);

  useEffect(() => {
    setTimeout(() => {
      navigateHandler();
    }, 3000);
  }, []);

  const navigateHandler = () => {
    navigation.replace(Screens.BottomStack);
    userAllTransactionApiCall({userId: getUserInfoData?.id, dispatch});
    const orderBody = {
      order_for: 'restaurant',
      offset: 0,
    };
    orderHistoryApiCall({dispatch, body: orderBody});
    dispatch(addDriverRate(rateGivenCount + 1));
  };

  return (
    <View style={styles.container}>
      <LottieView
        source={Animations.process_failed}
        style={styles.loti}
        autoPlay={true}
        loop={false}
      />
      <Text style={styles.title}>Order Cancelled Successfully</Text>
      <RedButton
        text={'Back to Home'}
        otherStyle={styles.btn}
        onPress={() => {
          navigateHandler();
        }}
      />
    </View>
  );
};

export default OrderCancelScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  loti: {
    height: 180,
    width: 180,
    marginVertical: 15,
  },
  title: {
    marginTop: Responsive.widthPx(10),
    ...TextStyles.darkGray_S_18_700,
    fontSize: 22,
  },
  btn: {
    paddingHorizontal: 50,
    backgroundColor: Colors.red_primary,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    borderRadius: 10,
    marginVertical: 25,
    width: '90%',
  },
});
