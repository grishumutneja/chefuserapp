import {View, SafeAreaView, FlatList} from 'react-native';
import React, {useState} from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';
import Colors from '../../Constants/Colors';
import AddressCard from '../../Components/Cards/AddressCard';
import Responsive from '../../Constants/Responsive';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import {LoadingModal} from '../../Components/Modals/LoadingModal';
import {UpdateLocationModal} from '../../Components/Modals/UpdateLocationModal';
import NoDataAvailable from '../../Components/Inputs/NoDataAvailable';

const SavedAddressScreen = () => {
  const navigation = useNavigation();
  const [defaultType, setDefaultType] = useState();
  const [fullAddress, setFullAddress] = useState();
  const [landmark, setLandmark] = useState();
  const [contact, setContact] = useState();
  const [isPrimary, setIsPrimary] = useState(false);
  const [updateModalVisible, setUpdateModalVisible] = useState(false);
  const [location, setLocation] = useState();
  const [locationName, setLocationName] = useState();
  const [addressId, setAddressId] = useState();
  // const [updateAddress, setUpdateAddress] = useState();
  const {isGuestAccount} = useSelector(state => state.userInfo);
  const {deliveryAddressData, isDeliveryAddressLoading, noAddressAvailable} =
    useSelector(state => state.deliveryAddress);

  const UpdateAddress = {
    setDefaultType: setDefaultType,
    defaultType: defaultType,
    setFullAddress: setFullAddress,
    fullAddress: fullAddress,
    setLandmark: setLandmark,
    landmark: landmark,
    contact: contact,
    setContact: setContact,
    setIsPrimary: setIsPrimary,
    isPrimary: isPrimary,
    setLocation: setLocation,
    location: location,
    setLocationName: setLocationName,
    locationName: locationName,
    setAddressId: setAddressId,
    addressId: addressId,
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack
          rightImg={isGuestAccount ? null : Images.plusIcon}
          text={'Your Saved Addresses'}
          backVisible={true}
          onPressRightImg={() => {
            navigation.navigate(Screens.Maps);
          }}
          rightImgOtherStyle={{tintColor: Colors.red_primary}}
        />
        <View
          style={{
            paddingVertical: Responsive.widthPx(2),
            marginBottom: Responsive.widthPx(10),
          }}>
          {noAddressAvailable ? (
            <NoDataAvailable text={'No addresses found'} />
          ) : (
            <FlatList
              data={deliveryAddressData}
              renderItem={({item, index}) => {
                return (
                  <AddressCard
                    item={item}
                    index={index}
                    setUpdateModalVisible={setUpdateModalVisible}
                    UpdateAddress={UpdateAddress}
                  />
                );
              }}
              contentContainerStyle={{paddingBottom: Responsive.widthPx(5)}}
            />
          )}
        </View>
        <LoadingModal modalVisible={isDeliveryAddressLoading} />
        <UpdateLocationModal
          modalVisible={updateModalVisible}
          onPressCancel={() => {
            setUpdateModalVisible(false);
          }}
          UpdateAddress={UpdateAddress}
          setUpdateModalVisible={setUpdateModalVisible}
          location={location}
        />
      </SafeAreaView>
    </View>
  );
};

export default SavedAddressScreen;
