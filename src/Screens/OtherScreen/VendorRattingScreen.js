import {View, SafeAreaView} from 'react-native';
import React, {useEffect, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import DeliveryRateCard from '../../Components/Cards/DeliveryRateCard';
import {useNavigation, useRoute} from '@react-navigation/native';
import {
  detailOrderRatingApiCall,
  saveOrderRatingApiCall,
} from '../../Core/Config/ApiCalls';
import Utility from '../../Constants/Utility';

const VendorRattingScreen = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const [starRate, setStarRate] = useState(0);
  const [suggestion, setSuggestion] = useState();
  const [deliveryData, setDeliveryData] = useState();
  const [productData, setProductData] = useState();
  const orderId = route?.params?.orderId;

  useEffect(() => {
    const body = {
      order_id: orderId,
    };
    detailOrderRatingApiCall({body}).then(res => {
      setDeliveryData(res);
      let temp = [];
      res?.products?.map((item, index) => {
        item.ratting = 0;
        temp.push(item);
      });
      setProductData(temp);
    });
  }, [orderId]);

  const submitHandler = () => {
    if (starRate == 0) {
      Utility.showToast('Click on Star, Give Star Rating to Vendor');
    } else {
      let sendProducts = [];
      productData?.map(item => {
        sendProducts.push({
          product_id: item.product_id,
          rating:
            item.ratting != 0
              ? item.ratting
              : Utility.showToast('Click on Star, Give Star Rating to Dishes'),
        });
      });
      let isRattingNotGiven = false;
      sendProducts?.map((item, index) => {
        if (!item?.rating) {
          isRattingNotGiven = true;
        }
      });
      if (!isRattingNotGiven) {
        const body = {
          order_id: orderId,
          vendor_id: deliveryData?.vendor_id,
          rating: starRate,
          review: suggestion ? suggestion : '',
          products: sendProducts,
        };
        saveOrderRatingApiCall({body, navigation});
      }
    }
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={'Rate Your Delivery'} backVisible={true} />
        <DeliveryRateCard
          text={'Rate Your Meal By'}
          name={deliveryData?.name}
          setStarRate={setStarRate}
          starRate={starRate}
          setSuggestion={setSuggestion}
          suggestion={suggestion}
          onPress={() => {
            submitHandler();
          }}
          imgUri={deliveryData?.image}
          products={productData}
          setProductData={setProductData}
        />
      </SafeAreaView>
    </View>
  );
};

export default VendorRattingScreen;
