import {View, SafeAreaView} from 'react-native';
import React, {useState} from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import FavRestaurants from '../../Components/BarComponents/FavRestaurants';
import FavDishes from '../../Components/BarComponents/FavDishes';
import SelectButton from '../../Components/Buttons/SelectButton';

const SearchResDishScreen = () => {
  const [active, setActive] = useState('Restaurants');

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={'Search'} backVisible={true} />
        <View style={CmnStyles.searchResDisCont}>
          <SelectButton active={active} setActive={setActive} />
          {active == 'Restaurants' && (
            <FavRestaurants
              isSearch={true}
              otherStyle={CmnStyles.searchResDisSubCont}
              active={active}
              setActive={setActive}
            />
          )}
          {active == 'Dishes' && (
            <FavDishes
              isSearch={true}
              active={active}
              setActive={setActive}
              otherStyle={CmnStyles.searchResDisSubCont}
            />
          )}
        </View>
      </SafeAreaView>
    </View>
  );
};

export default SearchResDishScreen;
