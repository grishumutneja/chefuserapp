import {
  View,
  SafeAreaView,
  FlatList,
  ScrollView,
  ActivityIndicator,
  Text,
} from 'react-native';
import React, {memo, useCallback, useEffect, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import HomeHeader from '../../Components/Headers/HomeHeader';
import Responsive from '../../Constants/Responsive';
import SearchBox from '../../Components/Inputs/SearchBox';
import TitleRestaurant from '../../Components/Inputs/TitleRestaurant';
import CravingCard from '../../Components/Cards/CravingCard';
import FoodiesFavCard from '../../Components/Cards/FoodiesFavCard';
import HomeDishCard from '../../Components/Cards/HomeDishCard';
import RestaurantCard from '../../Components/Cards/RestaurantCard';
import Slider from '../../Components/Slider/Slider';
import {useDispatch, useSelector} from 'react-redux';
import {restaurantToExploreApiCall} from '../../Core/Config/ApiCalls';
import Colors from '../../Constants/Colors';
import AppRightsCard from '../../Components/Cards/AppRightsCard';
import HomeSkelton from '../../Components/Loader/HomeSkelton';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import LocationRestrict from '../../Components/Cards/LocationRestrict';

const RestaurantHomeScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [pageLoading, setPageLoading] = useState(false);
  const {locationData} = useSelector(state => state.location);
  const [pageOffset, setPageOffset] = useState(0);
  const [initialHide, setInitialHide] = useState(true);
  const [isLocationRestrict, setIsLocationRestrict] = useState(false);

  const restHomeBanners = useSelector(state => state.restHomeBanners);
  const {restHomeBannerData} = restHomeBanners;

  const restaurantToExplore = useSelector(state => state.restaurantToExplore);
  const {restaurantToExploreData, isDataLimitComplete} = restaurantToExplore;
  const {
    topRatedResData,
    onesMoreResData,
    resMasterBlogData,
    mostViewedVendors,
  } = useSelector(state => state.resHomeData);

  const {foodForYouData, foodCategoriesData} = useSelector(
    state => state.foodForYou,
  );

  useEffect(() => {
    if (locationData) {
      if (!foodForYouData || !foodCategoriesData) {
        setInitialHide(true);
      } else {
        if (foodForYouData?.length > 0 && foodCategoriesData?.length > 0) {
          setIsLocationRestrict(false);
          setInitialHide(false);
        } else {
          setIsLocationRestrict(true);
          setInitialHide(false);
        }
      }
    } else {
      setIsLocationRestrict(true);
      setInitialHide(false);
    }
  }, [foodForYouData, foodCategoriesData]);

  // useEffect(() => {
  //   restaurantToExploreData &&
  //     setTimeout(() => {
  //       setInitialHide(false);
  //     }, 1000);
  // }, [restaurantToExploreData]);

  useEffect(() => {
    !restaurantToExploreData?.length && restToExpApiCallHandler();
  }, [locationData]);

  const restToExpApiCallHandler = offset => {
    if (locationData?.lat && locationData?.lng) {
      let body = {
        lat: locationData?.lat,
        lng: locationData?.lng,
        offset: offset ? offset : 0,
        limit: 10,
      };
      restaurantToExploreApiCall({dispatch: dispatch, body: body});
    } else {
      setIsLocationRestrict(true);
    }
  };

  useEffect(() => {
    pageLoading && setPageLoading(false);
  }, [restaurantToExploreData, isDataLimitComplete]);

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 120;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const renderFoodOfYourChoice = () => {
    return (
      <View>
        <TitleRestaurant titleBig={'Food Of Your Choice'} />
        <FlatList
          horizontal
          data={foodForYouData}
          renderItem={({item, index}) => {
            return <CravingCard item={item} index={index} type={'Choice'} />;
          }}
          contentContainerStyle={{marginHorizontal: Responsive.widthPx(4)}}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  const renderCraving = () => {
    return (
      <View>
        <TitleRestaurant titleBig={'Choose your Craving'} />
        <FlatList
          horizontal
          data={foodCategoriesData}
          renderItem={({item, index}) => {
            return <CravingCard item={item} index={index} type={'Craving'} />;
          }}
          contentContainerStyle={{marginHorizontal: Responsive.widthPx(4)}}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  const renderTopRatedRes = () => {
    return (
      <View>
        <TitleRestaurant
          titleBig={`Top Rated Restaurants`}
          titleSmall={'Top Rated'}
          otherStyle={{marginBottom: Responsive.widthPx(1)}}
        />
        <FlatList
          data={topRatedResData}
          horizontal
          renderItem={({item, index}) => {
            return <FoodiesFavCard item={item} index={index} />;
          }}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{marginHorizontal: Responsive.widthPx(2)}}
          initialNumToRender={5}
        />
      </View>
    );
  };
  const renderMostViewedVendors = () => {
    return (
      <View>
        <TitleRestaurant
          titleBig={`Most Viewed`}
          otherStyle={{marginBottom: Responsive.widthPx(1)}}
        />
        <FlatList
          data={mostViewedVendors}
          horizontal
          renderItem={({item, index}) => {
            return (
              <FoodiesFavCard item={item} index={index} isFavHide={true} />
            );
          }}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{marginHorizontal: Responsive.widthPx(2)}}
          initialNumToRender={5}
        />
      </View>
    );
  };

  const renderFeaturedRes = ({item, index}) => {
    var setDateTime = function (date, str) {
      var sp = str.split(':');
      date.setHours(parseInt(sp[0], 10));
      date.setMinutes(parseInt(sp[1], 10));
      date.setSeconds(parseInt(sp[2], 10));
      return date;
    };

    var current = new Date();

    var c = current.getTime(),
      start = setDateTime(new Date(current), item?.blog?.from),
      end = setDateTime(new Date(current), item?.blog?.to);

    let isVisible;

    if (c > start.getTime() && c < end.getTime()) {
      isVisible = true;
    } else {
      isVisible = false;
    }
    return (
      <View>
        {item?.blog?.vendors?.length > 0 && isVisible ? (
          <>
            <TitleRestaurant
              titleBig={item.blog?.name}
              titleSmall={'Sponsored'}
              otherStyle={{marginBottom: Responsive.widthPx(1)}}
            />
            <FlatList
              data={item?.blog?.vendors}
              horizontal
              renderItem={({item, index}) => {
                return (
                  <FoodiesFavCard item={item} index={index} isFavHide={true} />
                );
              }}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                marginHorizontal: Responsive.widthPx(2),
              }}
              initialNumToRender={5}
            />
          </>
        ) : null}
      </View>
    );
  };

  const renderMasterBlogData = () => {
    return (
      <View>
        <FlatList data={resMasterBlogData} renderItem={renderFeaturedRes} />
      </View>
    );
  };

  const renderOnesMoreRes = () => {
    return (
      <View>
        <TitleRestaurant
          titleBig={`Try Ones More !`}
          otherStyle={{marginBottom: Responsive.widthPx(1)}}
        />
        <FlatList
          horizontal
          data={onesMoreResData}
          renderItem={({item, index}) => {
            return (
              <FoodiesFavCard item={item} index={index} isOnesMoreRes={true} />
            );
          }}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{marginHorizontal: Responsive.widthPx(2)}}
          initialNumToRender={5}
        />
      </View>
    );
  };

  const renderTopDishes = () => {
    return (
      <View>
        <TitleRestaurant
          titleBig={`Top Dishes`}
          titleSmall={'Top Rated'}
          otherStyle={{marginBottom: Responsive.widthPx(1)}}
        />
        <FlatList
          horizontal
          data={[1, 2, 3, 4, 6, 7]}
          renderItem={({item, index}) => {
            return <HomeDishCard item={item} index={index} />;
          }}
          contentContainerStyle={{
            marginHorizontal: Responsive.widthPx(2),
          }}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  const getItemLayout = useCallback(
    (data, index) => ({
      length: Responsive.widthPx(50),
      offset: Responsive.widthPx(50) * index + 1,
      index,
    }),
    [],
  );

  const renderRestaurantExplore = () => {
    return (
      <View>
        <TitleRestaurant
          titleBig={`Restaurants to Explore`}
          otherStyle={{marginBottom: Responsive.widthPx(1)}}
        />
        <FlatList
          data={restaurantToExploreData}
          renderItem={({item, index}) => {
            return <RestaurantCard item={item} index={index} />;
          }}
          contentContainerStyle={{
            marginHorizontal: Responsive.widthPx(2),
          }}
          showsHorizontalScrollIndicator={false}
          getItemLayout={getItemLayout}
          nestedScrollEnabled={true}
          scrollEnabled={false}
        />
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HomeHeader backVisible={true} />
          {initialHide ? (
            <HomeSkelton />
          ) : isLocationRestrict ? (
            <LocationRestrict />
          ) : (
            <View
              style={{
                paddingBottom: Responsive.widthPx(11),
                marginTop: Responsive.widthPx(2),
              }}>
              <SearchBox
                isFixed={true}
                onPress={() => {
                  navigation.navigate(Screens.SearchResDish);
                }}
                otherStyle={{marginBottom: Responsive.widthPx(3)}}
              />
              <ScrollView
                onScroll={({nativeEvent}) => {
                  if (isCloseToBottom(nativeEvent)) {
                    if (!pageLoading) {
                      if (!isDataLimitComplete) {
                        setPageOffset(pageOffset + 10);
                        setPageLoading(true);
                        restToExpApiCallHandler(pageOffset + 10);
                      }
                    }
                  } else {
                    setPageLoading(false);
                  }
                }}
                overScrollMode="never"
                nestedScrollEnabled={true}>
                <View>
                  <Slider
                    data={restHomeBannerData?.response}
                    otherStyle={CmnStyles.restHomeSlider}
                  />
                  <>
                    {renderFoodOfYourChoice()}
                    {renderCraving()}
                    {onesMoreResData?.length > 0 && renderOnesMoreRes()}
                    {topRatedResData?.length > 0 && renderTopRatedRes()}
                    {renderMasterBlogData()}
                    {mostViewedVendors?.length > 0 && renderMostViewedVendors()}
                    {/* {renderTopDishes()} */}
                    {renderRestaurantExplore()}
                    {isDataLimitComplete && (
                      <Text style={CmnStyles.noMoreResText}>
                        No more Restaurants to explore
                      </Text>
                    )}
                    {pageLoading && (
                      <ActivityIndicator
                        size={'large'}
                        style={CmnStyles.paginationLoader}
                        color={Colors.red}
                      />
                    )}
                  </>
                  <AppRightsCard
                    otherStyle={{marginTop: Responsive.widthPx(10)}}
                  />
                </View>
              </ScrollView>
            </View>
          )}
        </View>
      </SafeAreaView>
    </View>
  );
};

export default memo(RestaurantHomeScreen);
