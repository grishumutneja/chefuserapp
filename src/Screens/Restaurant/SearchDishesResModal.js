import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Modal,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import Responsive from '../../Constants/Responsive';
import Images from '../../Constants/Images';
import SearchBox from '../../Components/Inputs/SearchBox';
import DishesCard from '../../Components/Cards/DishesCard';
import {getSearchDishesInResDetailCall} from '../../Core/Config/ApiCalls';

export const SearchDishesResModal = ({
  modalVisible,
  vendorId,
  setSelectedItem,
  setDishModalVisible,
  setDishMenuIndex,
  isClosed,
  setSearchModalVisible,
  menuRef,
  data,
  setMoveIndex,
  setSearchApply,
  searchApply,
}) => {
  const [searchText, setSearchText] = useState();
  const [searchData, setSearchData] = useState();

  useEffect(() => {
    setMoveIndex();
  }, []);

  useEffect(() => {
    if (searchText?.length > 1) {
      setTimeout(() => {
        searchDataHandler();
      }, 200);
    } else {
      setSearchData();
    }
  }, [searchText]);

  const searchDataHandler = () => {
    const body = {
      vendor_id: vendorId,
      keyword: searchText,
    };
    getSearchDishesInResDetailCall({body}).then(res => {
      setSearchData(res);
    });
  };

  return (
    <Modal
      visible={modalVisible}
      animationType="slide"
      useNativeDriver={true}
      transparent={true}
      onRequestClose={() => {
        setSearchModalVisible(false);
      }}>
      <View style={CmnStyles.bodyBg}>
        <View style={CmnStyles.searchDishesScreenCont}>
          <Text style={TextStyles.darkGray_S_24_700}>Search</Text>
          <TouchableOpacity
            onPress={() => {
              setSearchModalVisible(false);
            }}
            style={CmnStyles.searchDishesCloseCont}>
            <Image
              source={Images.close}
              style={CmnStyles.searchDishesCloseImg}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <SearchBox
          otherStyle={{width: Responsive.widthPx(88)}}
          placeholder={'Search Dishes...'}
          setSearchText={setSearchText}
          searchText={searchText}
        />
        {searchData && (
          <FlatList
            data={searchData}
            renderItem={({item, index}) => {
              return (
                <DishesCard
                  dishItem={item}
                  dishIndex={index}
                  setSelectedItem={setSelectedItem}
                  setDishModalVisible={setDishModalVisible}
                  setDishMenuIndex={setDishMenuIndex}
                  isClosed={isClosed}
                  setSearchModalVisible={setSearchModalVisible}
                  noMenuIndex={true}
                  isSearch={true}
                  menuRef={menuRef}
                  data={data}
                  setMoveIndex={setMoveIndex}
                  setSearchApply={setSearchApply}
                  searchApply={searchApply}
                />
              );
            }}
            contentContainerStyle={{marginTop: Responsive.widthPx(4)}}
          />
        )}
      </View>
    </Modal>
  );
};
