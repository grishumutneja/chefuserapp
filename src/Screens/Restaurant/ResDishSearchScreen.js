import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  SafeAreaView,
  BackHandler,
  AppState,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import LottieView from 'lottie-react-native';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import Responsive from '../../Constants/Responsive';
import Images from '../../Constants/Images';
import SearchBox from '../../Components/Inputs/SearchBox';
import DishesCard from '../../Components/Cards/DishesCard';
import {
  getResDetailPageApiCall,
  getSearchDishesInResDetailCall,
  updateCartApiCall,
} from '../../Core/Config/ApiCalls';
import {useIsFocused, useNavigation, useRoute} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {
  addProceedCartRefreshCount,
  getResDishSearchData,
} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantDetailsSlice';
import ProceedToCartButton from '../../Components/Buttons/ProceedToCartButton';
import {DishModal} from '../../Components/Modals/DishModal';
import Screens from '../../Core/Stack/Screens';
import Animations from '../../Constants/Animations';

const ResDishSearchScreen = () => {
  const route = useRoute();
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [searchText, setSearchText] = useState();
  const [searchData, setSearchData] = useState();
  const [dishMenuIndex, setDishMenuIndex] = useState();
  const [selectedItem, setSelectedItem] = useState();
  const [dishModalVisible, setDishModalVisible] = useState(false);
  const [addProducts, setAddProducts] = useState([]);
  const {vendorId, isClosed} = route?.params;
  const {resDishSearchData} = useSelector(state => state.restaurantDetails);
  const {getUserInfoData} = useSelector(state => state.userInfo);
  const {
    itemsInCart,
    cart_id,
    proceedCartRefreshCount,
    totalAmount,
    clearResItem,
  } = useSelector(state => state.restaurantDetails);
  const {cartVendorId} = useSelector(state => state.cart);
  const appState = useRef(AppState.currentState);
  const minimum_order_amount = route.params?.minimum_order_amount;
  const isFreeDelivery = route.params?.isFreeDelivery;
  const [isFreeDeliveryApply, setIsFreeDeliveryApply] = useState();
  const [animationApply, setAnimationApply] = useState(true);
  const [removeCalled, setRemoveCalled] = useState(false);
  const [refresh, setRefresh] = useState(0);

  useEffect(() => {
    if (isFreeDelivery) {
      if (parseFloat(minimum_order_amount) - totalAmount <= 0) {
        if (!animationApply) {
          setIsFreeDeliveryApply(true);
          setAnimationApply(true);
          setTimeout(() => {
            setIsFreeDeliveryApply(false);
          }, 2000);
        }
      } else {
        setAnimationApply(false);
      }
    }
  }, [isFreeDelivery, totalAmount]);

  useEffect(() => {
    setSearchText();
    setTimeout(() => {
      setRefresh(refresh + 1);
    }, 1000);
  }, [isFocused]);

  console.log('searchText', searchText);
  console.log('refresh', refresh);

  useEffect(() => {
    const subscriptionSearch = AppState.addEventListener(
      'change',
      nextAppState => {
        if (
          appState.current.match(/inactive|background/) &&
          nextAppState === 'active'
        ) {
          // console.log('App has come to the foreground!');
        } else {
          // console.log('background Cart');
          if (vendorId == cartVendorId) {
            const body = {
              cart_id: cart_id,
              user_id: getUserInfoData?.id,
              vendor_id: vendorId,
              products: itemsInCart,
            };
            if (
              itemsInCart?.length > 0 &&
              cart_id &&
              clearResItem?.length > 0 &&
              !removeCalled
            ) {
              console.log('removeCalled false', removeCalled);
              console.log('updateCartApiCall From Res Detail Screen');
              updateCartApiCall({body, dispatch});
            }
          }
        }

        appState.current = nextAppState;
      },
    );

    return () => {
      setRemoveCalled(true);
      subscriptionSearch.remove();
      setTimeout(() => {
        subscriptionSearch.remove();
      }, 3000);
      setTimeout(() => {
        subscriptionSearch.remove();
      }, 5000);
      setTimeout(() => {
        subscriptionSearch.remove();
      }, 7000);
    };
  }, [
    itemsInCart,
    getUserInfoData,
    vendorId,
    cart_id,
    cartVendorId,
    clearResItem,
  ]);

  useEffect(() => {
    if (searchText?.length > 1) {
      setTimeout(() => {
        searchDataHandler();
      }, 200);
    } else {
      setSearchData();
      dispatch(getResDishSearchData([]));
    }
  }, [searchText]);

  useEffect(() => {
    const backAction = () => {
      if (vendorId == cartVendorId) {
        const body = {
          cart_id: cart_id,
          user_id: getUserInfoData?.id,
          vendor_id: vendorId,
          products: itemsInCart,
        };
        if (cart_id) {
          console.log('sixth updateCartApiCall UsEFfect 44');
          updateCartApiCall({body, dispatch});
        }
        navigation.goBack();
      } else {
        navigation.goBack();
      }
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, [vendorId, itemsInCart, getUserInfoData, cart_id, cartVendorId]);

  const backHandler = () => {
    if (vendorId == cartVendorId) {
      const body = {
        cart_id: cart_id,
        user_id: getUserInfoData?.id,
        vendor_id: vendorId,
        products: itemsInCart,
      };
      if (cart_id) {
        console.log('sixth updateCartApiCall UsEFfect 44');
        updateCartApiCall({body, dispatch}).then(res => {
          dispatch(addProceedCartRefreshCount(proceedCartRefreshCount + 1));
          navigation.goBack();
        });
      }
    } else {
      navigation.goBack();
    }
  };

  const searchDataHandler = () => {
    const body = {
      vendor_id: vendorId,
      keyword: searchText,
    };
    getSearchDishesInResDetailCall({body}).then(res => {
      //   setSearchData(res);
      if (res?.length > 0) {
        dispatch(getResDishSearchData(res));
      } else {
        dispatch(getResDishSearchData([]));
      }
    });
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <View style={CmnStyles.searchDishesScreenCont2} key={refresh}>
            <Text style={TextStyles.darkGray_S_24_700}>Search</Text>
            <TouchableOpacity
              onPress={() => {
                // navigation.goBack();
                backHandler();
              }}
              style={CmnStyles.searchDishesCloseCont}>
              <Image
                source={Images.close}
                style={CmnStyles.searchDishesCloseImg}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <SearchBox
            otherStyle={{width: Responsive.widthPx(88)}}
            placeholder={'Search Dishes...'}
            setSearchText={setSearchText}
            searchText={searchText}
            refresh={refresh}
          />
          {resDishSearchData && (
            <FlatList
              data={resDishSearchData}
              renderItem={({item, index}) => {
                return (
                  <DishesCard
                    dishItem={item}
                    dishIndex={index}
                    isClosed={isClosed}
                    isSearchScreen={true}
                    setDishMenuIndex={setDishMenuIndex}
                    // menuIndex={menuIndex}
                    setSelectedItem={setSelectedItem}
                    setDishModalVisible={setDishModalVisible}
                  />
                );
              }}
              contentContainerStyle={{
                marginTop: Responsive.widthPx(4),
                // marginBottom: Responsive.widthPx(40),
                paddingBottom: Responsive.widthPx(22),
              }}
            />
          )}
          {dishModalVisible && (
            <DishModal
              modalVisible={dishModalVisible}
              onPressCancel={() => {
                setDishModalVisible(false);
              }}
              dishItem={selectedItem}
              dishMenuIndex={dishMenuIndex}
              setDishModalVisible={setDishModalVisible}
              vendorId={vendorId}
              isSearchScreen={true}
            />
          )}
        </View>
      </SafeAreaView>
      <ProceedToCartButton
        vendorId={vendorId}
        addProducts={addProducts}
        setAddProducts={setAddProducts}
        minimum_order_amount={minimum_order_amount}
        isFreeDelivery={isFreeDelivery}
      />
      {isFreeDeliveryApply && (
        <LottieView
          source={Animations.freeDelivery2}
          style={{
            width: Responsive.widthPx(100),
            position: 'absolute',
            bottom: 0,
            height: Responsive.heightPx(50),
          }}
          autoPlay={true}
          loop={true}
          speed={1}
        />
      )}
    </View>
  );
};

export default ResDishSearchScreen;
