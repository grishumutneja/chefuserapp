import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  BackHandler,
  AppState,
} from 'react-native';
import React, {memo, useEffect, useRef, useState} from 'react';
import LottieView from 'lottie-react-native';
import CmnStyles from '../../Styles/CmnStyles';
import Responsive from '../../Constants/Responsive';
import ResDetailTopButtons from '../../Components/Cards/ResDetailTopButtons';
import TextStyles from '../../Styles/TextStyles';
import OfferCard from '../../Components/Cards/OfferCard';
import Colors from '../../Constants/Colors';
import SwitchWithTitle from '../../Components/Switchs/SwitchWithTitle';
import DishMenuCard from '../../Components/Cards/DishMenuCard';
import MenuButton from '../../Components/Buttons/MenuButton';
import {MenuModal} from '../../Components/Modals/MenuModal';
import {useIsFocused, useNavigation, useRoute} from '@react-navigation/native';
import AppRightsCard from '../../Components/Cards/AppRightsCard';
import {
  getCartListApiCall,
  getResDetailByFoodTypeApiCall,
  getResDetailPageApiCall,
  updateCartApiCall,
} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';
import {DishModal} from '../../Components/Modals/DishModal';
import ProceedToCartButton from '../../Components/Buttons/ProceedToCartButton';
import {SearchDishesResModal} from './SearchDishesResModal';
import SpStyles from '../../Styles/SpStyles';
import Animations from '../../Constants/Animations';
import RestaurantNameCard from '../../Components/Cards/RestaurantNameCard';
import moment from 'moment';
import ResDetailSkelton from '../../Components/Loader/ResDetailSkelton';
import {removeItemInCart} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantDetailsSlice';

const RestaurantDetailScreen = () => {
  const route = useRoute();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const navigation = useNavigation();
  const [menuRef, setMenuRef] = useState(null);
  const [initialHide, setInitialHide] = useState(true);
  const [isFilterLoad, setIsFilterLoad] = useState(false);
  const [veg, setVeg] = useState(false);
  const [nonVeg, setNonVeg] = useState(false);
  const [egg, setEgg] = useState(false);
  const [menuVisible, setMenuVisible] = useState(false);
  const resData = route?.params?.resData;
  const isRefresh = route?.params?.isRefresh;
  const {restaurantDetailsData, dishesData, isOnline, totalAmount} =
    useSelector(state => state.restaurantDetails);
  const [isClosed, setIsClosed] = useState(false);
  const [searchModalVisible, setSearchModalVisible] = useState(false);
  const [dishModalVisible, setDishModalVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState();
  const [dishMenuIndex, setDishMenuIndex] = useState();
  const {getUserInfoData} = useSelector(state => state.userInfo);
  const {cartListData, cartVendorId} = useSelector(state => state.cart);
  const [addProducts, setAddProducts] = useState([]);
  const {itemsInCart, cart_id, proceedCartRefreshCount, clearResItem} =
    useSelector(state => state.restaurantDetails);
  const [moveIndex, setMoveIndex] = useState();
  const [searchApply, setSearchApply] = useState(0);
  const [offsetFromSearch, setOffsetFromSearch] = useState();
  const isFromSearched = route?.params?.isFromSearched;
  const dishFromSearch = route?.params?.dishFromSearch;
  const isBackBtnPressed = route?.params?.isBackBtnPressed;
  const [currentTime, setCurrentTime] = useState();
  const isClosedNow = route?.params?.resData?.isClosed;
  const closeTime = resData?.end_time;
  const openTime = resData?.start_time;
  const offer_id = dishesData && dishesData[0]?.products[0]?.offer_id;
  const offer_persentage =
    dishesData && dishesData[0]?.products[0]?.offer_persentage;
  const [currentIndex, setCurrentIndex] = useState();
  const [isCloseSoon, setIsCloseSoon] = useState(false);
  const [closeRemainTime, setCloseRemainTime] = useState();
  const [isFreeDeliveryApply, setIsFreeDeliveryApply] = useState();
  const [animationApply, setAnimationApply] = useState(true);
  const [removeCalled, setRemoveCalled] = useState(false);

  const appState = useRef(AppState.currentState);

  const currentTimeMin =
    +currentTime?.slice(0, 2) * 60 + +currentTime?.slice(3, 5);
  const closeTimeMin = +closeTime?.slice(0, 2) * 60 + +closeTime?.slice(3, 5);

  useEffect(() => {
    if (closeTimeMin > currentTimeMin && closeTimeMin <= currentTimeMin + 30) {
      setIsCloseSoon(true);
      setCloseRemainTime(closeTimeMin - currentTimeMin);
    } else {
      setCloseRemainTime();
      setIsCloseSoon(false);
    }
  }, [closeTimeMin, currentTimeMin]);

  useEffect(() => {
    dispatch(removeItemInCart(itemsInCart));
  }, [itemsInCart]);

  useEffect(() => {
    if (isCloseSoon) {
      setTimeout(() => {
        const time = new Date().getTime();
        const curTime = moment(time).format('HH:mm:ss');
        setCurrentTime(curTime);
      }, 60000);
    }
  }, [isCloseSoon, currentTime]);

  useEffect(() => {
    if (restaurantDetailsData?.free_delivery == '1') {
      if (
        parseFloat(restaurantDetailsData?.minimum_order_amount) - totalAmount <=
        0
      ) {
        if (!animationApply) {
          setTimeout(() => {
            setIsFreeDeliveryApply(true);
            setAnimationApply(true);
            setTimeout(() => {
              setIsFreeDeliveryApply(false);
            }, 2000);
          }, 1000);
        }
      } else {
        setAnimationApply(false);
      }
    }
  }, [restaurantDetailsData, totalAmount]);

  useEffect(() => {
    const subscriptionRes = AppState.addEventListener(
      'change',
      nextAppState => {
        if (
          appState.current.match(/inactive|background/) &&
          nextAppState === 'active'
        ) {
          // console.log('App has come to the foreground!');
        } else {
          // console.log('background Cart');
          console.log('removeCalled outSide', removeCalled);
          const body = {
            cart_id: cart_id,
            user_id: getUserInfoData?.id,
            vendor_id: resData?.vendor_id,
            products: itemsInCart,
          };
          if (
            itemsInCart?.length > 0 &&
            cart_id &&
            clearResItem?.length > 0 &&
            !removeCalled
          ) {
            console.log('removeCalled false', removeCalled);
            console.log('updateCartApiCall From Res Detail Screen');
            updateCartApiCall({body, dispatch});
          }
        }
        appState.current = nextAppState;
      },
    );

    return () => {
      setRemoveCalled(true);
      subscriptionRes.remove();
      setTimeout(() => {
        subscriptionRes.remove();
      }, 3000);
      setTimeout(() => {
        subscriptionRes.remove();
      }, 5000);
      setTimeout(() => {
        subscriptionRes.remove();
      }, 7000);
    };
  }, [itemsInCart, clearResItem]);

  useEffect(() => {
    setTimeout(() => {
      const time = new Date().getTime();
      const curTime = moment(time).format('HH:mm:ss');
      setCurrentTime(curTime);
    }, 60000);
  }, [currentTime]);

  useEffect(() => {
    const time = new Date().getTime();
    const curTime = moment(time).format('HH:mm:ss');
    setCurrentTime(curTime);
  }, []);

  useEffect(() => {
    const backAction = () => {
      if (resData?.vendor_id == cartVendorId) {
        const body = {
          cart_id: cart_id,
          user_id: getUserInfoData?.id,
          vendor_id: resData?.vendor_id,
          products: itemsInCart,
        };
        if (cart_id) {
          updateCartApiCall({body, dispatch});
        }
        navigation.goBack();
      } else {
        navigation.goBack();
      }
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [resData, itemsInCart, getUserInfoData, cart_id, cartVendorId]);

  useEffect(() => {
    if (isOnline == 1) {
      if (isClosed) {
        if (!isClosedNow) {
          setIsClosed(false);
        } else {
          if (
            closeTime &&
            openTime &&
            currentTime > openTime &&
            currentTime < closeTime
          ) {
            setIsClosed(false);
          }
        }
      } else {
        if (isClosedNow == 1) {
          setIsClosed(isClosedNow);
        } else {
          if (closeTime) {
            currentTime > closeTime ? setIsClosed(true) : setIsClosed(false);
          }
        }
      }
    } else if (isOnline == 0) {
      setTimeout(() => {
        setIsClosed(true);
      }, 1000);
    }
  }, [isClosedNow, currentTime, closeTime, isOnline]);

  useEffect(() => {
    const myBody = {
      user_id: getUserInfoData?.id,
    };
    if (!cartListData) {
      getCartListApiCall({body: myBody, dispatch: dispatch});
    }
  }, []);

  useEffect(() => {
    if (isFromSearched) {
      let offIndex = 0;
      let myIndex = dishesData?.length;
      let mnIndex = 0;
      setSearchApply(searchApply + 1);
      dishesData?.map((itm, ind) => {
        if (itm?.menu_id == dishFromSearch?.menu_id) {
          setMoveIndex(ind);
          itm?.products?.map((proItem, proIndex) => {
            if (proItem?.product_id == dishFromSearch?.product_id) {
              // offIndex = proIndex + offIndex;
              offIndex = proIndex + offIndex;
              myIndex = ind;
            }
          });
        } else {
          if (ind < myIndex) {
            // offIndex = itm?.products?.length + offIndex;
            mnIndex = mnIndex + 1;
          }
        }
      });
      const offset =
        mnIndex * Responsive.widthPx(20) + Responsive.widthPx(50) * offIndex;
      setOffsetFromSearch(offset);
      setSearchModalVisible(false);
    }
  }, [dishesData]);

  useEffect(() => {
    if (isFromSearched) {
      if (offsetFromSearch) {
        setTimeout(() => {
          menuRef?.scrollToOffset({
            animated: true,
            offset: offsetFromSearch,
          });
        }, 2000);
      }
    }
  }, [offsetFromSearch]);

  useEffect(() => {
    setInitialHide(true);
  }, [isFocused]);

  useEffect(() => {
    if (restaurantDetailsData && isRefresh) {
      if (isBackBtnPressed) {
        setTimeout(() => {
          setInitialHide(false);
        }, 4000);
      } else {
        setTimeout(() => {
          setInitialHide(false);
        }, 1200);
      }
    } else {
      // if (proceedCartRefreshCount > 0) {
      //   setTimeout(() => {
      //     setInitialHide(false);
      //     console.log('initialHide kl 3 ===>', initialHide);
      //   }, 4300);
      // } else {
      setTimeout(() => {
        setInitialHide(false);
      }, 300);
      // }
    }
  }, [restaurantDetailsData, isRefresh, isBackBtnPressed]);

  useEffect(() => {
    !nonVeg && !veg && allResDetail();
  }, [nonVeg, veg, isFocused]);

  const allResDetail = () => {
    const body = {
      vendor_id: resData?.vendor_id,
    };
    getResDetailPageApiCall({
      body: body,
      dispatch: dispatch,
      setIsFilterLoad,
    });
  };

  const filterApply = type => {
    const body = {vendor_id: resData?.vendor_id, food_type: type};
    getResDetailByFoodTypeApiCall({body, dispatch, setIsFilterLoad});
  };

  const topPart = () => {
    return (
      <View style={CmnStyles.ResDetailScreenTopCont}>
        <RestaurantNameCard
          resData={resData}
          isClosed={isClosed}
          closeRemainTime={closeRemainTime}
        />
      </View>
    );
  };

  const renderOffers = () => {
    return (
      <View style={CmnStyles.resDetailRenderOffersCont}>
        {restaurantDetailsData?.coupons?.length >= 0 && (
          <>
            <View style={SpStyles.FDR_ALC}>
              <Text
                style={
                  isClosed ? TextStyles.black_S_20_700 : TextStyles.red_20_700
                }>
                Offers
              </Text>
              <LottieView
                source={Animations.happyFace}
                style={CmnStyles.offerHappyFace}
                autoPlay={true}
                loop={true}
              />
            </View>
            <FlatList
              horizontal
              data={restaurantDetailsData?.coupons}
              renderItem={({item, index}) => {
                return (
                  <OfferCard item={item} index={index} isClosed={isClosed} />
                );
              }}
              ListHeaderComponent={() => {
                return (
                  restaurantDetailsData?.free_delivery == '1' && (
                    <OfferCard
                      item={{
                        name: `FREE DELIVERY*`,
                        code: `ORDER ABOVE ${restaurantDetailsData?.minimum_order_amount}`,
                      }}
                      isClosed={isClosed}
                      otherStyle={{
                        backgroundColor: Colors.redLight,
                      }}
                      textColor={{color: Colors.red_primary}}
                      isRed={true}
                    />
                  )
                );
              }}
              showsHorizontalScrollIndicator={false}
            />
          </>
        )}
      </View>
    );
  };

  const renderSwitches = () => {
    return (
      <View style={CmnStyles.resDetailSwitchesCont}>
        <SwitchWithTitle
          onPress={() => {
            const body = {
              cart_id: cart_id,
              user_id: getUserInfoData?.id,
              vendor_id: resData?.vendor_id,
              products: itemsInCart,
            };
            setVeg(!veg);
            if (cartVendorId == resData?.vendor_id && cart_id) {
              updateCartApiCall({body, dispatch}).then(res => {
                if (veg) {
                  if (nonVeg) {
                    filterApply('non_veg');
                  } else {
                    allResDetail();
                  }
                } else {
                  if (nonVeg) {
                    allResDetail();
                  } else {
                    filterApply('veg');
                  }
                }
              });
            } else {
              if (veg) {
                if (nonVeg) {
                  filterApply('non_veg');
                } else {
                  allResDetail();
                }
              } else {
                if (nonVeg) {
                  allResDetail();
                } else {
                  filterApply('veg');
                }
              }
            }
          }}
          value={veg}
          imgBgColor={Colors.greenPureVeg}
          text={'Veg'}
        />
        <SwitchWithTitle
          onPress={() => {
            const body = {
              cart_id: cart_id,
              user_id: getUserInfoData?.id,
              vendor_id: resData?.vendor_id,
              products: itemsInCart,
            };
            setNonVeg(!nonVeg);
            if (cartVendorId == resData?.vendor_id && cart_id) {
              updateCartApiCall({body, dispatch}).then(res => {
                if (nonVeg) {
                  if (veg) {
                    filterApply('veg');
                  } else {
                    allResDetail();
                  }
                } else {
                  if (veg) {
                    allResDetail();
                  } else {
                    filterApply('non_veg');
                  }
                }
              });
            } else {
              if (nonVeg) {
                if (veg) {
                  filterApply('veg');
                } else {
                  allResDetail();
                }
              } else {
                if (veg) {
                  allResDetail();
                } else {
                  filterApply('non_veg');
                }
              }
            }
          }}
          value={nonVeg}
          imgBgColor={Colors.red_primary}
          text={'Non -Veg'}
        />
      </View>
    );
  };

  const getItemLayoutMenu = (data, index) => ({
    length: Responsive.widthPx(300),
    offset: Responsive.widthPx(300) * index,
    index,
  });

  const renderAllDishes = () => {
    return (
      <View>
        <FlatList
          ref={ref => setMenuRef(ref)}
          data={dishesData}
          // onViewableItemsChanged={onViewableItemsChanged}
          // viewabilityConfig={{
          //   itemVisiblePercentThreshold: Responsive.widthPx(100),
          // }}
          renderItem={({item, index}) => {
            return (
              <>
                {initialHide || isFilterLoad ? null : (
                  <DishMenuCard
                    menuItem={item}
                    menuIndex={index}
                    setSelectedItem={setSelectedItem}
                    setDishModalVisible={setDishModalVisible}
                    setDishMenuIndex={setDishMenuIndex}
                    isClosed={isClosed}
                    addProducts={addProducts}
                    setAddProducts={setAddProducts}
                    moveIndex={moveIndex}
                    setSearchApply={setSearchApply}
                    searchApply={searchApply}
                    currentIndex={currentIndex}
                  />
                )}
              </>
            );
          }}
          key={dishesData}
          ListHeaderComponent={() => {
            return (
              <>
                {resData && topPart()}
                {!(initialHide || isFilterLoad) && (
                  <>
                    {renderOffers()}
                    {resData?.vendor_food_type != 1 && renderSwitches()}
                  </>
                )}
              </>
            );
          }}
          ListFooterComponent={() => {
            return (
              <AppRightsCard
                otherStyle={{
                  marginTop: Responsive.widthPx(16),
                }}
                licenseNumber={resData?.fssai_lic_no}
              />
            );
          }}
          // removeClippedSubviews={true} // Unmount components when outside of window
          // initialNumToRender={2} // Reduce initial render amount
          // // maxToRenderPerBatch={1} // Reduce number in each render batch
          // updateCellsBatchingPeriod={100} // Increase time between renders
          // windowSize={7}
          // scrollEnabled={false}
          onScrollToIndexFailed={info => {
            const wait = new Promise(resolve => setTimeout(resolve, 500));
            wait.then(() => {
              menuRef.scrollToIndex({
                index: info.index,
                animated: true,
              });
            });
          }}
          contentContainerStyle={{paddingBottom: Responsive.widthPx(10)}}
          overScrollMode="never"
          // onScrollToIndexFailed={error => {
          //   menuRef.scrollToOffset({
          //     offset: error.averageItemLength * error.index,
          //     animated: true,
          //   });
          //   setTimeout(() => {
          //     if (dishesData.length !== 0 && menuRef !== null) {
          //       menuRef.scrollToIndex({
          //         index: error.index,
          //         animated: true,
          //       });
          //     }
          //   }, 100);
          // }}
          // getItemLayout={getItemLayoutMenu}
          // scrollEnabled={false}
          // removeClippedSubviews={true}
        />
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <ResDetailTopButtons
            otherStyle={CmnStyles.resDetailTopButtonsOther}
            resData={resData}
            isClosed={isClosed}
            setSearchModalVisible={setSearchModalVisible}
            cartVendorId={cartVendorId}
            cart_id={cart_id}
            itemsInCart={itemsInCart}
            isFreeDelivery={
              restaurantDetailsData?.free_delivery == '0' ? false : true
            }
            minimum_order_amount={restaurantDetailsData?.minimum_order_amount}
          />
          <View
            style={{
              flex: 1,
              paddingTop: Responsive.widthPx(20),
            }}>
            {renderAllDishes()}
          </View>
          {(initialHide || isFilterLoad) && (
            <View style={CmnStyles.resDetailScreenLoadCont}>
              <ResDetailSkelton visible={true} />
            </View>
          )}
          <MenuButton
            onPress={() => {
              setMenuVisible(true);
            }}
          />
          {menuVisible && (
            <MenuModal
              modalVisible={menuVisible}
              onPressCancel={() => {
                setMenuVisible(false);
              }}
              data={dishesData}
              menuRef={menuRef}
              setMenuVisible={setMenuVisible}
              setMoveIndex={setMoveIndex}
            />
          )}
          {dishModalVisible && (
            <DishModal
              modalVisible={dishModalVisible}
              onPressCancel={() => {
                setDishModalVisible(false);
              }}
              dishItem={selectedItem}
              dishMenuIndex={dishMenuIndex}
              setDishModalVisible={setDishModalVisible}
              vendorId={resData?.vendor_id}
            />
          )}
        </View>
        {searchModalVisible && (
          <SearchDishesResModal
            modalVisible={searchModalVisible}
            vendorId={resData?.vendor_id}
            setSelectedItem={setSelectedItem}
            setDishModalVisible={setDishModalVisible}
            // menuIndex={menuIndex}
            setDishMenuIndex={setDishMenuIndex}
            isClosed={isClosed}
            setSearchModalVisible={setSearchModalVisible}
            menuRef={menuRef}
            data={dishesData}
            setMoveIndex={setMoveIndex}
            setSearchApply={setSearchApply}
            searchApply={searchApply}
          />
        )}
      </SafeAreaView>
      {!initialHide && !isFilterLoad && (
        <ProceedToCartButton
          vendorId={resData?.vendor_id}
          addProducts={addProducts}
          setAddProducts={setAddProducts}
          isFreeDelivery={
            restaurantDetailsData?.free_delivery == '0' ? false : true
          }
          minimum_order_amount={restaurantDetailsData?.minimum_order_amount}
        />
      )}
      {isFreeDeliveryApply && (
        <LottieView
          source={Animations.freeDelivery2}
          style={{
            width: Responsive.widthPx(100),
            position: 'absolute',
            bottom: 0,
            height: Responsive.heightPx(50),
          }}
          autoPlay={true}
          loop={true}
          speed={1}
        />
      )}
    </View>
  );
};

export default memo(RestaurantDetailScreen);
