import {
  View,
  Text,
  SafeAreaView,
  KeyboardAvoidingView,
  ScrollView,
  Platform,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import _ from 'lodash';
import CmnStyles from '../../Styles/CmnStyles';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import SignUpInputCard from '../../Components/Inputs/SignUpInputCard';
import AuthMobTextBox from '../../Components/Inputs/AuthMobTextBox';
import RedButton from '../../Components/Buttons/RedButton';
import {useNavigation, useRoute} from '@react-navigation/native';
import Utility from '../../Constants/Utility';
import {registerUserApiCall} from '../../Core/Config/ApiCalls';
import {useDispatch} from 'react-redux';
import KeyboardManager, {PreviousNextView} from 'react-native-keyboard-manager';

const SignUpScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [email, setEmail] = useState();
  const [referralCode, setReferralCode] = useState();
  const [altMobNum, setAltMobNum] = useState();
  const route = useRoute();

  useEffect(() => {
    if (Platform.OS === 'ios') {
      KeyboardManager.setEnable(true);
      KeyboardManager.setToolbarPreviousNextButtonEnable(true);
      KeyboardManager.setKeyboardDistanceFromTextField(100);
    }
    return () => {
      KeyboardManager.setEnable(false);
      KeyboardManager.setToolbarPreviousNextButtonEnable(false);
      KeyboardManager.setKeyboardDistanceFromTextField(0);
    };
  }, []);

  const signUpHandler = () => {
    if (!_.trim(firstName)) {
      return Utility.showToast('Please Enter First Name');
    } else if (!_.trim(lastName)) {
      return Utility.showToast('Please Enter Last Name');
    } else if (!_.trim(email)) {
      return Utility.showToast('Please Enter Email Id');
    } else if (Utility.isValid(email, true)) {
      return Utility.showToast('Please Enter Valid Email Id');
    } else if (!_.trim(altMobNum)) {
      return Utility.showToast('Please Enter Alternative Mobile Number');
    } else if (altMobNum?.length !== 10) {
      return Utility.showToast('Please Enter Valid Alternative Mobile Number');
    } else {
      const body = {
        mobile_number: route?.params?.mobileNumber,
        referralcode: referralCode,
        alternative_mobile: altMobNum,
        name: firstName,
        lastname: lastName,
        email: email,
      };
      registerUserApiCall({body, navigation, dispatch});
    }

    // navigation.navigate(Screens.BottomStack, {
    //   screen: Screens.Home,
    // });
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack text={'Sign Up'} backVisible={true} />
          <View style={CmnStyles.loginCont}>
            <PreviousNextView>
              <KeyboardAvoidingView
                behavior={Platform.OS == 'android' && 'padding'}
                keyboardVerticalOffset={Platform.OS == 'android' ? 20 : 130}>
                <ScrollView style={{flexGrow: 1}}>
                  <View>
                    <View style={CmnStyles.signUpTitleCont}>
                      <Text style={CmnStyles.signUpTitle}>Almost There !</Text>
                      <Text style={CmnStyles.signUpText}>
                        Let's know you a bit more
                      </Text>
                    </View>
                    <SignUpInputCard
                      title={'First Name'}
                      placeholder={'Elon Musk'}
                      starVisible={true}
                      onChangeText={text => setFirstName(text)}
                      autoCapitalize={'words'}
                    />
                    <SignUpInputCard
                      title={'Last Name'}
                      placeholder={'Elon Musk'}
                      starVisible={true}
                      onChangeText={text => setLastName(text)}
                      autoCapitalize={'words'}
                    />
                    <SignUpInputCard
                      title={'Email Id'}
                      placeholder={'abc@gmail.com'}
                      onChangeText={text => setEmail(text)}
                      keyboardType={'email-address'}
                      autoCapitalize={'none'}
                    />
                    <SignUpInputCard
                      title={'Referral Code'}
                      placeholder={'abc@gmail.com'}
                      optionalVisible={true}
                      keyboardType={'email-address'}
                      onChangeText={text => setReferralCode(text)}
                    />
                    <View style={CmnStyles.signUpAltBox}>
                      <Text style={CmnStyles.signUpInputCardTitle}>
                        {'Alternative Mobile No.'}
                      </Text>
                      <AuthMobTextBox
                        otherStyle={CmnStyles.signUpMobText}
                        signUp={true}
                        onChangeText={text => setAltMobNum(text)}
                      />
                    </View>
                  </View>
                  <RedButton
                    text={'Continue'}
                    onPress={() => {
                      signUpHandler();
                    }}
                    otherStyle={{marginBottom: 50}}
                  />
                </ScrollView>
              </KeyboardAvoidingView>
            </PreviousNextView>
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
};

export default SignUpScreen;
// import {
//   View,
//   Text,
//   SafeAreaView,
//   KeyboardAvoidingView,
//   ScrollView,
//   Platform,
// } from 'react-native';
// import React, {useState} from 'react';
// import _ from 'lodash';
// import CmnStyles from '../../Styles/CmnStyles';
// import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
// import SignUpInputCard from '../../Components/Inputs/SignUpInputCard';
// import AuthMobTextBox from '../../Components/Inputs/AuthMobTextBox';
// import RedButton from '../../Components/Buttons/RedButton';
// import {useNavigation, useRoute} from '@react-navigation/native';
// import Utility from '../../Constants/Utility';
// import {registerUserApiCall} from '../../Core/Config/ApiCalls';
// import {useDispatch} from 'react-redux';

// const SignUpScreen = () => {
//   const dispatch = useDispatch();
//   const navigation = useNavigation();
//   const [firstName, setFirstName] = useState();
//   const [lastName, setLastName] = useState();
//   const [email, setEmail] = useState();
//   const [referralCode, setReferralCode] = useState();
//   const [altMobNum, setAltMobNum] = useState();
//   const route = useRoute();

//   const signUpHandler = () => {
//     if (!_.trim(firstName)) {
//       return Utility.showToast('Please Enter First Name');
//     } else if (!_.trim(lastName)) {
//       return Utility.showToast('Please Enter Last Name');
//     } else if (!_.trim(email)) {
//       return Utility.showToast('Please Enter Email Id');
//     } else if (Utility.isValid(email, true)) {
//       return Utility.showToast('Please Enter Valid Email Id');
//     } else if (!_.trim(altMobNum)) {
//       return Utility.showToast('Please Enter Alternative Mobile Number');
//     } else if (altMobNum?.length !== 10) {
//       return Utility.showToast('Please Enter Valid Alternative Mobile Number');
//     } else {
//       const body = {
//         mobile_number: route?.params?.mobileNumber,
//         referralcode: referralCode,
//         alternative_mobile: altMobNum,
//         name: firstName,
//         lastname: lastName,
//         email: email,
//       };
//       registerUserApiCall({body, navigation, dispatch});
//     }

//     // navigation.navigate(Screens.BottomStack, {
//     //   screen: Screens.Home,
//     // });
//   };

//   return (
//     <View style={CmnStyles.bodyBg}>
//       <SafeAreaView style={CmnStyles.bodyBg}>
//         <View style={CmnStyles.bodyBg}>
//           <HeaderWithBack text={'Sign Up'} backVisible={true} />
//           <View style={CmnStyles.loginCont}>
//             <KeyboardAvoidingView
//               behavior={Platform.OS == 'android' ? 'padding' : 'height'}
//               keyboardVerticalOffset={Platform.OS == 'android' ? 20 : 130}>
//               <ScrollView>
//                 <View>
//                   <View style={CmnStyles.signUpTitleCont}>
//                     <Text style={CmnStyles.signUpTitle}>Almost There !</Text>
//                     <Text style={CmnStyles.signUpText}>
//                       Let's know you a bit more
//                     </Text>
//                   </View>
//                   <SignUpInputCard
//                     title={'First Name'}
//                     placeholder={'Elon Musk'}
//                     starVisible={true}
//                     onChangeText={text => setFirstName(text)}
//                     autoCapitalize={'words'}
//                   />
//                   <SignUpInputCard
//                     title={'Last Name'}
//                     placeholder={'Elon Musk'}
//                     starVisible={true}
//                     onChangeText={text => setLastName(text)}
//                     autoCapitalize={'words'}
//                   />
//                   <SignUpInputCard
//                     title={'Email Id'}
//                     placeholder={'abc@gmail.com'}
//                     onChangeText={text => setEmail(text)}
//                     keyboardType={'email-address'}
//                     autoCapitalize={'none'}
//                   />
//                   <SignUpInputCard
//                     title={'Referral Code'}
//                     placeholder={'abc@gmail.com'}
//                     optionalVisible={true}
//                     keyboardType={'email-address'}
//                     onChangeText={text => setReferralCode(text)}
//                   />
//                   <View style={CmnStyles.signUpAltBox}>
//                     <Text style={CmnStyles.signUpInputCardTitle}>
//                       {'Alternative Mobile No.'}
//                     </Text>
//                     <AuthMobTextBox
//                       otherStyle={CmnStyles.signUpMobText}
//                       signUp={true}
//                       onChangeText={text => setAltMobNum(text)}
//                     />
//                   </View>
//                 </View>
//                 <RedButton
//                   text={'Continue'}
//                   onPress={() => {
//                     signUpHandler();
//                   }}
//                 />
//               </ScrollView>
//             </KeyboardAvoidingView>
//           </View>
//         </View>
//       </SafeAreaView>
//     </View>
//   );
// };

// export default SignUpScreen;
