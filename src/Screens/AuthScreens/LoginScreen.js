import {
  View,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  Platform,
} from 'react-native';
import _ from 'lodash';
import React, {useEffect, useState} from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import AuthMobTextBox from '../../Components/Inputs/AuthMobTextBox';
import RedButton from '../../Components/Buttons/RedButton';
import OtpInputPart from '../../Constants/OtpInputPart';
import {useNavigation} from '@react-navigation/native';
import Utility from '../../Constants/Utility';
import {
  guestLoginApiCall,
  loginOptSendApiCall,
  loginOptVerifyApiCall,
} from '../../Core/Config/ApiCalls';
import {useDispatch} from 'react-redux';
import CusToast from '../../Components/Toast/CusToast';

const LoginScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [otpCode, setOtpCode] = useState();
  const [mobileNumber, setMobileNumber] = useState('');
  const [otpActive, setOtpActive] = useState(false);
  const [otpTime, setOtpTime] = useState(0);

  useEffect(() => {
    otpActive &&
      otpTime > 0 &&
      setTimeout(() => {
        setOtpTime(otpTime - 1);
      }, 1000);
  }, [otpTime]);

  useEffect(() => {
    otpCode?.length == 4 && verifyOtpHandler();
  }, [otpCode]);

  const requestOtpHandler = () => {
    if (!_.trim(mobileNumber)) {
      return Utility.showToast('Please Enter Mobile Number');
    } else if (mobileNumber?.length !== 10) {
      return Utility.showToast('Please Enter Valid Mobile Number');
    } else {
      setOtpActive(true);
      let body = {
        mobile_number: mobileNumber,
      };
      const isSend = loginOptSendApiCall({body: body, setOtpActive});
      if (isSend) {
        setOtpActive(true);
        otpTime == 0 && setOtpTime(30);
      }
    }
  };

  const verifyOtpHandler = () => {
    if (!_.trim(otpCode)) {
      return Utility.showToast('Please Enter OTP Code');
    } else if (otpCode?.length !== 4) {
      return Utility.showToast('Please Enter Valid OTP Code');
    } else {
      let body = {
        mobile_number: mobileNumber,
        otp: otpCode,
      };
      loginOptVerifyApiCall({
        body: body,
        navigation: navigation,
        dispatch: dispatch,
      });
    }
  };

  const guestHandler = () => {
    guestLoginApiCall({navigation, dispatch});
  };

  const otpPart = () => {
    return (
      <View style={CmnStyles.otpPartCont}>
        <Text style={CmnStyles.otpPartSucText}>
          OTP sent successfully on +91 {mobileNumber}
        </Text>
        <Text style={CmnStyles.otpTimeText}>
          00:{otpTime > 9 ? otpTime : `0${otpTime}`}
        </Text>
        <Text style={TextStyles.black_S_20_700}>Enter OTP here</Text>
        <View style={CmnStyles.OtpInputPartBox}>
          <OtpInputPart setOtpCode={setOtpCode} />
        </View>
        <RedButton
          text={'Continue'}
          onPress={() => {
            verifyOtpHandler();
          }}
          otherStyle={CmnStyles.loginContinueBtn}
        />
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack
            text={'Login'}
            rightText={Platform.OS === 'ios' ? 'Skip' : undefined}
            onPressRightText={() => {
              guestHandler();
            }}
          />
          {/* <KeyboardAvoidingView
            behavior="position"
            keyboardVerticalOffset={Platform.OS == 'android' ? -40 : 130}> */}
          <View style={CmnStyles.loginCont}>
            <ScrollView
              automaticallyAdjustKeyboardInsets
              keyboardShouldPersistTaps="always"
              style={{flexGrow: 1}}>
              <View style={CmnStyles.loginInfoImgBox}>
                <Image
                  source={Images.login_info}
                  style={CmnStyles.loginInfoImg}
                />
              </View>
              <Text style={CmnStyles.loginScreenTitle}>
                Please Enter Your Mobile Number
              </Text>
              <AuthMobTextBox
                otpActive={otpActive}
                value={mobileNumber}
                onChangeText={text => {
                  setMobileNumber(text);
                }}
                setOtpActive={setOtpActive}
                onPressEdit={() => {
                  setOtpActive(false);
                }}
              />
              <RedButton
                text={otpActive ? 'Resend OTP' : 'Request OTP'}
                onPress={() => {
                  requestOtpHandler();
                  setOtpTime(30);
                }}
              />
              {otpActive && otpPart()}
            </ScrollView>
          </View>
          {/* </KeyboardAvoidingView> */}
        </View>
      </SafeAreaView>
    </View>
  );
};

export default LoginScreen;
