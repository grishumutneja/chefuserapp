import {View, Text, Image, ImageBackground} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';

const SplashScreen = () => {
  return (
    <View style={CmnStyles.bodyBg}>
      <ImageBackground
        source={Images.splash}
        style={CmnStyles.splashGifBG}
        resizeMode={'cover'}>
        <Image
          source={Images.splash}
          style={CmnStyles.splashGif}
          resizeMode={'contain'}
        />
      </ImageBackground>
    </View>
  );
};

export default SplashScreen;
