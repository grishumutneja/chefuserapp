import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  AppState,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import Geolocation from 'react-native-geolocation-service';
import CmnStyles from '../../Styles/CmnStyles';
import HomeHeader from '../../Components/Headers/HomeHeader';
import Slider from '../../Components/Slider/Slider';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';
import {RestrictRestaurantModal} from '../../Components/Modals/RestrictRestaurantModal';
import {useIsFocused, useNavigation, useRoute} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import {RestrictDiningModal} from '../../Components/Modals/RestrictDiningModal';
import {requestLocationPermission} from '../../Components/Permissions/LocationPermission';
import {useDispatch, useSelector} from 'react-redux';
import AppRightsCard from '../../Components/Cards/AppRightsCard';
import {
  checkUpdateAvailable,
  pendingOrderRatingApiCall,
} from '../../Core/Config/ApiCalls';
import OrderStatusCard from '../../Components/Cards/OrderStatusCard';
import RateHomeCard from '../../Components/Cards/RateHomeCard';
import DeviceInfo from 'react-native-device-info';
import {requestUserPermission} from '../../Core/fireBase/notificationService';
import {SelectDeliveryModal} from '../../Components/Modals/SelectDeliveryModal';
import RestrictUpdateModal from '../../Components/Modals/RestrictUpdateModal';

const HomeScreen = () => {
  const dispatch = useDispatch();
  const route = useRoute();
  const isFocused = useIsFocused();
  const homeBanner = useSelector(state => state?.homeBanners);
  const navigation = useNavigation();
  const [restrictModalVisible, setRestrictModalVisible] = useState(false);
  const [diningModal, setDiningModal] = useState(false);
  const [chefModal, setChefModal] = useState(false);
  const [restrictReason, setRestrictReason] = useState();
  const [orderStatusDataVisible, setOrderStatusDataVisible] = useState();
  const [orderStatusData, setOrderStatusData] = useState();
  const [rateCardVisible, setRateCardVisible] = useState(true);
  const [venderRattingData, setVenderRattingData] = useState();
  const [riderRattingData, setRiderRattingData] = useState();
  const [forceRestrictVisible, setForceRestrictVisible] = useState(false);
  const [isSoftUpdate, setIsSoftUpdate] = useState(false);
  const [newAvailableVersion, setNewAvailableVersion] = useState();
  const isFromRate = route?.params?.isFromRate;
  const {isBannerLoading, homeBannerData} = homeBanner;
  const {rateGivenCount} = useSelector(state => state.driverRate);
  const [statusLoading, setStatusLoading] = useState(false);
  const [selectDelModalVisible, setSelectDelModalVisible] = useState(false);
  const {locationData, locationPermissionAndroid} = useSelector(
    state => state.location,
  );
  const {isGuestAccount} = useSelector(state => state.userInfo);
  const [appStatus, setAppStatus] = useState();
  const appState = useRef(AppState.currentState);

  const versionCode = DeviceInfo.getVersion();

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        // console.log('App has come to the foreground!');
      }
      appState.current = nextAppState;
      if (appState.current == 'active') {
        setTimeout(() => {
          setAppStatus('active');
        }, 2000);
      } else if (
        appState.current == 'background' ||
        appState.current == 'inactive'
      ) {
        setTimeout(() => {
          setAppStatus('inactive');
        }, 2000);
      }
    });
    return () => {
      subscription.remove();
    };
  }, []);

  useEffect(() => {
    let body = {
      version: versionCode,
    };
    checkUpdateAvailable({body}).then(res => {
      setNewAvailableVersion(res?.current_version);
      if (res?.force_update == 1) {
        console.log('res?.force_update', res?.force_update);
        setForceRestrictVisible(true);
        setIsSoftUpdate(false);
      } else if (res?.user_app_soft_update == 1) {
        setForceRestrictVisible(true);
        setIsSoftUpdate(true);
      }
    });
  }, []);

  useEffect(() => {
    if (isGuestAccount) {
      setSelectDelModalVisible(false);
    } else {
      if (!locationData) {
        if (Platform.OS == 'ios') {
          Geolocation.requestAuthorization('always').then(res => {
            console.log('res', res);
            if (res == 'denied') {
              setSelectDelModalVisible(true);
            } else {
              setSelectDelModalVisible(false);
              requestLocationPermission(dispatch);
            }
          });
        } else {
          console.log('locationPermissionAndroid', locationPermissionAndroid);
          if (locationPermissionAndroid) {
            setSelectDelModalVisible(false);
          } else {
            setSelectDelModalVisible(true);
          }
        }
      }
    }
  }, [locationData, appStatus]);

  useEffect(() => {
    isFromRate && orderStatueApiHandler();
  }, [isFocused]);

  useEffect(() => {
    requestUserPermission();
    setTimeout(() => {
      requestLocationPermission(dispatch);
    }, 1000);
  }, []);

  useEffect(() => {
    orderStatueApiHandler();
  }, [rateGivenCount]);

  const orderStatueApiHandler = () => {
    setStatusLoading(true);
    pendingOrderRatingApiCall({body: null})
      .then(res => {
        const appRun = res?.app_run == '0' ? true : false;
        setRestrictModalVisible(appRun);
        setRestrictReason(res?.reason);
        if (res?.ongoing_orders?.length > 0) {
          setOrderStatusDataVisible(true);
          setOrderStatusData(res?.ongoing_orders);
        } else {
          setOrderStatusDataVisible(false);
        }
        if (res?.pending_driver_review == null) {
          if (res?.response == null) {
            setRateCardVisible(false);
            setRiderRattingData();
          } else {
            setRateCardVisible(true);
            setVenderRattingData(res?.response);
            setRiderRattingData();
          }
        } else {
          setRateCardVisible(true);
          setRiderRattingData(res?.pending_driver_review);
        }
        setTimeout(() => {
          setStatusLoading(false);
        }, 700);
      })
      .catch(err => {
        console.warn('err pendingOrderRatingApiCall', err);
        setStatusLoading(false);
      });
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg} key={rateGivenCount + statusLoading}>
          <HomeHeader />
          <ScrollView>
            {homeBannerData?.response?.length > 0 && (
              <Slider
                data={homeBannerData?.response}
                isLoading={isBannerLoading}
              />
            )}
            <View
              style={{
                paddingHorizontal: Responsive.widthPx(5),
                marginTop: Responsive.widthPx(-4),
              }}>
              <Text style={TextStyles.black_S_18_700}>What's your mood ?</Text>
              <HomeScreenCard
                text={'Restaurants'}
                imgSource={Images.restaurant_black}
                onPress={() => {
                  navigation.navigate(Screens.RestaurantHome);
                }}
                scale={0.65}
              />
              <HomeScreenCard
                text={'Home Chef'}
                imgSource={Images.chef1}
                onPress={() => {
                  // navigation.navigate(Screens.ChefHome);
                  setChefModal(true);
                }}
              />
              <HomeScreenCard
                text={'Eggs and Meat'}
                imgSource={Images.dining_black}
                onPress={() => {
                  setDiningModal(true);
                }}
                scale={0.6}
              />
            </View>
            {/* <Button
              mode="contained-tonal"
              // rippleColor={'yellow'}
              onPress={() => {
                console.log('pressed');
              }}>
              jk
            </Button> */}
            <AppRightsCard />
          </ScrollView>

          {!statusLoading && orderStatusDataVisible && (
            <>
              {orderStatusData?.length > 0 && (
                <OrderStatusCard
                  orderStatusData={orderStatusData}
                  setOrderStatusDataVisible={setOrderStatusDataVisible}
                  rateCardVisible={rateCardVisible}
                />
              )}
            </>
          )}
          {rateCardVisible && riderRattingData && !statusLoading && (
            <RateHomeCard
              text={`Rate ${
                riderRattingData
                  ? riderRattingData?.driver_name
                  : venderRattingData?.name
              }...`}
              setVisible={setRateCardVisible}
              onPress={() => {
                riderRattingData
                  ? navigation.navigate(Screens.DriverRatting, {
                      orderId: riderRattingData?.id,
                    })
                  : navigation.navigate(Screens.VendorRatting, {
                      orderId: venderRattingData?.id,
                    });
              }}
            />
          )}
        </View>
        <SelectDeliveryModal
          modalVisible={selectDelModalVisible}
          onPressCancel={() => {}}
          onPressAddAddress={() => {
            setSelectDelModalVisible(false);
            navigation.navigate(Screens.Maps);
          }}
          isLocationIcon={true}
          setSelectDelModalVisible={setSelectDelModalVisible}
        />
        <RestrictDiningModal
          diningModalVisible={diningModal}
          onPressCancel={() => {
            setDiningModal(false);
            setChefModal(false);
          }}
          chefModalVisible={chefModal}
          setDiningModal={setDiningModal}
          setChefModal={setChefModal}
        />
      </SafeAreaView>
      <RestrictUpdateModal
        modalVisible={forceRestrictVisible}
        softUpdate={isSoftUpdate ? '1' : '0'}
        setModalVisible={setForceRestrictVisible}
        newAvailableVersion={newAvailableVersion}
      />
      {restrictModalVisible && (
        <RestrictRestaurantModal
          modalVisible={restrictModalVisible}
          text={restrictReason}
        />
      )}
    </View>
  );
};

export default HomeScreen;

const HomeScreenCard = ({text, imgSource, onPress, scale}) => {
  return (
    <TouchableOpacity style={CmnStyles.homeScreenCard} onPress={onPress}>
      <Text style={TextStyles.black_S_20_700}>{text}</Text>
      <Image
        source={imgSource}
        style={[
          CmnStyles.homeScreenCardImg,
          scale && {transform: [{scale: scale}]},
        ]}
        resizeMode="contain"
      />
    </TouchableOpacity>
  );
};
