import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  ScrollView,
  RefreshControl,
} from 'react-native';
import React, {useCallback, useState} from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import RechargeWalletCard from '../../Components/Cards/RechargeWalletCard';
import RechargeWalletModal from '../../Components/Modals/RechargeWalletModal';
import Responsive from '../../Constants/Responsive';
import TransactionCard from '../../Components/Cards/TransactionCard';
import ReferCardEarnCard from '../../Components/Cards/ReferCardEarnCard';
import {useDispatch, useSelector} from 'react-redux';
import NoDataAvailable from '../../Components/Inputs/NoDataAvailable';
import {PaymentWaitModal} from '../../Components/Modals/PaymentWaitModal';
import {userAllTransactionApiCall} from '../../Core/Config/ApiCalls';

const WalletScreen = () => {
  const dispatch = useDispatch();
  const [rechargeModalVisible, setRechargeModalVisible] = useState(false);
  const [paymentWaitVisible, setPaymentWaitVisible] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const {walletTransactionData, walletData, refMessage, noTransactions} =
    useSelector(state => state.wallet);
  const {getUserInfoData} = useSelector(state => state.userInfo);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
      if (getUserInfoData?.id) {
        userAllTransactionApiCall({userId: getUserInfoData?.id, dispatch});
      }
    }, 2000);
  }, []);

  const recentTransaction = () => {
    return (
      <View
        style={{
          padding: Responsive.widthPx(3),
          height: Responsive.widthPx(90),
        }}>
        <Text style={{...TextStyles.darkGray_S_18_700}}>
          Recent Transactions
        </Text>
        <View style={CmnStyles.recentTransHeadTextBox}>
          <Text style={CmnStyles.recentTransHeadText}>Type</Text>
          <Text style={CmnStyles.recentTransHeadText}>Date</Text>
          <Text style={CmnStyles.recentTransHeadText}>Amount</Text>
        </View>
        {noTransactions ? (
          <NoDataAvailable text={'No transactions found'} />
        ) : (
          <FlatList
            nestedScrollEnabled={true}
            data={walletTransactionData}
            renderItem={({item, index}) => (
              <TransactionCard item={item} index={index} />
            )}
            key={(item, index) => {
              'walletTransactionData' + index;
            }}
          />
        )}
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack text={'Wallet'} backVisible={true} />
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            nestedScrollEnabled={true}>
            <View style={{height: Responsive.heightPx(92) - 64}}>
              <RechargeWalletCard
                setRechargeModalVisible={setRechargeModalVisible}
                walletData={walletData}
              />
              {recentTransaction()}
              <ReferCardEarnCard
                walletData={walletData}
                refMessage={refMessage}
              />
            </View>
          </ScrollView>
          <RechargeWalletModal
            visible={rechargeModalVisible}
            onPressCancel={() => {
              setRechargeModalVisible(false);
            }}
            setRechargeModalVisible={setRechargeModalVisible}
            setPaymentWaitVisible={setPaymentWaitVisible}
          />
          <PaymentWaitModal
            modalVisible={paymentWaitVisible}
            onPressCancel={() => {
              setPaymentWaitVisible(false);
            }}
          />
        </View>
      </SafeAreaView>
    </View>
  );
};

export default WalletScreen;
