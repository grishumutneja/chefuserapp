import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import React, {useCallback, useEffect, useState} from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import OrderHistoryCard from '../../Components/Cards/OrderHistoryCard';
import {orderHistoryApiCall} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';
import Colors from '../../Constants/Colors';
import NoDataAvailable from '../../Components/Inputs/NoDataAvailable';
import TrackSkelton from '../../Components/Loader/TrackSkelton';

const OrderHistoryScreen = () => {
  const dispatch = useDispatch();
  const [pageLoading, setPageLoading] = useState(false);
  const [pageOffset, setPageOffset] = useState(0);
  const [isRefreshing, setIsRefreshing] = useState();
  const {orderHistoryData, isHistoryLimitComplete, noDataAvailable} =
    useSelector(state => state.orderHistory);

  useEffect(() => {
    orderHistoryApiCallHandler();
  }, []);

  useEffect(() => {
    isHistoryLimitComplete && setPageLoading(false);
  }, [isHistoryLimitComplete]);

  const onRefresh = useCallback(() => {
    setIsRefreshing(true);
    setTimeout(() => {
      setIsRefreshing(false);
      const body = {
        order_for: 'restaurant',
        offset: pageOffset,
      };
      orderHistoryApiCall({dispatch, body: body});
    }, 2000);
  }, []);

  const orderHistoryApiCallHandler = () => {
    const body = {
      order_for: 'restaurant',
      offset: pageOffset,
    };
    orderHistoryApiCall({dispatch, body: body});
  };

  const restToExpApiCallHandler = offset => {
    let body = {
      order_for: 'restaurant',
      offset: offset,
    };
    orderHistoryApiCall({dispatch: dispatch, body: body});
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const footerRender = () => {
    return (
      <>
        {isHistoryLimitComplete && (
          <Text style={CmnStyles.noMoreResText2}>
            No more record to explore
          </Text>
        )}
        {pageLoading && (
          <ActivityIndicator
            size={'small'}
            style={CmnStyles.paginationLoader2}
            color={Colors.red}
          />
        )}
      </>
    );
  };

  const statusKey = orderHistoryData && orderHistoryData[0]?.order_status;

  return (
    <View style={CmnStyles.bodyBg} key={statusKey}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack text={'Order History'} backVisible={true} />
          <View style={{flex: 1}}>
            {!noDataAvailable && !orderHistoryData ? (
              <TrackSkelton />
            ) : noDataAvailable ? (
              <NoDataAvailable text={'No orders found'} />
            ) : (
              <FlatList
                data={orderHistoryData}
                extraData={orderHistoryData}
                renderItem={({item, index}) => {
                  return <OrderHistoryCard item={item} index={index} />;
                }}
                refreshControl={
                  <RefreshControl
                    refreshing={isRefreshing}
                    onRefresh={onRefresh}
                  />
                }
                onScroll={({nativeEvent}) => {
                  if (isCloseToBottom(nativeEvent)) {
                    if (!isHistoryLimitComplete) {
                      if (!pageLoading) {
                        setPageOffset(pageOffset + 10);
                        setPageLoading(true);
                        restToExpApiCallHandler(pageOffset + 10);
                      }
                    }
                  } else {
                    setPageLoading(false);
                  }
                }}
                ListFooterComponent={() => {
                  return <>{footerRender()}</>;
                }}
              />
            )}
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
};

export default OrderHistoryScreen;
