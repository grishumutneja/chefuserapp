import {View, SafeAreaView, Platform, ScrollView} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import ProFileCard from '../../Components/Cards/ProFileCard';
import TextCard from '../../Components/Cards/TextCard';
import FollowOnCard from '../../Components/Cards/FollowOnCard';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import Open from '../../Constants/Open';
import MyData from '../../Constants/MyData';

const ProfileBottomScreen = () => {
  const navigation = useNavigation();

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <HeaderWithBack text={'Your Profile'} backVisible={true} />
        <ScrollView>
          <ProFileCard />
          <TextCard
            text={'Favorites'}
            onPress={() => {
              navigation.navigate(Screens.Favorites);
            }}
          />
          <TextCard
            text={'Saved Address'}
            onPress={() => {
              navigation.navigate(Screens.SavedAddress);
            }}
          />
          <TextCard
            text={'Wallet / Refer & Earn'}
            onPress={() => {
              navigation.navigate(Screens.Wallet);
            }}
          />
          <TextCard
            text={'Feedback'}
            onPress={() => {
              navigation.navigate(Screens.FeedBack);
            }}
          />
          <TextCard
            text={'Chat with us'}
            onPress={() => {
              Open.whatsapp(MyData.clientMoNumber);
            }}
          />
          <TextCard
            text={'About ChefLab'}
            onPress={() => {
              navigation.navigate(Screens.About);
            }}
          />
          <TextCard
            text={
              Platform.OS === 'android'
                ? 'Rate us on Play Store / App Store'
                : 'Rate us on App Store'
            }
            onPress={() => {
              Platform.OS === 'android'
                ? Open.anyUrl(MyData.playStoreUrl)
                : Open.anyUrl(MyData.appleUrl);
            }}
          />
          <FollowOnCard />
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default ProfileBottomScreen;
