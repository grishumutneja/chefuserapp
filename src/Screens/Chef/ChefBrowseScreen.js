import {View, SafeAreaView, FlatList} from 'react-native';
import React, {useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import SearchBox from '../../Components/Inputs/SearchBox';
import TitleRestaurant from '../../Components/Inputs/TitleRestaurant';
import Responsive from '../../Constants/Responsive';
import BrowseDishesCard from '../../Components/Cards/BrowseDishesCard';
import {FilterChefModal} from '../../Components/Modals/FilterChefModal';

const ChefBrowseScreen = () => {
  const [filterModal, setFilterModal] = useState(false);

  const renderBrowseDishes = () => {
    return (
      <View>
        <TitleRestaurant
          titleBig={'Browse by Cuisines'}
          filterVisible={true}
          onPressFilter={() => {
            setFilterModal(true);
          }}
        />
        <FlatList
          data={[1, 2, 3, 4, 6, 7]}
          renderItem={({item, index}) => {
            return <BrowseDishesCard item={item} index={index} />;
          }}
          contentContainerStyle={{
            marginHorizontal: Responsive.widthPx(2),
            paddingBottom: Responsive.widthPx(45),
          }}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack text={'South Indian'} backVisible={true} />
          <SearchBox placeholder="Search by Chef or Dish..." />
          {renderBrowseDishes()}
        </View>
        <FilterChefModal
          modalVisible={filterModal}
          onPressCancel={() => {
            setFilterModal(false);
          }}
        />
      </SafeAreaView>
    </View>
  );
};

export default ChefBrowseScreen;
