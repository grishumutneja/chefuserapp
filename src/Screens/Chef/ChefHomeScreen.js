import {View, SafeAreaView, FlatList, ScrollView} from 'react-native';
import React, {useEffect, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import HomeHeader from '../../Components/Headers/HomeHeader';
import Responsive from '../../Constants/Responsive';
import SearchBox from '../../Components/Inputs/SearchBox';
import TitleRestaurant from '../../Components/Inputs/TitleRestaurant';
import CravingCard from '../../Components/Cards/CravingCard';
import FoodiesFavCard from '../../Components/Cards/FoodiesFavCard';
import HomeDishCard from '../../Components/Cards/HomeDishCard';
import RestaurantCard from '../../Components/Cards/RestaurantCard';
import Slider from '../../Components/Slider/Slider';
import ChefCardBox from '../../Components/Cards/ChefCardBox';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import HomeSkelton from '../../Components/Loader/HomeSkelton';

const imgURL =
  'https://d4t7t8y8xqo0t.cloudfront.net/resized/750X436/eazytrendz%2F2975%2Ftrend20201030124515.jpg';

const imgURL2 =
  'https://c.ndtvimg.com/2020-10/p0n323j_delhi-restaurants-generic-650-_625x300_08_October_20.jpg?im=FeatureCrop,algorithm=dnn,width=620,height=350';

const imgURL3 =
  'https://lh3.googleusercontent.com/WTktXbhs0QHsZHCVZ8a8VSXiu67zTKvWeLvpZN1sGEce0igA2zmXBUULchAvu1ViEl4TfrEK9VGpYyT9VWLkYhleqP8=h450-rw';
const data = [
  {
    banner_image: imgURL,
  },
  {banner_image: imgURL2},
  {banner_image: imgURL3},
];

const ChefHomeScreen = () => {
  const [initialHide, setInitialHide] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setInitialHide(false);
    }, 100);
  }, []);

  const renderCuisines = () => {
    return (
      <View>
        <TitleRestaurant titleBig={'Browse by Cuisines'} />
        <FlatList
          horizontal
          data={[1, 2, 3, 4, 6, 7]}
          renderItem={({item, index}) => {
            return <CravingCard item={item} index={index} />;
          }}
          contentContainerStyle={{marginHorizontal: Responsive.widthPx(4)}}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  const renderWhatsInMind = () => {
    return (
      <View>
        <TitleRestaurant titleBig={`What's in your mind ?`} />
        <FlatList
          horizontal
          data={[1, 2, 3, 4, 6, 7]}
          renderItem={({item, index}) => {
            return <CravingCard item={item} index={index} />;
          }}
          contentContainerStyle={{marginHorizontal: Responsive.widthPx(4)}}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  const renderTryOnceMore = () => {
    return (
      <View>
        <TitleRestaurant titleBig={`Try once more !`} />
        <ChefCardBox data={[0, 1, 2, 3]} />
      </View>
    );
  };

  const renderTopRatedChef = () => {
    return (
      <View style={{marginTop: Responsive.widthPx(-4)}}>
        <TitleRestaurant titleBig={`Top Rated Chef`} viewAllVisible={true} />
        <ChefCardBox data={[0, 1, 2, 3]} specialVisible={true} />
      </View>
    );
  };

  const renderFeaturedChef = () => {
    return (
      <View style={{marginTop: Responsive.widthPx(-4)}}>
        <TitleRestaurant
          titleBig={`Featured Chef`}
          viewAllVisible={true}
          titleSmall={'Sponsored'}
        />
        <ChefCardBox data={[0, 1, 2, 3]} specialVisible={true} />
      </View>
    );
  };

  const renderFoodiesFav = () => {
    return (
      <View>
        <TitleRestaurant
          titleBig={`Foody's Favorite`}
          titleSmall={'Top Rated'}
          otherStyle={{marginBottom: Responsive.widthPx(1)}}
        />
        <FlatList
          horizontal
          data={[1, 2, 3, 4, 6, 7]}
          renderItem={({item, index}) => {
            return <FoodiesFavCard item={item} index={index} />;
          }}
          contentContainerStyle={{
            marginHorizontal: Responsive.widthPx(2),
          }}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  const renderTopDishes = () => {
    return (
      <View>
        <TitleRestaurant
          titleBig={`Top Dishes`}
          titleSmall={'Top Rated'}
          otherStyle={{marginBottom: Responsive.widthPx(1)}}
          viewAllVisible={true}
        />
        <FlatList
          horizontal
          data={[1, 2, 3, 4, 6, 7]}
          renderItem={({item, index}) => {
            return <HomeDishCard item={item} index={index} />;
          }}
          contentContainerStyle={{
            marginHorizontal: Responsive.widthPx(2),
          }}
          keyExtractor={(item, index) => `key-HomeDishCard${index}`}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  const renderChefLabSpecial = () => {
    const data = [1, 2, 3, 4, 5, 6];
    return (
      <View>
        <TitleRestaurant
          titleBig={`ChefLab Special`}
          titleSmall={'Official'}
          otherStyle={{
            marginBottom: Responsive.widthPx(1),
          }}
        />
        <View style={CmnStyles.chefMapStyle}>
          {data.map(({item, index}) => {
            return (
              <View key={'ChefLab INdex' + index}>
                <HomeDishCard item={item} index={index} />
              </View>
            );
          })}
        </View>
      </View>
    );
  };

  const renderFeaturedDishes = () => {
    const daataa = [1, 2, 3, 4, 6, 7];
    return (
      <View>
        <TitleRestaurant
          titleBig={`Featured Dishes`}
          titleSmall={'Sponsored'}
          otherStyle={{marginBottom: Responsive.widthPx(1)}}
          viewAllVisible={true}
        />
        {/* <FlatList
          horizontal
          data={daataa}
          renderItem={({item, index}) => {
            return <HomeDishCard item={item} index={index} />;
          }}
          keyExtractor={(item, index) => 'HomeDishCard' + index}
          contentContainerStyle={{
            marginHorizontal: Responsive.widthPx(2),
          }}
          showsHorizontalScrollIndicator={false}
        /> */}
        <ScrollView horizontal={true} nestedScrollEnabled={true}>
          {daataa?.map((item, index) => {
            return (
              <View key={'HomeDishCard' + index}>
                <HomeDishCard item={item} index={index} />
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  };

  const renderRestaurantExplore = () => {
    return (
      <View>
        <TitleRestaurant
          titleBig={`Restaurants Explore`}
          otherStyle={{marginBottom: Responsive.widthPx(1)}}
        />
        <FlatList
          data={[1, 2, 3, 4, 6, 7]}
          renderItem={({item, index}) => {
            return <RestaurantCard item={item} index={index} />;
          }}
          contentContainerStyle={{
            marginHorizontal: Responsive.widthPx(2),
          }}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        {/* <View style={CmnStyles.bodyBg}> */}
        <HomeHeader backVisible={true} />
        <View
          style={{
            paddingBottom: Responsive.widthPx(11),
            marginTop: Responsive.widthPx(2),
          }}>
          <ScrollView style={{flexGrow: 1}} nestedScrollEnabled={true}>
            <View>
              <SearchBox placeholder="Search by Chef or Dish..." />
              {/* <Slider
                  data={data}
                  otherStyle={{
                    marginTop: Responsive.widthPx(3),
                    marginBottom: Responsive.widthPx(-14),
                  }}
                /> */}
              {/* {renderCuisines()} */}
              {/* {renderWhatsInMind()} */}
              {!initialHide ? (
                <>
                  {renderTryOnceMore()}
                  {renderTopRatedChef()}
                  {renderFeaturedChef()}
                  {renderFeaturedDishes()}
                  {renderTopDishes()}
                  {renderChefLabSpecial()}
                </>
              ) : (
                <HomeSkelton />
              )}
            </View>
          </ScrollView>
        </View>
        {/* </View> */}
      </SafeAreaView>
    </View>
  );
};

export default ChefHomeScreen;
