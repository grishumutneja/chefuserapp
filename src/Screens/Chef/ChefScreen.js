import {
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import React from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import Responsive from '../../Constants/Responsive';
import SpStyles from '../../Styles/SpStyles';
import Images from '../../Constants/Images';
import FavButton from '../../Components/Buttons/FavButton';
import DishesCard from '../../Components/Cards/DishesCard';
import TitleRestaurant from '../../Components/Inputs/TitleRestaurant';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';

const chefUri =
  'https://nationaltoday.com/wp-content/uploads/2021/07/shutterstock_1518533924-min.jpg';

const ChefScreen = () => {
  const navigation = useNavigation();

  const renderProfile = () => {
    return (
      <>
        <View style={CmnStyles.chefScreenProCont}>
          <View style={CmnStyles.chefScreenProImg}>
            <Image
              source={{uri: chefUri}}
              style={CmnStyles.chefScreenProImg}
              resizeMode="cover"
            />
            <Image
              source={Images.rest_pure_veg}
              style={CmnStyles.browseDishPureVeg}
              resizeMode="contain"
            />
          </View>
          <View style={CmnStyles.chefScreenCont}>
            <Text style={TextStyles.black_S_16_700}>Chef Ankur Bajaj</Text>
            <Text
              style={[
                TextStyles.darkGray_S_12_500,
                {marginTop: Responsive.widthPx(0.7)},
              ]}>
              Age - 31 Yrs, Cooking Exp - 2 Yrs
            </Text>
            <View
              style={[SpStyles.FDR_ALC, {marginTop: Responsive.widthPx(2)}]}>
              <Image
                source={Images.star}
                style={CmnStyles.chefCardStarImg}
                resizeMode="contain"
              />
              <Text style={TextStyles.black_S_12_700}>4.5</Text>
              <TouchableOpacity style={{marginLeft: 6}}>
                <Text style={TextStyles.blue_S_11_400}>(15 Reviews)</Text>
              </TouchableOpacity>
            </View>
            <View
              style={[
                CmnStyles.chefCardBottomTextCont,
                {marginRight: Responsive.widthPx(6)},
              ]}>
              <Text style={TextStyles.darkGray_S_12_500}>
                Orders Served - 100
              </Text>
              <View style={SpStyles.FDR_ALC}>
                <Image
                  source={Images.scooter}
                  style={CmnStyles.chefCardScooterImg}
                  resizeMode="contain"
                />
                <Text style={TextStyles.darkGray_S_12_500}>2.5KM</Text>
              </View>
            </View>
            <TouchableOpacity
              style={{marginTop: Responsive.widthPx(1)}}
              onPress={() => {
                navigation.navigate(Screens.ChefProfile);
              }}>
              <Text
                style={[
                  TextStyles.red_12_700,
                  {textDecorationLine: 'underline'},
                ]}>
                View Full Profile
              </Text>
            </TouchableOpacity>
            <FavButton />
          </View>
          <View
            style={[
              CmnStyles.browseDishesCardBottomCont,
              {left: Responsive.widthPx(2), bottom: Responsive.widthPx(-4)},
            ]}>
            <Text style={TextStyles.white_S_12_700}>
              Speciality - North Indian
            </Text>
          </View>
        </View>
      </>
    );
  };

  const renderDishes = () => {
    return (
      <View style={{marginTop: Responsive.widthPx(6)}}>
        <TitleRestaurant titleBig={'Must Try !'} />
        <FlatList
          data={[1, 2, 3, 4, 5]}
          renderItem={({item, index}) => {
            return <DishesCard item={item} index={index} />;
          }}
        />
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack
            text={'Chef Ankur Bajaj'}
            backVisible={true}
            cartVisible={true}
          />
          <ScrollView>
            {renderProfile()}
            {renderDishes()}
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
};

export default ChefScreen;
