import {View, Text, SafeAreaView, Image, ScrollView} from 'react-native';
import React from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import ChefProfileCard from '../../Components/Cards/ChefProfileCard';
import TitleRestaurant from '../../Components/Inputs/TitleRestaurant';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';

const videoData = [1, 2, 3, 4, 5, 6];
const imgUri =
  'https://imgsrv2.voi.id/bLlWcWzk8Ernu8fdHHlBPZfcpIgi5JFuNzEGQIjHVL4/auto/1200/675/sm/1/bG9jYWw6Ly8vcHVibGlzaGVycy8yNDE0MDEvMjAyMzAxMDMxNTU5LW1haW4uanBlZw.jpg';

const ChefProfileScreen = () => {
  const renderVideo = () => {
    return (
      <View>
        <TitleRestaurant titleBig={`Videos`} />
        <View style={CmnStyles.chefProVideoCont}>
          {videoData.map(({item, index}) => {
            return (
              <View style={CmnStyles.chefProVideoBox}>
                <Image
                  source={{uri: imgUri}}
                  style={CmnStyles.chefProVideoThumb}
                  resizeMode="cover"
                />
                <Text
                  style={[
                    TextStyles.black_S_16_700,
                    {marginTop: Responsive.widthPx(1.2)},
                  ]}>
                  Dal Bati Churma
                </Text>
                <Text
                  style={[
                    TextStyles.darkGray_S_14_500,
                    {marginTop: Responsive.widthPx(0.5)},
                  ]}>
                  North Indian
                </Text>
              </View>
            );
          })}
        </View>
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack
            text={`Chef's Profile`}
            backVisible={true}
            cartVisible={true}
          />
          <ScrollView>
            <ChefProfileCard />
            {renderVideo()}
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
};

export default ChefProfileScreen;
