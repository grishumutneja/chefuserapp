import {View, SafeAreaView, Image, Text, ScrollView} from 'react-native';
import React, {useState} from 'react';
import HeaderWithBack from '../../Components/Headers/HeaderWithBack';
import CmnStyles from '../../Styles/CmnStyles';
import FavButton from '../../Components/Buttons/FavButton';
import Responsive from '../../Constants/Responsive';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import StarDisplay from '../../Components/Star/StarDisplay';
import TextStyles from '../../Styles/TextStyles';
import RedBorderButton from '../../Components/Buttons/RedBorderButton';
import OnePlusItemBox from '../../Components/Buttons/OnePlusItemBox';

const img =
  'https://d4t7t8y8xqo0t.cloudfront.net/resized/750X436/eazytrendz%2F3070%2Ftrend20210218124824.jpg';

const DishScreen = () => {
  const [count, setCount] = useState();

  const addPart = () => {
    return (
      <View style={CmnStyles.homeDetailDishAddCont}>
        <Text style={{...TextStyles.blue_S_9_400}}>Customizable</Text>
        {!count > 0 ? (
          <RedBorderButton
            text={'Add'}
            otherStyle={CmnStyles.homeDishCardAddBtn}
            otherTextStyle={TextStyles.red_16_700}
          />
        ) : (
          <OnePlusItemBox otherStyle={CmnStyles.onePlusItemBoxCont2} />
        )}
      </View>
    );
  };

  const renderDish = () => {
    return (
      <View style={CmnStyles.dishScreenDishCont}>
        <Image
          source={{uri: img}}
          style={CmnStyles.dishScreenDishImg}
          resizeMode="cover"
        />
        <FavButton
          otherStyle={CmnStyles.dishScreenFav}
          imgOtherStyle={CmnStyles.dishScreenFav}
        />
      </View>
    );
  };

  const renderDishDescription = () => {
    return (
      <View style={{padding: Responsive.widthPx(4)}}>
        <View style={CmnStyles.dishScreenMainTitleCont}>
          <Text style={TextStyles.black_S_20_700}>
            Jaini Food Jaini Food Jaini
          </Text>
          <Image
            source={Images.pure_veg}
            style={CmnStyles.pure_vegDishScreen}
            resizeMode="contain"
          />
        </View>
        <Text style={CmnStyles.dishScreenDishText}>
          Jaini Food Jaini Food Jaini Food Jaini Jaini
        </Text>
        <View
          style={[SpStyles.FDR_ALC_JCS, {marginTop: Responsive.widthPx(1.6)}]}>
          <View>
            <StarDisplay
              scale={12}
              otherStyle={{
                marginBottom: Responsive.widthPx(1.3),
              }}
            />
            <Text style={TextStyles.black_S_18_700}>₹ 255.00</Text>
          </View>
          {addPart()}
        </View>
        <View style={{marginTop: Responsive.widthPx(6)}}>
          <Text style={CmnStyles.dishScreenDescTitle}>Description</Text>
          <Text style={TextStyles.black_S_14_400}>
            Dal baati in appe pan with detailed photo and video recipe. A
            traditional rajasthani delicacy recipe made from dal, bati or wheat
            rolls and churma which is powdered wheat ball. it is generally
            served for lunch or dinner by mixing the dal with crushed baati and
            ghee topped on it. it is also popular in uttar pradesh and madhya
            pradesh within the malwa regions.
          </Text>
        </View>
      </View>
    );
  };

  return (
    <View style={CmnStyles.bodyBg}>
      <SafeAreaView style={CmnStyles.bodyBg}>
        <View style={CmnStyles.bodyBg}>
          <HeaderWithBack
            text={'Dal Bati Churma'}
            backVisible={true}
            cartVisible={true}
          />
          <ScrollView>
            {renderDish()}
            {renderDishDescription()}
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
};

export default DishScreen;
