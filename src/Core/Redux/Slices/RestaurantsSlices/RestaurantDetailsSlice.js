import {createSlice} from '@reduxjs/toolkit';
import {
  addToCartApiCall,
  removeEmptyCartApiCall,
} from '../../../Config/ApiCalls';
import {addFavDishesApiCall} from '../../../Config/ApiCalls';

const RestaurantDetailsSlice = createSlice({
  name: 'restaurantDetails',
  initialState: {
    isLoading: false,
    restaurantDetailsData: null,
    dishesData: null,
    nonVegData: [],
    vegData: [],
    totalAmount: 0,
    totalItem: 0,
    itemsInCart: [],
    cart_id: null,
    vendor_id: null,
    resDishSearchData: null,
    isOnline: null,
    clearResItem: [],
  },
  reducers: {
    getRestaurantDetails(state, actions) {
      state.restaurantDetailsData = actions.payload;
      state.isOnline = actions.payload?.is_online;
      state.dishesData = actions.payload?.products;
      if (actions.payload !== undefined) {
        state.isLoading = false;
      } else {
        state.isLoading = true;
      }
    },
    getFilterApply(state, actions) {
      state.dishesData = actions.payload;
    },
    updateFavDishInResDetail(state, actions) {
      const menuIndex = actions.payload.menuIndex;
      state.dishesData[menuIndex]?.products?.map((item, index) => {
        if (item.product_id == actions.payload.product_id) {
          state.dishesData[menuIndex].products[index].is_like =
            actions.payload.is_like;
        }
      });
      const body = {
        user_id: actions.payload.user_id,
        product_id: actions.payload.product_id,
      };

      addFavDishesApiCall({
        body: body,
        dispatch: actions?.payload?.dispatch,
        lat: actions?.payload?.lat,
        lng: actions?.payload?.lng,
      });
    },
    addItemToCartRest(state, actions) {
      console.log('addItemToCartRest');
      let ind = actions.payload?.menuIndex;
      const item = actions.payload?.products;
      const dishInd = actions.payload?.dishIndex;
      const myMenuData = state.dishesData[ind];
      var tempTransItem = Object.assign({});
      let cartItem = Object.assign({});
      let isExist = false;

      myMenuData?.products?.map((itm, index) => {
        if (itm?.product_id == actions?.payload?.products?.product_id) {
          state.dishesData[ind].products[index].cart_qty =
            parseInt(state.dishesData[ind].products[index].cart_qty) + 1;
          itm.product_qty = state.dishesData[ind].products[index].cart_qty;
          tempTransItem.product_qty =
            state.dishesData[ind].products[index].cart_qty;
          tempTransItem.product_id = itm.product_id;
          cartItem.product_qty = tempTransItem.product_qty;
          cartItem.product_id = tempTransItem.product_id;
          cartItem.product_price = parseFloat(actions?.payload?.productPrice);
        }
      });
      console.log(
        'state.itemsInCart at map',
        JSON.stringify(state.itemsInCart),
      );

      state.itemsInCart?.map((itm, indx) => {
        console.log('itm?.product_id', itm?.product_id);
        console.log(
          'actions?.payload?.products?.product_id',
          actions?.payload?.products?.product_id,
        );
        if (itm?.product_id == actions?.payload?.products?.product_id) {
          console.log('indx', indx);
          console.log('isExist', isExist);
          console.log(
            'state.itemsInCart[indx].product_qty',
            state.itemsInCart[indx].product_qty,
          );
          console.log('state.itemsInCart[indx]', state.itemsInCart[indx]);
          if (state.itemsInCart[indx].product_qty >= 0) {
            if (state.itemsInCart[indx].product_qty == 0) {
              state.totalItem = state.totalItem + 1;
            }
            isExist = true;
          } else {
            isExist = false;
          }
          state.itemsInCart[indx].product_qty =
            parseFloat(itm?.product_qty) + 1;
          console.log(
            'state.itemsInCart[indx].product_qty',
            state.itemsInCart[indx].product_qty,
          );
        }
      });
      state.cart_id = actions.payload?.cart_id;
      state.vendor_id = actions.payload?.vendor_id;
      if (actions.payload.addOptions) {
        tempTransItem.variants = [actions.payload.addOptions];
        cartItem.variants = [actions.payload.addOptions];
      }
      if (actions.payload.addAddons) {
        tempTransItem.addons = actions.payload.addAddons;
        cartItem.addons = actions.payload.addAddons;
        cartItem.addonPrice = actions.payload.addonPrice;
      }
      console.log('isExist', isExist);
      if (!isExist) {
        state.totalItem = state.totalItem + 1;
        state.itemsInCart = [...state.itemsInCart, cartItem];
      }
      let price = 0;
      console.log('state.itemsInCart ====>', state.itemsInCart);
      state.itemsInCart?.map((itemMy, index) => {
        let addPrice = itemMy?.addonPrice ? itemMy?.addonPrice : 0;
        let productPrice = itemMy?.variants
          ? itemMy?.product_price
          : itemMy?.product_price;
        console.log('price before', price);
        console.log('productPrice', productPrice);
        console.log('itemMy?.product_qty', itemMy?.product_qty);
        console.log('addPrice', addPrice);
        price =
          price + productPrice * parseFloat(itemMy?.product_qty) + addPrice;
        console.log('price After', price);
      });

      console.log('price', price);

      state.totalAmount = price;
      console.log('state.totalAmount', state.totalAmount);
      const body = {
        cart_id: actions.payload?.cart_id,
        user_id: actions.payload?.user_id,
        vendor_id: actions.payload?.vendor_id,
        products: [tempTransItem],
      };
      const dispatch = actions.payload?.dispatch;
      const rest = false;
      // updateCartApiCall({body, dispatch, rest});
    },
    minusItemToCartRest(state, actions) {
      console.log('minusItemToCartRest');
      const ind = actions.payload?.menuIndex;
      const myMenuData = state.dishesData[ind];
      let passItem;
      let cartItem = Object.assign({});

      myMenuData?.products?.map((item, index) => {
        if (item?.product_id == actions?.payload?.products?.product_id) {
          state.dishesData[ind].products[index].cart_qty =
            parseInt(state.dishesData[ind].products[index].cart_qty) - 1;
          item.product_qty = state.dishesData[ind].products[index].cart_qty;
          cartItem.product_qty = state.dishesData[ind].products[index].cart_qty;
          cartItem.product_id = actions?.payload?.products?.product_id;
          cartItem.product_price = parseFloat(actions?.payload?.productPrice);
          state.itemsInCart?.map((itm, indx) => {
            if (itm?.product_id == actions?.payload?.products?.product_id) {
              state.itemsInCart[indx].product_qty =
                parseFloat(itm?.product_qty) - 1;
            }
          });
          if (cartItem.product_qty == 0) {
            state.totalItem = state.totalItem - 1;
          }
          if (item?.variants) {
            item.variants[0].variant_qty = item.product_qty;
            cartItem.variants = [actions.payload.addOptions];
          }
          if (item?.addons) {
            item.addons?.map((it, indI) => {
              item.addons[indI].addon_id = it.id;
              item.addons[indI].addon_qty = 0;
            });
            cartItem.addons = actions.payload.addAddons;
          }

          passItem = item;
        }
      });
      state.cart_id = actions.payload?.cart_id;
      state.vendor_id = actions.payload?.vendor_id;
      let price = 0;
      state.itemsInCart?.map((itemMy, index) => {
        if (itemMy?.product_qty > 0) {
          let addPrice = itemMy?.addonPrice ? itemMy?.addonPrice : 0;
          let productPrice = itemMy?.variants
            ? itemMy?.product_price
            : itemMy?.product_price;
          price =
            price + productPrice * parseFloat(itemMy?.product_qty) + addPrice;
        }
      });
      state.totalAmount = price;
      const body = {
        cart_id: actions.payload?.cart_id,
        user_id: actions.payload?.user_id,
        vendor_id: actions.payload?.vendor_id,
        products: [passItem],
      };
      const dispatch = actions.payload?.dispatch;
      const empty = true;
      const removeBody = {
        user_id: actions.payload?.user_id,
      };
      state.totalItem == 0 &&
        removeEmptyCartApiCall({body: removeBody, dispatch});
    },
    addNewItemToCartRest(state, actions) {
      console.log('addNewItemToCartRest');

      const ind = actions.payload?.menuIndex;
      const myMenuData = state.dishesData[ind];
      let passItem;
      var tempTransItem = Object.assign({});
      let cartItem = Object.assign({});
      myMenuData?.products?.map((item, index) => {
        const vIndex = actions?.payload?.selectedVariantIndex
          ? actions?.payload?.selectedVariantIndex
          : 0;
        if (item?.product_id == actions?.payload?.products?.product_id) {
          state.dishesData[ind].products[index].cart_qty =
            parseInt(state.dishesData[ind].products[index].cart_qty) + 1;
          item.product_qty = state.dishesData[ind].products[index].cart_qty;
          item.cartItem = state.dishesData[ind].products[index].cart_qty;
          tempTransItem.product_qty =
            state.dishesData[ind].products[index].cart_qty;
          tempTransItem.product_id = item.product_id;
          cartItem.product_qty = tempTransItem.product_qty;
          cartItem.product_id = tempTransItem.product_id;
          cartItem.product_price = parseFloat(actions?.payload?.productPrice);
        }
      });
      if (actions.payload.addOptions) {
        tempTransItem.variants = [actions.payload.addOptions];
        cartItem.variants = [actions.payload.addOptions];
      }
      if (actions.payload.addAddons) {
        tempTransItem.addons = actions.payload.addAddons;
        cartItem.addons = actions.payload.addAddons;
        cartItem.addonPrice = actions.payload.addonPrice;
      }
      state.totalItem = 1;
      state.itemsInCart = [cartItem];

      let price = 0;
      state.itemsInCart?.map((itemMy, index) => {
        let addPrice = itemMy?.addonPrice ? itemMy?.addonPrice : 0;
        let productPrice = itemMy?.variants
          ? itemMy?.product_price
          : itemMy?.product_price;
        price =
          price + productPrice * parseFloat(itemMy?.product_qty) + addPrice;
      });
      state.totalAmount = price;
      const body = {
        user_id: actions.payload?.user_id,
        vendor_id: actions.payload?.vendor_id,
        products: [tempTransItem],
      };
      const dispatch = actions.payload?.dispatch;
      const rest = false;
      addToCartApiCall({body, dispatch, rest});
    },
    updateCheckOut(state, actions) {
      let price = 0;
      actions.payload?.map((item, index) => {
        price =
          price +
          parseFloat(item?.product_price) * parseFloat(item?.product_qty) +
          parseFloat(item?.addonPrice);
      });
      state.totalAmount = price;
      state.totalItem = actions.payload?.length;
      state.itemsInCart = actions.payload;
    },
    removeCartDetail(state, actions) {
      state.totalAmount = actions.payload?.totalAmount;
      state.totalItem = actions.payload?.totalItem;
      state.itemsInCart = actions.payload?.itemsInCart;
      state.cart_id = actions.payload?.cart_id;
      state.vendor_id = actions.payload?.vendor_id;
    },
    updateCartId(state, actions) {
      state.cart_id = actions.payload?.cart_id;
      state.vendor_id = actions.payload?.vendor_id;
    },
    getResDishSearchData(state, actions) {
      state.resDishSearchData = actions.payload;
    },
    addItemToCartSearchDish(state, actions) {
      console.log('addItemToCartSearchDish');

      const dishArr = state.resDishSearchData;
      var tempTransItem = Object.assign({});
      let cartItem = Object.assign({});
      dishArr?.map((item, index) => {
        console.log('item', item);
        if (item?.product_id == actions.payload?.products?.product_id) {
          state.resDishSearchData[index].cart_qty =
            actions.payload?.products?.cart_qty + 1;
          item.product_qty = actions.payload?.products?.cart_qty + 1;
          tempTransItem.product_qty = actions.payload?.products?.cart_qty + 1;
          tempTransItem.product_id = item.product_id;
          cartItem.product_qty = tempTransItem.product_qty;
          cartItem.product_id = tempTransItem.product_id;
          cartItem.product_price = parseFloat(actions?.payload?.productPrice);
        }
      });
      let isExist;
      state.itemsInCart?.map((itm, indx) => {
        if (itm?.product_id == actions?.payload?.products?.product_id) {
          state.itemsInCart[indx].product_qty =
            parseFloat(itm?.product_qty) + 1;
          isExist = true;
        }
      });
      state.cart_id = actions.payload?.cart_id;
      state.vendor_id = actions.payload?.vendor_id;
      console.log('actions.payload.addOptions', actions.payload.addOptions);
      if (actions.payload.addOptions) {
        tempTransItem.variants = [actions.payload.addOptions];
        cartItem.variants = [actions.payload.addOptions];
      }
      if (actions.payload.addAddons) {
        tempTransItem.addons = actions.payload.addAddons;
        cartItem.addons = actions.payload.addAddons;
        cartItem.addonPrice = actions.payload.addonPrice;
      }
      console.log('isExist', isExist);
      if (!isExist) {
        state.totalItem = state.totalItem + 1;
        state.itemsInCart = [...state.itemsInCart, cartItem];
      }
      let price = 0;
      state.itemsInCart?.map((itemMy, index) => {
        let addPrice = itemMy?.addonPrice ? itemMy?.addonPrice : 0;
        let productPrice = itemMy?.variants
          ? itemMy?.product_price
          : itemMy?.product_price;
        price =
          price + productPrice * parseFloat(itemMy?.product_qty) + addPrice;
      });

      state.totalAmount = price;
    },
    minusItemToCartSearchDish(state, actions) {
      const dishArr = state.resDishSearchData;
      let passItem;
      let cartItem = Object.assign({});
      dishArr?.map((item, index) => {
        if (item?.product_id == actions?.payload?.products?.product_id) {
          state.resDishSearchData[index].cart_qty =
            actions.payload?.products?.cart_qty - 1;
          item.product_qty = actions.payload?.products?.cart_qty - 1;
          cartItem.product_qty = actions.payload?.products?.cart_qty - 1;
          cartItem.product_id = actions?.payload?.products?.product_id;
          cartItem.product_price = parseFloat(actions?.payload?.productPrice);
          state.itemsInCart?.map((itm, indx) => {
            if (itm?.product_id == actions?.payload?.products?.product_id) {
              state.itemsInCart[indx].product_qty =
                parseFloat(itm?.product_qty) - 1;
            }
          });
          if (cartItem.product_qty == 0) {
            state.totalItem = state.totalItem - 1;
          }
          if (item?.variants) {
            item.variants[0].variant_qty = item.product_qty;
            cartItem.variants = [actions.payload.addOptions];
          }
          if (item?.addons) {
            item.addons?.map((it, indI) => {
              item.addons[indI].addon_id = it.id;
              item.addons[indI].addon_qty = 0;
            });
            cartItem.addons = actions.payload.addAddons;
          }

          passItem = item;
        }
      });
      state.cart_id = actions.payload?.cart_id;
      state.vendor_id = actions.payload?.vendor_id;
      let price = 0;
      state.itemsInCart?.map((itemMy, index) => {
        if (itemMy?.product_qty > 0) {
          let addPrice = itemMy?.addonPrice ? itemMy?.addonPrice : 0;
          let productPrice = itemMy?.variants
            ? itemMy?.product_price
            : itemMy?.product_price;
          price =
            price + productPrice * parseFloat(itemMy?.product_qty) + addPrice;
        }
      });
      state.totalAmount = price;
      const body = {
        cart_id: actions.payload?.cart_id,
        user_id: actions.payload?.user_id,
        vendor_id: actions.payload?.vendor_id,
        products: [passItem],
      };
      const dispatch = actions.payload?.dispatch;
      const empty = true;
      const removeBody = {
        user_id: actions.payload?.user_id,
      };
      state.totalItem == 0 &&
        removeEmptyCartApiCall({body: removeBody, dispatch});
    },
    addNewItemToCartSearchDish(state, actions) {
      const ind = actions.payload?.menuIndex;
      const dishArr = state.resDishSearchData;
      var tempTransItem = Object.assign({});
      let cartItem = Object.assign({});
      dishArr?.map((item, index) => {
        if (item?.product_id == actions?.payload?.products?.product_id) {
          state.resDishSearchData[index].cart_qty =
            parseInt(actions.payload?.products.cart_qty) + 1;
          item.product_qty = actions.payload?.products?.cart_qty + 1;
          item.cartItem = actions.payload?.products?.cart_qty + 1;
          tempTransItem.product_qty = actions.payload?.products?.cart_qty + 1;
          tempTransItem.product_id = item.product_id;
          cartItem.product_qty = tempTransItem.product_qty;
          cartItem.product_id = tempTransItem.product_id;
          console.log(
            'actions?.payload?.productPrice',
            actions?.payload?.productPrice,
          );
          cartItem.product_price = parseFloat(actions?.payload?.productPrice);
        }
      });
      if (actions.payload.addOptions) {
        tempTransItem.variants = [actions.payload.addOptions];
        cartItem.variants = [actions.payload.addOptions];
      }
      if (actions.payload.addAddons) {
        tempTransItem.addons = actions.payload.addAddons;
        cartItem.addons = actions.payload.addAddons;
        cartItem.addonPrice = actions.payload.addonPrice;
      }
      state.totalItem = 1;
      state.itemsInCart = [cartItem];

      console.log('state.itemsInCart', JSON.stringify(state.itemsInCart));

      let price = 0;
      state.itemsInCart?.map((itemMy, index) => {
        let addPrice = itemMy?.addonPrice ? itemMy?.addonPrice : 0;
        let productPrice = itemMy?.variants
          ? itemMy?.product_price
          : itemMy?.product_price;
        price =
          price + productPrice * parseFloat(itemMy?.product_qty) + addPrice;
      });
      console.log('price', price);
      state.totalAmount = price;
      const body = {
        user_id: actions.payload?.user_id,
        vendor_id: actions.payload?.vendor_id,
        products: [tempTransItem],
      };
      const dispatch = actions.payload?.dispatch;
      const rest = false;
      addToCartApiCall({body, dispatch, rest});
    },
    addProceedCartRefreshCount(state, actions) {
      state.proceedCartRefreshCount = actions.payload;
    },
    removeItemInCart(state, actions) {
      state.clearResItem = actions.payload;
    },
  },
});

export const {
  getRestaurantDetails,
  addItemToCartRest,
  minusItemToCartRest,
  addNewItemToCartRest,
  getFilterApply,
  updateFavDishInResDetail,
  updateCheckOut,
  removeCartDetail,
  updateCartId,
  getResDishSearchData,
  addItemToCartSearchDish,
  minusItemToCartSearchDish,
  addProceedCartRefreshCount,
  addNewItemToCartSearchDish,
  removeItemInCart,
} = RestaurantDetailsSlice.actions;
export default RestaurantDetailsSlice.reducer;
