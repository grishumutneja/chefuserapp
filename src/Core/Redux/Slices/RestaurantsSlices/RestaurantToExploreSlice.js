import {
  addFavRestaurantsApiCall,
  removeFavRestaurantsApiCall,
} from '../../../Config/ApiCalls';

const {createSlice} = require('@reduxjs/toolkit');

const RestaurantToExploreSlice = createSlice({
  name: 'restaurantToExplore',
  initialState: {
    restaurantToExploreData: null,
    isRestToExploreLoading: false,
    isDataLimitComplete: false,
  },
  reducers: {
    addRestaurantToExploreData(state, actions) {
      // state.restaurantToExploreData.push(actions.payload);
      state.restaurantToExploreData = actions.payload;
      state.isDataLimitComplete = false;
    },
    addRestaurantToExploreMoreData(state, actions) {
      if (actions.payload?.length <= 0) {
        state.isDataLimitComplete = true;
      } else {
        state.restaurantToExploreData = [
          ...state.restaurantToExploreData,
          ...actions.payload,
        ];
      }
    },
    updateFavResExplore(state, actions) {
      let TempData = [];
      let newItem;
      state.restaurantToExploreData.map((item, index) => {
        if (item.vendor_id == actions.payload.vendor_id) {
          item.is_like = actions.payload.is_like;
        }
        TempData.push(item);
      });
      state.restaurantToExploreData = TempData;
      const newBody = {
        vendor_id: actions.payload.vendor_id,
        user_id: actions.payload.user_id,
      };
      if (actions.payload.is_like == 1) {
        addFavRestaurantsApiCall({
          body: newBody,
          dispatch: actions.payload.dispatch,
          item: actions.payload.item,
        });
      } else {
        removeFavRestaurantsApiCall({
          body: newBody,
          dispatch: actions.payload.dispatch,
          vendorId: actions.payload.item?.vendor_id,
        });
      }
    },
  },
});

export const {
  addRestaurantToExploreData,
  addRestaurantToExploreMoreData,
  updateFavResExplore,
} = RestaurantToExploreSlice.actions;
export default RestaurantToExploreSlice.reducer;
