import {removeEmptyCartApiCall} from '../../../Config/ApiCalls';

const {createSlice} = require('@reduxjs/toolkit');

const initialState = {
  cartListData: null,
  totalPrice: null,
  taxPrice: null,
  promoCodeList: null,
  payAmount: null,
  totalAmount: null,
  cartItems: null,
  cartTotalItem: 0,
  cartVendorId: null,
  isLoading: false,
  calTaxAmount: null,
  calPlatFormCharge: null,
  cartPassData: null,
  offerValue: 0,
  deliveryPrice: 0,
};

const CartSlice = createSlice({
  name: 'cart',
  initialState: initialState,
  reducers: {
    getCartListRefresh(state, actions) {
      state.cartListData = actions.payload;
      state.deliveryPrice = parseFloat(actions.payload?.delivery_charge);
    },
    getCartListLoading(state, actions) {
      state.isLoading = actions.payload;
    },
    getCartList(state, actions) {
      state.cartPassData = null;
      if (state.cartListData == null) {
        state.cartListData = actions.payload;
      }
      if (actions.payload == null) {
        state.cartListData = null;
        state.totalPrice = null;
        state.taxPrice = null;
        state.promoCodeList = null;
        state.payAmount = null;
        state.totalAmount = null;
        state.cartItems = null;
        state.cartTotalItem = 0;
        state.cartVendorId = null;
        state.deliveryPrice = 0;
      } else {
        state.cartItems = actions.payload.cart;
        state.cartTotalItem = actions.payload.cart.length;
        state.totalPrice = actions.payload?.cart_sub_toatl_amount;
        state.taxPrice =
          (actions.payload?.cart_sub_toatl_amount * actions.payload.tax) / 100 +
          parseFloat(actions.payload?.platform_charges);
        state.payAmount = state?.totalPrice + state?.taxPrice;
        console.log('first state.payAmount ==>', state.payAmount);
        state.totalAmount = state?.totalPrice + state?.taxPrice;
        state.cartVendorId = actions.payload?.vendor?.vendor_id;
        state.calTaxAmount = actions.payload?.tax;
        state.calPlatFormCharge = actions.payload?.platform_charges;
        state.offerValue = parseFloat(actions.payload?.saving_amount);
        state.deliveryPrice = parseFloat(actions.payload?.delivery_charge);
      }
    },
    applyPromoCode(state, actions) {
      console.log('state', state);
      console.log('actions', actions);
      state.taxPrice =
        ((state.totalPrice - actions.payload.discount) *
          state.cartListData.tax) /
          100 +
        parseFloat(state.cartListData?.platform_charges);
      console.log('state.taxPrice Slice', state.payAmount);
      state.payAmount =
        state?.totalPrice + state?.taxPrice - actions.payload.discount;
      console.log('state.payAmount Slice', state.payAmount);
      state.totalAmount =
        state?.totalPrice + state?.taxPrice - actions.payload.discount;
      console.log('state.totalAmount Slice', state.totalAmount);
      console.log('payAmount.payAmount Slice', state.payAmount);
    },
    clearPromoCode(state, actions) {
      state.taxPrice =
        (state.totalPrice * state.cartListData.tax) / 100 +
        parseFloat(state.cartListData?.platform_charges);
    },
    getPromoCodeList(state, actions) {
      state.promoCodeList = actions.payload;
    },
    applyWallet(state, actions) {
      console.log('state.payAmount', state.payAmount);
      console.log('actions.payload', actions.payload);
      state.payAmount = state.payAmount - actions.payload;
    },
    grossAmountDispatch(state, actions) {
      console.log('actions.payload', actions.payload);
      state.payAmount = actions.payload;
    },
    addItemToCart(state, actions) {
      const tempData = state?.cartItems;
      let isExisting = false;
      const myPassArr = [];
      tempData?.map((item, index) => {
        if (item?.product_id == actions.payload?.products?.product_id) {
          var tempTransItem = Object.assign({});
          item.product_qty = item?.product_qty + 1;
          tempTransItem.product_qty = item?.product_qty;
          tempTransItem.product_id = item.product_id;
          isExisting = true;
          if (actions.payload?.variant) {
            tempTransItem.variants = [
              {
                variant_id: actions.payload?.variant?.variant_id,
                variant_qty: actions.payload?.variant?.qty,
              },
            ];
          }
          if (actions.payload.addons?.length > 0) {
            let addArray = [];
            actions.payload.addons?.map((item, index) => {
              var tempItem = Object.assign({});
              tempItem.addon_id = item.addon_id;
              tempItem.addon_qty = item.addon_qty;
              tempItem.addonPrice = item.addon_price;
              addArray.push(tempItem);
            });
            tempTransItem.addons = addArray;
          }
          myPassArr?.push(tempTransItem);
        } else {
          var tempTransItem = Object.assign({});
          tempTransItem.product_qty = item?.product_qty;
          tempTransItem.product_id = item.product_id;
          if (actions.payload?.variant) {
            tempTransItem.variants = [
              {
                variant_id: actions.payload?.variant?.variant_id,
                variant_qty: actions.payload?.variant?.qty,
              },
            ];
          }
          if (item?.addons?.length > 0) {
            let addArray = [];
            item?.addons?.map((itm, ind) => {
              if (itm?.added) {
                var tempItem = Object.assign({});
                tempItem.addon_id = itm.addon_id;
                tempItem.addon_qty = itm.addon_qty;
                addArray.push(tempItem);
              }
            });
            tempTransItem.addons = addArray;
          }
          myPassArr?.push(tempTransItem);
        }
      });

      let price = 0;
      let offerTotalPrice = 0;
      tempData &&
        tempData?.map((itemMy, index) => {
          let addPrice = 0;
          let varPrice;
          let varOfferPrice;
          let pPrice;
          let pOfferPrice;

          const adPrice = itemMy?.addons?.map((it, ind) => {
            if (it?.added) {
              addPrice = addPrice + parseFloat(it?.addon_price);
            }
          });
          const adPrice2 = itemMy?.variants?.map((it, ind) => {
            if (it?.added) {
              if (it?.offer_id != 0) {
                varOfferPrice = parseFloat(it?.after_offer_price);
                pPrice = it?.variant_price;
                pOfferPrice = it?.after_offer_price;
              } else {
                varPrice = parseFloat(it?.variant_price);
                pOfferPrice = it?.after_offer_price;
              }
            }
          });
          // let pOfferPrice = itemMy?.after_offer_price;
          // let pPrice = itemMy?.product_price;
          let proPrice = varPrice || pPrice;
          let proOfferPrice = varOfferPrice || pOfferPrice;

          price = price + proPrice * parseFloat(itemMy?.product_qty) + addPrice;
          offerTotalPrice =
            offerTotalPrice +
            proOfferPrice * parseFloat(itemMy?.product_qty) +
            addPrice;
        });
      state.totalPrice = offerTotalPrice ? offerTotalPrice : price;
      state.offerValue = price - offerTotalPrice;
      console.log('state.calPlatFormCharge ===>', state.calPlatFormCharge);
      console.log('state.calTaxAmount ===>', state.calTaxAmount);
      console.log('price', price);
      console.log('state.totalPrice', state.totalPrice);
      state.taxPrice =
        (state.totalPrice * state.calTaxAmount) / 100 +
        parseFloat(state.calPlatFormCharge);
      console.log('state.taxPrice ===>', state.taxPrice);
      state.payAmount = state?.totalPrice + state?.taxPrice;
      state.totalAmount = state?.totalPrice + state?.taxPrice;
      state.cartPassData = myPassArr;
      const body = {
        cart_id: actions.payload?.cart_id,
        user_id: actions.payload?.user_id,
        vendor_id: actions.payload?.vendor_id,
      };
      const dispatch = actions.payload?.dispatch;
    },
    minusItemToCart(state, actions) {
      const tempData = state?.cartItems;
      let isExisting = false;
      let isZero = false;
      const myPassArr = [];
      tempData?.map((item, index) => {
        if (item?.product_id == actions.payload?.products?.product_id) {
          var tempTransItem = Object.assign({});
          if (item.product_qty > 1) {
            item.product_qty = item?.product_qty - 1;
            tempTransItem.product_qty = item?.product_qty;
            tempTransItem.product_id = item.product_id;
          } else {
            item.product_qty = item?.product_qty - 1;
            tempTransItem.product_qty = item?.product_qty;
            tempTransItem.product_id = item.product_id;
            isZero = true;
          }
          isExisting = true;
          if (actions.payload?.variant) {
            tempTransItem.variants = [
              {
                variant_id: actions.payload?.variant?.variant_id,
                variant_qty: actions.payload?.variant?.qty,
              },
            ];
          }
          if (item?.addons?.length > 0) {
            let addArray = [];
            item?.addons?.map((itm, ind) => {
              if (itm?.added) {
                var tempItem = Object.assign({});
                tempItem.addon_id = itm.addon_id;
                tempItem.addon_qty = itm.addon_qty;
                addArray.push(tempItem);
              }
            });
            tempTransItem.addons = addArray;
          }
          myPassArr?.push(tempTransItem);
        } else {
          var tempTransItem = Object.assign({});
          tempTransItem.product_qty = item?.product_qty;
          tempTransItem.product_id = item.product_id;
          if (actions.payload?.variant) {
            tempTransItem.variants = [
              {
                variant_id: actions.payload?.variant?.variant_id,
                variant_qty: actions.payload?.variant?.qty,
              },
            ];
          }
          if (item?.addons?.length > 0) {
            let addArray = [];
            item?.addons?.map((itm, ind) => {
              if (itm?.added) {
                var tempItem = Object.assign({});
                tempItem.addon_id = itm.addon_id;
                tempItem.addon_qty = itm.addon_qty;
                addArray.push(tempItem);
              }
            });
            tempTransItem.addons = addArray;
          }
          myPassArr?.push(tempTransItem);
        }
      });
      if (!isExisting) {
        tempData.push(actions.payload?.products);
      }
      let NewData;
      if (isZero) {
        NewData = tempData.filter(item => {
          return item.product_id != actions.payload?.products?.product_id;
        });
        state.cartTotalItem = NewData?.length;
      }
      const transData = isZero ? NewData : tempData;
      let price = 0;
      let offerTotalPrice = 0;
      console.log('tempDatatempDatatempDatatempData', tempData);
      tempData &&
        tempData?.map((itemMy, index) => {
          let variantPrice;
          let variantOfferPrice;
          let addPrice = 0;
          let proPrice;
          let proOfferPrice;

          // if (itemMy?.product_qty > 0) {
          itemMy?.addons?.map((itm, ind) => {
            if (itm?.added) {
              addPrice =
                itm?.addon_qty * parseFloat(itm?.addon_price) + addPrice;
            }
          });
          itemMy?.variants?.map((itm, ind) => {
            if (itm?.added) {
              if (itm?.offer_id != 0) {
                variantOfferPrice = parseFloat(itm?.after_offer_price);
                proPrice = itm?.variant_price;
                proOfferPrice = itm?.after_offer_price;
              } else {
                variantPrice = parseFloat(itm?.variant_price);
                proOfferPrice = itm?.after_offer_price;
              }
            }
          });
          // }

          // console.log('proPriceproPrice', proPrice);
          // const proPrice = itemMy?.product_price;

          // const proOfferPrice = itemMy?.after_offer_price;
          let productPrice = variantPrice || proPrice;
          let produceOfferPrice = variantOfferPrice || proOfferPrice;
          console.log('productPrice', productPrice);
          console.log('proOfferPrice', proOfferPrice);
          price =
            price + productPrice * parseFloat(itemMy?.product_qty) + addPrice;
          offerTotalPrice =
            offerTotalPrice +
            produceOfferPrice * parseFloat(itemMy?.product_qty) +
            addPrice;
        });
      console.log('pricepricepricepriceprice', price);
      console.log('offerTotalPriceofferTotalPrice', offerTotalPrice);
      state.totalPrice = offerTotalPrice ? offerTotalPrice : price;
      state.offerValue = price - offerTotalPrice;
      state.cartItems = tempData;
      console.log('state.calPlatFormCharge ===>', state.calPlatFormCharge);
      console.log('state.calTaxAmount ===>', state.calTaxAmount);
      console.log('state.taxPrice ===>', state.taxPrice);
      state.taxPrice =
        (state.totalPrice * state.calTaxAmount) / 100 +
        parseFloat(state.calPlatFormCharge);
      state.payAmount = state?.totalPrice + state?.taxPrice;
      state.totalAmount = state?.totalPrice + state?.taxPrice;

      const body = {
        user_id: actions.payload?.user_id,
      };
      state.cartPassData = myPassArr;
      const dispatch = actions.payload?.dispatch;
      price == 0 && removeEmptyCartApiCall({body, dispatch});
    },
    resetCart(state, actions) {
      state.cartListData = null;
      state.totalPrice = null;
      state.taxPrice = null;
      state.promoCodeList = null;
      state.payAmount = null;
      state.totalAmount = null;
      state.cartItems = null;
      state.cartTotalItem = 0;
      state.cartVendorId = null;
      state.isLoading = false;
      state.calTaxAmount = null;
      state.calPlatFormCharge = null;
      state.cartPassData = null;
      state.offerValue = 0;
    },
  },
});

export const {
  getCartList,
  getPromoCodeList,
  applyPromoCode,
  addPromoCodeAmount,
  applyWallet,
  addItemToCart,
  minusItemToCart,
  getCartListRefresh,
  getCartListLoading,
  resetCart,
  grossAmountDispatch,
} = CartSlice.actions;
export default CartSlice.reducer;
