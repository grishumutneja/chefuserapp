const {createSlice} = require('@reduxjs/toolkit');

const initialState = {
  deliveryAddressData: null,
  isDeliveryAddressLoading: null,
  noAddressAvailable: false,
};

const DeliveryAddressSlice = createSlice({
  name: 'deliveryAddress',
  initialState: initialState,
  reducers: {
    getDeliveryAddressList(state, actions) {
      if (actions.payload?.length == 0) {
        state.noAddressAvailable = true;
      } else {
        state.noAddressAvailable = false;
        state.deliveryAddressData = actions.payload;
      }
    },
    addDeliveryAddress(state, actions) {
      state.deliveryAddressData.push(actions.payload);
    },
    isNoAddressAvailable(state, actions) {
      state.noAddressAvailable = actions.payload;
    },
    deleteDeliveryAddress(state, actions) {
      let newArr = state.deliveryAddressData?.filter(item => {
        return item.id !== actions.payload;
      });
      state.deliveryAddressData = newArr;
    },
    resetDeliveryAddress(state, actions) {
      state.deliveryAddressData = null;
      state.isDeliveryAddressLoading = null;
      state.noAddressAvailable = false;
    },
  },
});

export const {
  addDeliveryAddress,
  deleteDeliveryAddress,
  getDeliveryAddressList,
  resetDeliveryAddress,
  isNoAddressAvailable,
} = DeliveryAddressSlice.actions;
export default DeliveryAddressSlice.reducer;
