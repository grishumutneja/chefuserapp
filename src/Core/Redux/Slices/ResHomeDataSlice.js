const {createSlice} = require('@reduxjs/toolkit');

const ResHomeDataSlice = createSlice({
  name: 'resHomeData',
  initialState: {
    topRatedResData: null,
    onesMoreResData: null,
    resMasterBlogData: null,
    featuredResData: null,
    mostViewedVendors: null,
  },
  reducers: {
    getTopRatedResList(state, actions) {
      state.topRatedResData = actions.payload;
    },
    onesMoreResList(state, actions) {
      state.onesMoreResData = actions.payload;
    },
    getResMasterBlogList(state, actions) {
      state.resMasterBlogData = actions.payload;
      state.featuredResData = actions.payload[0]?.blog?.vendors?.slice(0, 10);
    },
    updateFavToRatedRes(state, actions) {
      let TempData = [];
      state.topRatedResData?.map((item, index) => {
        if (item.vendor_id == actions.payload.vendor_id) {
          item.is_like = actions.payload.is_like;
        }
        TempData.push(item);
      });
      state.topRatedResData = TempData;
    },
    getMostViewVendorList(state, actions) {
      state.mostViewedVendors = actions.payload;
    },
  },
});

export const {
  getTopRatedResList,
  onesMoreResList,
  getResMasterBlogList,
  updateFavToRatedRes,
  getMostViewVendorList,
} = ResHomeDataSlice.actions;
export default ResHomeDataSlice.reducer;
