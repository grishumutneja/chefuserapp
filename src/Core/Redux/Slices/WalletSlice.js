const {createSlice} = require('@reduxjs/toolkit');

const WalletSlice = createSlice({
  name: 'wallet',
  initialState: {
    walletTransactionData: null,
    walletData: null,
    refMessage: null,
    noTransactions: false,
  },
  reducers: {
    getWalletTransaction(state, actions) {
      if (actions.payload?.length == 0) {
        state.noTransactions = true;
      } else {
        state.walletTransactionData = actions.payload;
        state.noTransactions = false;
      }
    },
    getWalletData(state, actions) {
      state.walletData = actions.payload;
    },
    getRefMessage(state, actions) {
      state.refMessage = actions.payload;
    },
  },
});

export const {getWalletTransaction, getWalletData, getRefMessage} =
  WalletSlice.actions;
export default WalletSlice.reducer;
