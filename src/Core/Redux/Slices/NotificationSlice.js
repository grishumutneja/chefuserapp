const {createSlice} = require('@reduxjs/toolkit');

const NotificationSlice = createSlice({
  name: 'notification',
  initialState: {
    notificationData: null,
  },
  reducers: {
    getNotificationData(state, actions) {
      state.notificationData = actions.payload;
    },
  },
});

export const {getNotificationData} = NotificationSlice.actions;
export default NotificationSlice.reducer;
