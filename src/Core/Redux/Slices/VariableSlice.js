const {createSlice} = require('@reduxjs/toolkit');

const VariableSlice = createSlice({
  name: 'variables',
  initialState: {
    variableData: null,
  },
  reducers: {
    addVariables(state, actions) {
      state.variableData = actions.payload;
    },
  },
});

export const {addVariables} = VariableSlice.actions;
export default VariableSlice.reducer;
