import {createSlice} from '@reduxjs/toolkit';

const AboutDataSlice = createSlice({
  name: 'about',
  initialState: {
    aboutUsData: null,
    isAboutData: false,
    termsAndConditionData: null,
    isTermsAndConditionData: false,
    privacyPolicyData: null,
    isPrivacyPolicyData: false,
    cancellationPolicyData: null,
    isCancellationPolicyData: false,
    getFaqsData: null,
    isGetFaqs: false,
  },
  reducers: {
    getAboutUsData(state, actions) {
      state.aboutUsData = actions.payload;
    },
    getTermsAndConditionData(state, actions) {
      state.termsAndConditionData = actions.payload;
    },
    getPrivacyPolicy(state, actions) {
      state.privacyPolicyData = actions.payload;
    },
    getCancellationPolicy(state, actions) {
      state.cancellationPolicyData = actions.payload;
    },
    getFaqs(state, actions) {
      state.getFaqsData = actions.payload;
    },
  },
});

export const {
  getAboutUsData,
  getTermsAndConditionData,
  getPrivacyPolicy,
  getCancellationPolicy,
  getFaqs,
} = AboutDataSlice.actions;
export default AboutDataSlice.reducer;
