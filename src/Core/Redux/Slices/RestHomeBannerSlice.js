const {createSlice} = require('@reduxjs/toolkit');

const RestHomeBannerSlice = createSlice({
  name: 'restHomeBanner',
  initialState: {
    restHomeBannerData: null,
    isRestHomeBannerData: false,
    restTrackBannerData: false,
  },
  reducers: {
    addRestHomeBanner(state, actions) {
      state.restHomeBannerData = actions.payload;
    },
    addRestTrackBanner(state, actions) {
      state.restTrackBannerData = actions?.payload;
    },
  },
});

export const {addRestHomeBanner, addRestTrackBanner} =
  RestHomeBannerSlice.actions;
export default RestHomeBannerSlice.reducer;
