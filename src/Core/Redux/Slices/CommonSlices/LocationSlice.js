const {createSlice} = require('@reduxjs/toolkit');

const LocationSlice = createSlice({
  name: 'location',
  initialState: {
    locationData: null,
    currentLocationData: null,
    locationPermissionAndroid: false,
  },
  reducers: {
    addLocation(state, actions) {
      state.locationData = actions.payload;
    },
    resetLocation(state, actions) {
      state.locationData = null;
      state.currentLocationData = null;
    },
    locationPermissionStatus(state, actions) {
      console.log('locationPermissionStatus actions.payload', actions.payload);
      state.locationPermissionAndroid = actions.payload;
    },
  },
});

export const {addLocation, resetLocation, locationPermissionStatus} =
  LocationSlice.actions;
export default LocationSlice.reducer;
