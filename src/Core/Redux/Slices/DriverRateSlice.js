const {createSlice} = require('@reduxjs/toolkit');

const DriverRateSlice = createSlice({
  name: 'driverRate',
  initialState: {
    rateGivenCount: 0,
  },
  reducers: {
    addDriverRate(state, actions) {
      state.rateGivenCount = actions.payload;
    },
  },
});

export const {addDriverRate} = DriverRateSlice.actions;
export default DriverRateSlice.reducer;
