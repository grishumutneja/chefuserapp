const {createSlice} = require('@reduxjs/toolkit');

const GetUserInfoSlice = createSlice({
  name: 'userInfo',
  initialState: {
    getUserInfoData: null,
    loginData: null,
    isGuestAccount: false,
  },
  reducers: {
    getUserInfoList(state, actions) {
      if (actions.payload.isLogin) {
        state.loginData = actions.payload.loginData;
      } else {
        state.getUserInfoData = actions.payload;
      }
    },
    guestAccount(state, actions) {
      state.isGuestAccount = actions.payload;
    },
    resetGetUserInfoList(state, actions) {
      state.getUserInfoData = null;
      state.loginData = null;
      state.isGuestAccount = false;
    },
  },
});

export const {getUserInfoList, guestAccount, resetGetUserInfoList} =
  GetUserInfoSlice.actions;
export default GetUserInfoSlice.reducer;
