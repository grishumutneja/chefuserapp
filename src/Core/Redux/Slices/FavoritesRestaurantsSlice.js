const {createSlice} = require('@reduxjs/toolkit');

const FavoritesRestaurantsSlice = createSlice({
  name: 'favoritesRestaurants',
  initialState: {
    getAllFavRestaurantData: null,
    isAllFavRestaurantsData: false,
    locationWhileList: null,
  },
  reducers: {
    getAllFavRestaurantsList(state, actions) {
      state.getAllFavRestaurantData = actions.payload;
    },
    removeFavRestaurants(state, actions) {
      let newArr = state.getAllFavRestaurantData?.filter(item => {
        return item.vendor_id != actions.payload;
      });
      state.getAllFavRestaurantData = newArr;
    },
    addFavRestaurants(state, actions) {
      let NewData = actions.payload;
      NewData.is_like = 1;
      state.getAllFavRestaurantData = [
        ...state.getAllFavRestaurantData,
        NewData,
      ];
    },
    getLocationWhileFavRes(state, actions) {
      state.locationWhileList = actions.payload;
    },
  },
});

export const {
  getAllFavRestaurantsList,
  removeFavRestaurants,
  addFavRestaurants,
  getLocationWhileFavRes,
} = FavoritesRestaurantsSlice.actions;
export default FavoritesRestaurantsSlice.reducer;
