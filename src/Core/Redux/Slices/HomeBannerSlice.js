const {createSlice} = require('@reduxjs/toolkit');

const HomeBannerSlice = createSlice({
  name: 'homeBanners',
  initialState: {
    homeBannerData: null,
    isBannerLoading: false,
  },
  reducers: {
    addHomeBanners(state, actions) {
      state.homeBannerData = actions.payload;
    },
  },
});

export const {addHomeBanners} = HomeBannerSlice.actions;
export default HomeBannerSlice.reducer;
