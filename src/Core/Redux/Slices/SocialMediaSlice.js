const {createSlice} = require('@reduxjs/toolkit');

const SocialMediaSlice = createSlice({
  name: 'socialMedia',
  initialState: {
    socialMediaData: null,
  },
  reducers: {
    getSocialMedia(state, actions) {
      state.socialMediaData = actions.payload;
    },
  },
});

export const {getSocialMedia} = SocialMediaSlice.actions;
export default SocialMediaSlice.reducer;
