const {createSlice} = require('@reduxjs/toolkit');

const FavoritesDishesSlice = createSlice({
  name: 'favoritesDishes',
  initialState: {
    getAllFavDishesData: null,
    isAllFavDishesData: false,
  },
  reducers: {
    getAllFavDishesList(state, actions) {
      state.getAllFavDishesData = actions.payload;
    },
    removeFavDishes(state, actions) {
      let newArr = state.getAllFavDishesData?.filter(item => {
        return item.product_id != actions.payload;
      });
      state.getAllFavDishesData = newArr;
    },
    addFavDishes(state, actions) {
      let newArr = state.getAllFavDishesData;
      state.getAllFavDishesData = [...newArr, actions.payload];
    },
  },
});

export const {getAllFavDishesList, removeFavDishes, addFavDishes} =
  FavoritesDishesSlice.actions;
export default FavoritesDishesSlice.reducer;
