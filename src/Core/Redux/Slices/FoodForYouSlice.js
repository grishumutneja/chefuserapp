const {createSlice} = require('@reduxjs/toolkit');

const FoodForYouSlice = createSlice({
  name: 'foodForYou',
  initialState: {
    foodForYouData: null,
    foodForYouRestaurantData: null,
    foodCategoriesData: null,
    isFoodCategoriesDataComplete: false,
    // categoriesRestaurantData: null,
    // isFoodForYouDataComplete: false,
  },
  reducers: {
    getFoodForYouData(state, actions) {
      state.foodForYouData = actions.payload;
    },
    getFoodForYouRestaurantData(state, actions) {
      state.foodForYouRestaurantData = actions.payload;
    },
    updateFavFoodForYouRestaurant(state, actions) {
      let TempData = [];
      state.foodForYouRestaurantData?.map((item, index) => {
        if (item.vendor_id == actions.payload.vendor_id) {
          item.is_like = actions.payload.is_like;
        }
        TempData.push(item);
      });
      state.foodForYouRestaurantData = TempData;
    },
    getFoodCategoriesData(state, actions) {
      state.foodCategoriesData = actions.payload;
    },
    addFoodForYouRestaurantMoreData(state, actions) {
      if (actions.payload?.length <= 0) {
        state.isFoodCategoriesDataComplete = true;
      } else {
        state.foodForYouRestaurantData = [
          ...state.foodForYouRestaurantData,
          ...actions.payload,
        ];
      }
    },
    getFoodCategoriesRestaurantData(state, actions) {
      state.foodForYouRestaurantData = actions.payload;
    },
    addCategoriesRestaurantMoreData(state, actions) {
      if (actions.payload?.length <= 0) {
        state.isFoodCategoriesDataComplete = true;
      } else {
        state.foodForYouRestaurantData = [
          ...state.foodForYouRestaurantData,
          ...actions.payload,
        ];
      }
    },
  },
});

export const {
  getFoodForYouData,
  getFoodCategoriesData,
  getFoodForYouRestaurantData,
  addFoodForYouRestaurantMoreData,
  addCategoriesRestaurantMoreData,
  getFoodCategoriesRestaurantData,
  updateFavFoodForYouRestaurant,
} = FoodForYouSlice.actions;
export default FoodForYouSlice.reducer;
