const {createSlice} = require('@reduxjs/toolkit');

const OrderHistorySlice = createSlice({
  name: 'orderHistory',
  initialState: {
    orderHistoryData: null,
    isHistoryLimitComplete: false,
    noDataAvailable: false,
  },
  reducers: {
    getOrderHistory(state, actions) {
      if (actions.payload == 0) {
        state.noDataAvailable = true;
      } else {
        state.orderHistoryData = actions.payload;
        state.noDataAvailable = false;
      }
    },
    addOrderHistoryMoreData(state, actions) {
      if (actions.payload?.length <= 0) {
        state.isHistoryLimitComplete = true;
      } else {
        state.orderHistoryData = [
          ...state.orderHistoryData,
          ...actions.payload,
        ];
      }
    },
  },
});

export const {getOrderHistory, addOrderHistoryMoreData} =
  OrderHistorySlice.actions;
export default OrderHistorySlice.reducer;
