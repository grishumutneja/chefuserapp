const {configureStore} = require('@reduxjs/toolkit');
import HomeBannersReducer from './Slices/HomeBannerSlice';
import RestHomeBannerReducer from './Slices/RestHomeBannerSlice';
import VariableReducer from './Slices/VariableSlice';
import LocationReducer from './Slices/CommonSlices/LocationSlice';
import DeliveryAddressReducer from './Slices/DeliveryAddressSlice';
import RestaurantToExploreReducer from './Slices/RestaurantsSlices/RestaurantToExploreSlice';
import RestaurantDetailsReducer from './Slices/RestaurantsSlices/RestaurantDetailsSlice';
import FavoritesRestaurantsReducer from './Slices/FavoritesRestaurantsSlice';
import FavoritesDishesReducer from './Slices/FavoritesDishesSlice';
import SocialMediaReducer from './Slices/SocialMediaSlice';
import GetUserInfoReducer from './Slices/GetUserInfoSlice';
import AboutDataReducer from './Slices/AboutDataSlice';
import WalletReducer from './Slices/WalletSlice';
import OrderHistoryReducer from './Slices/OrderHistorySlice';
import FoodForYouReducer from './Slices/FoodForYouSlice';
import CartReducer from './Slices/RestaurantsSlices/CartSlice';
import ResHomeDataReducer from './Slices/ResHomeDataSlice';
import NotificationReducer from './Slices/NotificationSlice';
import DriverRateReducer from './Slices/DriverRateSlice';

export const store = configureStore({
  reducer: {
    homeBanners: HomeBannersReducer,
    restHomeBanners: RestHomeBannerReducer,
    variables: VariableReducer,
    location: LocationReducer,
    deliveryAddress: DeliveryAddressReducer,
    restaurantToExplore: RestaurantToExploreReducer,
    restaurantDetails: RestaurantDetailsReducer,
    favoritesRestaurants: FavoritesRestaurantsReducer,
    favoritesDishes: FavoritesDishesReducer,
    socialMedia: SocialMediaReducer,
    userInfo: GetUserInfoReducer,
    about: AboutDataReducer,
    wallet: WalletReducer,
    orderHistory: OrderHistoryReducer,
    foodForYou: FoodForYouReducer,
    cart: CartReducer,
    resHomeData: ResHomeDataReducer,
    notification: NotificationReducer,
    driverRate: DriverRateReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
      immutableCheck: false,
    }),
});
