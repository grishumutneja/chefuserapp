export const BASE_URL = 'https://cheflab.co.in/api/system-api/';
// export const BASE_URL = 'https://recreation.mangao.in/api/system-api/'; //re-creation URL
// export const BASE_URL = 'https://development.mangao.in/api/system-api/'; //development URL

export const GET_HOME_BANNER = 'getHomeBanner';
export const GET_REST_HOME_BANNER = 'getPromotionBanner';
export const GET_DELIVERY_ADDRESS = 'get-delivery-address';
export const DELIVERY_ADDRESS_DELETE = 'delivery-address-delete';
export const ADD_DELIVERY_ADDRESS = 'delivery-address-user';
export const UPDATE_DELIVERY_ADDRESS = 'delivery-address-update';
export const GET_USER_INFO = 'get-user-info';
export const UPDATE_USER_INFO = 'update-user-info';
export const SAVE_CONTACT_US = 'save-contact-us';
export const DELETE_USER = 'delete-user';
export const GET_UPDATE_VERSION = 'get-update-version';
export const GET_IOS_UPDATE_VERSION = 'get-ios-update-version';
export const RESTAURANT_TO_EXPLORE = 'get-vendor-rating-review';
export const RESTAURANT_DETAILS_BY_FOOD_TYPE =
  'v3/getRestaurantDetailByFoodtype';
export const GET_CUISINES = 'getCuisines';
export const GET_RESTAURANTS_BY_CUISINES = 'getRestaurantByCuisines';
export const GET_FOOD_CATEGORIES = 'getCategories';
export const GET_RESTAURANT_FOOD_CATEGORIES = 'getRestaurantByCategory';
export const GET_RESTAURANT_DETAILS = 'get-restaurant-detail';
export const GET_VENDER_ALL_REVIEW = 'get-vendor-all-review';
export const SEARCH_RES_DETAIL = 'search-RestaurantDetail-Page';
export const SEARCH_RES_DETAIL_V3 = 'v3/search-RestaurantDetail-Page';
// export const SEARCH_DATA = 'search-data';
export const SEARCH_DATA = 'v3/search-data';

export const GET_RESTAURANT_DETAILS_PAGE = 'v3/getRestaurantDetailPage';
export const GET_ALL_FAV_RESTAURANTS = 'get-all-liked-restaurant';
export const GET_ALL_FAV_DISHES = 'v3/get-all-liked-products';
export const DISLIKE_VENDOR = 'dislike-vendor';
export const LIKE_VENDOR = 'like-vendor';
export const DISLIKE_DISHES = 'dislike-product';
export const LIKE_DISHES = 'like-product';
export const SOCIAL_MEDIA = 'socialmedia';

export const PENDING_ORDER_RATING = 'pending-order-rating';
export const GET_DRIVER_RATE_DATA = 'get-driver-rating-data';
export const SAVE_RIDER_RATE = 'save-rider-rating-review';
export const SAVE_ORDER_RATE = 'save-order-rating';

export const GET_ORDER = 'get-order';
export const GET_ORDER_DETAILS = 'get-order-details';
export const DETAIL_ORDER_RATING = 'detail-order-rating';
export const RE_ORDER = 're-order';
export const CANCEL_ORDER = 'cancel-order';

export const LOGIN_OTP_SEND = 'login-otp-send-v2';
export const LOGIN_OTP_VERIFY = 'login-otp-verify-v2';
export const REGISTER_VERIFIED_USER = 'register-verified-user';

export const ABOUT_US = 'aboutus';
export const CANCELLATION_POLICY = 'cancellation-policy';
export const PRIVACY_AND_POLICY = 'privacy-and-policy';
export const TERMS_AND_CONDITION = 'terms-and-condition-userapp';
export const USER_FAQS = 'user-faq';

export const RECHARGE_WALLET = 'recharge-wallet';
export const TEMP_RECHARGE_WALLET = 'temp-recharge-wallet';
export const REFER_AMOUNT = 'refer-amount';
export const CHECK_RECHARGE_WALLET = 'check-recharge-wallet';

export const VIEW_CART = 'view-cart';
export const PROMO_CODE_DETAIL = 'procode-coupon-details';
export const PROMO_CODE_APPLY = 'procode-coupon-apply';
export const UPDATE_CART = 'update-cart';
export const ADD_TO_CART = 'add-to-cart';
export const CUSTOMIZABLE_DATA = 'custmizable-data';
export const REMOVE_EMPTY_CART = 'empty-cart';
export const CREATE_ORDER = 'create-order';
export const CREATE_TEMP_ORDER = 'v3-create-order';
export const MAKE_ORDER_REFUND = 'make-order-refund';
export const GET_TEMPORARY_ORDER_DETAIL = 'get-temporary-order-detail';
export const GUEST_LOGIN = 'guest-login';

export const TOP_RATED_RES = 'v3/getTopRatedRestaurant';
export const ONES_MORE_RES = 'v3/tryOnesMoreRestaurant';
export const GET_RES_MASTER_BLOG = 'v3/getRestauantMasterBlog';
export const GET_MOST_VIEW_VENDOR = 'v3/geMostViewVendors';

export const STORE_FCM_TOKEN = 'user-fcm-token';
