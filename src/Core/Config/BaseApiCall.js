import Utility from '../../Constants/Utility';
import instance from './apiConfig';

export const baseApiCall = config => {
  return new Promise((resolve, reject) => {
    instance(config)
      .then(response => {
        if (response?.status === 200) {
          resolve(response?.data);
        } else if (response?.data?.status === 200) {
          resolve(response?.data);
        }
      })
      .catch(e => {
        if (e?.response && e?.response?.data) {
          console.warn(
            'Base Api Response Error ======> ',
            e?.response?.data?.error,
          );
          console.warn('Base Api Error ======> ', e?.response?.data);
          if (
            e?.response?.data?.error !== 'your cart is empty' &&
            e?.response?.data?.error !== 'Vendor Not Found' &&
            e?.response?.data?.error !==
              'provided product not available under given vendor' &&
            e?.response?.data?.error !== 'Cart not found'
          ) {
            Utility.showToast(e?.response?.data?.error);
          }
          reject(e?.response?.data);
        } else {
          reject(e);
        }
      });
  });
};
