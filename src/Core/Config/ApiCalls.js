import Utility from '../../Constants/Utility';
import _ from 'lodash';
import {
  deleteDeliveryAddress,
  getDeliveryAddressList,
  isNoAddressAvailable,
} from '../Redux/Slices/DeliveryAddressSlice';
import RazorpayCheckout from 'react-native-razorpay';
import {addHomeBanners} from '../Redux/Slices/HomeBannerSlice';
import {
  addRestHomeBanner,
  addRestTrackBanner,
} from '../Redux/Slices/RestHomeBannerSlice';
import {
  aboutUsApi,
  addDeliveryAddressApi,
  addFavDishesApi,
  addFavRestaurantsApi,
  addToCartApi,
  cancelOrderApi,
  cancellationPolicyApi,
  checkRechargeWalletApi,
  createOrderApi,
  createTempOrderApi,
  deleteDeliveryAddressApi,
  deleteUserApi,
  detailOrderRatingApi,
  getAllFavDishesApi,
  getAllFavRestaurantsApi,
  getCartListApi,
  getCustomizableDataApi,
  getDeliveryAddress,
  getDriverRateDataApi,
  getFaqsApi,
  getFoodCategoriesApi,
  getFoodForYouApi,
  getFoodForYouRestaurantsApi,
  getHomeBanner,
  getIosUpdateVersion,
  getMostViewVendorApi,
  getOrderDetailsApi,
  getPromoCodeDetailApi,
  getResDetailByFoodTypeApi,
  getResDetailPageApi,
  getResMasterBlog,
  getRestDetailApi,
  getRestHomeBanner,
  getRestaurantFoodCategoriesApi,
  getSearchDataApi,
  getSearchDishesInResDetail,
  getSocialMediaApi,
  getTempOrderDetailApi,
  getTopRatedResApi,
  getUserInfo,
  getVenderAllReviewApi,
  guestLoginApi,
  loginOptSendApi,
  loginOptVerifyApi,
  makeOrderRefundApi,
  onesMoreResApi,
  orderHistoryApi,
  pendingOrderRatingApi,
  privacyPolicyApi,
  promoCodeApplyApi,
  reOrderApi,
  rechargeWalletApi,
  referAmountApi,
  registerUserApi,
  removeEmptyCartApi,
  removeFavDishesApi,
  removeFavRestaurantsApi,
  restaurantToExploreApi,
  saveDriverRateDataApi,
  saveFeedbackApi,
  saveOrderRatingApi,
  sendFcmTokenApi,
  tempRechargeWalletApi,
  termsAndConditionsApi,
  updateCartApi,
  updateDeliveryAddressApi,
  userAllTransactionApi,
} from './CallApi';
import Screens from '../Stack/Screens';
import {addLocation} from '../Redux/Slices/CommonSlices/LocationSlice';
import {
  addRestaurantToExploreData,
  addRestaurantToExploreMoreData,
} from '../Redux/Slices/RestaurantsSlices/RestaurantToExploreSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AsKey from '../../Constants/AsKey';
import {
  getFilterApply,
  getRestaurantDetails,
  updateCartId,
} from '../Redux/Slices/RestaurantsSlices/RestaurantDetailsSlice';
import {
  addFavRestaurants,
  getAllFavRestaurantsList,
  getLocationWhileFavRes,
  removeFavRestaurants,
} from '../Redux/Slices/FavoritesRestaurantsSlice';
import {
  addFavDishes,
  getAllFavDishesList,
  removeFavDishes,
} from '../Redux/Slices/FavoritesDishesSlice';
import {getSocialMedia} from '../Redux/Slices/SocialMediaSlice';
import {getUserInfoList} from '../Redux/Slices/GetUserInfoSlice';
import {setAccessToken} from '../../Constants/GlobalFunction';
import CallInitialApi from '../../Constants/CallInitiaclApi';
import {
  getAboutUsData,
  getCancellationPolicy,
  getFaqs,
  getPrivacyPolicy,
  getTermsAndConditionData,
} from '../Redux/Slices/AboutDataSlice';
import {
  getRefMessage,
  getWalletData,
  getWalletTransaction,
} from '../Redux/Slices/WalletSlice';
import {
  addOrderHistoryMoreData,
  getOrderHistory,
} from '../Redux/Slices/OrderHistorySlice';
import {
  addCategoriesRestaurantMoreData,
  addFoodForYouRestaurantMoreData,
  getFoodCategoriesData,
  getFoodCategoriesRestaurantData,
  getFoodForYouData,
  getFoodForYouRestaurantData,
} from '../Redux/Slices/FoodForYouSlice';
import {
  getCartList,
  getCartListLoading,
  getCartListRefresh,
  getPromoCodeList,
} from '../Redux/Slices/RestaurantsSlices/CartSlice';
import {Alert, Platform} from 'react-native';
import MyData from '../../Constants/MyData';
import axios from 'axios';
import {
  getMostViewVendorList,
  getResMasterBlogList,
  getTopRatedResList,
  onesMoreResList,
} from '../Redux/Slices/ResHomeDataSlice';
import {getUpdateVersion} from './CallApi';

export const locationCall = async ({dispatch, location, setLoading}) => {
  AsyncStorage.setItem(AsKey.userLocation, JSON.stringify(location));
  dispatch(addLocation(location));
  let body = {
    lat: location.lat,
    lng: location.lng,
    offset: 0,
    limit: 10,
  };
  let favBody = {
    lat: location.lat,
    lng: location.lng,
  };
  await getFoodForYouApiCall({
    dispatch: dispatch,
    body: favBody,
    isFirst: true,
  });
  await getRestHomeBannerCall({dispatch, location: favBody});
  // await getFoodCategoriesApiCall({dispatch: dispatch, body: favBody});
  // await getResMasterBlogCall({dispatch: dispatch, body: favBody});
  // await onesMoreResApiCall({dispatch: dispatch, body: favBody});
  // await getAllFavRestaurantsApiCall({dispatch: dispatch, body: favBody});
  await restaurantToExploreApiCall({
    dispatch: dispatch,
    body: body,
    isFirst: true,
    setLoading: setLoading,
  });
  // await getTopRatedResApiCall({dispatch: dispatch, body: body});
};

export const getHomeBannerCall = dispatch => {
  let body = {
    for: 'cheflab',
  };
  getHomeBanner(body)
    .then(res => {
      dispatch(addHomeBanners(res));
    })
    .catch(err => {
      console.warn('err getHomeBanner', err);
    });
};

export const getRestHomeBannerCall = ({dispatch, location}) => {
  let body = {
    lat: location?.lat,
    lng: location?.lng,
    for: 'restaurant',
  };
  getRestHomeBanner(body)
    .then(res => {
      dispatch(addRestHomeBanner(res));
    })
    .catch(err => {
      console.warn('err getRestHomeBannerCall', err);
    });
};

export const getRestTrackBannerCall = ({body, dispatch}) => {
  getRestHomeBanner(body)
    .then(res => {
      dispatch(addRestTrackBanner(res?.response));
    })
    .catch(err => {
      console.warn('err getRestTrackBannerCall', err);
    });
};

export const getDeliveryAddressCall = async ({
  dispatch,
  userId,
  isFromCart,
  navigation,
}) => {
  console.log('userId', userId);
  let body = {user_id: userId};
  await getDeliveryAddress(body)
    .then(res => {
      dispatch(getDeliveryAddressList(res?.response));
      if (isFromCart) {
        res?.response?.map((item, index) => {
          if (item?.primary_key == 1) {
            // setDefaultAddress(index);
            navigation.navigate(Screens.Cart, {
              updateLocation: item,
              isFromAdd: true,
            });
          }
        });
      }
      if (res?.response?.length == 0) {
        dispatch(isNoAddressAvailable(true));
      } else {
        dispatch(isNoAddressAvailable(false));
      }
    })
    .catch(err => {
      console.warn('getDeliveryAddressCall err', err);
    });
};

export const deleteDeliveryAddressCall = ({dispatch, userId, id}) => {
  let body = {user_id: userId, id: id};
  deleteDeliveryAddressApi(body)
    .then(res => {
      Utility.showToast(res?.message);
      dispatch(deleteDeliveryAddress(id));
    })
    .catch(err => {
      console.warn('getDeliveryAddressCall err', err);
    });
};

export const addDeliveryAddressApiCall = ({
  dispatch,
  userId,
  data,
  location,
  setAddDeliveryVisible,
  navigation,
  isFromCart,
}) => {
  if (!_.trim(data?.fullAddress)) {
    Utility.showToast('Please Enter Address');
  } else if (!_.trim(data?.landmark)) {
    Utility.showToast('Please Enter Landmark');
  } else if (!_.trim(data?.contact)) {
    Utility.showToast('Please Enter Contact Number');
  } else if (!_.trim(data?.defaultType)) {
    Utility.showToast('Please Select Address Type');
  } else {
    let body = {
      user_id: userId,
      house_no: data?.fullAddress,
      reach: data?.landmark,
      contact_no: data?.contact,
      address_type: data?.defaultType,
      lat: location?.lat,
      long: location?.lng,
      is_primary: isFromCart ? 1 : data?.isPrimary ? 1 : 0,
    };
    addDeliveryAddressApi(body)
      .then(res => {
        setAddDeliveryVisible(false);
        console.log('isFromCart at api', isFromCart);
        Utility.showToast('Add Address Successfully');
        if (isFromCart) {
          getDeliveryAddressCall({dispatch, userId, isFromCart, navigation});
        } else {
          getDeliveryAddressCall({dispatch, userId});
          navigation.goBack();
        }
        const locationObg = {
          address_type: data?.defaultType,
          fullAddress: data?.fullAddress,
          lat: location.lat,
          lng: location.lng,
        };
        data?.isPrimary &&
          dispatch &&
          locationCall({dispatch: dispatch, location: locationObg});
      })
      .catch(err => {
        console.warn('addDeliveryAddressApi err', err.error[0]);
      });
  }
};

export const updateDeliveryAddressApiCall = ({
  dispatch,
  userId,
  data,
  location,
  setUpdateModalVisible,
  navigation,
  isOnlyPrimaryUpdate,
  setLoading,
}) => {
  if (!_.trim(data?.fullAddress)) {
    Utility.showToast('Please Enter Address');
  } else if (!_.trim(data?.landmark)) {
    Utility.showToast('Please Enter Landmark');
  } else if (!_.trim(data?.contact)) {
    Utility.showToast('Please Enter Contact Number');
  } else if (!_.trim(data?.defaultType)) {
    Utility.showToast('Please Select Address Type');
  } else {
    let body = {
      user_id: userId,
      house_no: data?.fullAddress,
      reach: data?.landmark,
      contact_no: data?.contact,
      address_type: data?.defaultType,
      lat: location?.lat,
      long: location?.lng,
      is_primary: data?.isPrimary ? 1 : 0,
      id: data?.addressId,
    };
    updateDeliveryAddressApi(body)
      .then(async res => {
        // dispatch(addDeliveryAddress(body));
        const locationObg = {
          address_type: data?.defaultType,
          fullAddress: data?.fullAddress,
          lat: location.lat,
          lng: location.lng,
        };
        data?.isPrimary &&
          dispatch &&
          (await locationCall({
            dispatch: dispatch,
            location: locationObg,
            setLoading,
          }));
        const cartBody = {
          user_id: userId,
          lat: location?.lat,
          lng: location?.lng,
        };
        await getCartListApiCall({
          body: cartBody,
          dispatch,
          isRechargeDone: true,
        });
        await getDeliveryAddressCall({dispatch, userId});
        if (isOnlyPrimaryUpdate) {
          setUpdateModalVisible(false);
        } else {
          navigation.navigate(Screens.SavedAddress);
          setUpdateModalVisible(false);
        }
        await Utility.showToast(res?.message);
      })
      .catch(err => {
        console.warn('updateDeliveryAddressApi err', err);
      });
  }
};

export const restaurantToExploreApiCall = async ({
  body,
  dispatch,
  isFirst,
  setLoading,
}) => {
  await restaurantToExploreApi(body)
    .then(res => {
      if (body.offset == 0) {
        dispatch(addRestaurantToExploreData(res.response));
      } else {
        dispatch(addRestaurantToExploreMoreData(res.response));
      }
      isFirst &&
        getTopRatedResApiCall({dispatch: dispatch, body: body, setLoading});
    })
    .catch(err => {
      console.warn('restaurantToExploreApiCall err', err);
    });
};

export const getResDetailPageApiCall = ({body, dispatch, setIsFilterLoad}) => {
  setIsFilterLoad && setIsFilterLoad(true);
  if (body?.vendor_id) {
    getResDetailPageApi(body)
      .then(res => {
        dispatch(getRestaurantDetails(res.response));
        setIsFilterLoad && setIsFilterLoad(false);
      })
      .catch(err => {
        console.warn('getResDetailPageApiCall err', err);
        setIsFilterLoad && setIsFilterLoad(false);
      });
  } else {
    setIsFilterLoad && setIsFilterLoad(false);
  }
};

export const getAllFavRestaurantsApiCall = async ({body, dispatch}) => {
  await getAllFavRestaurantsApi(body)
    .then(res => {
      dispatch(getAllFavRestaurantsList(res.vendors));
      dispatch(getLocationWhileFavRes(body));
    })
    .catch(err => {
      console.warn('getAllFavRestaurantsApiCall err', err);
    });
};

export const removeFavRestaurantsApiCall = ({body, dispatch, vendorId}) => {
  removeFavRestaurantsApi(body)
    .then(res => {
      dispatch(removeFavRestaurants(vendorId));
    })
    .catch(err => {
      console.warn('removeFavRestaurantsApiCall err', err);
    });
};

export const addFavRestaurantsApiCall = ({body, dispatch, item}) => {
  addFavRestaurantsApi(body)
    .then(res => {
      dispatch(addFavRestaurants(item));
    })
    .catch(err => {
      console.warn('addFavRestaurantsApi err', err);
    });
};

export const getAllFavDishesApiCall = ({body, dispatch}) => {
  getAllFavDishesApi(body)
    .then(res => {
      dispatch(getAllFavDishesList(res.response));
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const addFavDishesApiCall = ({body}) => {
  addFavDishesApi(body)
    .then(res => {})
    .catch(err => {
      console.warn('addFavDishesApiCall err', err);
    });
};

export const removeFavDishesApiCall = ({body, dispatch, productId}) => {
  removeFavDishesApi(body)
    .then(res => {
      dispatch(removeFavDishes(productId));
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const getSocialMediaApiCall = ({dispatch}) => {
  getSocialMediaApi()
    .then(res => {
      dispatch(getSocialMedia(res?.response));
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const getUserInfoCall = ({userId, dispatch}) => {
  const body = {user_id: userId};
  getUserInfo()
    .then(res => {
      dispatch(getUserInfoList(res?.response));
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const saveFeedbackApiCall = ({body, navigation}) => {
  saveFeedbackApi(body)
    .then(res => {
      navigation.goBack();
      Utility.showToast('FeedBack Send Successfully');
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const loginOptSendApiCall = ({body}) => {
  loginOptSendApi(body)
    .then(res => {
      console.log('res', res);
      Utility.showToast(res?.message);
      return true;
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
      return false;
    });
};

export const loginOptVerifyApiCall = ({body, navigation, dispatch}) => {
  loginOptVerifyApi(body)
    .then(res => {
      if (res?.status) {
        console.log('res', res);
        Utility.showToast(res?.message);
        if (res?.token?.is_exists) {
          navigation.reset({routes: [{name: Screens.BottomStack}]});
          AsyncStorage.setItem(
            AsKey.userToken,
            JSON.stringify(res?.token?.token),
          );
          AsyncStorage.setItem(AsKey.userLoginData, JSON.stringify(res?.token));
          AsyncStorage.setItem(AsKey.isGuestAccount, JSON.stringify(false));
          setAccessToken(res?.token?.token);
          CallInitialApi({dispatch});
        } else {
          navigation.navigate(Screens.SignUp, {
            mobileNumber: body?.mobile_number,
          });
        }
      } else {
        Utility.showToast(res?.error);
      }
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const registerUserApiCall = ({body, navigation, dispatch}) => {
  registerUserApi(body)
    .then(res => {
      if (res?.status) {
        Utility.showToast(res?.message);
        navigation.reset({routes: [{name: Screens.BottomStack}]});
        AsyncStorage.setItem(
          AsKey.userToken,
          JSON.stringify(res?.token?.token),
        );
        AsyncStorage.setItem(AsKey.userLoginData, JSON.stringify(res?.token));
        AsyncStorage.setItem(AsKey.isGuestAccount, JSON.stringify(false));
        setAccessToken(res?.token?.token);
        CallInitialApi({dispatch});
      } else {
        Utility.showToast(res?.error[0]);
      }
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const deleteUserApiCall = ({body, navigation}) => {
  deleteUserApi(body)
    .then(res => {
      if (res?.status) {
        navigation.reset({routes: [{name: Screens.Login}]});
        AsyncStorage.clear();
        Utility.showToast('Delete Account Successfully');
      }
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const aboutUsApiCall = ({dispatch}) => {
  aboutUsApi()
    .then(res => {
      if (res?.status) {
        dispatch(getAboutUsData(res?.response[0].aboutus));
      }
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const termsAndConditionsApiCall = ({dispatch}) => {
  termsAndConditionsApi()
    .then(res => {
      if (res?.status) {
        dispatch(
          getTermsAndConditionData(res?.response[0].terms_conditions_user),
        );
      }
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const privacyPolicyApiCall = ({dispatch}) => {
  privacyPolicyApi()
    .then(res => {
      if (res?.status) {
        dispatch(getPrivacyPolicy(res?.response[0].user_privacy_policy));
      }
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const cancellationPolicyApiCall = ({dispatch}) => {
  cancellationPolicyApi()
    .then(res => {
      if (res?.status) {
        dispatch(
          getCancellationPolicy(res?.response[0].refund_cancellation_user),
        );
      }
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const getFaqsApiCall = ({dispatch}) => {
  getFaqsApi()
    .then(res => {
      if (res?.status) {
        dispatch(getFaqs(res?.response));
      }
    })
    .catch(err => {
      console.warn('getAllFavDishesApiCall err', err);
    });
};

export const tempRechargeWalletApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    tempRechargeWalletApi(body)
      .then(res => {
        if (res?.status) {
          resolve(res?.transaction_id);
        }
      })
      .catch(err => {
        console.warn('getOrderDetailsApiCall err', err);
      });
  });
};

export const checkRechargeWalletApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    checkRechargeWalletApi(body)
      .then(res => {
        if (res?.status) {
          resolve(res);
        }
      })
      .catch(err => {
        console.warn('getOrderDetailsApiCall err', err);
      });
  });
};

export const rechargeWalletApiCall = ({
  body,
  setRechargeModalVisible,
  cartBody,
  dispatch,
}) => {
  rechargeWalletApi(body)
    .then(res => {
      const transBody = {
        userId: cartBody?.user_id,
        dispatch: dispatch,
      };
      userAllTransactionApiCall(transBody);
      setTimeout(() => {
        console.log('first getCartListApiCall rechargeWalletApiCall');
        getCartListApiCall({body: cartBody, dispatch, isRechargeDone: true});
      }, 1000);
      setRechargeModalVisible(false);
      Alert.alert(
        'Recharge Success',
        '₹' + body?.amount + ' has been successfully credited in your wallet.',
      );
    })
    .catch(err => {
      console.warn('rechargeWalletApiCall err', err);
      setRechargeModalVisible(false);
    });
};

export const userAllTransactionApiCall = ({dispatch, userId}) => {
  const body = {user_id: userId};
  referAmountApi(body)
    .then(res => {
      if (res?.status) {
        dispatch(getWalletTransaction(res?.transactions));
        dispatch(getWalletData(res?.respons[0]));
        dispatch(getRefMessage(res?.reff_message));
      }
    })
    .catch(err => {
      console.warn('userAllTransactionApiCall err', err);
    });
};

export const orderHistoryApiCall = ({
  dispatch,
  body,
  navigation,
  createOrderId,
}) => {
  orderHistoryApi(body)
    .then(res => {
      if (res?.status) {
        if (body.offset == 0) {
          dispatch(getOrderHistory(res?.response));
          createOrderId &&
            navigation.navigate(Screens.OrderPlaced, {orderId: createOrderId});
        } else {
          dispatch(addOrderHistoryMoreData(res.response));
        }
      }
    })
    .catch(err => {
      console.warn('orderHistoryApi err', err);
    });
};

export const getOrderDetailsApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    body?.order_id &&
      getOrderDetailsApi(body)
        .then(res => {
          if (res?.status) {
            resolve(res?.response);
          }
        })
        .catch(err => {
          console.warn('getOrderDetailsApiCall err', err);
        });
  });
};

export const reOrderApiCall = ({body, myBody, dispatch}) => {
  return new Promise((resolve, reject) => {
    reOrderApi(body)
      .then(res => {
        if (res?.status) {
          resolve(res?.response);
          console.log('first getCartListApiCall reOrderApiCall');
          getCartListApiCall({body: myBody, dispatch: dispatch});
        }
      })
      .catch(err => {
        console.warn('reOrderApiCall err', err);
      });
  });
};

export const getFoodForYouApiCall = async ({dispatch, body, isFirst}) => {
  await getFoodForYouApi(body)
    .then(res => {
      dispatch(getFoodForYouData(res?.response));
      isFirst &&
        getFoodCategoriesApiCall({
          dispatch: dispatch,
          body: body,
          isFirst: isFirst,
        });
      // dispatch(addOrderHistoryMoreData(res.response));
    })
    .catch(err => {
      console.warn('getFoodForYouApiCall err', err);
    });
};

export const getFoodCategoriesApiCall = async ({dispatch, body, isFirst}) => {
  await getFoodCategoriesApi(body)
    .then(res => {
      dispatch(getFoodCategoriesData(res?.response));
      isFirst &&
        getMostViewVendorApiCall({
          dispatch: dispatch,
          body: body,
          isFirst: isFirst,
        });
      // dispatch(addOrderHistoryMoreData(res.response));
    })
    .catch(err => {
      console.warn('getFoodCategoriesApiCall err', err);
    });
};

export const getFoodForYouRestaurantsApiCall = ({
  body,
  dispatch,
  setLoading,
}) => {
  getFoodForYouRestaurantsApi(body)
    .then(res => {
      setTimeout(() => {
        setLoading && setLoading(false);
      }, 1000);
      if (res?.status) {
        if (body.vendor_offset == 0) {
          dispatch(getFoodForYouRestaurantData(res?.response?.vendors));
        } else {
          dispatch(addFoodForYouRestaurantMoreData(res?.response?.vendors));
        }
      }
    })
    .catch(err => {
      console.warn('getFoodForYouRestaurantsApi err', err);
      setLoading && setLoading(false);
    });
};

export const getRestaurantFoodCategoriesApiCall = ({
  body,
  dispatch,
  setLoading,
}) => {
  getRestaurantFoodCategoriesApi(body)
    .then(res => {
      if (res?.status) {
        setTimeout(() => {
          setLoading && setLoading(false);
        }, 1000);
        if (body.vendor_offset == 0) {
          dispatch(getFoodCategoriesRestaurantData(res?.response?.vendors));
        } else {
          dispatch(addCategoriesRestaurantMoreData(res?.response?.vendors));
        }
      }
    })
    .catch(err => {
      setLoading && setLoading(false);
      console.warn('getRestaurantFoodCategoriesApi err', err);
    });
};

export const getCartListApiCall = async ({body, dispatch, isRechargeDone}) => {
  const newBody = {
    user_id: body?.user_id,
    lat: body?.lat,
    lng: body?.lng,
    discount_amount: body?.discount_amount ? body?.discount_amount : 0,
    delivery_check: true,
  };
  console.log('newBody getCartListApiCall', newBody);
  body?.user_id &&
    (await getCartListApi(newBody)
      .then(res => {
        if (res?.status) {
          dispatch(updateCartId(res?.response));
          if (isRechargeDone) {
            dispatch(getCartListRefresh(res?.response));
            dispatch(getCartListLoading(false));
          } else {
            dispatch(getCartList(res?.response));
            dispatch(getCartListLoading(false));
          }
        }
      })
      .catch(err => {
        dispatch(updateCartId());
        dispatch(getCartListLoading(false));
        console.warn('getCartListApi err', err);
        if (err.error == 'Cart not found') {
          dispatch(getCartList(null));
        } else if (err?.error == 'your cart is empty') {
          dispatch(getCartList(null));
        } else if (err?.error == 'Vendor Not Found') {
          let newBody = {
            user_id: body?.user_id,
          };
          removeEmptyCartApiCall({body: newBody, dispatch});
        }
      }));
};

export const getPromoCodeDetailApiCall = ({body, dispatch}) => {
  getPromoCodeDetailApi(body)
    .then(res => {
      if (res?.status) {
        dispatch(getPromoCodeList(res?.response));
      }
    })
    .catch(err => {
      console.warn('getPromoCodeDetailApi err', err);
    });
};

export const promoCodeApplyApiCall = ({body, dispatch}) => {
  return new Promise((resolve, reject) => {
    promoCodeApplyApi(body)
      .then(res => {
        if (res?.status) {
          resolve(res.response);
        }
      })
      .catch(err => {
        console.warn('promoCodeApplyApi err', err);
      });
  });
};

export const getCustomizableDataApiCall = ({body, dispatch}) => {
  return new Promise((resolve, reject) => {
    getCustomizableDataApi(body)
      .then(res => {
        if (res?.status) {
          resolve(res.response);
        }
      })
      .catch(err => {
        console.warn('promoCodeApplyApi err', err);
      });
  });
};

export const getRestDetailApiCall = ({body, dispatch}) => {
  return new Promise((resolve, reject) => {
    getRestDetailApi(body)
      .then(res => {
        if (res?.status) {
          resolve(res.response);
        }
      })
      .catch(err => {
        console.warn('promoCodeApplyApi err', err);
      });
  });
};

export const updateCartApiCall = ({body, dispatch, empty}) => {
  return new Promise((resolve, reject) => {
    const newDataBody = JSON.stringify(body);
    const myBody = {
      user_id: body?.user_id,
    };
    let updateBody = empty ? newDataBody : body;
    console.log('updateBody Api side ======>', JSON.stringify(updateBody));
    updateCartApi(updateBody)
      .then(res => {
        resolve(res);
        console.log('first getCartListApiCall updateCartApiCall');
        getCartListApiCall({body: myBody, dispatch: dispatch});
      })
      .catch(err => {
        console.warn('updateCartApiCall err', err);
        getCartListApiCall({body: myBody, dispatch: dispatch});
      });
  });
};

export const addToCartApiCall = ({body, dispatch}) => {
  const newDataBody = JSON.stringify(body);
  console.log('addToCartApiCall newDataBody', newDataBody);
  addToCartApi(newDataBody)
    .then(res => {
      const myBody = {
        user_id: body?.user_id,
      };
      console.log('first getCartListApiCall addToCartApiCall');
      getCartListApiCall({body: myBody, dispatch: dispatch});
    })
    .catch(err => {
      console.warn('addToCartApiCall err', err);
    });
};

export const getResDetailByFoodTypeApiCall = ({
  body,
  dispatch,
  setIsFilterLoad,
}) => {
  setIsFilterLoad(true);
  getResDetailByFoodTypeApi(body)
    .then(res => {
      dispatch(getFilterApply(res?.response));
      setIsFilterLoad(false);
    })
    .catch(err => {
      console.warn('addToCartApiCall err', err);
      setIsFilterLoad(false);
    });
};

export const getVenderAllReviewApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    getVenderAllReviewApi(body)
      .then(res => {
        if (res?.status) {
          resolve(res.response);
        }
      })
      .catch(err => {
        console.warn('getVenderAllReviewApi err', err);
      });
  });
};

export const getSearchDishesInResDetailCall = ({body}) => {
  return new Promise((resolve, reject) => {
    getSearchDishesInResDetail(body)
      .then(res => {
        if (res?.status) {
          resolve(res.response);
        }
      })
      .catch(err => {
        console.warn('getVenderAllReviewApi err', err);
      });
  });
};

export const getSearchDataApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    getSearchDataApi(body)
      .then(res => {
        if (res?.status) {
          resolve(res.response);
        }
      })
      .catch(err => {
        console.warn('getSearchDataApi err', err);
      });
  });
};

export const createOrderIdForRazorPay = ({
  amount,
  tempOrderId,
  transaction_for,
}) => {
  return new Promise((resolve, reject) => {
    // fetch('https://api.razorpay.com/v1/orders', {
    //   method: 'post',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   auth: {
    // username: MyData.razorPayKey,
    // password: MyData.razorPaySecretCode,
    //   },
    //   body: {
    //     amount: amount,
    //     currency: 'INR',
    //     receipt: 'Receipt no. 1',
    //     notes: {
    // transaction_for: 'order',
    // order_transaction_id: 'jk',
    //     },
    //   },
    // })
    axios({
      url: 'https://api.razorpay.com/v1/orders',
      method: 'Post',
      headers: {
        'Content-Type': 'application/json',
      },
      auth: {
        username: MyData.razorPayKey,
        password: MyData.razorPaySecretCode,
      },
      data: {
        amount: amount,
        currency: 'INR',
        receipt: 'Receipt no. 1',
        notes: {
          transaction_for: transaction_for,
          order_transaction_id: tempOrderId,
        },
      },
    })
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        console.warn('err createOrderIdForRazorPay', err);
      });
  });
};

export const razorPayCheckOutFunction = ({
  body,
  cartBody,
  dispatch,
  setLoading,
}) => {
  return new Promise((resolve, reject) => {
    RazorpayCheckout.open(body)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        setLoading && setLoading(false);
        console.warn('err razorPayCheckOutFunction', err);
        reject(err);
        cartBody &&
          getCartListApiCall({
            body: cartBody,
            dispatch,
          });
      });
  });
};

export const pendingOrderRatingApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    pendingOrderRatingApi(body)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        console.warn('err pendingOrderRatingApiCall', err);
      });
  });
};

export const getDriverRateDataApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    getDriverRateDataApi(body)
      .then(res => {
        resolve(res?.response);
      })
      .catch(err => {
        console.warn('err getDriverRateDataApiCall', err);
      });
  });
};

export const saveDriverRateDataApiCall = ({body, navigation}) => {
  saveDriverRateDataApi(body)
    .then(res => {
      navigation.navigate(Screens.BottomStack, {
        screens: Screens.Home,
        routes: {isFromRate: true},
      });
    })
    .catch(err => {
      console.warn('err saveDriverRateDataApiCall', err);
    });
};

export const detailOrderRatingApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    detailOrderRatingApi(body)
      .then(res => {
        resolve(res?.response);
      })
      .catch(err => {
        console.warn('err detailOrderRatingApiCall', err);
      });
  });
};

export const removeEmptyCartApiCall = ({body, dispatch}) => {
  removeEmptyCartApi(body)
    .then(res => {
      dispatch(getCartList(null));
    })
    .catch(err => {
      console.warn('err removeEmptyCartApiCall', err);
    });
};

export const saveOrderRatingApiCall = ({body, navigation}) => {
  saveOrderRatingApi(body)
    .then(res => {
      navigation.navigate(Screens.BottomStack, {screens: Screens.Home});
    })
    .catch(err => {
      console.warn('err saveOrderRatingApiCall', err);
    });
};

export const createOrderApiCall = ({body, dispatch, navigation}) => {
  createOrderApi(body)
    .then(async res => {
      let orderHisBody = {
        order_for: 'restaurant',
        offset: 0,
      };
      orderHistoryApiCall({
        dispatch: dispatch,
        body: orderHisBody,
        navigation,
        createOrderId: res?.response?.order_id,
      });
      const removeBody = {
        user_id: body?.user_id,
      };
      removeEmptyCartApiCall({body: removeBody, dispatch});
    })
    .catch(err => {
      console.warn('err createOrderApiCall', err?.error);
      if (err?.error == 'Vendor not available') {
        const refundBody = {amount: err?.gateway_amount};
        makeOrderRefundApiCall({body: refundBody});
      }
    });
};

export const createTempOrderApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    createTempOrderApi(body)
      .then(res => {
        resolve(res?.response);
      })
      .catch(err => {
        console.warn('err createTempOrderApiCall', err);
      });
  });
};

export const getTempOrderDetailApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    getTempOrderDetailApi(body)
      .then(res => {
        resolve(res?.response);
      })
      .catch(err => {
        console.warn('err getTempOrderDetailApiCall', err);
      });
  });
};

export const sendFcmTokenApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    sendFcmTokenApi(body)
      .then(res => {
        resolve(res?.response);
      })
      .catch(err => {
        console.warn('err sendFcmTokenApi', err);
      });
  });
};

export const makeOrderRefundApiCall = ({body}) => {
  makeOrderRefundApi(body)
    .then(res => {
      Alert.alert(
        'Opps...!',
        'Vendor is not available, your will get refund soon.',
      );
    })
    .catch(err => {
      console.warn('err RazorpayCheckout', err);
    });
};

export const guestLoginApiCall = ({navigation, dispatch}) => {
  guestLoginApi(null)
    .then(res => {
      if (res?.status) {
        Utility.showToast(res?.message);
        navigation.reset({routes: [{name: Screens.BottomStack}]});
        AsyncStorage.setItem(
          AsKey.userToken,
          JSON.stringify(res?.token?.token),
        );
        AsyncStorage.setItem(AsKey.userLoginData, JSON.stringify(res?.token));
        AsyncStorage.setItem(AsKey.isGuestAccount, JSON.stringify(true));
        setAccessToken(res?.token?.token);
        CallInitialApi({dispatch});
      } else {
        Utility.showToast(res?.error);
      }
    })
    .catch(err => {
      console.warn('err guestLoginApiCall', err);
    });
};

export const guestAccountHandler = ({navigation}) => {
  Alert.alert('Hold on !', 'You need to Create Account to use this Feature', [
    {
      text: 'Cancel',
      onPress: () => console.warn('Cancel Pressed'),
      // style: 'cancel',
    },
    {
      text: 'OK',
      onPress: () => {
        AsyncStorage.clear();
        navigation.reset({routes: [{name: Screens.Login}]});
      },
    },
  ]);
};

export const getTopRatedResApiCall = async ({body, dispatch, setLoading}) => {
  await getTopRatedResApi(body)
    .then(res => {
      dispatch(getTopRatedResList(res?.response?.slice(0, 10)));
    })
    .catch(err => {
      console.warn('err getTopRatedResApiCall', err);
    });
};

export const onesMoreResApiCall = async ({body, dispatch, isFirst}) => {
  await onesMoreResApi(body)
    .then(res => {
      dispatch(onesMoreResList(res?.response?.slice(0, 10)));
      isFirst &&
        getAllFavRestaurantsApiCall({dispatch: dispatch, body: body, isFirst});
    })
    .catch(err => {
      console.warn('err onesMoreResApiCall', err);
    });
};

export const getResMasterBlogCall = async ({body, dispatch, isFirst}) => {
  await getResMasterBlog(body)
    .then(res => {
      dispatch(getResMasterBlogList(res?.response));
      isFirst && onesMoreResApiCall({dispatch: dispatch, body: body});
    })
    .catch(err => {
      console.warn('err getResMasterBlogCall', err);
    });
};

export const getMostViewVendorApiCall = async ({body, dispatch, isFirst}) => {
  await getMostViewVendorApi(body)
    .then(res => {
      dispatch(getMostViewVendorList(res?.response));
      isFirst && getResMasterBlogCall({dispatch: dispatch, body: body});
    })
    .catch(err => {
      console.warn('err getMostViewVendorApiCall', err);
    });
};

export const checkUpdateAvailable = ({body}) => {
  return new Promise((resolve, reject) => {
    if (Platform.OS == 'android') {
      getUpdateVersion(body)
        .then(res => {
          resolve(res?.data);
        })
        .catch(err => {
          console.warn('err getUpdateVersion', err);
        });
    } else {
      getIosUpdateVersion(body)
        .then(res => {
          resolve(res?.data);
          console.log('res', res);
        })
        .catch(err => {
          console.warn('err getUpdateVersion', err);
        });
    }
  });
};

export const cancelOrderApiCall = ({body}) => {
  return new Promise((resolve, reject) => {
    cancelOrderApi(body)
      .then(res => {
        resolve(res?.data);
      })
      .catch(err => {
        console.warn('err getUpdateVersion', err);
        reject(err);
      });
  });
};
