import {baseApiCall} from './BaseApiCall';
import {
  ABOUT_US,
  ADD_DELIVERY_ADDRESS,
  ADD_TO_CART,
  CANCELLATION_POLICY,
  CANCEL_ORDER,
  CHECK_RECHARGE_WALLET,
  CREATE_ORDER,
  CREATE_TEMP_ORDER,
  CUSTOMIZABLE_DATA,
  DELETE_USER,
  DELIVERY_ADDRESS_DELETE,
  DETAIL_ORDER_RATING,
  DISLIKE_DISHES,
  DISLIKE_VENDOR,
  GET_ALL_FAV_DISHES,
  GET_ALL_FAV_RESTAURANTS,
  GET_CUISINES,
  GET_DRIVER_RATE_DATA,
  GET_FOOD_CATEGORIES,
  GET_IOS_UPDATE_VERSION,
  GET_MOST_VIEW_VENDOR,
  GET_ORDER,
  GET_ORDER_DETAILS,
  GET_RESTAURANTS_BY_CUISINES,
  GET_RESTAURANT_DETAILS,
  GET_RESTAURANT_DETAILS_PAGE,
  GET_RESTAURANT_FOOD_CATEGORIES,
  GET_RES_MASTER_BLOG,
  GET_TEMPORARY_ORDER_DETAIL,
  GET_UPDATE_VERSION,
  GET_USER_INFO,
  GET_VENDER_ALL_REVIEW,
  GUEST_LOGIN,
  LIKE_DISHES,
  LIKE_VENDOR,
  LOGIN_OTP_SEND,
  LOGIN_OTP_VERIFY,
  MAKE_ORDER_REFUND,
  ONES_MORE_RES,
  PENDING_ORDER_RATING,
  PRIVACY_AND_POLICY,
  PROMO_CODE_APPLY,
  PROMO_CODE_DETAIL,
  RECHARGE_WALLET,
  REFER_AMOUNT,
  REGISTER_VERIFIED_USER,
  REMOVE_EMPTY_CART,
  RESTAURANT_DETAILS_BY_FOOD_TYPE,
  RESTAURANT_TO_EXPLORE,
  RE_ORDER,
  SAVE_CONTACT_US,
  SAVE_ORDER_RATE,
  SAVE_RIDER_RATE,
  SEARCH_DATA,
  SEARCH_RES_DETAIL,
  SEARCH_RES_DETAIL_V3,
  SOCIAL_MEDIA,
  STORE_FCM_TOKEN,
  TEMP_RECHARGE_WALLET,
  TERMS_AND_CONDITION,
  TOP_RATED_RES,
  UPDATE_CART,
  UPDATE_DELIVERY_ADDRESS,
  UPDATE_USER_INFO,
  USER_All_TRANSACTION,
  USER_FAQS,
  VIEW_CART,
} from './EndPoint';
import {
  GET_DELIVERY_ADDRESS,
  GET_HOME_BANNER,
  GET_REST_HOME_BANNER,
} from './EndPoint';

export const getHomeBanner = data => {
  return baseApiCall({
    url: GET_HOME_BANNER,
    method: 'post',
    data,
  });
};

export const getRestHomeBanner = data => {
  return baseApiCall({
    url: GET_REST_HOME_BANNER,
    method: 'post',
    data,
  });
};

export const getSearchDishesInResDetail = data => {
  return baseApiCall({
    url: SEARCH_RES_DETAIL_V3,
    method: 'post',
    data,
  });
};

export const getSearchDataApi = data => {
  return baseApiCall({
    url: SEARCH_DATA,
    method: 'post',
    data,
  });
};

export const getDeliveryAddress = data => {
  return baseApiCall({
    url: GET_DELIVERY_ADDRESS,
    method: 'post',
    data,
  });
};

export const deleteDeliveryAddressApi = data => {
  return baseApiCall({
    url: DELIVERY_ADDRESS_DELETE,
    method: 'post',
    data,
  });
};

export const addDeliveryAddressApi = data => {
  return baseApiCall({
    url: ADD_DELIVERY_ADDRESS,
    method: 'post',
    data,
  });
};

export const updateDeliveryAddressApi = data => {
  return baseApiCall({
    url: UPDATE_DELIVERY_ADDRESS,
    method: 'post',
    data,
  });
};

export const restaurantToExploreApi = data => {
  return baseApiCall({
    url: RESTAURANT_TO_EXPLORE,
    method: 'post',
    data,
  });
};

export const getResDetailByFoodTypeApi = data => {
  return baseApiCall({
    url: RESTAURANT_DETAILS_BY_FOOD_TYPE,
    method: 'post',
    data,
  });
};

export const getResDetailPageApi = data => {
  return baseApiCall({
    url: GET_RESTAURANT_DETAILS_PAGE,
    method: 'post',
    data,
  });
};

export const getAllFavRestaurantsApi = data => {
  return baseApiCall({
    url: GET_ALL_FAV_RESTAURANTS,
    method: 'post',
    data,
  });
};

export const removeFavRestaurantsApi = data => {
  return baseApiCall({
    url: DISLIKE_VENDOR,
    method: 'post',
    data,
  });
};

export const addFavRestaurantsApi = data => {
  return baseApiCall({
    url: LIKE_VENDOR,
    method: 'post',
    data,
  });
};

export const getAllFavDishesApi = data => {
  return baseApiCall({
    url: GET_ALL_FAV_DISHES,
    method: 'post',
    data,
  });
};

export const removeFavDishesApi = data => {
  return baseApiCall({
    url: DISLIKE_DISHES,
    method: 'post',
    data,
  });
};

export const addFavDishesApi = data => {
  return baseApiCall({
    url: LIKE_DISHES,
    method: 'post',
    data,
  });
};

export const getSocialMediaApi = () => {
  return baseApiCall({
    url: SOCIAL_MEDIA,
    method: 'get',
  });
};

export const getUserInfo = data => {
  return baseApiCall({
    url: GET_USER_INFO,
    method: 'post',
    data,
  });
};

export const saveFeedbackApi = data => {
  return baseApiCall({
    url: SAVE_CONTACT_US,
    method: 'post',
    data,
  });
};

export const loginOptSendApi = data => {
  return baseApiCall({
    url: LOGIN_OTP_SEND,
    method: 'post',
    data,
  });
};

export const loginOptVerifyApi = data => {
  return baseApiCall({
    url: LOGIN_OTP_VERIFY,
    method: 'post',
    data,
  });
};

export const registerUserApi = data => {
  return baseApiCall({
    url: REGISTER_VERIFIED_USER,
    method: 'post',
    data,
  });
};

export const updateUserProfile = data => {
  return baseApiCall({
    url: UPDATE_USER_INFO,
    method: 'post',
    data,
  });
};

export const deleteUserApi = data => {
  return baseApiCall({
    url: DELETE_USER,
    method: 'post',
    data,
  });
};

export const aboutUsApi = data => {
  return baseApiCall({
    url: ABOUT_US,
    method: 'get',
  });
};

export const termsAndConditionsApi = data => {
  return baseApiCall({
    url: TERMS_AND_CONDITION,
    method: 'get',
  });
};

export const privacyPolicyApi = data => {
  return baseApiCall({
    url: PRIVACY_AND_POLICY,
    method: 'get',
  });
};

export const cancellationPolicyApi = data => {
  return baseApiCall({
    url: CANCELLATION_POLICY,
    method: 'get',
  });
};

export const getFaqsApi = data => {
  return baseApiCall({
    url: USER_FAQS,
    method: 'get',
  });
};

export const rechargeWalletApi = data => {
  return baseApiCall({
    url: RECHARGE_WALLET,
    method: 'post',
    data,
  });
};

export const tempRechargeWalletApi = data => {
  return baseApiCall({
    url: TEMP_RECHARGE_WALLET,
    method: 'post',
    data,
  });
};

export const checkRechargeWalletApi = data => {
  return baseApiCall({
    url: CHECK_RECHARGE_WALLET,
    method: 'post',
    data,
  });
};

export const referAmountApi = data => {
  return baseApiCall({
    url: REFER_AMOUNT,
    method: 'post',
    data,
  });
};

export const orderHistoryApi = data => {
  return baseApiCall({
    url: GET_ORDER,
    method: 'post',
    data,
  });
};

export const getOrderDetailsApi = data => {
  return baseApiCall({
    url: GET_ORDER_DETAILS,
    method: 'post',
    data,
  });
};

export const reOrderApi = data => {
  return baseApiCall({
    url: RE_ORDER,
    method: 'post',
    data,
  });
};

export const getFoodForYouApi = data => {
  return baseApiCall({
    url: GET_CUISINES,
    method: 'post',
    data,
  });
};

export const getFoodForYouRestaurantsApi = data => {
  return baseApiCall({
    url: GET_RESTAURANTS_BY_CUISINES,
    method: 'post',
    data,
  });
};

export const getFoodCategoriesApi = data => {
  return baseApiCall({
    url: GET_FOOD_CATEGORIES,
    method: 'post',
    data,
  });
};

export const getRestaurantFoodCategoriesApi = data => {
  return baseApiCall({
    url: GET_RESTAURANT_FOOD_CATEGORIES,
    method: 'post',
    data,
  });
};

export const getCartListApi = data => {
  return baseApiCall({
    url: VIEW_CART,
    method: 'post',
    data,
  });
};

export const getPromoCodeDetailApi = data => {
  return baseApiCall({
    url: PROMO_CODE_DETAIL,
    method: 'post',
    data,
  });
};

export const promoCodeApplyApi = data => {
  return baseApiCall({
    url: PROMO_CODE_APPLY,
    method: 'post',
    data,
  });
};

export const updateCartApi = data => {
  return baseApiCall({
    url: UPDATE_CART,
    method: 'post',
    data,
  });
};

export const addToCartApi = data => {
  return baseApiCall({
    url: ADD_TO_CART,
    method: 'post',
    data,
  });
};

export const getCustomizableDataApi = data => {
  return baseApiCall({
    url: CUSTOMIZABLE_DATA,
    method: 'post',
    data,
  });
};

export const getRestDetailApi = data => {
  return baseApiCall({
    url: GET_RESTAURANT_DETAILS,
    method: 'post',
    data,
  });
};

export const getVenderAllReviewApi = data => {
  return baseApiCall({
    url: GET_VENDER_ALL_REVIEW,
    method: 'post',
    data,
  });
};

export const pendingOrderRatingApi = data => {
  return baseApiCall({
    url: PENDING_ORDER_RATING,
    method: 'post',
    data,
  });
};

export const getDriverRateDataApi = data => {
  return baseApiCall({
    url: GET_DRIVER_RATE_DATA,
    method: 'post',
    data,
  });
};

export const saveDriverRateDataApi = data => {
  return baseApiCall({
    url: SAVE_RIDER_RATE,
    method: 'post',
    data,
  });
};

export const detailOrderRatingApi = data => {
  return baseApiCall({
    url: DETAIL_ORDER_RATING,
    method: 'post',
    data,
  });
};

export const saveOrderRatingApi = data => {
  return baseApiCall({
    url: SAVE_ORDER_RATE,
    method: 'post',
    data,
  });
};

export const removeEmptyCartApi = data => {
  return baseApiCall({
    url: REMOVE_EMPTY_CART,
    method: 'post',
    data,
  });
};

export const createOrderApi = data => {
  return baseApiCall({
    url: CREATE_ORDER,
    method: 'post',
    data,
  });
};

export const createTempOrderApi = data => {
  return baseApiCall({
    url: CREATE_TEMP_ORDER,
    method: 'post',
    data,
  });
};

export const makeOrderRefundApi = data => {
  return baseApiCall({
    url: MAKE_ORDER_REFUND,
    method: 'post',
    data,
  });
};

export const getTempOrderDetailApi = data => {
  return baseApiCall({
    url: GET_TEMPORARY_ORDER_DETAIL,
    method: 'post',
    data,
  });
};

export const guestLoginApi = data => {
  return baseApiCall({
    url: GUEST_LOGIN,
    method: 'post',
    data,
  });
};

export const getTopRatedResApi = data => {
  return baseApiCall({
    url: TOP_RATED_RES,
    method: 'post',
    data,
  });
};

export const onesMoreResApi = data => {
  return baseApiCall({
    url: ONES_MORE_RES,
    method: 'post',
    data,
  });
};

export const getResMasterBlog = data => {
  return baseApiCall({
    url: GET_RES_MASTER_BLOG,
    method: 'post',
    data,
  });
};

export const getMostViewVendorApi = data => {
  return baseApiCall({
    url: GET_MOST_VIEW_VENDOR,
    method: 'post',
    data,
  });
};

export const sendFcmTokenApi = data => {
  return baseApiCall({
    url: STORE_FCM_TOKEN,
    method: 'post',
    data,
  });
};

export const getUpdateVersion = data => {
  return baseApiCall({
    url: GET_UPDATE_VERSION,
    method: 'post',
    data,
  });
};

export const getIosUpdateVersion = data => {
  return baseApiCall({
    url: GET_IOS_UPDATE_VERSION,
    method: 'post',
    data,
  });
};

export const cancelOrderApi = data => {
  return baseApiCall({
    url: CANCEL_ORDER,
    method: 'post',
    data,
  });
};
