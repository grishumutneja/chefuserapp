import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Screens from './Screens';
import LoginScreen from '../../Screens/AuthScreens/LoginScreen';
import SignUpScreen from '../../Screens/AuthScreens/SignUpScreen';
import BottomStackScreen from './BottomStackScreen';
import ProfileScreen from '../../Screens/OtherScreen/ProfileScreen';
import AboutScreen from '../../Screens/OtherScreen/AboutScreen';
import AboutDetailsScreen from '../../Screens/OtherScreen/AboutDetailsScreen';
import FaqsScreen from '../../Screens/OtherScreen/FaqsScreen';
import FavoritesScreen from '../../Screens/OtherScreen/FavoritesScreen';
import FeedBackScreen from '../../Screens/OtherScreen/FeedBackScreen';
import SavedAddressScreen from '../../Screens/OtherScreen/SavedAddressScreen';
import CartScreen from '../../Screens/OtherScreen/CartScreen';
import TrackOrderScreen from '../../Screens/OtherScreen/TrackOrderScreen';
import MapsScreen from '../../Screens/OtherScreen/MapsScreen';
import RestaurantDetailScreen from '../../Screens/Restaurant/RestaurantDetailScreen';
import RestaurantHomeScreen from '../../Screens/Restaurant/RestaurantHomeScreen';
import ChefHomeScreen from '../../Screens/Chef/ChefHomeScreen';
import ChefBrowseScreen from '../../Screens/Chef/ChefBrowseScreen';
import ChefScreen from '../../Screens/Chef/ChefScreen';
import ChefProfileScreen from '../../Screens/Chef/ChefProfileScreen';
import DishScreen from '../../Screens/Chef/DishScreen';
import DeleteScreen from '../../Screens/OtherScreen/DeleteScreen';
import RestaurantListScreen from '../../Screens/OtherScreen/RestaurantListScreen';
import SearchResDishScreen from '../../Screens/Restaurant/SearchResDishScreen';
import DriverRattingScreen from '../../Screens/OtherScreen/DriverRattingScreen';
import VendorRattingScreen from '../../Screens/OtherScreen/VendorRattingScreen';
import OrderPlacedScreen from '../../Screens/OtherScreen/OrderPlacedScreen';
import OrderCancelScreen from '../../Screens/OtherScreen/OrderCancelScreen';
import ResDishSearchScreen from '../../Screens/Restaurant/ResDishSearchScreen';

const MainStack = ({token}) => {
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      // initialRouteName={token ? Screens.SignUp : Screens.SignUp}>
      initialRouteName={token ? Screens.BottomStack : Screens.Login}>
      <Stack.Screen
        name={Screens.Login}
        component={LoginScreen}
        options={{animation: 'fade', animationDuration: 2000}}
      />
      <Stack.Screen name={Screens.SignUp} component={SignUpScreen} />
      <Stack.Screen name={Screens.BottomStack} component={BottomStackScreen} />
      <Stack.Screen name={Screens.Profile} component={ProfileScreen} />
      <Stack.Screen
        name={Screens.About}
        component={AboutScreen}
        options={{animation: 'slide_from_right', animationDuration: 1000}}
      />
      <Stack.Screen
        name={Screens.AboutDetails}
        component={AboutDetailsScreen}
        options={{animation: 'slide_from_right', animationDuration: 1000}}
      />
      <Stack.Screen
        name={Screens.Faqs}
        component={FaqsScreen}
        options={{animation: 'slide_from_right', animationDuration: 1000}}
      />
      <Stack.Screen
        name={Screens.Favorites}
        component={FavoritesScreen}
        options={{animation: 'slide_from_right', animationDuration: 1000}}
      />
      <Stack.Screen
        name={Screens.FeedBack}
        component={FeedBackScreen}
        options={{animation: 'slide_from_right', animationDuration: 1000}}
      />
      <Stack.Screen
        name={Screens.SavedAddress}
        component={SavedAddressScreen}
        options={{animation: 'slide_from_right', animationDuration: 1000}}
      />
      <Stack.Screen
        name={Screens.RestaurantHome}
        component={RestaurantHomeScreen}
        options={{animation: 'slide_from_right', animationDuration: 1000}}
      />
      <Stack.Screen
        name={Screens.RestaurantDetail}
        component={RestaurantDetailScreen}
        options={{animation: 'slide_from_right', animationDuration: 1000}}
      />
      <Stack.Screen
        name={Screens.ResDishSearch}
        component={ResDishSearchScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 1000}}
      />
      <Stack.Screen
        name={Screens.Cart}
        component={CartScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 700}}
      />
      <Stack.Screen
        name={Screens.TrackOrder}
        component={TrackOrderScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 700}}
      />
      <Stack.Screen
        name={Screens.Maps}
        component={MapsScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.ChefHome}
        component={ChefHomeScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 700}}
      />
      <Stack.Screen
        name={Screens.ChefBrowse}
        component={ChefBrowseScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.Chef}
        component={ChefScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.ChefProfile}
        component={ChefProfileScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.Dish}
        component={DishScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.Delete}
        component={DeleteScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.RestaurantList}
        component={RestaurantListScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.SearchResDish}
        component={SearchResDishScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.DriverRatting}
        component={DriverRattingScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.VendorRatting}
        component={VendorRattingScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.OrderPlaced}
        component={OrderPlacedScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
      <Stack.Screen
        name={Screens.OrderCancel}
        component={OrderCancelScreen}
        options={{animation: 'slide_from_bottom', animationDuration: 500}}
      />
    </Stack.Navigator>
  );
};

export default MainStack;
