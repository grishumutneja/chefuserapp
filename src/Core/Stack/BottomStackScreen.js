import {Image, StyleSheet, Platform, View} from 'react-native';
import React from 'react';
import Screens from './Screens';
import HomeScreen from '../../Screens/BottomScreens/HomeScreen';
import OrderHistoryScreen from '../../Screens/BottomScreens/OrderHistoryScreen';
import WalletScreen from '../../Screens/BottomScreens/WalletScreen';
import ProfileBottomScreen from '../../Screens/BottomScreens/ProfileBottomScreen';
import Colors from '../../Constants/Colors';
import Images from '../../Constants/Images';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Responsive from '../../Constants/Responsive';

const BottomStackScreen = () => {
  const Tab = createMaterialBottomTabNavigator();

  return (
    <Tab.Navigator
      shifting={true}
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        // tabBarStyle: {
        //   backgroundColor: 'red',
        //   // backgroundColor: Colors.white,
        //   height: Platform.OS === 'ios' ? 90 : 64,
        //   borderTopRightRadius: 15,
        //   borderTopLeftRadius: 15,
        // },
        // tabBarItemStyle: {
        //   marginHorizontal: 10,
        // },
        tabBarLabel: false,
      }}
      activeColor="yellow"
      // activeColor="#f0edf6"
      // inactiveColor="#3e2465"
      barStyle={{
        backgroundColor: 'white',
        // backgroundColor: Colors.white,
        height: Platform.OS === 'ios' ? 90 : 64,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        padding: 0,
        margin: 0,
        borderTopWidth: 0.2,
        borderColor: '#999',
        width: Responsive.widthPx(100),
      }}>
      <Tab.Screen
        name={Screens.Home}
        component={HomeScreen}
        theme={{
          colors: {
            secondaryContainer: 'transparent',
            tertiaryContainer: 'transparent',
            primaryContainer: 'transparent',
            surfaceVariant: 'transparent',
            primary: 'transparent',
            onPrimary: 'transparent',
            onPrimaryContainer: 'transparent',
            secondary: 'transparent',
            onSecondary: 'rgb(255, 255, 255)',
            onSecondaryContainer: 'transparent',
            tertiary: 'transparent',
            onTertiary: 'transparent',
            onTertiaryContainer: 'transparent',
            error: 'transparent',
            onError: 'transparent',
            errorContainer: 'transparent',
            onErrorContainer: 'transparent',
            background: 'transparent',
            onBackground: 'transparent',
            surface: 'transparent',
            onSurface: 'transparent',
            onSurfaceVariant: 'transparent',
            outline: 'transparent',
            outlineVariant: 'transparent',
            shadow: 'transparent',
            scrim: 'transparent',
            inverseSurface: 'transparent',
            inverseOnSurface: 'transparent',
            inversePrimary: 'transparent',
            elevation: {
              level0: 'transparent',
              level1: 'transparent',
              level2: 'transparent',
              level3: 'transparent',
              level4: 'transparent',
              level5: 'transparent',
            },
            surfaceDisabled: 'transparent',
            onSurfaceDisabled: 'transparent',
            backdrop: 'transparent',
          },
        }}
        options={{
          tabBarIcon: ({focused}) => (
            <TabBarIcon imageSource={Images.home} focused={focused} />
          ),
        }}
      />
      <Tab.Screen
        name={Screens.OrderHistory}
        component={OrderHistoryScreen}
        options={{
          tabBarIcon: ({focused}) => (
            <TabBarIcon imageSource={Images.order} focused={focused} />
          ),
        }}
      />
      <Tab.Screen
        name={Screens.Wallet}
        component={WalletScreen}
        options={{
          tabBarIcon: ({focused}) => (
            <TabBarIcon imageSource={Images.wallet} focused={focused} />
          ),
        }}
      />
      <Tab.Screen
        name={Screens.ProfileBottom}
        component={ProfileBottomScreen}
        options={{
          tabBarIcon: ({focused}) => (
            <TabBarIcon imageSource={Images.user_profile} focused={focused} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomStackScreen;

const styles = StyleSheet.create({
  text: {
    fontSize: 13,
    fontWeight: 'bold',
  },
  image: {
    width: 25,
    resizeMode: 'contain',
    height: 25,
  },
  bottomLine: {
    height: 3,
    width: '70%',
    position: 'absolute',
    bottom: 0,
    borderRadius: 15,
  },
});

const TabBarIcon = ({focused, imageSource}) => {
  return (
    <View
      style={{
        alignItems: 'center',
        // justifyContent: 'center',
        // backgroundColor: Colors.greenButtonBgColor,
        // flex: 1,
        width: 60,
        height: 40,
        transform: [{scale: focused ? 1.05 : 1}],
      }}>
      <Image
        source={imageSource}
        style={[
          styles.image,
          {tintColor: focused ? Colors.red_primary : Colors.gray},
        ]}
      />
      {focused ? (
        <View
          style={[
            {
              backgroundColor: focused ? Colors.red_primary : Colors.gray,
            },
            styles.bottomLine,
          ]}
        />
      ) : null}
    </View>
  );
};

// import {Image, StyleSheet, Platform, View} from 'react-native';
// import React from 'react';
// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
// import Screens from './Screens';
// import HomeScreen from '../../Screens/BottomScreens/HomeScreen';
// import OrderHistoryScreen from '../../Screens/BottomScreens/OrderHistoryScreen';
// import WalletScreen from '../../Screens/BottomScreens/WalletScreen';
// import ProfileBottomScreen from '../../Screens/BottomScreens/ProfileBottomScreen';
// import Colors from '../../Constants/Colors';
// import Images from '../../Constants/Images';

// const BottomStackScreen = () => {
//   const Tab = createBottomTabNavigator();

//   return (
//     <Tab.Navigator
//       screenOptions={{
//         headerShown: false,
//         tabBarShowLabel: false,
//         tabBarStyle: {
//           backgroundColor: Colors.white,
//           height: Platform.OS === 'ios' ? 90 : 64,
//           borderTopRightRadius: 15,
//           borderTopLeftRadius: 15,
//         },
//         tabBarItemStyle: {
//           marginHorizontal: 10,
//         },
//       }}
//       style={{
//         backgroundColor: Colors.white,
//         borderTopRightRadius: 15,
//         borderTopLeftRadius: 15,
//       }}>
//       <Tab.Screen
//         name={Screens.Home}
//         component={HomeScreen}
//         options={{
//           tabBarIcon: ({focused}) => (
//             <TabBarIcon imageSource={Images.home} focused={focused} />
//           ),
//         }}
//       />
//       <Tab.Screen
//         name={Screens.OrderHistory}
//         component={OrderHistoryScreen}
//         options={{
//           tabBarIcon: ({focused}) => (
//             <TabBarIcon imageSource={Images.order} focused={focused} />
//           ),
//         }}
//       />
//       <Tab.Screen
//         name={Screens.Wallet}
//         component={WalletScreen}
//         options={{
//           tabBarIcon: ({focused}) => (
//             <TabBarIcon imageSource={Images.wallet} focused={focused} />
//           ),
//         }}
//       />
//       <Tab.Screen
//         name={Screens.ProfileBottom}
//         component={ProfileBottomScreen}
//         options={{
//           tabBarIcon: ({focused}) => (
//             <TabBarIcon imageSource={Images.user_profile} focused={focused} />
//           ),
//         }}
//       />
//     </Tab.Navigator>
//   );
// };

// export default BottomStackScreen;

// const styles = StyleSheet.create({
//   text: {
//     fontSize: 13,
//     fontWeight: 'bold',
//   },
//   image: {
//     width: 25,
//     resizeMode: 'contain',
//     height: 25,
//   },
//   bottomLine: {
//     height: 3,
//     width: '70%',
//     position: 'absolute',
//     bottom: 0,
//     borderRadius: 15,
//   },
// });

// const TabBarIcon = ({focused, imageSource}) => {
//   return (
//     <View
//       style={{
//         alignItems: 'center',
//         justifyContent: 'center',
//         // backgroundColor: Colors.greenButtonBgColor,
//         flex: 1,
//       }}>
//       <Image
//         source={imageSource}
//         style={[
//           styles.image,
//           {tintColor: focused ? Colors.red_primary : Colors.gray},
//         ]}
//       />
//       {focused ? (
//         <View
//           style={[
//             {
//               backgroundColor: focused ? Colors.red_primary : Colors.gray,
//             },
//             styles.bottomLine,
//           ]}
//         />
//       ) : null}
//     </View>
//   );
// };
