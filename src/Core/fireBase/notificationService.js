import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PushNotification from 'react-native-push-notification';
import {PermissionsAndroid, Platform} from 'react-native';
import AsKey from '../../Constants/AsKey';
import {sendFcmTokenApiCall} from '../Config/ApiCalls';

export async function requestUserPermission() {
  PushNotification.requestPermissions();
  if (Platform.OS == 'android') {
    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS,
    );
    getFcmToken();
  }
  const authStatus = await messaging().requestPermission();
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  if (enabled) {
    getFcmToken();
  }
}

const getFcmToken = async () => {
  let fcmToken = await AsyncStorage.getItem(AsKey.fcmToken);
  // let fcmToken = '';
  try {
    const fcmToken2 = await messaging().getToken();
    if (fcmToken2) {
      await AsyncStorage.setItem(AsKey.fcmToken, fcmToken2);
      getRiderInfoFromStorage(fcmToken2);
    } else {
    }
  } catch (error) {
    console.log(error, 'error raised in fcmToken');
  }
  if (fcmToken) {
    console.log('FCM TOKEN is already generated ==>', fcmToken);
    getRiderInfoFromStorage(fcmToken);
  }
};

export const notificationListener = async () => {
  if (Platform.OS == 'android') {
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state: ',
        remoteMessage.notification,
      );
    });

    messaging().onMessage(async remoteMessage => {
      PushNotification.createChannel(
        {
          channelId: 'ChefLab', // (required)
          channelName: 'ChefLab_Channel', // (required)
          channelDescription: 'ChefLab app', // (optional) default: undefined.
          playSound: true, // (optional) default: true
          soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
          importance: 4, // (optional) default: 4. Int value of the Android notification importance
          vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
        },
        created => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
      );

      PushNotification.localNotification({
        message: remoteMessage?.notification.body,
        title: remoteMessage?.notification.title,
        bigPictureUrl: remoteMessage?.notification.android.imageUrl,
        smallIcon: remoteMessage?.notification.android.imageUrl,
        importance: 4,
      });
    });

    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage?.notification,
          );
        }
      });
  } else {
  }
};

const getRiderInfoFromStorage = async token => {
  let t = '';
  AsyncStorage.getItem(AsKey.userToken).then(value => {
    if (value !== '' && value !== null) {
      t = value;
      callUpdateTokenApi(value, token);
    }
  });
};

const callUpdateTokenApi = (apiToken, token) => {
  const body = {fcm_token: token};
  sendFcmTokenApiCall({body})
    .then(res => {
      // console.log('res sendFcmTokenApiCall', res);
    })
    .catch(err => {
      console.warn('err', err);
    });
};
