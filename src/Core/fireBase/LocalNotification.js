import PushNotification from 'react-native-push-notification';

const LocalNotification = data => {
  PushNotification.createChannel(
    {
      channelId: 'ch1', // (required)
      channelName: 'My channel', // (required)
      channelDescription: 'A channel to categorise your notifications', // (optional) default: undefined.
      playSound: false, // (optional) default: true
      soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
      // importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
      vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
    },
    created => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
  );

  PushNotification.localNotification({
    title: data.title,
    message: data.message,
    channelId: 'ch1',
  });
};

export default LocalNotification;
