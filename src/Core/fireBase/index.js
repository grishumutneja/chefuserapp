import {initializeApp} from 'firebase/app';

const firebaseConfig = {
  apiKey: 'AIzaSyC0XTAcHDhk-YzguedH8yjg4hkRRNoi94k',
  authDomain: 'cheflab-user.firebaseapp.com',
  projectId: 'cheflab-user',
  storageBucket: 'cheflab-user.appspot.com',
  messagingSenderId: '180746879110',
  appId: '1:180746879110:web:8440a4aab32734182e5107',
  measurementId: 'G-CGNPVL7FKZ',
};

export const firebaseApp = initializeApp(firebaseConfig);
