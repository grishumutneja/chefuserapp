import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import messaging from '@react-native-firebase/messaging';
import SplashScreen from '../Screens/AuthScreens/SplashScreen';
import MainStack from './Stack/MainStack';
import {setAccessToken} from '../Constants/GlobalFunction';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch, useSelector} from 'react-redux';
import AsKey from '../Constants/AsKey';
import CallInitialApi from '../Constants/CallInitiaclApi';
import {requestUserPermission} from './fireBase/notificationService';
import {Platform} from 'react-native';
import {getNotificationData} from './Redux/Slices/NotificationSlice';
import {orderHistoryApiCall} from './Config/ApiCalls';
import {addDriverRate} from './Redux/Slices/DriverRateSlice';

const Route = () => {
  const dispatch = useDispatch();
  const [token, setToken] = useState();
  const [splashVisible, setSplashVisible] = useState(true);
  const {rateGivenCount} = useSelector(state => state.driverRate);

  useEffect(() => {
    requestUserPermission();
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      notificationHandler(remoteMessage);
    });
    return unsubscribe;
  }, []);

  useEffect(() => {
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      notificationHandler(remoteMessage);
    });
  }, []);

  const notificationHandler = remoteMessage => {
    dispatch(getNotificationData(remoteMessage));
    if (Platform.OS == 'android') {
      console.log('remoteMessage onMessage android route', remoteMessage);
    } else {
      console.log('remoteMessage onMessage IOS route ', remoteMessage);
    }
    let data = JSON.parse(remoteMessage.data?.data);
    dispatch(addDriverRate(rateGivenCount + 1));
    const body = {
      order_for: 'restaurant',
      offset: 0,
    };
    orderHistoryApiCall({dispatch, body: body});
  };

  const getUserDataHandler = async () => {
    AsyncStorage.getItem(AsKey.userToken)
      .then(JSON.parse)
      .then(async res => {
        if (res) {
          setAccessToken(res);
          setToken(res);
          CallInitialApi({dispatch});
        }
      });
  };

  console.log('token', token);

  useEffect(() => {
    getUserDataHandler();
    setTimeout(() => {
      setSplashVisible(false);
    }, 3200);
  }, []);

  return (
    <NavigationContainer>
      {splashVisible ? <SplashScreen /> : <MainStack token={token} />}
    </NavigationContainer>
  );
};

export default Route;
