import {View, Text, TextInput, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Colors from '../../Constants/Colors';
import Images from '../../Constants/Images';

const AuthMobTextBox = ({
  onChangeText,
  value,
  otpActive,
  onPressEdit,
  otherStyle,
  signUp,
}) => {
  return (
    <View style={[CmnStyles.authMobTextBoxCont, otherStyle]}>
      <View style={CmnStyles.authMobTextBox}>
        <Text style={[CmnStyles.authMobText, signUp && {fontSize: 15}]}>
          +91
        </Text>
      </View>
      {!otpActive ? (
        <TextInput
          value={value}
          onChangeText={onChangeText}
          placeholder="Mobile Number"
          style={[
            CmnStyles.authMobInputText,
            signUp && {fontSize: 15, fontWeight: '400'},
          ]}
          placeholderTextColor={Colors.gray}
          maxLength={10}
          keyboardType={'decimal-pad'}
        />
      ) : (
        <>
          <Text style={[CmnStyles.authMobText2, signUp && {fontSize: 15}]}>
            {value}
          </Text>
          <TouchableOpacity style={CmnStyles.editImgBox} onPress={onPressEdit}>
            <Image
              source={Images.edit}
              style={CmnStyles.editImg}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

export default AuthMobTextBox;
