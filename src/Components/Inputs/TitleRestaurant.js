import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';
import CmnStyles from '../../Styles/CmnStyles';
import SpStyles from '../../Styles/SpStyles';
import Images from '../../Constants/Images';

const TitleRestaurant = ({
  titleBig,
  titleSmall,
  otherStyle,
  viewAllVisible,
  filterVisible,
  onPressFilter,
}) => {
  return (
    <View style={[CmnStyles.titleRestaurantBox, otherStyle]}>
      <View style={SpStyles.FDR_ALC}>
        <Text style={TextStyles.black_S_18_700}>{titleBig}</Text>
        {titleSmall && (
          <Text style={CmnStyles.titleResSmallText}>{`  (${titleSmall})`}</Text>
        )}
      </View>
      {viewAllVisible && (
        <TouchableOpacity>
          <Text style={CmnStyles.viewAllText}>View all</Text>
        </TouchableOpacity>
      )}
      {filterVisible && (
        <TouchableOpacity onPress={onPressFilter}>
          <Image
            source={Images.filter}
            style={CmnStyles.titleResBoxFilterImg}
            resizeMode="contain"
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

export default TitleRestaurant;
