import {View, Text, TextInput} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';

const SignUpInputCard = ({
  title,
  placeholder,
  onChangeText,
  value,
  starVisible,
  optionalVisible,
  keyboardType,
  autoCapitalize,
}) => {
  return (
    <View style={CmnStyles.signUpInputCardCont}>
      <Text style={CmnStyles.signUpInputCardTitle}>
        {title}
        {starVisible && <Text style={TextStyles.red_17_700}>*</Text>}
        {optionalVisible && (
          <Text style={TextStyles.gray_S_12_400}> (Optional)</Text>
        )}
      </Text>
      <TextInput
        value={value}
        onChangeText={onChangeText}
        placeholder={placeholder}
        style={CmnStyles.signUpInputCardTextInput}
        placeholderTextColor={Colors.gray}
        keyboardType={keyboardType}
        autoCapitalize={autoCapitalize}
      />
    </View>
  );
};

export default SignUpInputCard;
