import {View, Text, TextInput, Image, TouchableOpacity} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';

const SearchBox = ({
  otherStyle,
  setSearchText,
  searchText,
  placeholder,
  isFixed,
  onPress,
  refresh,
}) => {
  const searchRef = useRef();

  useEffect(() => {
    setTimeout(() => {
      searchRef?.current?.focus();
    }, 1200);
  }, [refresh]);

  const func = async text => {
    setSearchText(text);
  };

  function debounce(fn, wait) {
    var timeout;
    return function () {
      var ctx = this,
        args = arguments;
      clearTimeout(timeout);
      timeout = setTimeout(function () {
        fn.apply(ctx, args);
      }, wait || 500);
    };
  }

  const debouncedSearch = debounce(func, 500);

  return (
    <TouchableOpacity
      style={[CmnStyles.searchBoxCont, otherStyle]}
      onPress={onPress}
      disabled={!isFixed}
      key={refresh}>
      {isFixed ? (
        <Text
          style={{
            ...TextStyles.darkGray_S_15_700,
            width: '84%',
          }}>
          Search...
        </Text>
      ) : (
        <TextInput
          ref={searchRef}
          onChangeText={debouncedSearch}
          placeholder={placeholder ? placeholder : 'Search...'}
          placeholderTextColor={Colors.darkGray}
          style={{
            ...TextStyles.black_S_15_700,
            width: '84%',
          }}
        />
      )}
      <View style={CmnStyles.searchBoxImgBox}>
        <Image
          source={Images.search}
          style={CmnStyles.searchBoxImg}
          resizeMode="contain"
        />
      </View>
    </TouchableOpacity>
  );
};

export default SearchBox;
// import {View, Text, TextInput, Image, TouchableOpacity} from 'react-native';
// import React, {useEffect, useRef} from 'react';
// import CmnStyles from '../../Styles/CmnStyles';
// import Colors from '../../Constants/Colors';
// import TextStyles from '../../Styles/TextStyles';
// import Images from '../../Constants/Images';

// const SearchBox = ({
//   otherStyle,
//   setSearchText,
//   searchText,
//   placeholder,
//   isFixed,
//   onPress,
// }) => {
//   const searchRef = useRef();

//   useEffect(() => {
//     setTimeout(() => {
//       searchRef?.current?.focus();
//     }, 150);
//   }, []);

//   return (
//     <TouchableOpacity
//       style={[CmnStyles.searchBoxCont, otherStyle]}
//       onPress={onPress}
//       disabled={!isFixed}>
//       {isFixed ? (
//         <Text
//           style={{
//             ...TextStyles.darkGray_S_15_700,
//             width: '84%',
//           }}>
//           Search...
//         </Text>
//       ) : (
//         <TextInput
//           ref={searchRef}
//           value={searchText}
//           onChangeText={text => {
//             setSearchText(text);
//           }}
//           placeholder={placeholder ? placeholder : 'Search...'}
//           placeholderTextColor={Colors.darkGray}
//           style={{
//             ...TextStyles.black_S_15_700,
//             width: '84%',
//           }}
//         />
//       )}
//       <View style={CmnStyles.searchBoxImgBox}>
//         <Image
//           source={Images.search}
//           style={CmnStyles.searchBoxImg}
//           resizeMode="contain"
//         />
//       </View>
//     </TouchableOpacity>
//   );
// };

// export default SearchBox;
