import {View, Text} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';

const NoDataAvailable = ({text, otherStyle}) => {
  return (
    <View style={otherStyle}>
      <Text style={CmnStyles.noMoreResText}>{text}</Text>
    </View>
  );
};

export default NoDataAvailable;
