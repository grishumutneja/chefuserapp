import {View, Text, TextInput} from 'react-native';
import React from 'react';
import TextStyles from '../../Styles/TextStyles';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import CmnStyles from '../../Styles/CmnStyles';

const ProfileInput = ({
  title,
  value,
  setValue,
  placeholder,
  editActive,
  isRequired,
  maxLength,
  keyboardType,
  nonEditActive,
}) => {
  return (
    <View
      style={{
        marginHorizontal: Responsive.widthPx(4),
        marginTop: Responsive.widthPx(6),
      }}>
      <Text style={TextStyles.black_S_16_700}>
        {title}
        {isRequired && <Text style={{color: Colors.red_primary}}>*</Text>}
      </Text>
      <View style={CmnStyles.profileInputBox}>
        {editActive ? (
          <TextInput
            value={value}
            placeholder={placeholder}
            onChangeText={text => {
              setValue(text);
            }}
            placeholderTextColor={Colors.darkGray}
            style={[
              TextStyles.black_S_16_400,
              {paddingBottom: 0, lineHeight: 22},
            ]}
            maxLength={maxLength}
            keyboardType={keyboardType}
          />
        ) : (
          <Text
            style={
              nonEditActive
                ? CmnStyles.profileInputText2
                : CmnStyles.profileInputText
            }>
            {value}
          </Text>
        )}
      </View>
    </View>
  );
};

export default ProfileInput;
