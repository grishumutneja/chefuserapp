import ImageCropPicker from 'react-native-image-crop-picker';
import {Alert, Linking, PermissionsAndroid, Platform} from 'react-native';

const onCameraPressHandler = async ({setLogoImage, setCameraModalVisible}) => {
  if (Platform.OS === 'android') {
    console.log('second');
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(
      response => {
        if (response == false) {
          Alert.alert(
            'Hold On..!',
            'You need to give Camera Access to use this feature',
            [{text: 'OK', onPress: () => Linking.openSettings()}],
          );
        } else {
          openCamera();
        }
      },
    );
  } else {
    // openCamera();
    ImageCropPicker.openCamera({
      width: 300,
      height: 300,
      cropping: true,
    })
      .then(image => {
        setLogoImage(image.path);
        setCameraModalVisible(false);
      })
      .catch(err => {
        setCameraModalVisible(false);
        console.warn('err', err);
      });
  }

  const openCamera = () => {
    ImageCropPicker.openCamera({
      width: 300,
      height: 300,
      cropping: true,
    })
      .then(image => {
        console.log('image', image);
        setLogoImage(image.path);
        setCameraModalVisible(false);
      })
      .catch(err => {
        setCameraModalVisible(false);
        console.warn('err', err);
      });
  };
};

export default onCameraPressHandler;
