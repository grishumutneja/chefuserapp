import ImageCropPicker from 'react-native-image-crop-picker';

const onGalleryPressHandler = ({setLogoImage, setCameraModalVisible}) => {
  ImageCropPicker.openPicker({
    width: 300,
    height: 300,
    cropping: true,
  })
    .then(image => {
      console.log(image.path);
      setLogoImage(image.path);
      setCameraModalVisible(false);
    })
    .catch(err => {
      setCameraModalVisible(false);
      console.warn('err', err);
    });
};

export default onGalleryPressHandler;
