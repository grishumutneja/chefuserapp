import {PermissionsAndroid, Platform} from 'react-native';

const requestCameraPermission = async () => {
  try {
    if (Platform.OS == 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'App Camera Permission',
          message: 'App needs access to your camera ',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.warn('Camera permission given');
      } else {
        console.warn('Camera permission denied');
      }
    }
  } catch (err) {
    console.warn(err);
  }
};

export default requestCameraPermission;
