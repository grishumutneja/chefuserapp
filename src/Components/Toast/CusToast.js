import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  Text,
  DeviceEventEmitter,
  TouchableOpacity,
  Platform,
  ToastAndroid,
  View,
  StyleSheet,
  Image,
} from 'react-native';
import Animated, {
  withTiming,
  useSharedValue,
  useAnimatedStyle,
} from 'react-native-reanimated';
import {SHOW_TOAST_MESSAGE} from './toast';
import Images from '../../Constants/Images';

const colors = {
  info: '#343a40',
  success: '#28a745',
  danger: '#dc3545',
};

const CusToast = () => {
  const [messageType, setMessageType] = useState(null);
  const timeOutRef = useRef(null);

  const animatedOpacity = useSharedValue(0);
  const animateY = useSharedValue(50);

  const animatedStyle = useAnimatedStyle(() => {
    return {
      opacity: animatedOpacity.value,
      transform: [{translateY: animateY.value}],
    };
  }, []);

  const [timeOutDuration, setTimeOutDuration] = useState(5000);

  const [message, setMessage] = useState(null);

  const onNewToast = data => {
    if (Platform.OS === 'android' && data.useNativeToast) {
      return ToastAndroid.show(data.message, ToastAndroid.LONG);
    }
    if (data.duration) {
      setTimeOutDuration(data.duration);
    }
    setTimeout(() => {
      DeviceEventEmitter.removeAllListeners();
    }, data.duration);
    setMessage(data.message);
    setMessageType(data.type);
  };

  const closeToast = useCallback(() => {
    setMessage(null);
    setTimeOutDuration(5000);
    animatedOpacity.value = withTiming(0);
    animateY.value = withTiming(50);
    clearInterval(timeOutRef.current);
  }, [animatedOpacity, animateY]);

  useEffect(() => {
    if (message) {
      timeOutRef.current = setInterval(() => {
        if (timeOutDuration === 0) {
          closeToast();
        } else {
          setTimeOutDuration(prev => prev - 1000);
        }
      }, 1000);
    }

    return () => {
      clearInterval(timeOutRef.current);
    };
  }, [closeToast, message, timeOutDuration]);

  useEffect(() => {
    if (message) {
      animatedOpacity.value = withTiming(1, {duration: 500});
    }
  }, [message, animatedOpacity]);
  useEffect(() => {
    if (message) {
      animateY.value = withTiming(0, {duration: 700});
    }
  }, [message, animateY]);

  useEffect(() => {
    DeviceEventEmitter.addListener(SHOW_TOAST_MESSAGE, onNewToast);
    return () => {
      DeviceEventEmitter.removeAllListeners();
    };
  }, []);

  if (!message) {
    return null;
  }

  return (
    <>
      {message ? (
        <Animated.View style={[styles.container, animatedStyle]}>
          <View style={styles.logoImgBox}>
            <Image
              source={Images.app_logo}
              style={styles.logoImg}
              resizeMode="contain"
            />
          </View>
          <TouchableOpacity onPress={closeToast} style={styles.textBox}>
            <Text style={styles.text}>{message}</Text>
          </TouchableOpacity>
        </Animated.View>
      ) : null}
    </>
  );
};

export default CusToast;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: '12%',
    left: '4%',
    right: '4%',
    backgroundColor: '#eee',
    zIndex: 1,
    elevation: 1,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.7)',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.84,
    height: 50,
  },
  line: {
    width: '2%',
    height: '100%',
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
  },
  textBox: {
    width: '98%',
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
  },
  text: {
    color: 'black',
    fontSize: 12,
    textAlign: 'left',
    fontWeight: '700',
    marginLeft: 12,
  },
  logoImg: {
    width: 28,
    height: 28,
    borderRadius: 4,
  },
  logoImgBox: {
    width: 28,
    height: 28,
    marginLeft: 12,
    backgroundColor: 'white',
    borderRadius: 4,
  },
});
