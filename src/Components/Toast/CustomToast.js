import {View, Text, StyleSheet, Image} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import Images from '../../Constants/Images';

const CustomToast = ({text1, props}) => {
  return (
    <View style={styles.container}>
      <View style={styles.logoImgBox}>
        <Image
          source={Images.app_logo}
          style={styles.logoImg}
          resizeMode="contain"
        />
      </View>
      <View style={styles.textBox}>
        <Text style={styles.text}>{text1}</Text>
      </View>
    </View>
  );
};

export default CustomToast;
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eee',
    zIndex: 1,
    elevation: 1,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.7)',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.84,
    height: 50,
    width: Responsive.widthPx(90),
  },
  line: {
    width: '2%',
    height: '100%',
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
  },
  textBox: {
    width: '98%',
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
  },
  text: {
    color: 'black',
    fontSize: 12,
    textAlign: 'left',
    fontWeight: '700',
    marginLeft: 12,
  },
  logoImg: {
    width: 28,
    height: 28,
    borderRadius: 4,
  },
  logoImgBox: {
    width: 28,
    height: 28,
    marginLeft: 12,
    backgroundColor: 'white',
    borderRadius: 4,
  },
});
