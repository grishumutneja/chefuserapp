import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import RedButton from '../Buttons/RedButton';
import CloseModalButton from '../Buttons/CloseModalButton';
import {useDispatch, useSelector} from 'react-redux';
import {
  addItemToCartSearchDish,
  addNewItemToCartRest,
  addNewItemToCartSearchDish,
  removeCartDetail,
} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantDetailsSlice';
import {
  getCustomizableDataApiCall,
  removeEmptyCartApiCall,
} from '../../Core/Config/ApiCalls';
import StarReviewDish from '../Star/StarReviewDish';
import {addItemToCartRest} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantDetailsSlice';
import RedBorderButton from '../Buttons/RedBorderButton';

export const DishModal = ({
  modalVisible,
  onPressCancel,
  dishItem,
  dishMenuIndex,
  setDishModalVisible,
  vendorId,
  isSearchScreen,
}) => {
  const dispatch = useDispatch();
  const {getUserInfoData} = useSelector(state => state.userInfo);
  const {cartListData, cartTotalItem} = useSelector(state => state.cart);
  const [loading, setLoading] = useState(false);
  const {itemsInCart} = useSelector(state => state.restaurantDetails);
  const [addonsData, setAddonData] = useState();
  const [optionsData, setOptionsData] = useState();
  const [addOptions, setAddOptions] = useState();
  const [variantPrice, setVariantPrice] = useState(
    dishItem?.offer_id > 0
      ? dishItem?.after_offer_price
      : dishItem?.product_price,
  );
  const [addonTotalPrice, setAddonTotalPrice] = useState(0);
  const [addAddons, setAddAddons] = useState([]);
  const [isSelected, setIsSelected] = useState(0);
  const [productPrice, setProductPrice] = useState();
  let existCartVendorName = cartListData?.vendor?.name;
  let isExist =
    vendorId !== cartListData?.vendor?.vendor_id && cartTotalItem > 0
      ? true
      : false;

  useEffect(() => {
    setProductPrice(addonTotalPrice + parseFloat(variantPrice));
  }, [addonTotalPrice, variantPrice]);

  useEffect(() => {
    const body = {
      product_id: dishItem?.product_id,
    };
    if (dishItem?.customizable == 'true') {
      setLoading(true);
      getCustomizableDataApiCall({body})
        .then(res => {
          setLoading(false);
          setAddonData(res?.addons);
          setOptionsData(res?.options);
          setAddOptions(res?.options[0]);
          const optionAdd = {
            variant_id: res?.options[0]?.id,
            variant_qty: res?.options[0]?.qty + '',
          };
          setAddOptions(optionAdd);
        })
        .catch(err => {
          console.warn('err getCustomizableDataApiCall', err);
          setLoading(false);
        });
    }
  }, []);

  const cartBody = {
    cart_id: cartListData?.cart_id,
    user_id: getUserInfoData?.id,
    vendor_id: dishItem?.vendor_id,
    menuIndex: dishMenuIndex,
    dispatch: dispatch,
    selectedVariantIndex: isSelected,
    products: dishItem,
    addOptions: addOptions,
    addAddons: addAddons,
    productPrice: variantPrice,
    addonPrice: addonTotalPrice,
  };

  const renderImagePart = () => {
    return (
      <View>
        <View style={styles.imageBox}>
          <Image
            source={{uri: dishItem?.image}}
            style={styles.image}
            resizeMode="stretch"
          />
        </View>
      </View>
    );
  };

  const renderOptions = ({item, index}) => {
    return (
      <View style={styles.optionBox} key={'renderOptions DishModal' + index}>
        <Text style={styles.name}>{item?.variant_name}</Text>
        <View style={[SpStyles.FDR_ALC, {width: Responsive.widthPx(45)}]}>
          {/* {index == isSelected ? (
            <OnePlusItemBox
              otherStyle={CmnStyles.onePlusItemBoxCont2}
              productQty={item?.qty}
            />
          ) : ( */}
          <View style={{width: Responsive.widthPx(4)}} />
          {/* )} */}
          <View style={styles.priceCont}>
            {item?.offer_id != 0 && (
              <Text style={styles.varPrice}>
                ₹ {item?.after_offer_price?.toFixed(2)}
              </Text>
            )}
            <Text
              style={item?.offer_id != 0 ? styles.offerPrice : styles.varPrice}>
              ₹ {item?.variant_price}
            </Text>
          </View>
          <TouchableOpacity
            style={styles.circle}
            onPress={() => {
              addItemOption({item: item, index: index});
            }}>
            {index == isSelected && <View style={styles.dot} />}
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const addItemAddon = itm => {
    let a = addonsData?.map((item, index) => {
      var temp = Object.assign({}, item);
      let addPrice = addonTotalPrice;
      if (temp.addon === itm.addon) {
        temp.added = !temp.added;
        const addonsAdd = {
          addon_id: item?.id,
          addon_qty: item?.qty + '',
        };
        if (temp.added) {
          setAddAddons([...addAddons, addonsAdd]);
          addPrice = addPrice + parseFloat(temp?.price);
          setAddonTotalPrice(addPrice);
        } else {
          let ARR = [];
          let ADD = 0;
          addAddons.map((itmM, ind) => {
            if (itmM.addon_id !== temp.id) {
              ARR.push(itmM);
              ADD = ADD + parseFloat(temp.price);
            }
          });
          setAddonTotalPrice(ADD);
          setAddAddons(ARR);
        }
      }
      return temp;
    });
    setAddonData(a);
  };

  const addItemOption = ({item, index}) => {
    setIsSelected(index);
    if (index > 0) {
      const optionAdd = {
        variant_id: item?.id,
        variant_qty: item?.qty + '',
      };
      setAddOptions(optionAdd);
      setVariantPrice(
        item?.offer_id != 0 ? item?.after_offer_price : item?.variant_price,
      );
    } else {
      const optionAdd = {
        variant_id: item?.id,
        variant_qty: item?.qty + '',
      };
      setAddOptions(optionAdd);
      dishItem?.after_offer_price > 0
        ? setVariantPrice(dishItem?.after_offer_price)
        : setVariantPrice(dishItem?.product_price);
    }
  };

  const renderAddons = ({item, index}) => {
    return (
      <View
        style={styles.optionBox}
        key={'renderAddons DishModal' + item?.added}>
        <Text style={styles.name2}>{item?.addon}</Text>
        <View style={SpStyles.FDR_ALC_JCS} focusable={item.cart_addon_qty}>
          <Text style={styles.price}>₹ {item?.price}</Text>
          <TouchableOpacity
            onPress={() => {
              addItemAddon(item);
            }}>
            <Image
              source={item?.added ? Images.checked : Images.unchecked}
              style={styles.checkBox}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const renderOrderInfo = () => {
    return (
      <View style={{paddingBottom: Responsive.widthPx(4)}}>
        <Text style={styles.titleText}>{dishItem?.product_name}</Text>
        <Text style={styles.dishModalText}>{dishItem?.description}</Text>
        <StarReviewDish
          vendorRatings={dishItem?.product_rating}
          otherStyle={styles.star}
        />
        {optionsData?.length > 0 && (
          <>
            <Text style={styles.options}>Options</Text>
            <FlatList
              data={optionsData}
              renderItem={renderOptions}
              keyExtractor={(item, index) => {
                'optionsData Add Modal' + index;
              }}
              scrollEnabled={false}
            />
          </>
        )}
        {addonsData?.length > 0 && (
          <>
            <Text style={styles.options}>Addons</Text>
            <FlatList
              data={addonsData}
              renderItem={renderAddons}
              keyExtractor={(item, index) => {
                'addonData Add Modal' + index;
              }}
              scrollEnabled={false}
            />
          </>
        )}
      </View>
    );
  };

  const bottomPart = () => {
    return (
      <View style={styles.bottomCont}>
        <RedButton
          text={`Add Item ₹ ${productPrice?.toFixed(2)}`}
          otherStyle={styles.addItem}
          onPress={() => {
            if (isSearchScreen) {
              cartTotalItem > 0
                ? dispatch(addItemToCartSearchDish(cartBody))
                : dispatch(addNewItemToCartSearchDish(cartBody));
              setDishModalVisible(false);
            } else {
              cartTotalItem > 0
                ? dispatch(addItemToCartRest(cartBody))
                : dispatch(addNewItemToCartRest(cartBody));
              setDishModalVisible(false);
            }
          }}
        />
      </View>
    );
  };

  const renderLoader = () => {
    return (
      <View
        style={{
          height: Responsive.widthPx(20),
          justifyContent: 'center',
        }}>
        <ActivityIndicator size={'small'} color={Colors.red_primary} />
      </View>
    );
  };

  const renderDish = () => {
    return (
      <View style={styles.modalBox}>
        <CloseModalButton onPress={onPressCancel} />
        <ScrollView>
          {dishItem?.image && renderImagePart()}
          {loading ? renderLoader() : renderOrderInfo()}
        </ScrollView>
        {bottomPart()}
      </View>
    );
  };

  const renderExist = () => {
    return (
      <View style={styles.renderExistCont}>
        <View style={styles.renderExistHeaderBox}>
          <Text style={{...TextStyles.black_S_20_700}}>
            Item already in cart
          </Text>
          <TouchableOpacity onPress={onPressCancel}>
            <Image
              source={Images.cancel}
              style={{
                width: Responsive.widthPx(7),
                height: Responsive.widthPx(7),
              }}
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.renderExistText}>
          Wait, you have some products of other restaurant "
          {existCartVendorName}". Do you want to continue?
        </Text>
        <View style={styles.btnCont}>
          <RedBorderButton
            text={'Cancel'}
            otherStyle={styles.addAddressBtn}
            otherTextStyle={styles.addAddressText}
            onPress={onPressCancel}
          />
          <RedButton
            text={'Continue'}
            otherStyle={styles.confirmBtn}
            otherTextStyle={styles.confirmText}
            onPress={() => {
              let body = {
                user_id: getUserInfoData?.id,
              };
              removeEmptyCartApiCall({body, dispatch});
              let removeData = {
                totalAmount: 0,
                totalItem: 0,
                itemsInCart: [],
                cart_id: null,
                vendor_id: null,
              };
              dispatch(removeCartDetail(removeData));
            }}
          />
        </View>
      </View>
    );
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          {isExist ? renderExist() : renderDish()}
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
    maxHeight: Responsive.heightPx(80),
  },
  modalBox: {
    backgroundColor: 'white',
    borderTopLeftRadius: Responsive.heightPx(3.5),
    borderTopRightRadius: Responsive.heightPx(3.5),
    width: Responsive.widthPx(100),
    maxHeight: Responsive.heightPx(84),
    paddingBottom: Responsive.widthPx(20),
  },
  imageBox: {
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(64),
    alignItems: 'center',
    justifyContent: 'center',
    ...SpStyles.shadowLight,
    backgroundColor: Colors.white,
    alignSelf: 'center',
    marginVertical: Responsive.widthPx(3),
    borderRadius: Responsive.widthPx(2),
  },
  image: {
    width: Responsive.widthPx(90),
    height: Responsive.widthPx(60),
    borderRadius: Responsive.widthPx(2),
  },
  titleText: {
    ...TextStyles.black_S_18_700,
    marginLeft: Responsive.widthPx(5),
    marginTop: Responsive.widthPx(4),
  },
  dishModalText: {
    marginVertical: Responsive.widthPx(3),
    marginTop: Responsive.widthPx(1.5),
    marginLeft: Responsive.widthPx(5),
    ...TextStyles.black_S_15_400,
  },
  options: {
    ...TextStyles.black_S_15_400,
    marginLeft: Responsive.widthPx(5),
  },
  optionBox: {
    ...SpStyles.FDR_ALC_JCS,
    marginHorizontal: Responsive.widthPx(5),
    marginTop: Responsive.widthPx(1.2),
    paddingLeft: Responsive.widthPx(2),
  },
  dot: {
    width: Responsive.widthPx(2),
    height: Responsive.widthPx(2),
    backgroundColor: Colors.red_primary,
    borderRadius: Responsive.widthPx(1.5),
  },
  circle: {
    width: Responsive.widthPx(5),
    height: Responsive.widthPx(5),
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: Colors.red_primary,
    borderRadius: Responsive.widthPx(2.5),
  },
  price: {
    ...TextStyles.black_S_14_400,
    textAlign: 'left',
    width: Responsive.widthPx(16),
  },
  varPrice: {
    ...TextStyles.black_S_14_700,
    textAlign: 'right',
    width: Responsive.widthPx(16),
    marginRight: Responsive.widthPx(2),
  },
  priceCont: {
    marginHorizontal: Responsive.widthPx(3),
    width: Responsive.widthPx(30),
    ...SpStyles.FDR_ALC,
  },
  offerPrice: {
    ...TextStyles.black_S_14_400,
    width: Responsive.widthPx(16),
    textAlign: 'left',
    textDecorationLine: 'line-through',
  },
  star: {
    marginHorizontal: Responsive.widthPx(5),
    marginBottom: Responsive.widthPx(3),
  },
  bottomCont: {
    borderTopColor: Colors.black,
    borderTopWidth: 0.8,
    position: 'absolute',
    backgroundColor: Colors.white,
    paddingVertical: Responsive.widthPx(3),
    bottom: 0,
    width: Responsive.widthPx(100),
    marginBottom: Responsive.widthPx(2),
  },
  addItem: {
    marginTop: Responsive.widthPx(2),
    width: 220,
  },
  checkBox: {
    width: Responsive.widthPx(5),
    height: Responsive.widthPx(5),
  },
  name: {
    ...TextStyles.black_S_13_400,
    width: Responsive.widthPx(43),
  },
  name2: {
    ...TextStyles.black_S_13_400,
    width: Responsive.widthPx(60),
  },
  renderExistHeaderBox: {
    backgroundColor: Colors.white,
    height: Responsive.widthPx(16),
    width: Responsive.widthPx(100),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    paddingHorizontal: Responsive.widthPx(5),
    ...SpStyles.FDR_ALC_JCS,
    marginBottom: Responsive.widthPx(2),
  },
  renderExistCont: {
    backgroundColor: '#efefef',
    paddingBottom: Responsive.widthPx(10),
    borderTopLeftRadius: Responsive.widthPx(8),
    borderTopRightRadius: Responsive.widthPx(8),
  },
  renderExistText: {
    width: Responsive.widthPx(90),
    alignSelf: 'center',
    ...TextStyles.black_S_15_400,
    marginVertical: Responsive.widthPx(2),
  },
  confirmBtn: {
    marginTop: Responsive.widthPx(0),
    width: Responsive.widthPx(42),
  },
  addAddressText: {
    ...TextStyles.red_16_700,
  },
  confirmText: {
    ...TextStyles.white_S_16_700,
  },
  btnCont: {
    ...SpStyles.FDR_ALC_JCS,
    width: Responsive.widthPx(88),
    alignSelf: 'center',
    paddingTop: Responsive.widthPx(4),
  },
  addAddressBtn: {
    height: 42,
    borderRadius: Responsive.widthPx(3),
    width: Responsive.widthPx(42),
  },
});
