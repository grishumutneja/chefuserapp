import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import RedBorderButton from '../Buttons/RedBorderButton';
import RedButton from '../Buttons/RedButton';

export const FilterChefModal = ({modalVisible, onPressCancel, text}) => {
  const [isPrimary, setIsPrimary] = useState(false);

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View
            style={{
              backgroundColor: Colors.white,
              height: Responsive.widthPx(16),
              width: Responsive.widthPx(100),
              borderTopLeftRadius: Responsive.widthPx(4),
              borderTopRightRadius: Responsive.widthPx(4),
              paddingHorizontal: Responsive.widthPx(5),
              ...SpStyles.FDR_ALC_JCS,
              ...SpStyles.shadowLight,
              marginBottom: Responsive.widthPx(2),
            }}>
            <Text style={{...TextStyles.black_S_20_700}}>Browse Menu</Text>
            <TouchableOpacity onPress={onPressCancel}>
              <Image
                source={Images.cancel}
                style={{
                  width: Responsive.widthPx(7),
                  height: Responsive.widthPx(7),
                }}
              />
            </TouchableOpacity>
          </View>
          <FlatList
            data={[1, 2, 3, 4, 5, 6]}
            renderItem={({item, index}) => {
              return (
                <View
                  style={{
                    paddingHorizontal: Responsive.widthPx(5),
                    ...SpStyles.FDR_ALC_JCS,
                    width: Responsive.widthPx(100),
                    height: Responsive.widthPx(10),
                  }}>
                  <Text style={TextStyles.black_S_15_400}>Suggested</Text>
                  <TouchableOpacity
                    onPress={() => {
                      setIsPrimary(!isPrimary);
                    }}>
                    <Image
                      source={isPrimary ? Images.checked : Images.unchecked}
                      style={styles.checkBox}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
              );
            }}
          />
          <View style={styles.btnCont}>
            <RedBorderButton
              text={'Clear All'}
              otherStyle={styles.btn}
              otherTextStyle={styles.btnText}
            />
            <RedButton
              text={'Apply'}
              otherStyle={[styles.btn, {marginTop: 0}]}
              otherTextStyle={styles.btnText}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    maxHeight: Responsive.heightPx(80),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    backgroundColor: Colors.white,
    paddingBottom: Responsive.widthPx(16),
  },
  checkBox: {
    width: Responsive.widthPx(5.5),
    height: Responsive.widthPx(5.5),
    marginLeft: Responsive.widthPx(4),
    marginRight: Responsive.widthPx(2),
  },
  btn: {
    width: Responsive.widthPx(36),
    height: 40,
    borderRadius: Responsive.widthPx(3),
  },
  btnText: {fontSize: 18},
  btnCont: {
    paddingHorizontal: Responsive.widthPx(10),
    marginTop: Responsive.widthPx(6),
    ...SpStyles.FDR_ALC_JCS,
  },
});
