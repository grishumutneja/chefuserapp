import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import StarRating from 'react-native-star-rating-widget';
import {getVenderAllReviewApiCall} from '../../Core/Config/ApiCalls';

export const ReviewModal = ({
  modalVisible,
  onPressCancel,
  isClosed,
  venderId,
}) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();

  useEffect(() => {
    setLoading(true);
    let body = {
      vendor_id: venderId,
      offset: '0',
      limit: '10',
    };
    venderId &&
      getVenderAllReviewApiCall({body})
        .then(res => {
          setData(res);
          setLoading(false);
        })
        .catch(err => {
          console.warn('err', err);
        });
  }, []);

  const renderReView = ({item, index}) => {
    return (
      <View style={styles.textBox}>
        <Text style={TextStyles.darkGray_S_14_500}>{item?.name}</Text>
        <StarRating
          rating={item?.rating}
          onChange={() => {}}
          starSize={Responsive.widthPx(3.2)}
          starStyle={{marginLeft: 0, marginVertical: Responsive.widthPx(2)}}
          color={isClosed ? '#bbb' : '#fdd835'}
        />
        <Text style={TextStyles.black_S_15_400}>{item?.review}</Text>
      </View>
    );
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={styles.header}>
            <Text style={{...TextStyles.black_S_20_700}}>Reviews</Text>
            <TouchableOpacity onPress={onPressCancel} style={styles.cancelBox}>
              <Image
                source={Images.cancel}
                style={styles.cancelImg}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <View style={styles.reViewCont}>
            <FlatList
              data={data}
              renderItem={renderReView}
              key={(item, index) => {
                'ReviewModal  Brouse menu ' + index;
              }}
              contentContainerStyle={{paddingBottom: Responsive.widthPx(5)}}
            />
            {loading && (
              <View style={styles.loader}>
                <ActivityIndicator color={Colors.red_primary} size={'large'} />
              </View>
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    maxHeight: Responsive.heightPx(80),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    backgroundColor: Colors.lightGray2,
    paddingBottom: Responsive.widthPx(18),
    // padding: Responsive.widthPx(5),
  },
  header: {
    backgroundColor: Colors.white,
    height: Responsive.widthPx(16),
    width: Responsive.widthPx(100),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    paddingHorizontal: Responsive.widthPx(5),
    ...SpStyles.FDR_ALC_JCS,
    marginBottom: Responsive.widthPx(2),
  },
  cancelBox: {
    width: Responsive.widthPx(9),
    height: Responsive.widthPx(9),
    ...SpStyles.ALC_JCC,
  },
  cancelImg: {
    width: Responsive.widthPx(7),
    height: Responsive.widthPx(7),
  },
  textBox: {
    paddingHorizontal: Responsive.widthPx(5),
    backgroundColor: Colors.white,
    width: Responsive.widthPx(94),
    alignSelf: 'center',
    borderRadius: Responsive.widthPx(4),
    paddingVertical: Responsive.widthPx(2),
    marginBottom: Responsive.widthPx(3),
    marginTop: Responsive.widthPx(1),
  },
  reViewCont: {
    flex: 1,
    maxHeight: Responsive.heightPx(70),
  },
  loader: {
    position: 'absolute',
    backgroundColor: Colors.lightGray2,
    width: Responsive.widthPx(100),
    height: '100%',
    paddingTop: Responsive.widthPx(6),
  },
});
