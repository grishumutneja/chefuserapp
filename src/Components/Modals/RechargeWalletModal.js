import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Image,
  Alert,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Colors from '../../Constants/Colors';
import Responsive from '../../Constants/Responsive';
import SpStyles from '../../Styles/SpStyles';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import RedButton from '../Buttons/RedButton';
import {
  checkRechargeWalletApiCall,
  createOrderIdForRazorPay,
  getCartListApiCall,
  razorPayCheckOutFunction,
  rechargeWalletApiCall,
  tempRechargeWalletApiCall,
  userAllTransactionApiCall,
} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';
import MyData from '../../Constants/MyData';
import {decode, encode} from 'js-base64';
import Utility from '../../Constants/Utility';
import TextStyles from '../../Styles/TextStyles';

const RadioButtonsData = [
  {
    name: '₹ 100',
    id: '100',
  },
  {
    name: '₹ 200',
    id: '200',
  },
  {
    name: '₹ 500',
    id: '500',
  },
  {
    name: '₹ 1000',
    id: '1000',
  },
];

const RechargeWalletModal = ({
  onPressCancel,
  visible,
  setRechargeModalVisible,
  setPaymentWaitVisible,
}) => {
  const dispatch = useDispatch();
  const [amount, setAmount] = useState('0');
  const [selectedOption, setSelectedOption] = useState(null);
  const {getUserInfoData, loginData} = useSelector(state => state.userInfo);
  const {locationData} = useSelector(state => state.location);

  useEffect(() => {
    if (!global.btoa) {
      global.btoa = encode;
    }
    if (!global.atob) {
      global.atob = decode;
    }
  }, []);

  const onSelect = (item, index) => {
    if (selectedOption && selectedOption.id === item.id) {
      setSelectedOption(null);
    } else {
      setSelectedOption(item);
      setAmount(item?.id);
    }
  };

  const rechargeHandler = () => {
    setRechargeModalVisible(false);
    const razAmount = amount * 100;
    const body = {
      user_id: getUserInfoData?.id,
      amount: amount,
    };
    tempRechargeWalletApiCall({body}).then(res => {
      createOrderIdForRazorPay({
        amount: razAmount,
        tempOrderId: res,
        transaction_for: 'wallet',
      }).then(response => {
        const razorBody = {
          description: 'Wallet recharge',
          currency: 'INR',
          key: MyData.razorPayKey, //Real key
          amount: razAmount,
          order_id: response?.data?.id,
          prefill: {
            email: loginData?.email,
            contact: loginData?.mobile,
            name: getUserInfoData?.name,
          },
        };
        razorPayCheckOutFunction({body: razorBody})
          .then(resp => {
            const checkBody = {
              transaction_id: res,
            };
            setPaymentWaitVisible(true);
            checkRechargeWalletApiCall({body: checkBody})
              .then(resP => {
                if (resP?.response?.payment_status == 0) {
                  setPaymentWaitVisible(true);
                } else if (resP?.response?.payment_status == 1) {
                  const cartBody = {
                    user_id: loginData?.user_id,
                    lat: locationData?.lat,
                    lng: locationData?.lng,
                  };
                  const transBody = {
                    userId: cartBody?.user_id,
                    dispatch: dispatch,
                  };
                  setPaymentWaitVisible(false);
                  userAllTransactionApiCall(transBody);
                  setTimeout(() => {
                    getCartListApiCall({
                      body: cartBody,
                      dispatch,
                      isRechargeDone: true,
                    });
                  }, 1000);
                  Alert.alert(
                    'Recharge Success',
                    '₹' +
                      body?.amount +
                      ' has been successfully credited in your wallet.',
                  );
                } else {
                  setPaymentWaitVisible(false);
                  Utility.showToast(
                    'Your Payment is Rejected Please Try Again',
                  );
                }
              })
              .catch(err => {
                console.warn('err', err);
                setPaymentWaitVisible(false);
              });
            // let b = {
            //   user_id: loginData?.user_id,
            //   amount: amount,
            //   type: '1',
            //   transaction_id: res + '',
            //   // transaction_id: resp?.razorpay_payment_id + '',
            // };
            // const cartBody = {
            //   user_id: loginData?.user_id,
            //   lat: locationData?.lat,
            //   lng: locationData?.lng,
            // };
            // rechargeWalletApiCall({
            //   body: b,
            //   setRechargeModalVisible: setRechargeModalVisible,
            //   cartBody: cartBody,
            //   dispatch: dispatch,
            // });
          })
          .catch(err => console.warn('err razorPayCheckOutFunction', err));
      });
    });
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={onPressCancel}>
      <View style={CmnStyles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={CmnStyles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={CmnStyles.modalSubContainer}>
          <View style={CmnStyles.rechargeModalHeader}>
            <Text style={CmnStyles.rechargeModalHeaderTitle}>
              Recharge Wallet
            </Text>
            <TouchableOpacity
              style={CmnStyles.modalCloseBtn}
              onPress={onPressCancel}>
              <Image
                source={Images.close}
                style={CmnStyles.modalCloseImg}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <Text style={CmnStyles.rechargeModalHeaderText}>
            Pick one of them or enter manually
          </Text>
          <RadioButtons
            onSelect={onSelect}
            selectedOption={selectedOption}
            options={RadioButtonsData}
          />
          <View style={CmnStyles.rechargeModalOrTextCont}>
            <Text style={CmnStyles.rechargeModalOrText}>OR</Text>
          </View>
          <View style={CmnStyles.rechargeModalTextInputBox}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              placeholder={'Enter amount to recharge wallet'}
              style={CmnStyles.rechargeModalTextInput}
              value={amount}
              keyboardType="number-pad"
              onChangeText={v => {
                setAmount(v);
              }}
              placeholderTextColor={Colors.gray}
            />
          </View>
          <RedButton
            text={'Recharge Now'}
            otherStyle={{width: Responsive.widthPx(50)}}
            onPress={() => {
              if (amount > 0) {
                rechargeHandler();
              } else {
                Utility.showToast('Please write/select Amount');
              }
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

export default RechargeWalletModal;

const RadioButtons = ({options, selectedOption, onSelect}) => {
  return (
    <View style={SpStyles.FDR_ALC}>
      {options.map((item, index) => {
        const selected = selectedOption && selectedOption?.id === item?.id;
        return (
          <TouchableOpacity
            onPress={() => {
              onSelect(item, index);
            }}
            key={item.id}
            style={CmnStyles.radioSubButtonCont}>
            <View
              style={
                selected
                  ? CmnStyles.radioSubBtnBoxSel
                  : CmnStyles.radioSubBtnBox
              }>
              <Text
                style={
                  selected ? TextStyles.red_14_700 : TextStyles.black_S_14_400
                }>
                {item?.name}
              </Text>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
