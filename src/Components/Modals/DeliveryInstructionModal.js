import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  Image,
  TextInput,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import RedButton from '../Buttons/RedButton';

export const DeliveryInstructionModal = ({
  modalVisible,
  onPressCancel,
  setDeliveryInstruction,
  deliveryInstruction,
  setModalVisible,
}) => {
  const [insText, setInsText] = useState(deliveryInstruction);

  const bodyArea = () => {
    return (
      <View style={styles.bodyBox}>
        <Text style={TextStyles.black_S_15_700}>Instructions</Text>
        <View style={styles.input}>
          <TextInput
            onChangeText={text => {
              setInsText(text);
            }}
            value={insText}
            placeholder="Enter instructions"
            placeholderTextColor={Colors.darkGray}
            style={[TextStyles.black_S_15_400, {padding: 4}]}
          />
        </View>
        <Text style={styles.text}>
          Add delivery instructions so that our delivery boy will follow the
          instructions and delivery your food safely.
        </Text>
      </View>
    );
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={styles.modalBox}>
            <View style={SpStyles.FDR_ALC_JCS}>
              <Text style={styles.titleText}>Add delivery instructions</Text>
              <TouchableOpacity
                onPress={() => {
                  if (insText) {
                    setModalVisible(false);
                  } else {
                    setDeliveryInstruction();
                    setModalVisible(false);
                  }
                }}>
                <Image source={Images.close} style={styles.close} />
              </TouchableOpacity>
            </View>
            {bodyArea()}
            <RedButton
              text={'Add'}
              otherStyle={styles.confirmBtn}
              otherTextStyle={styles.confirmText}
              onPress={() => {
                setDeliveryInstruction(insText);
                setModalVisible(false);
              }}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
  },
  modalBox: {
    backgroundColor: 'white',
    borderRadius: Responsive.widthPx(3.5),
    width: Responsive.widthPx(100),
    maxHeight: Responsive.heightPx(84),
    paddingBottom: Responsive.widthPx(16),
  },
  titleText: {
    ...TextStyles.black_S_20_700,
    marginLeft: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
  },
  close: {
    width: Responsive.widthPx(4),
    height: Responsive.widthPx(4),
    marginRight: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
  },
  bodyBox: {
    width: Responsive.widthPx(92),
    alignSelf: 'center',
    paddingTop: Responsive.widthPx(4),
  },
  input: {
    height: Responsive.widthPx(10),
    borderBottomColor: Colors.black,
    borderBottomWidth: 1.2,
    marginTop: Responsive.widthPx(2),
    paddingHorizontal: Responsive.widthPx(2),
  },
  text: {
    marginTop: Responsive.widthPx(6),
    marginBottom: Responsive.widthPx(16),
    ...TextStyles.darkGray_S_12_500,
  },
});
