import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import RedButton from '../Buttons/RedButton';
import ProfileInput from '../Inputs/ProfileInput';
import {updateDeliveryAddressApiCall} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';
import RedBorderButton from '../Buttons/RedBorderButton';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import Toast from 'react-native-toast-message';
import CustomToast from '../Toast/CustomToast';

const AddressCard = ({item, index, defaultType, setDefaultType}) => {
  return (
    <TouchableOpacity
      style={{
        ...styles.addressCardCont,
        borderColor:
          defaultType == index + 1 ? Colors.red_primary : Colors.darkGray,
        borderWidth: defaultType == index ? 1.6 : 1.2,
      }}
      onPress={() => {
        setDefaultType(index + 1);
      }}>
      <Text
        style={
          defaultType == index + 1
            ? TextStyles.red_16_700
            : TextStyles.darkGray_S_16_400
        }>
        {item}
      </Text>
    </TouchableOpacity>
  );
};

export const UpdateLocationModal = ({
  modalVisible,
  onPressCancel,
  setUpdateModalVisible,
  location,
  UpdateAddress,
}) => {
  const dispatch = useDispatch();
  const {loginData} = useSelector(state => state.userInfo);
  const navigation = useNavigation();

  const addAddressHandler = () => {
    let passData = {
      fullAddress: UpdateAddress.fullAddress,
      landmark: UpdateAddress.landmark,
      contact: UpdateAddress.contact,
      defaultType: UpdateAddress.defaultType,
      isPrimary: UpdateAddress.isPrimary,
      addressId: UpdateAddress.addressId,
    };
    updateDeliveryAddressApiCall({
      data: passData,
      dispatch: dispatch,
      setUpdateModalVisible: setUpdateModalVisible,
      location: location,
      userId: loginData?.user_id,
      navigation: navigation,
    });
  };

  const toastConfig = {
    tomatoToast: ({text1, props}) => (
      <CustomToast text1={text1} props={props} />
    ),
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={styles.modalBox}>
            <View style={SpStyles.FDR_ALC_JCS}>
              <Text style={styles.titleText}>Update address</Text>
              <TouchableOpacity onPress={onPressCancel}>
                <Image source={Images.close} style={styles.close} />
              </TouchableOpacity>
            </View>
            {UpdateAddress.locationName && (
              <Text style={styles.text}>{UpdateAddress.locationName}</Text>
            )}
            <ProfileInput
              title="Full Address"
              placeholder="Enter full address"
              value={UpdateAddress.fullAddress}
              setValue={UpdateAddress.setFullAddress}
              editActive={true}
              isRequired={true}
            />
            <ProfileInput
              title="Nearest Landmark"
              placeholder="Enter nearest landmark"
              value={UpdateAddress.landmark}
              setValue={UpdateAddress.setLandmark}
              editActive={true}
              isRequired={true}
            />
            <ProfileInput
              title="Contact"
              placeholder="1234567890"
              value={UpdateAddress.contact}
              setValue={UpdateAddress.setContact}
              editActive={true}
              isRequired={true}
              maxLength={10}
              keyboardType="number-pad"
            />
            <View style={styles.addressCont}>
              <Text style={styles.title}>Select address type</Text>
              <FlatList
                horizontal
                data={['Home', 'Work', 'Other']}
                renderItem={({item, index}) => {
                  return (
                    <AddressCard
                      item={item}
                      index={index}
                      setDefaultType={UpdateAddress.setDefaultType}
                      defaultType={UpdateAddress.defaultType}
                    />
                  );
                }}
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                UpdateAddress.isPrimary == 0
                  ? UpdateAddress.setIsPrimary(1)
                  : UpdateAddress.setIsPrimary(0);
              }}
              style={styles.checkBoxCont}>
              <Image
                source={
                  UpdateAddress.isPrimary == 1
                    ? Images.checked
                    : Images.unchecked
                }
                style={styles.checkBox}
                resizeMode="contain"
              />
              <Text style={TextStyles.black_S_16_400}>Set as primary</Text>
            </TouchableOpacity>
            <View style={styles.btnCont}>
              <RedBorderButton
                text={'Go to map'}
                otherStyle={styles.confirmBtn}
                otherTextStyle={styles.confirmText2}
                onPress={() => {
                  navigation.navigate(Screens.Maps, {
                    isUpdate: true,
                    UpdateAddress: UpdateAddress,
                  });
                  setUpdateModalVisible(false);
                }}
              />
              <RedButton
                text={'Update'}
                otherStyle={styles.confirmBtn}
                otherTextStyle={styles.confirmText}
                onPress={() => {
                  addAddressHandler();
                }}
              />
            </View>
          </View>
        </View>
      </View>
      <Toast config={toastConfig} />
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
  },
  modalBox: {
    backgroundColor: 'white',
    borderRadius: Responsive.widthPx(3.5),
    width: Responsive.widthPx(100),
  },
  titleText: {
    ...TextStyles.black_S_20_700,
    marginLeft: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
    marginBottom: Responsive.widthPx(0),
  },
  text: {
    ...TextStyles.black_S_16_400,
    marginLeft: Responsive.widthPx(5),
    marginRight: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(7),
    marginBottom: Responsive.widthPx(0),
  },
  close: {
    width: Responsive.widthPx(4),
    height: Responsive.widthPx(4),
    marginRight: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
  },
  addressCardCont: {
    width: Responsive.widthPx(22),
    borderColor: Colors.darkGray,
    borderWidth: 1,
    height: 38,
    borderRadius: 6,
    marginRight: Responsive.widthPx(3),
    alignItems: 'center',
    justifyContent: 'center',
  },
  typeText: {
    ...TextStyles.red_14_700,
  },
  locationText: {
    ...TextStyles.black_S_16_700,
    marginTop: Responsive.widthPx(0.7),
  },
  landmarkText: {
    ...TextStyles.darkGray_S_14_500,
    marginTop: Responsive.widthPx(0.7),
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ACACAC',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: Responsive.widthPx(4),
    top: Responsive.widthPx(3),
  },
  checkedCircle: {
    width: 12,
    height: 12,
    borderRadius: 6,
    backgroundColor: Colors.red_primary,
  },
  addressCont: {
    marginTop: Responsive.widthPx(6),
    alignItems: 'center',
  },
  addAddressBtn: {
    height: 42,
    borderRadius: Responsive.widthPx(3),
    width: Responsive.widthPx(45),
  },
  confirmBtn: {
    marginTop: Responsive.widthPx(0),
    width: 120,
    height: 40,
    borderRadius: 10,
  },
  addAddressText: {
    ...TextStyles.red_16_700,
  },
  confirmText: {
    ...TextStyles.white_S_16_700,
  },
  confirmText2: {
    ...TextStyles.red_16_700,
  },
  btnCont: {
    width: 260,
    alignSelf: 'center',
    paddingTop: Responsive.widthPx(6),
    paddingBottom: Responsive.widthPx(12),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    ...TextStyles.black_S_16_700,
    textAlign: 'left',
    width: Responsive.widthPx(92),
    marginBottom: Responsive.widthPx(3),
  },
  checkBox: {
    width: Responsive.widthPx(5.5),
    height: Responsive.widthPx(5.5),
    marginLeft: Responsive.widthPx(4),
    marginRight: Responsive.widthPx(2),
    tintColor: Colors.red_primary,
  },
  checkBoxCont: {
    marginTop: Responsive.widthPx(6),
    marginBottom: Responsive.widthPx(4),
    ...SpStyles.FDR_ALC,
  },
});
