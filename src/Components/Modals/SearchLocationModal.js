import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
} from 'react-native';
import React from 'react';
import Colors from '../../Constants/Colors';
import Responsive from '../../Constants/Responsive';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import MyData from '../../Constants/MyData';
import SpStyles from '../../Styles/SpStyles';

const SearchLocationModal = ({
  onPressCancel,
  visible,
  setLocation,
  setSearchLocModalVisible,
  searchRef,
  setLocationName,
  setIsSearched,
}) => {
  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={visible}
      onRequestClose={onPressCancel}
      style={{flexGrow: 1}}>
      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS == 'ios' ? 'padding' : null}>
        <View style={styles.cont}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={onPressCancel}
            style={styles.outCont}
          />
          <View style={styles.mainCont}>
            <View style={styles.Fdr}>
              <Text style={styles.locationText}>Select a location</Text>
              <TouchableOpacity
                style={CmnStyles.modalCloseBtn}
                onPress={onPressCancel}>
                <Image
                  source={Images.close}
                  style={CmnStyles.modalCloseImg}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
            <ScrollView
              style={{flexGrow: 1}}
              keyboardShouldPersistTaps="always">
              <GooglePlacesAutocomplete
                ref={searchRef}
                placeholder="Search destination"
                minLength={2}
                textInputProps={{
                  autoFocus: true,
                }}
                onPress={(data, details = null) => {
                  setLocationName(data?.description);
                  setLocation(details?.geometry?.location);
                  setSearchLocModalVisible(false);
                  setIsSearched(true);
                }}
                query={{
                  key: MyData.googleMapApiKey,
                  language: 'en',
                }}
                GooglePlacesDetailsQuery={{
                  fields: 'geometry',
                }}
                renderLeftButton={() => (
                  <View style={CmnStyles.searchModalInputHeaderIconBox}>
                    <Image
                      source={Images.location}
                      style={CmnStyles.locationIconMap}
                      resizeMode="contain"
                    />
                  </View>
                )}
                styles={{
                  textInputContainer: CmnStyles.searchModalInputHeaderTextCont,
                  textInput: CmnStyles.searchModalInputHeaderInputText,
                }}
                renderRow={(data, index) => (
                  <View style={SpStyles.FDR_Wr100}>
                    <Image
                      source={Images.location}
                      style={[
                        CmnStyles.locationIconMap,
                        {tintColor: Colors.black},
                      ]}
                      resizeMode="contain"
                    />
                    <Text style={CmnStyles.searchModalInputDec}>
                      {data?.description}
                    </Text>
                  </View>
                )}
                fetchDetails={true}
                listViewDisplayed={false}
              />
            </ScrollView>
          </View>
        </View>
      </KeyboardAvoidingView>
    </Modal>
  );
};

export default SearchLocationModal;

const styles = StyleSheet.create({
  cont: {
    backgroundColor: '#00000090',
    flexGrow: 1,
    justifyContent: 'flex-end',
  },
  outCont: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  mainCont: {
    maxHeight: Responsive.heightPx(80),
    minHeight: Responsive.heightPx(30),
    backgroundColor: Colors.white,
    borderTopRightRadius: Responsive.widthPx(4),
    borderTopLeftRadius: Responsive.widthPx(4),
    paddingTop: Responsive.widthPx(2),
    paddingBottom: Responsive.widthPx(4),
  },
  Fdr: {flexDirection: 'row'},
  locationText: {
    marginBottom: 10,
    marginTop: 5,
    marginStart: 20,
    fontSize: 20,
    fontFamily: Platform.OS == 'ios' ? 'Roboto-Bold' : 'Segoe UI Bold',
    textAlign: 'center',
    color: Colors.black,
  },
});
