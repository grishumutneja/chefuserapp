import React from 'react';
import {
  View,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';

export const LoadingModal = ({modalVisible, onPressCancel}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={styles.modalBox}>
            <ActivityIndicator size={'large'} color={Colors.red_primary} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    height: '100%',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: 0,
    borderRadius: 10,
    width: Responsive.widthPx(20),
    height: Responsive.widthPx(20),
    justifyContent: 'center',
  },
});
