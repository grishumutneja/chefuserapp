import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';

export const MenuModal = ({
  modalVisible,
  onPressCancel,
  data,
  setMenuVisible,
  menuRef,
  setMoveIndex,
}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={styles.header}>
            <Text style={{...TextStyles.black_S_20_700}}>Browse Menu</Text>
            <TouchableOpacity onPress={onPressCancel}>
              <Image source={Images.cancel} style={styles.cancelBox} />
            </TouchableOpacity>
          </View>
          <FlatList
            data={data}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  style={styles.textBox}
                  onPress={() => {
                    menuRef?.scrollToIndex({
                      animated: true,
                      index: index,
                    });
                    setMenuVisible(false);
                    setMoveIndex(index);
                  }}>
                  <Text style={TextStyles.black_S_15_400}>
                    {item?.menuName}
                  </Text>
                  <Text style={TextStyles.black_S_15_400}>
                    {item?.products?.length}
                  </Text>
                </TouchableOpacity>
              );
            }}
            key={(item, index) => {
              'MenuModal  Brouse menu ' + index;
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    maxHeight: Responsive.heightPx(80),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    backgroundColor: '#efefef',
    paddingBottom: Responsive.widthPx(18),
    // padding: Responsive.widthPx(5),
  },
  header: {
    backgroundColor: Colors.white,
    height: Responsive.widthPx(16),
    width: Responsive.widthPx(100),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    paddingHorizontal: Responsive.widthPx(5),
    ...SpStyles.FDR_ALC_JCS,
    ...SpStyles.shadowLight,
    marginBottom: Responsive.widthPx(2),
  },
  cancelBox: {
    width: Responsive.widthPx(7),
    height: Responsive.widthPx(7),
  },
  textBox: {
    paddingHorizontal: Responsive.widthPx(5),
    ...SpStyles.FDR_ALC_JCS,
    height: Responsive.widthPx(10),
  },
});
