import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  TouchableOpacity,
  Linking,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import WhiteButton from '../Buttons/WhiteButton';
import MyData from '../../Constants/MyData';

export const RestrictDiningModal = ({
  diningModalVisible,
  chefModalVisible,
  onPressCancel,
  setDiningModal,
  setChefModal,
}) => {
  const chefContent = () => {
    return (
      <>
        <Text style={styles.text}>
          Love cooking for Others ?{'\n'}
          {'\n'}
          Then we are looking for you.
          {'\n'}
          {'\n'}
          Register your 5 speciality Dishes with us and we will share it with
          the world.
        </Text>
      </>
    );
  };
  const diningContent = () => {
    return (
      <>
        <Text style={styles.text}>
          Coming Soon...
          {'\n'}
          {'\n'}
          {'\n'}
          {/* Now you can offer discounts to your customers on their DineIn bills
          with Cheflab. */}
        </Text>
      </>
    );
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={diningModalVisible || chefModalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={SpStyles.FDR_ALC_JCS}>
            <Text style={styles.textTitle}>
              {chefModalVisible ? 'Home Chef' : 'Eggs and Meat'}
            </Text>
            <TouchableOpacity onPress={onPressCancel} style={styles.closeCont}>
              <Image
                source={Images.close}
                style={styles.closeIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          {chefModalVisible ? chefContent() : diningContent()}
          {chefModalVisible && (
            <WhiteButton
              text={'Register Now'}
              otherStyle={styles.btn}
              otherTextStyle={TextStyles.red_17_700}
              onPress={() => {
                if (chefModalVisible) {
                  setChefModal(false);
                  Linking.openURL(MyData.chefUrl);
                } else {
                  Linking.openURL(MyData.diningUrl);
                  setDiningModal(false);
                }
              }}
            />
          )}
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    minHeight: '33%',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: Responsive.widthPx(6),
    borderTopRightRadius: Responsive.widthPx(6),
    backgroundColor: Colors.red_primary,
    padding: Responsive.widthPx(5),
    paddingBottom: Responsive.widthPx(10),
  },
  modalBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: 0,
    borderRadius: 10,
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(44),
    justifyContent: 'center',
  },
  selectPhoto: {
    fontSize: 12,
    fontWeight: '700',
    color: '#888',
    marginBottom: Responsive.widthPx(1),
  },
  modalBorderLine: {
    width: '88%',
    backgroundColor: '#fff',
    borderTopWidth: 0.35,
    borderColor: '#d4d4d4',
    borderRadius: Responsive.widthPx(2.5),
    height: Responsive.widthPx(14),
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Responsive.widthPx(3),
  },
  modalBorderLine2: {
    width: '88%',
    backgroundColor: '#fff',
    borderTopWidth: 0.35,
    borderColor: '#d4d4d4',
    borderRadius: 10,
    height: Responsive.widthPx(14),
    alignItems: 'center',
    justifyContent: 'center',
  },
  blue_700_21: {
    color: Colors.red_primary,
    fontWeight: '700',
    fontSize: 18,
  },
  modalCancelBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: Responsive.widthPx(0.2),
    borderRadius: Responsive.widthPx(2.5),
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(14),
    justifyContent: 'center',
  },
  textTitle: {
    ...TextStyles.white_S_25_700,
  },
  text: {
    ...TextStyles.white_S_15_700,
    marginTop: Responsive.widthPx(4),
  },
  btn: {
    borderRadius: Responsive.widthPx(2),
    marginTop: Responsive.widthPx(8),
    height: Responsive.widthPx(10),
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeIcon: {
    width: Responsive.widthPx(4),
    height: Responsive.widthPx(4),
    tintColor: Colors.white,
  },
  closeCont: {
    width: Responsive.widthPx(6),
    height: Responsive.widthPx(6),
    marginRight: Responsive.widthPx(1),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
