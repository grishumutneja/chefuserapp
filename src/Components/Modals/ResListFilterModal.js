import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import TextCheckButton from '../Buttons/TextCheckButton';
import RedBorderButton from '../Buttons/RedBorderButton';
import RedButton from '../Buttons/RedButton';
import {
  getFoodForYouRestaurantsApiCall,
  getRestaurantFoodCategoriesApiCall,
} from '../../Core/Config/ApiCalls';
import {useDispatch} from 'react-redux';

export const ResListFilterModal = ({
  modalVisible,
  onPressCancel,
  orderData,
  type,
  setFilterModalVisible,
  cuisines_id,
  locationData,
  setOffset,
  setLoading,
}) => {
  const dispatch = useDispatch();
  const [topRatedSelected, setTopRatedSelected] = useState(false);
  const [pureVegSelected, setPureVegSelected] = useState(false);
  const [nonVegSelected, setNonVegSelected] = useState(false);

  const applyFilterHandler = () => {
    let passArr = [];
    topRatedSelected && passArr.push('1');
    pureVegSelected && passArr.push('2');
    nonVegSelected && passArr.push('3');
    if (type == 'Choice') {
      if (passArr?.length > 0) {
        setOffset(0);
        let body = {
          cuisines_id: cuisines_id,
          vendor_offset: 0,
          vendor_limit: 10,
          filter: passArr,
          lat: locationData?.lat,
          lng: locationData?.lng,
        };
        setLoading(true);
        getFoodForYouRestaurantsApiCall({body, dispatch, setLoading});
        setFilterModalVisible(false);
      } else {
        setFilterModalVisible(false);
        setOffset(0);
        let body = {
          cuisines_id: cuisines_id,
          vendor_offset: 0,
          vendor_limit: 10,
          lat: locationData?.lat,
          lng: locationData?.lng,
        };
        setLoading(true);
        getFoodForYouRestaurantsApiCall({body, dispatch, setLoading});
      }
    } else {
      if (passArr?.length > 0) {
        setOffset(0);
        let body = {
          category_id: cuisines_id,
          vendor_offset: 0,
          vendor_limit: 10,
          filter: passArr,
          lat: locationData?.lat,
          lng: locationData?.lng,
        };
        setLoading(true);
        getRestaurantFoodCategoriesApiCall({body, dispatch, setLoading});
        setFilterModalVisible(false);
      } else {
        setFilterModalVisible(false);
        setOffset(0);
        let body = {
          category_id: cuisines_id,
          vendor_offset: 0,
          vendor_limit: 10,
          lat: locationData?.lat,
          lng: locationData?.lng,
        };
        setLoading(true);
        getRestaurantFoodCategoriesApiCall({body, dispatch, setLoading});
      }
    }
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={styles.header}>
            <Text style={{...TextStyles.black_S_20_700}}>Filter</Text>
            <TouchableOpacity onPress={onPressCancel}>
              <Image source={Images.cancel} style={styles.cancelBox} />
            </TouchableOpacity>
          </View>
          <TextCheckButton
            text={'Top Rated'}
            onPress={() => {
              setTopRatedSelected(!topRatedSelected);
            }}
            selected={topRatedSelected}
          />
          <TextCheckButton
            text={'Pure Veg'}
            onPress={() => {
              setPureVegSelected(!pureVegSelected);
            }}
            selected={pureVegSelected}
          />
          <TextCheckButton
            text={'Non Veg'}
            onPress={() => {
              setNonVegSelected(!nonVegSelected);
            }}
            selected={nonVegSelected}
          />
          <View style={styles.btnCont}>
            <RedBorderButton
              text={'Clear All'}
              otherStyle={styles.addAddressBtn}
              otherTextStyle={styles.addAddressText}
              onPress={() => {
                setNonVegSelected(false);
                setPureVegSelected(false);
                setTopRatedSelected(false);
              }}
            />
            <RedButton
              text={'Apply'}
              otherStyle={styles.confirmBtn}
              otherTextStyle={styles.confirmText}
              onPress={() => {
                applyFilterHandler();
              }}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    maxHeight: Responsive.heightPx(80),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    backgroundColor: '#fffffff0',
    paddingBottom: Responsive.widthPx(2),
    // padding: Responsive.widthPx(5),
  },
  header: {
    backgroundColor: Colors.white,
    height: Responsive.widthPx(16),
    width: Responsive.widthPx(100),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    paddingHorizontal: Responsive.widthPx(5),
    ...SpStyles.FDR_ALC_JCS,
    marginBottom: Responsive.widthPx(2),
  },
  cancelBox: {
    width: Responsive.widthPx(7),
    height: Responsive.widthPx(7),
  },
  addAddressBtn: {
    height: 42,
    borderRadius: Responsive.widthPx(3),
    width: Responsive.widthPx(42),
  },
  confirmBtn: {
    marginTop: Responsive.widthPx(0),
    width: Responsive.widthPx(42),
  },
  addAddressText: {
    ...TextStyles.red_16_700,
  },
  confirmText: {
    ...TextStyles.white_S_16_700,
  },
  btnCont: {
    ...SpStyles.FDR_ALC_JCS,
    width: Responsive.widthPx(88),
    alignSelf: 'center',
    paddingTop: Responsive.widthPx(4),
    paddingBottom: Responsive.widthPx(12),
  },
});
