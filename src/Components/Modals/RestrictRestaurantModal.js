import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';

export const RestrictRestaurantModal = ({
  modalVisible,
  onPressCancel,
  text,
}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <Text style={styles.textTitle}>Restaurants</Text>
          <Text style={styles.text}>{text}</Text>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    height: '33%',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: Responsive.widthPx(6),
    borderTopRightRadius: Responsive.widthPx(6),
    backgroundColor: Colors.red_primary,
    padding: Responsive.widthPx(5),
  },
  modalBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: 0,
    borderRadius: 10,
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(44),
    justifyContent: 'center',
  },
  selectPhoto: {
    fontSize: 12,
    fontWeight: '700',
    color: '#888',
    marginBottom: Responsive.widthPx(1),
  },
  modalBorderLine: {
    width: '88%',
    backgroundColor: '#fff',
    borderTopWidth: 0.35,
    borderColor: '#d4d4d4',
    borderRadius: Responsive.widthPx(2.5),
    height: Responsive.widthPx(14),
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Responsive.widthPx(3),
  },
  modalBorderLine2: {
    width: '88%',
    backgroundColor: '#fff',
    borderTopWidth: 0.35,
    borderColor: '#d4d4d4',
    borderRadius: 10,
    height: Responsive.widthPx(14),
    alignItems: 'center',
    justifyContent: 'center',
  },
  blue_700_21: {
    color: Colors.red_primary,
    fontWeight: '700',
    fontSize: 21,
  },
  modalCancelBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: Responsive.widthPx(0.2),
    borderRadius: Responsive.widthPx(2.5),
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(14),
    justifyContent: 'center',
  },
  textTitle: {
    ...TextStyles.white_S_30_700,
  },
  text: {
    ...TextStyles.white_S_16_400,
    marginTop: Responsive.widthPx(6),
  },
});
