import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Colors from '../../Constants/Colors';

const screenWidth = Dimensions.get('window').width;

export const PayMentMadeWaitModal = ({
  modalVisible,
  onPressCancel,
  onPressContinue,
}) => {
  const [hide, setHide] = useState(false);

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={{marginTop: screenWidth * 0.8}}>
            {/* <LottieView
              source={Animations.waitPayment}
              style={{
                width: screenWidth * 0.38,
              }}
              autoPlay={true}
              loop={true}
            /> */}
          </View>

          <Text style={styles.text}>
            ( Please do not close the screen for 15 seconds after the payment is
            made )
          </Text>
          {!hide && (
            <TouchableOpacity style={styles.btnCont} onPress={onPressContinue}>
              <Text style={styles.btnText}>Continue</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: Colors.red_primary,
    alignSelf: 'center',
  },
  btnCont: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    marginTop: 48,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 24,
    height: 40,
  },
  btnText: {
    fontSize: 20,
    color: Colors.red_primary,
    fontWeight: '700',
    fontFamily: 'Segoe UI',
  },
  text: {
    fontSize: 18,
    color: 'black',
    fontFamily: 'Segoe UI',
    paddingHorizontal: screenWidth * 0.06,
    textAlign: 'center',
    marginTop: screenWidth * 0.08,
  },
});
