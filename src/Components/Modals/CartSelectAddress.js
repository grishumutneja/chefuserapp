import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import RedBorderButton from '../Buttons/RedBorderButton';
import RedButton from '../Buttons/RedButton';
import {useDispatch, useSelector} from 'react-redux';
import {
  getCartListApiCall,
  getDeliveryAddressCall,
} from '../../Core/Config/ApiCalls';
import Toast from 'react-native-toast-message';
import NoDataAvailable from '../Inputs/NoDataAvailable';
import CustomToast from '../Toast/CustomToast';
import Utility from '../../Constants/Utility';

const AddressCard = ({
  item,
  index,
  defaultAddress,
  setDefaultAddress,
  setSelectedAddress,
  setLocationData,
  isPrimary,
}) => {
  return (
    <TouchableOpacity
      style={styles.addressCardCont}
      onPress={() => {
        setDefaultAddress(index);
        setSelectedAddress(item);
      }}>
      <View style={SpStyles.FDR_ALC}>
        <Text style={styles.typeText}>
          {item?.address_type == '1'
            ? 'Home'
            : item?.address_type == '2'
            ? 'Work'
            : item.address_type == 3
            ? 'Others'
            : null}
        </Text>
        {isPrimary == index && <Text style={styles.primary}>( Primary )</Text>}
      </View>
      <Text style={styles.locationText}>{item?.house_no}</Text>
      <Text style={styles.landmarkText}>Nearest landmark: {item?.reach}</Text>
      <View style={styles.circle}>
        {index == defaultAddress ? <View style={styles.checkedCircle} /> : null}
      </View>
    </TouchableOpacity>
  );
};

export const CartSelectAddress = ({
  modalVisible,
  onPressCancel,
  onPressAddAddress,
  setSelectDelModalVisible,
  setLocationData,
  discountAmount,
}) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [defaultAddress, setDefaultAddress] = useState();
  const [selectedAddress, setSelectedAddress] = useState();
  const {deliveryAddressData} = useSelector(state => state.deliveryAddress);
  const {loginData} = useSelector(state => state.userInfo);
  const [isPrimary, setIsPrimary] = useState(false);

  useEffect(() => {
    if (deliveryAddressData) {
      deliveryAddressData?.map((item, index) => {
        if (item?.primary_key == 1) {
          // setDefaultAddress(index);
          setIsPrimary(index);
          {
            /* setSelectedAddress(item); */
          }
        }
      });
    } else {
      getDeliveryAddressCall({dispatch, userId: loginData?.user_id});
    }
  }, [modalVisible]);

  useEffect(() => {
    setLoading(false);
  }, [modalVisible]);

  const confirmHandler = () => {
    if (defaultAddress >= 0) {
      setLocationData && setLocationData(selectedAddress);
      setLoading(true);
      const cartBody = {
        user_id: loginData?.user_id,
        lat: selectedAddress.lat,
        lng: selectedAddress.long,
        discount_amount: discountAmount,
      };
      getCartListApiCall({
        body: cartBody,
        dispatch,
        isRechargeDone: true,
      }).then(res => {
        setSelectDelModalVisible(false);
      });
    } else {
      Utility.showToast('Please select the Address');
    }
  };

  const toastConfig = {
    tomatoToast: ({text1, props}) => (
      <CustomToast text1={text1} props={props} />
    ),
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View
          style={styles.modalSubContainer}
          key={loading + deliveryAddressData?.length}>
          {!loading ? (
            <View style={styles.modalBox}>
              <View style={SpStyles.FDR_ALC_JCS}>
                <Text style={styles.titleText}>Select delivery address</Text>
                <TouchableOpacity
                  onPress={() => {
                    setSelectDelModalVisible(false);
                  }}>
                  <Image source={Images.close} style={styles.close} />
                </TouchableOpacity>
              </View>
              <View style={styles.addressCont}>
                {deliveryAddressData?.length == 0 ? (
                  <NoDataAvailable
                    text={'No address found'}
                    otherStyle={{marginBottom: Responsive.widthPx(10)}}
                  />
                ) : (
                  <FlatList
                    data={deliveryAddressData}
                    renderItem={({item, index}) => {
                      return (
                        <AddressCard
                          item={item}
                          index={index}
                          setDefaultAddress={setDefaultAddress}
                          defaultAddress={defaultAddress}
                          setSelectedAddress={setSelectedAddress}
                          setLocationData={setLocationData}
                          isPrimary={isPrimary}
                        />
                      );
                    }}
                  />
                )}
              </View>
              <View style={styles.btnCont}>
                <RedBorderButton
                  text={'+ Add Address'}
                  otherStyle={styles.addAddressBtn}
                  otherTextStyle={styles.addAddressText}
                  onPress={() => {
                    setSelectedAddress();
                    setDefaultAddress();
                    onPressAddAddress();
                  }}
                />
                {deliveryAddressData?.length > 0 && (
                  <RedButton
                    text={'Confirm'}
                    otherStyle={styles.confirmBtn}
                    otherTextStyle={styles.confirmText}
                    onPress={() => {
                      confirmHandler();
                    }}
                  />
                )}
              </View>
            </View>
          ) : (
            <View
              style={[
                styles.modalBox,
                {
                  height: Responsive.heightPx(50),
                  paddingTop: Responsive.widthPx(30),
                },
              ]}>
              <View style={styles.loaderBox}>
                <ActivityIndicator size={'large'} color={Colors.red_primary} />
              </View>
            </View>
          )}
        </View>
      </View>
      <Toast config={toastConfig} />
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
  },
  modalBox: {
    backgroundColor: 'white',
    borderRadius: Responsive.widthPx(3.5),
    width: Responsive.widthPx(100),
    minHeight: Responsive.widthPx(40),
  },
  titleText: {
    ...TextStyles.black_S_20_700,
    marginLeft: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
  },
  close: {
    width: Responsive.widthPx(4),
    height: Responsive.widthPx(4),
    marginRight: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
  },
  addressCardCont: {
    width: Responsive.widthPx(94),
    paddingVertical: Responsive.widthPx(3),
    paddingHorizontal: Responsive.widthPx(4),
    backgroundColor: Colors.white,
    ...SpStyles.shadowLight,
    marginVertical: Responsive.widthPx(2),
    alignSelf: 'center',
    borderRadius: Responsive.widthPx(4),
  },
  typeText: {
    ...TextStyles.red_14_700,
  },
  locationText: {
    ...TextStyles.black_S_16_700,
    marginTop: Responsive.widthPx(0.7),
  },
  landmarkText: {
    ...TextStyles.darkGray_S_14_500,
    marginTop: Responsive.widthPx(0.7),
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ACACAC',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: Responsive.widthPx(4),
    top: Responsive.widthPx(3),
  },
  checkedCircle: {
    width: 12,
    height: 12,
    borderRadius: 6,
    backgroundColor: Colors.red_primary,
  },
  addressCont: {
    marginTop: Responsive.widthPx(2),
    marginBottom: Responsive.widthPx(4),
    maxHeight: Responsive.heightPx(58),
  },
  addAddressBtn: {
    height: 42,
    borderRadius: Responsive.widthPx(3),
    width: Responsive.widthPx(45),
  },
  confirmBtn: {
    marginTop: Responsive.widthPx(0),
    width: Responsive.widthPx(45),
    marginLeft: Responsive.widthPx(2),
  },
  addAddressText: {
    ...TextStyles.red_16_700,
  },
  confirmText: {
    ...TextStyles.white_S_16_700,
  },
  btnCont: {
    width: Responsive.widthPx(94),
    alignSelf: 'center',
    paddingTop: Responsive.widthPx(4),
    paddingBottom: Responsive.widthPx(12),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  loaderBox: {
    backgroundColor: '#efefef',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: 0,
    borderRadius: 10,
    width: Responsive.widthPx(20),
    height: Responsive.widthPx(20),
    justifyContent: 'center',
    alignSelf: 'center',
  },
  primary: {
    ...TextStyles.black_S_10_700,
    marginLeft: 8,
    color: Colors.blue_224887,
  },
});
