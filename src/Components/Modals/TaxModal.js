import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import WhiteButton from '../Buttons/WhiteButton';

export const TaxModal = ({modalVisible, onPressCancel, platFormFee, tax}) => {
  const chefContent = () => {
    const taxAmount = parseFloat(tax - platFormFee);
    return (
      <View style={{marginTop: Responsive.widthPx(2)}}>
        <Text style={styles.text}>
          ChefLab has no role to play in taxes and charges being levied upon by
          the govt. and restaurants
        </Text>
        <View style={styles.box}>
          <Text style={TextStyles.white_S_15_400}>Tax</Text>
          <Text style={TextStyles.white_S_15_700}>
            ₹ {taxAmount?.toFixed(2)}
          </Text>
        </View>
        <View style={styles.box}>
          <Text style={TextStyles.white_S_15_400}>Platform fee</Text>
          <Text style={TextStyles.white_S_15_700}>
            ₹ {platFormFee?.toFixed(2)}
          </Text>
        </View>
        <View style={styles.box}>
          <Text style={TextStyles.white_S_15_400}>Total Amount</Text>
          <Text style={TextStyles.white_S_15_700}>₹ {tax?.toFixed(2)}</Text>
        </View>
      </View>
    );
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={SpStyles.FDR_ALC_JCS}>
            <Text style={styles.textTitle}>{'Tax Information'}</Text>
            <TouchableOpacity onPress={onPressCancel}>
              <Image
                source={Images.close}
                style={styles.closeBtn}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          {chefContent()}
          <WhiteButton
            text={'Okay'}
            otherStyle={styles.button}
            otherTextStyle={TextStyles.red_17_700}
            onPress={onPressCancel}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    minHeight: '33%',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: Responsive.widthPx(6),
    borderTopRightRadius: Responsive.widthPx(6),
    backgroundColor: Colors.red_primary,
    padding: Responsive.widthPx(5),
    paddingBottom: Responsive.widthPx(10),
  },
  modalBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: 0,
    borderRadius: 10,
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(44),
    justifyContent: 'center',
  },
  selectPhoto: {
    fontSize: 12,
    fontWeight: '700',
    color: '#888',
    marginBottom: Responsive.widthPx(1),
  },
  modalBorderLine: {
    width: '88%',
    backgroundColor: '#fff',
    borderTopWidth: 0.35,
    borderColor: '#d4d4d4',
    borderRadius: Responsive.widthPx(2.5),
    height: Responsive.widthPx(14),
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Responsive.widthPx(3),
  },
  modalBorderLine2: {
    width: '88%',
    backgroundColor: '#fff',
    borderTopWidth: 0.35,
    borderColor: '#d4d4d4',
    borderRadius: 10,
    height: Responsive.widthPx(14),
    alignItems: 'center',
    justifyContent: 'center',
  },
  blue_700_21: {
    color: Colors.red_primary,
    fontWeight: '700',
    fontSize: 18,
  },
  modalCancelBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: Responsive.widthPx(0.2),
    borderRadius: Responsive.widthPx(2.5),
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(14),
    justifyContent: 'center',
  },
  textTitle: {
    ...TextStyles.white_S_20_700,
  },
  text: {
    ...TextStyles.white_S_12_400,
    marginTop: Responsive.widthPx(4),
    marginBottom: Responsive.widthPx(2),
  },
  box: {
    ...SpStyles.FDR_ALC_JCS,
    marginVertical: Responsive.widthPx(2),
  },
  button: {
    borderRadius: Responsive.widthPx(2),
    marginTop: Responsive.widthPx(8),
    height: Responsive.widthPx(10),
    alignItems: 'center',
    justifyContent: 'center',
    width: Responsive.widthPx(44),
    alignSelf: 'center',
  },
  closeBtn: {
    width: Responsive.widthPx(4),
    height: Responsive.widthPx(4),
    marginRight: Responsive.widthPx(1),
    tintColor: Colors.white,
  },
});
