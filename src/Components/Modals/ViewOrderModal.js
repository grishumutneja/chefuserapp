import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import OrderDetailCard from '../Cards/OrderDetailCard';

export const ViewOrderModal = ({modalVisible, onPressCancel, orderData}) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={styles.header}>
            <Text style={{...TextStyles.black_S_20_700}}>Order details</Text>
            <TouchableOpacity onPress={onPressCancel}>
              <Image source={Images.cancel} style={styles.cancelBox} />
            </TouchableOpacity>
          </View>
          <ScrollView style={{flex: 1, marginBottom: Responsive.widthPx(10)}}>
            <OrderDetailCard orderData={orderData} />
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    maxHeight: Responsive.heightPx(80),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    backgroundColor: Colors.lightGray2,
    // padding: Responsive.widthPx(5),
  },
  header: {
    backgroundColor: Colors.white,
    height: Responsive.widthPx(16),
    width: Responsive.widthPx(100),
    borderTopLeftRadius: Responsive.widthPx(4),
    borderTopRightRadius: Responsive.widthPx(4),
    paddingHorizontal: Responsive.widthPx(5),
    ...SpStyles.FDR_ALC_JCS,
    marginBottom: Responsive.widthPx(2),
  },
  cancelBox: {
    width: Responsive.widthPx(7),
    height: Responsive.widthPx(7),
  },
});
