import {
  View,
  TouchableWithoutFeedback,
  Modal,
  StyleSheet,
  Text,
  Linking,
  Platform,
  TouchableOpacity,
  Image,
} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import WhiteButton from '../Buttons/WhiteButton';
import SpStyles from '../../Styles/SpStyles';
import MyData from '../../Constants/MyData';
import Images from '../../Constants/Images';

const RestrictUpdateModal = ({
  modalVisible,
  onPressCancel,
  softUpdate,
  setModalVisible,
  newAvailableVersion,
}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={SpStyles.FDR_ALC_JCS}>
            <Text style={styles.textTitle}>
              App update available - {newAvailableVersion}
            </Text>
            {softUpdate == '1' && (
              <TouchableOpacity
                onPress={() => {
                  setModalVisible(false);
                }}>
                <Image source={Images.close} style={styles.close} />
              </TouchableOpacity>
            )}
          </View>
          <Text style={styles.text}>
            App update available, please update the app to use the latest
            version.
          </Text>
          <View style={styles.btnCont}>
            {softUpdate == '1' && (
              <WhiteButton
                text={'Cancel'}
                otherStyle={styles.btn}
                otherTextStyle={TextStyles.red_17_700}
                onPress={() => {
                  setModalVisible(false);
                }}
              />
            )}
            <WhiteButton
              text={'Update'}
              otherStyle={styles.btn}
              otherTextStyle={TextStyles.red_17_700}
              onPress={() => {
                Platform.OS == 'android'
                  ? Linking.openURL(MyData.playStoreLink)
                  : Linking.openURL(MyData.AppStoreLink);
              }}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default RestrictUpdateModal;

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    height: '33%',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: Responsive.widthPx(6),
    borderTopRightRadius: Responsive.widthPx(6),
    backgroundColor: Colors.red_primary,
    paddingBottom: Responsive.widthPx(5),
    paddingHorizontal: Responsive.widthPx(6),
    paddingTop: Responsive.widthPx(7),
  },
  textTitle: {
    ...TextStyles.white_S_23_700,
  },
  text: {
    ...TextStyles.white_S_16_400,
    marginTop: Responsive.widthPx(6),
  },
  btnCont: {
    ...SpStyles.FDR_ALC_JCS,
    width: Responsive.widthPx(82),
    alignSelf: 'center',
    marginTop: Responsive.widthPx(4),
    justifyContent: 'center',
  },
  btn: {
    borderRadius: Responsive.widthPx(2),
    marginTop: Responsive.widthPx(8),
    height: Responsive.widthPx(10),
    alignItems: 'center',
    justifyContent: 'center',
    width: Responsive.widthPx(36),
    marginHorizontal: Responsive.widthPx(3),
  },
  close: {
    width: Responsive.widthPx(5),
    height: Responsive.widthPx(5),
    marginRight: Responsive.widthPx(2.5),
    tintColor: Colors.white,
  },
});
// import {
//   View,
//   TouchableWithoutFeedback,
//   Modal,
//   StyleSheet,
//   Text,
//   Linking,
//   Platform,
//   TouchableOpacity,
// } from 'react-native';
// import React from 'react';
// import Responsive from '../../Constants/Responsive';
// import Colors from '../../Constants/Colors';
// import TextStyles from '../../Styles/TextStyles';

// const RestrictUpdateModal = ({
//   modalVisible,
//   onPressCancel,
//   softUpdate,
//   setModalVisible,
//   forceUpdate,
//   newAvailableVersion,
// }) => {
//   return (
//     <Modal
//       animationType="fade"
//       transparent={true}
//       visible={modalVisible}
//       onRequestClose={onPressCancel}>
//       <View style={styles.modalContainer}>
//         <TouchableWithoutFeedback onPress={onPressCancel}>
//           <View style={styles.modalContainer} />
//         </TouchableWithoutFeedback>
//         <View style={styles.modalSubContainer}>
//           <View
//             style={{
//               backgroundColor: '#00000090',
//               flexGrow: 1,
//               justifyContent: 'flex-end',
//             }}>
//             <TouchableOpacity
//               activeOpacity={1}
//               onPress={() => {
//                 if (forceUpdate == '1') {
//                   // setModalVisible(false);
//                 } else {
//                   setModalVisible(false);
//                 }
//               }}
//               style={{
//                 flex: 1,
//                 justifyContent: 'flex-end',
//                 alignItems: 'center',
//               }}
//             />
//             <View style={{maxHeight: Responsive.heightPx(70)}}>
//               <View
//                 style={{
//                   backgroundColor: Colors.white,
//                   borderTopRightRadius: 15,
//                   borderTopLeftRadius: 15,
//                 }}>
//                 <View
//                   style={{
//                     flexDirection: 'row',
//                     alignItems: 'center',
//                     justifyContent: 'space-between',
//                     paddingVertical: 15,
//                   }}>
//                   <Text
//                     style={[
//                       {
//                         marginStart: 20,
//                         fontSize: 18,
//                         textAlign: 'center',
//                         ...TextStyles.black_S_20_700,
//                       },
//                     ]}>
// App update available - {newAvailableVersion}
//                   </Text>
//                   {/* <FontAwesome
//                     name="close"
//                     size={20}
//                     color={COLORS.black}
//                     style={{
//                       marginEnd: 25,
//                     }}
//                     onPress={() => {
//                       if (forceUpdate == '1') {
//                         // setModalVisible(false);
//                       } else {
//                         setModalVisible(false);
//                       }
//                     }}
//                   /> */}
//                 </View>
//                 <Text
//                   style={{
//                     marginVertical: 10,
//                     marginHorizontal: 25,
//                     ...TextStyles.black_S_16_400,
//                   }}>
//   App update available, please update the app to use the latest
//   version.
//                 </Text>
//               </View>
//             </View>
//             <View
//               style={{
//                 flexDirection: 'row',
//                 justifyContent: 'center',
//                 paddingVertical: 10,
//                 backgroundColor: Colors.gray,
//               }}>
//               <View
//                 style={{
//                   flexDirection: 'row',
//                   // alignItems: 'center',
//                   justifyContent: 'center',
//                 }}>
//                 {softUpdate == '1' ? (
//                   <TouchableOpacity
//                     onPress={() => {
//   setModalVisible(false);
//                     }}
//                     activeOpacity={0.8}
//                     style={{
//                       borderRadius: 10,
//                       borderWidth: 1,
//                       borderColor: Colors.red_primary,
//                       paddingHorizontal: 10,
//                       paddingVertical: 5,
//                       alignItems: 'center',
//                       justifyContent: 'center',
//                       marginVertical: 5,
//                       width: '40%',
//                       height: 45,
//                       marginHorizontal: 10,
//                     }}>
//                     <Text
//                       style={[
//                         {
//                           color: Colors.black,
//                           fontSize: 18,
//                           fontFamily: 'Segoe UI Bold',
//                           textAlign: 'center',
//                         },
//                       ]}>
//                       Cancel
//                     </Text>
//                   </TouchableOpacity>
//                 ) : null}

//                 <TouchableOpacity
//                   onPress={() => {
//                     // if (forceUpdate == '0') {

//                     // } else {
//                     //   ShowMessage('User will be able to update the app');
//                     // }
// Platform.OS == 'android'
//   ? Linking.openURL(
//       'https://play.google.com/store/apps/details?id=com.cheflab',
//     )
//   : Linking.openURL(
//       'https://apps.apple.com/in/app/cheflab/id1663671845',
//     );
//                   }}
//                   activeOpacity={0.8}
//                   style={{
//                     backgroundColor: Colors.red_primary,
//                     paddingHorizontal: 10,
//                     paddingVertical: 5,
//                     alignItems: 'center',
//                     justifyContent: 'center',
//                     marginVertical: 5,
//                     marginHorizontal: 10,
//                     borderRadius: 10,
//                     width: softUpdate == '1' ? '40%' : '60%',
//                     height: 45,
//                   }}>
//                   <Text
//                     style={[
//                       {
//                         color: Colors.white,
//                         fontSize: 18,
//                         fontFamily: 'Segoe UI Bold',
//                         textAlign: 'center',
//                       },
//                     ]}>
//                     Update
//                   </Text>
//                 </TouchableOpacity>
//               </View>
//             </View>
//           </View>
//         </View>
//       </View>
//     </Modal>
//   );
// };

// export default RestrictUpdateModal;

// const styles = StyleSheet.create({
//   modalContainer: {
//     width: '100%',
//     height: '100%',
//     backgroundColor: 'rgba(0,0,0,0.4)',
//   },
//   modalSubContainer: {
//     width: Responsive.widthPx(100),
//     position: 'absolute',
//     bottom: 0,
//     maxHeight: Responsive.heightPx(80),
//     borderTopLeftRadius: Responsive.widthPx(4),
//     borderTopRightRadius: Responsive.widthPx(4),
//     backgroundColor: Colors.lightGray2,
//     paddingBottom: Responsive.widthPx(18),
//     // padding: Responsive.widthPx(5),
//   },
// });
