import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  TextInput,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import WhiteButton from '../Buttons/WhiteButton';
import RedBorderButton from '../Buttons/RedBorderButton';
import Utility from '../../Constants/Utility';
import {
  applyPromoCode,
  applyWallet,
} from '../../Core/Redux/Slices/RestaurantsSlices/CartSlice';
import {useDispatch} from 'react-redux';
import {promoCodeApplyApiCall} from '../../Core/Config/ApiCalls';
import Toast from 'react-native-toast-message';
import CustomToast from '../Toast/CustomToast';

export const PromoCodeModal = ({
  modalVisible,
  onPressCancel,
  promoCodeList,
  setDiscountAmount,
  totalPrice,
  setPromoCodeModal,
  setAppliedCode,
  successCall,
  appliedCode,
  setWalletAmountApply,
  walletAmountApply,
  setCouponId,
}) => {
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(-1);
  const [code, setCode] = useState();

  const promoCodeHandler = item => {
    const discount = parseFloat(item?.discount);
    if (totalPrice <= item?.minimum_order_amount) {
      Utility.showToast(
        'Minimum Order Value Required : ' + item?.minimum_order_amount,
      );
    } else {
      setWalletAmountApply();
      walletAmountApply && dispatch(applyWallet(-walletAmountApply));
      setAppliedCode(item?.code);
      setCouponId(item?.id);
      if (item?.discount_type == 1) {
        const disAmount = (discount * totalPrice) / 100;
        if (disAmount < parseFloat(item?.maxim_dis_amount)) {
          setDiscountAmount(parseFloat(disAmount));
          dispatch(applyPromoCode({discount: parseFloat(disAmount)}));
        } else {
          setDiscountAmount(parseFloat(item?.maxim_dis_amount));
          dispatch(
            applyPromoCode({
              discount: parseFloat(item?.maxim_dis_amount),
            }),
          );
        }
        successCall();
        // setDiscountAmount(item?.discount);
        setPromoCodeModal(false);
      } else {
        const disAmount = discount;
        if (disAmount < parseFloat(item?.maxim_dis_amount)) {
          setDiscountAmount(parseFloat(disAmount));
          dispatch(applyPromoCode({discount: parseFloat(disAmount)}));
        } else {
          setDiscountAmount(parseFloat(item?.maxim_dis_amount));
          dispatch(
            applyPromoCode({
              discount: parseFloat(item?.maxim_dis_amount),
            }),
          );
        }
        successCall();
        // setDiscountAmount(item?.discount);
        setPromoCodeModal(false);
      }
    }
  };

  const chefContent = () => {
    return (
      <View style={{marginTop: Responsive.widthPx(2)}}>
        {promoCodeList?.map((item, index) => {
          return (
            <TouchableOpacity
              style={styles.box}
              onPress={() => {
                isOpen == index ? setIsOpen(-1) : setIsOpen(index);
              }}
              key={'Promo Code Modal chefContent' + index}>
              <View
                style={{
                  width: Responsive.widthPx(60),
                }}>
                <Text style={[TextStyles.red_17_700, {marginBottom: 4}]}>
                  {item?.code}
                </Text>
                <Text style={TextStyles.black_S_12_700}>{item?.name}</Text>
                {isOpen == index && (
                  <View style={{marginTop: 4}}>
                    <Text style={TextStyles.black_S_12_400}>
                      {item?.discount_type == '1'
                        ? `Discount: ${item?.discount}%`
                        : `Discount: ₹${item?.discount}`}
                    </Text>
                    <Text style={TextStyles.black_S_12_400}>
                      Valid upto: {item?.to}
                    </Text>
                    <Text style={TextStyles.black_S_12_400}>
                      Min. order value: ₹{item?.minimum_order_amount}
                    </Text>
                    <Text style={TextStyles.black_S_12_400}>
                      Max. Discount Amount: ₹{item?.maxim_dis_amount}
                    </Text>
                    <Text style={TextStyles.black_S_12_400}>
                      {item?.description}
                    </Text>
                  </View>
                )}
              </View>
              {appliedCode == item?.code ? (
                <Text
                  style={[
                    TextStyles.red_16_700,
                    {marginRight: Responsive.widthPx(2)},
                  ]}>
                  Applied
                </Text>
              ) : (
                <RedBorderButton
                  text={'Apply'}
                  otherStyle={{paddingVertical: 5}}
                  onPress={() => {
                    promoCodeHandler(item);
                  }}
                />
              )}
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  const toastConfig = {
    tomatoToast: ({text1, props}) => (
      <CustomToast text1={text1} props={props} />
    ),
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}
      style={{flexGrow: 1}}>
      <KeyboardAvoidingView
        behavior={
          Platform.OS == 'ios'
            ? promoCodeList?.length > 2
              ? null
              : 'padding'
            : null
        }
        style={{flexGrow: 1}}>
        <View style={styles.modalContainer}>
          <TouchableWithoutFeedback onPress={onPressCancel}>
            <View style={styles.modalContainer} />
          </TouchableWithoutFeedback>
          <View style={styles.modalSubContainer}>
            <View style={SpStyles.FDR_ALC_JCS}>
              <Text style={styles.textTitle}>{'Promo Code / Coupons'}</Text>
              <TouchableOpacity
                onPress={onPressCancel}
                style={styles.closeBtnCont}>
                <Image
                  source={Images.cancel}
                  style={styles.closeBtn}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>

            <View style={styles.inputBox}>
              <TextInput
                placeholder="Enter Coupon Code"
                placeholderTextColor={Colors.darkGray}
                style={styles.textInput}
                onChangeText={text => {
                  setCode(text);
                }}
                value={code}
              />
              {code?.length > 1 && (
                <RedBorderButton
                  text={'Apply'}
                  otherStyle={{
                    paddingVertical: 5,
                    marginRight: Responsive.widthPx(2),
                  }}
                  onPress={() => {
                    const body = {
                      code: code,
                    };
                    promoCodeApplyApiCall({body}).then(res => {
                      promoCodeHandler(res);
                    });
                  }}
                />
              )}
            </View>
            {chefContent()}
            <WhiteButton
              text={'Okay'}
              otherStyle={styles.button}
              otherTextStyle={TextStyles.white_S_18_700}
              onPress={onPressCancel}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
      <Toast config={toastConfig} />
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    minHeight: '33%',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: Responsive.widthPx(6),
    borderTopRightRadius: Responsive.widthPx(6),
    backgroundColor: Colors.white,
    padding: Responsive.widthPx(5),
    paddingBottom: Responsive.widthPx(10),
  },
  modalBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: 0,
    borderRadius: 10,
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(44),
    justifyContent: 'center',
  },
  selectPhoto: {
    fontSize: 12,
    fontWeight: '700',
    color: '#888',
    marginBottom: Responsive.widthPx(1),
  },
  modalBorderLine: {
    width: '88%',
    backgroundColor: '#fff',
    borderTopWidth: 0.35,
    borderColor: '#d4d4d4',
    borderRadius: Responsive.widthPx(2.5),
    height: Responsive.widthPx(14),
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Responsive.widthPx(3),
  },
  modalBorderLine2: {
    width: '88%',
    backgroundColor: '#fff',
    borderTopWidth: 0.35,
    borderColor: '#d4d4d4',
    borderRadius: 10,
    height: Responsive.widthPx(14),
    alignItems: 'center',
    justifyContent: 'center',
  },
  blue_700_21: {
    color: Colors.red_primary,
    fontWeight: '700',
    fontSize: 18,
  },
  modalCancelBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: Responsive.widthPx(0.2),
    borderRadius: Responsive.widthPx(2.5),
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(14),
    justifyContent: 'center',
  },
  textTitle: {
    ...TextStyles.black_S_20_700,
    marginBottom: Responsive.widthPx(4),
  },
  text: {
    ...TextStyles.white_S_12_400,
    marginTop: Responsive.widthPx(4),
    marginBottom: Responsive.widthPx(2),
  },
  box: {
    ...SpStyles.FDR_ALC_JCS,
    marginVertical: Responsive.widthPx(2),
    backgroundColor: Colors.white,
    paddingVertical: Responsive.widthPx(3),
    borderRadius: Responsive.widthPx(3),
    paddingHorizontal: Responsive.widthPx(4),
    ...SpStyles.shadowLight,
  },
  button: {
    borderRadius: Responsive.widthPx(2),
    marginTop: Responsive.widthPx(8),
    height: Responsive.widthPx(10),
    alignItems: 'center',
    justifyContent: 'center',
    width: Responsive.widthPx(44),
    alignSelf: 'center',
    backgroundColor: Colors.red_primary,
  },
  closeBtn: {
    width: Responsive.widthPx(6),
    height: Responsive.widthPx(6),
    marginBottom: Responsive.widthPx(4),
  },
  closeBtnCont: {
    width: Responsive.widthPx(9),
    height: Responsive.widthPx(9),
    alignItems: 'center',
  },
  textInput: {
    ...TextStyles.black_S_16_400,
    paddingBottom: 0,
    width: Responsive.widthPx(60),
  },
  inputBox: {
    marginHorizontal: Responsive.widthPx(2),
    borderBottomColor: Colors.darkGray,
    borderBottomWidth: 2,
    paddingVertical: Responsive.widthPx(2),
    ...SpStyles.FDR_ALC_JCS,
    height: Responsive.widthPx(14),
    paddingLeft: Responsive.widthPx(2),
  },
});

// discount type 1 == percentage discount
// discount type 0 == flat discount
