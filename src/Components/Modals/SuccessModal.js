import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
} from 'react-native';
import LottieView from 'lottie-react-native';
import Responsive from '../../Constants/Responsive';
import Animations from '../../Constants/Animations';
import TextStyles from '../../Styles/TextStyles';

export const SuccessModal = ({
  modalVisible,
  onPressCancel,
  appliedCode,
  discountAmount,
}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <LottieView
          source={Animations.cong}
          style={styles.lotiTop}
          autoPlay={true}
          loop={true}
        />
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={styles.modalBox}>
            <LottieView
              source={Animations.suc}
              style={styles.lotiSuccess}
              autoPlay={true}
              loop={false}
            />
            <Text style={styles.selectPhoto}>{appliedCode} applied</Text>
            <Text style={styles.text}>
              You saved ₹{discountAmount?.toFixed(2)}
            </Text>
            <Text style={styles.selectPhoto}>with this coupon code</Text>
          </View>
        </View>
        <LottieView
          source={Animations.cong}
          style={styles.lotiBottom}
          autoPlay={true}
          loop={true}
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
    alignItems: 'center',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    minHeight: '40%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
    position: 'absolute',
    height: '100%',
    justifyContent: 'center',
  },
  modalBox: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: 0,
    borderRadius: 10,
    width: Responsive.widthPx(92),
    paddingVertical: Responsive.widthPx(5),
    justifyContent: 'center',
  },
  selectPhoto: {
    ...TextStyles.black_S_12_700,
    marginVertical: Responsive.widthPx(1),
  },
  text: {
    ...TextStyles.black_S_16_700,
    marginVertical: Responsive.widthPx(1),
  },
  lotiSuccess: {
    height: 120,
    width: 120,
    marginBottom: Responsive.widthPx(-8),
    marginTop: Responsive.widthPx(-1),
  },
  lotiBottom: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
  },
  lotiTop: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    top: 0,
  },
});
