import React, {useEffect} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import LottieView from 'lottie-react-native';
import Animations from '../../Constants/Animations';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';

const screenWidth = Dimensions.get('window').width;

export const PaymentWaitModal = ({
  modalVisible,
  onPressCancel,
  setWaitTimeComplete,
}) => {
  useEffect(() => {
    if (Platform.OS == 'ios') {
      setTimeout(() => {
        setWaitTimeComplete && setWaitTimeComplete(true);
      }, 5000);
    }
  }, []);

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={{marginTop: screenWidth * 0.6}}>
            <LottieView
              source={Animations.waitPayment}
              style={{width: Responsive.widthPx(38)}}
              autoPlay={true}
              loop={true}
            />
          </View>
          <Text style={styles.title}>Waiting For Payment Conformation</Text>
          <Text style={styles.text}>
            Do not close Application until process done
          </Text>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: Colors.red_primary,
    alignSelf: 'center',
  },
  title: {
    paddingHorizontal: Responsive.widthPx(6),
    textAlign: 'center',
    ...TextStyles.white_S_21_700,
  },
  text: {
    paddingHorizontal: Responsive.widthPx(6),
    textAlign: 'center',
    marginTop: Responsive.widthPx(8),
    ...TextStyles.white_S_14_400,
  },
});
