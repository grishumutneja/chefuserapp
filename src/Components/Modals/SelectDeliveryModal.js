import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
  ActivityIndicator,
  Platform,
  Linking,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import RedBorderButton from '../Buttons/RedBorderButton';
import RedButton from '../Buttons/RedButton';
import {useDispatch, useSelector} from 'react-redux';
import {
  getDeliveryAddressCall,
  updateDeliveryAddressApiCall,
} from '../../Core/Config/ApiCalls';
import {useNavigation} from '@react-navigation/native';
import Toast from 'react-native-toast-message';
import NoDataAvailable from '../Inputs/NoDataAvailable';
import CustomToast from '../Toast/CustomToast';

const AddressCard = ({
  item,
  index,
  defaultAddress,
  setDefaultAddress,
  setSelectedAddress,
  setLocationData,
  isLocationIcon,
}) => {
  return (
    <TouchableOpacity
      style={styles.addressCardCont}
      onPress={() => {
        setDefaultAddress(index);
        setSelectedAddress(item);
        setLocationData && setLocationData(item);
      }}>
      <Text style={styles.typeText}>
        {item?.address_type == '1'
          ? 'Home'
          : item?.address_type == '2'
          ? 'Work'
          : item.address_type == 3
          ? 'Others'
          : null}
      </Text>
      <Text style={styles.locationText}>{item?.house_no}</Text>
      <Text style={styles.landmarkText}>Nearest landmark: {item?.reach}</Text>
      <View style={styles.circle}>
        {index == defaultAddress ? <View style={styles.checkedCircle} /> : null}
      </View>
    </TouchableOpacity>
  );
};

export const SelectDeliveryModal = ({
  modalVisible,
  onPressCancel,
  onPressAddAddress,
  setSelectDelModalVisible,
  setLocationData,
  isLocationIcon,
}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [defaultAddress, setDefaultAddress] = useState();
  const [selectedAddress, setSelectedAddress] = useState();
  const {deliveryAddressData, noAddressAvailable} = useSelector(
    state => state.deliveryAddress,
  );
  const {loginData} = useSelector(state => state.userInfo);

  useEffect(() => {
    if (deliveryAddressData) {
      deliveryAddressData?.map((item, index) => {
        if (item?.primary_key == 1) {
          setDefaultAddress(index);
          setSelectedAddress(item);
          modalVisible && setLocationData && setLocationData(item);
        }
      });
    } else {
      getDeliveryAddressCall({dispatch, userId: loginData?.user_id});
    }
  }, [modalVisible]);

  useEffect(() => {
    setLoading(false);
  }, [modalVisible]);

  const confirmHandler = () => {
    const location = {
      lat: selectedAddress.lat,
      lng: selectedAddress.long,
    };
    let passData = {
      fullAddress: selectedAddress.house_no,
      landmark: selectedAddress.reach,
      contact: selectedAddress.contact_no,
      defaultType: selectedAddress.address_type,
      isPrimary: true,
      addressId: selectedAddress.id,
    };
    setLoading(true);
    updateDeliveryAddressApiCall({
      data: passData,
      dispatch: dispatch,
      setUpdateModalVisible: setSelectDelModalVisible,
      location: location,
      userId: loginData?.user_id,
      navigation: navigation,
      isOnlyPrimaryUpdate: true,
      setLoading: setLoading,
    });
  };

  const toastConfig = {
    tomatoToast: ({text1, props}) => (
      <CustomToast text1={text1} props={props} />
    ),
  };

  let confirmVisible = !noAddressAvailable && deliveryAddressData?.length > 0;

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View
          style={styles.modalSubContainer}
          key={loading + noAddressAvailable + deliveryAddressData?.length}>
          {!loading ? (
            <View style={styles.modalBox}>
              {isLocationIcon && (
                <View style={styles.locationCont}>
                  <View style={SpStyles.FDR_ALC}>
                    <Image
                      source={Images.location}
                      style={styles.locationImg}
                      resizeMode="contain"
                    />
                    <View style={styles.locationTextCont}>
                      <Text style={TextStyles.black_S_16_700}>
                        Device location not enabled
                      </Text>
                      <Text style={TextStyles.black_S_14_400}>
                        Enable your device location for a better delivery
                        experience
                      </Text>
                    </View>
                  </View>
                  <RedButton
                    text={'Enable'}
                    otherStyle={styles.enableBtn}
                    otherTextStyle={TextStyles.white_S_12_700}
                    onPress={() => {
                      Platform.OS === 'android'
                        ? Linking.openSettings()
                        : Linking.openURL('app-settings:');
                    }}
                  />
                </View>
              )}
              <View style={SpStyles.FDR_ALC_JCS}>
                <Text style={styles.titleText}>Select delivery address</Text>
                {!isLocationIcon && (
                  <TouchableOpacity onPress={onPressCancel}>
                    <Image source={Images.close} style={styles.close} />
                  </TouchableOpacity>
                )}
              </View>
              <View style={styles.addressCont}>
                {deliveryAddressData?.length == 0 ? (
                  <NoDataAvailable
                    text={'No address found'}
                    otherStyle={{marginBottom: Responsive.widthPx(10)}}
                  />
                ) : (
                  <FlatList
                    data={deliveryAddressData}
                    renderItem={({item, index}) => {
                      return (
                        <AddressCard
                          item={item}
                          index={index}
                          setDefaultAddress={setDefaultAddress}
                          defaultAddress={defaultAddress}
                          setSelectedAddress={setSelectedAddress}
                          setLocationData={setLocationData}
                        />
                      );
                    }}
                  />
                )}
              </View>
              <View style={styles.btnCont}>
                <RedBorderButton
                  text={'+ Add Address'}
                  otherStyle={styles.addAddressBtn}
                  otherTextStyle={styles.addAddressText}
                  onPress={onPressAddAddress}
                />
                {deliveryAddressData?.length > 0 && (
                  <RedButton
                    text={'Confirm'}
                    otherStyle={styles.confirmBtn}
                    otherTextStyle={styles.confirmText}
                    onPress={() => {
                      confirmHandler();
                    }}
                  />
                )}
              </View>
            </View>
          ) : (
            <View
              style={[
                styles.modalBox,
                {
                  height: Responsive.heightPx(50),
                  paddingTop: Responsive.widthPx(30),
                },
              ]}>
              <View style={styles.loaderBox}>
                <ActivityIndicator size={'large'} color={Colors.red_primary} />
              </View>
            </View>
          )}
        </View>
      </View>
      <Toast config={toastConfig} />
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
  },
  modalBox: {
    backgroundColor: 'white',
    borderTopRightRadius: Responsive.widthPx(3.5),
    borderTopLeftRadius: Responsive.widthPx(3.5),
    width: Responsive.widthPx(100),
    minHeight: Responsive.widthPx(40),
  },
  titleText: {
    ...TextStyles.black_S_20_700,
    marginLeft: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
  },
  close: {
    width: Responsive.widthPx(4),
    height: Responsive.widthPx(4),
    marginRight: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
  },
  addressCardCont: {
    width: Responsive.widthPx(94),
    paddingVertical: Responsive.widthPx(3),
    paddingHorizontal: Responsive.widthPx(4),
    backgroundColor: Colors.white,
    ...SpStyles.shadowLight,
    marginVertical: Responsive.widthPx(2),
    alignSelf: 'center',
    borderRadius: Responsive.widthPx(4),
  },
  typeText: {
    ...TextStyles.red_14_700,
  },
  locationText: {
    ...TextStyles.black_S_16_700,
    marginTop: Responsive.widthPx(0.7),
  },
  landmarkText: {
    ...TextStyles.darkGray_S_14_500,
    marginTop: Responsive.widthPx(0.7),
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ACACAC',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: Responsive.widthPx(4),
    top: Responsive.widthPx(3),
  },
  checkedCircle: {
    width: 12,
    height: 12,
    borderRadius: 6,
    backgroundColor: Colors.red_primary,
  },
  addressCont: {
    marginTop: Responsive.widthPx(2),
    marginBottom: Responsive.widthPx(4),
    maxHeight: Responsive.heightPx(58),
  },
  addAddressBtn: {
    height: 42,
    borderRadius: Responsive.widthPx(3),
    width: Responsive.widthPx(45),
  },
  confirmBtn: {
    marginTop: Responsive.widthPx(0),
    width: Responsive.widthPx(45),
    marginLeft: Responsive.widthPx(2),
  },
  addAddressText: {
    ...TextStyles.red_16_700,
  },
  confirmText: {
    ...TextStyles.white_S_16_700,
  },
  btnCont: {
    width: Responsive.widthPx(94),
    alignSelf: 'center',
    paddingTop: Responsive.widthPx(4),
    paddingBottom: Responsive.widthPx(12),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  loaderBox: {
    backgroundColor: '#efefef',
    alignItems: 'center',
    margin: Responsive.widthPx(1.5),
    marginTop: 0,
    borderRadius: 10,
    width: Responsive.widthPx(20),
    height: Responsive.widthPx(20),
    justifyContent: 'center',
    alignSelf: 'center',
  },
  locationImg: {
    width: Responsive.widthPx(10),
    height: Responsive.widthPx(10),
    marginLeft: Responsive.widthPx(3),
    marginRight: Responsive.widthPx(2),
  },
  locationCont: {
    ...SpStyles.FDR_ALC_JCS,
    backgroundColor: Colors.redLight,
    height: Responsive.widthPx(20),
    borderTopRightRadius: Responsive.widthPx(3.5),
    borderTopLeftRadius: Responsive.widthPx(3.5),
  },
  locationTextCont: {
    width: Responsive.widthPx(60),
  },
  enableBtn: {
    width: Responsive.widthPx(20),
    height: 30,
    marginTop: 0,
    marginRight: Responsive.widthPx(3),
  },
});
