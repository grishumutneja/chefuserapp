import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
  Platform,
} from 'react-native';
import Responsive from '../../Constants/Responsive';
import Toast from 'react-native-toast-message';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import RedButton from '../Buttons/RedButton';
import ProfileInput from '../Inputs/ProfileInput';
import {addDeliveryAddressApiCall} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import CustomToast from '../Toast/CustomToast';
import KeyboardManager from 'react-native-keyboard-manager';

const AddressCard = ({item, index, defaultType, setDefaultType}) => {
  useEffect(() => {
    if (Platform.OS === 'ios') {
      KeyboardManager.setEnable(true);
      KeyboardManager.setToolbarPreviousNextButtonEnable(true);
      KeyboardManager.setKeyboardDistanceFromTextField(100);
    }
    return () => {
      KeyboardManager.setEnable(false);
      KeyboardManager.setToolbarPreviousNextButtonEnable(false);
      KeyboardManager.setKeyboardDistanceFromTextField(0);
    };
  }, []);
  return (
    <TouchableOpacity
      style={{
        ...styles.addressCardCont,
        borderColor:
          defaultType === index + 1 ? Colors.red_primary : Colors.darkGray,
        borderWidth: defaultType == index ? 1.6 : 1.2,
      }}
      onPress={() => {
        setDefaultType(index + 1);
      }}>
      <Text
        style={
          defaultType === index + 1
            ? TextStyles.red_16_700
            : TextStyles.darkGray_S_16_400
        }>
        {item}
      </Text>
    </TouchableOpacity>
  );
};

export const AddDeliveryAddressModal = ({
  modalVisible,
  onPressCancel,
  setAddDeliveryVisible,
  location,
  locationName,
  isFromCart,
}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [defaultType, setDefaultType] = useState();
  const [fullAddress, setFullAddress] = useState();
  const [landmark, setLandmark] = useState();
  const [contact, setContact] = useState();
  const [isPrimary, setIsPrimary] = useState(false);
  const {loginData} = useSelector(state => state.userInfo);

  const addAddressHandler = () => {
    let passData = {
      fullAddress: fullAddress,
      landmark: landmark,
      contact: contact,
      defaultType: defaultType,
      isPrimary: isPrimary,
    };
    const newLocation = {
      address_type: '2',
      contact_no: '2424234324',
      created_at: '2023-06-24T07:15:36.000000Z',
      deleted_at: null,
      house_no: 'Suret',
      id: 2695,
      lat: '21.170239858284',
      long: '72.831060737371',
      primary_key: 1,
      reach: 'Fvdfvdf',
      updated_at: '2023-06-24T07:15:36.000000Z',
      user_id: '2080',
    };

    addDeliveryAddressApiCall({
      data: passData,
      dispatch: dispatch,
      setAddDeliveryVisible: setAddDeliveryVisible,
      location: location,
      userId: loginData?.user_id,
      navigation: navigation,
      isFromCart: isFromCart,
    });
  };

  const toastConfig = {
    tomatoToast: ({text1, props}) => (
      <CustomToast text1={text1} props={props} />
    ),
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={onPressCancel}
      style={{flexGrow: 1}}>
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={styles.modalContainer} />
        </TouchableWithoutFeedback>
        <View style={styles.modalSubContainer}>
          <View style={styles.modalBox}>
            <View style={SpStyles.FDR_ALC_JCS}>
              <Text style={styles.titleText}>Add delivery address</Text>
              <TouchableOpacity onPress={onPressCancel}>
                <Image source={Images.close} style={styles.close} />
              </TouchableOpacity>
            </View>
            <Text style={styles.text}>{locationName}</Text>
            <ProfileInput
              title="Full Address"
              placeholder="Enter full address"
              value={fullAddress}
              setValue={setFullAddress}
              editActive={true}
              isRequired={true}
            />
            <ProfileInput
              title="Nearest Landmark"
              placeholder="Enter nearest landmark"
              value={landmark}
              setValue={setLandmark}
              editActive={true}
              isRequired={true}
            />
            <ProfileInput
              title="Contact"
              placeholder="1234567890"
              value={contact}
              setValue={setContact}
              editActive={true}
              isRequired={true}
              maxLength={10}
              keyboardType="number-pad"
            />
            <View style={styles.addressCont}>
              <Text style={styles.title}>Select address type</Text>
              <FlatList
                horizontal
                data={['Home', 'Work', 'Other']}
                renderItem={({item, index}) => {
                  return (
                    <AddressCard
                      item={item}
                      index={index}
                      setDefaultType={setDefaultType}
                      defaultType={defaultType}
                    />
                  );
                }}
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                setIsPrimary(!isPrimary);
              }}
              style={styles.checkBoxCont}>
              <Image
                source={isPrimary ? Images.checked : Images.unchecked}
                style={styles.checkBox}
                resizeMode="contain"
              />
              <Text style={TextStyles.black_S_16_400}>Set as primary</Text>
            </TouchableOpacity>
            <View style={styles.btnCont}>
              <RedButton
                text={'Add Address'}
                otherStyle={styles.confirmBtn}
                otherTextStyle={styles.confirmText}
                onPress={() => {
                  addAddressHandler();
                }}
              />
            </View>
          </View>
        </View>
      </View>
      <Toast config={toastConfig} />
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexGrow: 1,
  },
  modalSubContainer: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    // alignItems: 'center',
  },
  modalBox: {
    backgroundColor: 'white',
    borderRadius: Responsive.widthPx(3.5),
    width: Responsive.widthPx(100),
  },
  titleText: {
    ...TextStyles.black_S_20_700,
    marginLeft: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
  },
  text: {
    ...TextStyles.black_S_16_400,
    marginLeft: Responsive.widthPx(5),
    marginRight: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(3),
    marginBottom: Responsive.widthPx(0),
  },
  close: {
    width: Responsive.widthPx(4),
    height: Responsive.widthPx(4),
    marginRight: Responsive.widthPx(5),
    marginVertical: Responsive.widthPx(4),
  },
  addressCardCont: {
    width: Responsive.widthPx(22),
    borderColor: Colors.darkGray,
    borderWidth: 1,
    height: 38,
    borderRadius: 6,
    marginRight: Responsive.widthPx(3),
    alignItems: 'center',
    justifyContent: 'center',
  },
  typeText: {
    ...TextStyles.red_14_700,
  },
  locationText: {
    ...TextStyles.black_S_16_700,
    marginTop: Responsive.widthPx(0.7),
  },
  landmarkText: {
    ...TextStyles.darkGray_S_14_500,
    marginTop: Responsive.widthPx(0.7),
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ACACAC',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: Responsive.widthPx(4),
    top: Responsive.widthPx(3),
  },
  checkedCircle: {
    width: 12,
    height: 12,
    borderRadius: 6,
    backgroundColor: Colors.red_primary,
  },
  addressCont: {
    marginTop: Responsive.widthPx(6),
    alignItems: 'center',
  },
  addAddressBtn: {
    height: 42,
    borderRadius: Responsive.widthPx(3),
    width: Responsive.widthPx(45),
  },
  confirmBtn: {
    marginTop: Responsive.widthPx(0),
    width: Responsive.widthPx(45),
  },
  addAddressText: {
    ...TextStyles.red_16_700,
  },
  confirmText: {
    ...TextStyles.white_S_16_700,
  },
  btnCont: {
    width: Responsive.widthPx(94),
    alignSelf: 'center',
    paddingTop: Responsive.widthPx(6),
    paddingBottom: Responsive.widthPx(12),
  },
  title: {
    ...TextStyles.black_S_16_700,
    textAlign: 'left',
    width: Responsive.widthPx(92),
    marginBottom: Responsive.widthPx(3),
  },
  checkBox: {
    width: Responsive.widthPx(5.5),
    height: Responsive.widthPx(5.5),
    marginLeft: Responsive.widthPx(4),
    marginRight: Responsive.widthPx(2),
    tintColor: Colors.red_primary,
  },
  checkBoxCont: {
    marginTop: Responsive.widthPx(6),
    marginBottom: Responsive.widthPx(4),
    ...SpStyles.FDR_ALC,
  },
});
