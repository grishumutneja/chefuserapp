import {View, FlatList, ActivityIndicator, Text, Platform} from 'react-native';
import React, {useEffect, useState} from 'react';
import FavRestaurantCard from '../Cards/FavRestaurantCard';
import CmnStyles from '../../Styles/CmnStyles';
import NoDataAvailable from '../Inputs/NoDataAvailable';
import SearchBox from '../Inputs/SearchBox';
import {useSelector} from 'react-redux';
import {getSearchDataApiCall} from '../../Core/Config/ApiCalls';
import Colors from '../../Constants/Colors';
import SelectButton from '../Buttons/SelectButton';
import Responsive from '../../Constants/Responsive';

const FavRestaurants = ({data, isSearch, otherStyle, setActive, active}) => {
  const [searchText, setSearchText] = useState();
  const [offset, setOffset] = useState(0);
  const [loading, setLoading] = useState(true);
  const {locationData} = useSelector(state => state.location);
  const [restaurantData, setRestaurantData] = useState(data);
  const [isComplete, setIsComplete] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);
  const {getAllFavRestaurantData} = useSelector(
    state => state.favoritesRestaurants,
  );

  useEffect(() => {
    if (!isSearch) {
      setRestaurantData(getAllFavRestaurantData);
    }
  }, [getAllFavRestaurantData]);

  useEffect(() => {
    if (isSearch) {
      setIsComplete(false);
      getSearchDataApiCallHandler(0);
      setOffset(0);
    }
  }, [searchText]);

  const getSearchDataApiCallHandler = pageOffset => {
    if (searchText?.length > 0) {
      const body = {
        keyword: searchText,
        search_for: 'restaurant',
        offset: pageOffset + '',
        lat: locationData?.lat,
        lng: locationData?.lng,
      };
      getSearchDataApiCall({body}).then(res => {
        console.log('res?.length restaurant ===>', res?.length);
        if (res?.length > 4) {
          if (pageOffset == 0) {
            setRestaurantData(res);
          } else {
            setRestaurantData([...restaurantData, ...res]);
          }
        } else if (res?.length > 0) {
          if (pageOffset == 0) {
            setRestaurantData(res);
          } else {
            setRestaurantData([...restaurantData, ...res]);
          }
          setIsComplete(true);
          setPageLoading(false);
        } else {
          setIsComplete(true);
          pageOffset == 0 && setRestaurantData();
        }
        setPageLoading(false);
      });
    } else {
      setRestaurantData();
    }
  };

  // const getSearchExtraDataApiCallHandler = () => {
  //   if (searchText?.length > 0) {
  //     const body = {
  //       keyword: searchText,
  //       search_for: 'restaurant',
  //       offset: pageOffset + '',
  //       lat: locationData?.lat,
  //       lng: locationData?.lng,
  //     };
  //     getSearchDataApiCall({body}).then(res => {
  //       console.log('res?.length', res?.length);
  //       if (res?.length > 0) {
  //         setRestaurantData([...restaurantData, ...res]);
  //       } else {
  //         console.log('res?.length', res?.length);
  //         setIsComplete(true);
  //       }
  //     });
  //   } else {
  //     setRestaurantData([]);
  //   }
  // };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const footerRender = () => {
    return (
      <>
        {isComplete && (
          <Text style={CmnStyles.noMoreResText2}>
            No more record to explore
          </Text>
        )}
        {pageLoading && (
          <ActivityIndicator
            size={'small'}
            style={CmnStyles.paginationLoader2}
            color={Colors.red}
          />
        )}
      </>
    );
  };

  return (
    <View style={[CmnStyles.bodyBg, otherStyle]}>
      {isSearch && (
        <SearchBox setSearchText={setSearchText} searchText={searchText} />
      )}
      {isSearch && <SelectButton active={active} setActive={setActive} />}

      <View style={isSearch && {marginTop: Responsive.heightPx(10)}}>
        {restaurantData?.length > 0 ? (
          <View
            style={{
              height: isSearch
                ? Platform.OS == 'ios'
                  ? Responsive.heightPx(72)
                  : Responsive.heightPx(72)
                : Responsive.heightPx(83),
            }}>
            <FlatList
              data={restaurantData}
              renderItem={({item, index}) => {
                return (
                  <FavRestaurantCard
                    item={item}
                    index={index}
                    isSearch={isSearch}
                  />
                );
              }}
              onScroll={({nativeEvent}) => {
                if (isCloseToBottom(nativeEvent)) {
                  if (!isComplete) {
                    if (!pageLoading) {
                      if (isSearch) {
                        setOffset(offset + 10);
                        setPageLoading(true);
                        getSearchDataApiCallHandler(offset + 10);
                      }
                    }
                  }
                } else {
                  setPageLoading(false);
                }
              }}
              keyExtractor={(item, index) =>
                'Fav Screen FavRestaurants' + index
              }
              key={(item, index) => 'Fav Screen FavRestaurants' + index}
              ListFooterComponent={() => {
                return <>{footerRender()}</>;
              }}
            />
          </View>
        ) : (
          <View>
            {!isSearch ? (
              <NoDataAvailable text={'No Favorite Restaurant Found'} />
            ) : (
              <>
                {searchText?.length > 1 && (
                  <NoDataAvailable text={'No Restaurant Found'} />
                )}
              </>
            )}
          </View>
        )}
      </View>
    </View>
  );
};

export default FavRestaurants;
