import {View, FlatList, Text, ActivityIndicator} from 'react-native';
import React, {useEffect, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import FavDishesCard from '../Cards/FavDishesCard';
import NoDataAvailable from '../Inputs/NoDataAvailable';
import {useSelector} from 'react-redux';
import {getSearchDataApiCall} from '../../Core/Config/ApiCalls';
import SearchBox from '../Inputs/SearchBox';
import Colors from '../../Constants/Colors';
import SelectButton from '../Buttons/SelectButton';
import Responsive from '../../Constants/Responsive';

const FavDishes = ({data, isSearch, setActive, active, otherStyle}) => {
  const [searchText, setSearchText] = useState();
  const [offset, setOffset] = useState(0);
  const {locationData} = useSelector(state => state.location);
  const [dishesData, setDishesData] = useState(data);
  const [isComplete, setIsComplete] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);

  useEffect(() => {
    setDishesData(data);
  }, [data]);

  useEffect(() => {
    if (isSearch) {
      setIsComplete(false);
      getSearchDataApiCallHandler(0);
      setOffset(0);
    } else {
      setDishesData(data);
    }
  }, [searchText]);

  const getSearchDataApiCallHandler = pageOffset => {
    if (searchText?.length > 0) {
      const body = {
        keyword: searchText,
        search_for: 'dishes',
        offset: pageOffset + '',
        lat: locationData?.lat,
        lng: locationData?.lng,
      };
      getSearchDataApiCall({body}).then(res => {
        if (res?.length > 4) {
          if (pageOffset == 0) {
            setDishesData(res);
          } else {
            setDishesData([...dishesData, ...res]);
          }
        } else if (res?.length > 0) {
          if (pageOffset == 0) {
            setDishesData(res);
          } else {
            setDishesData([...dishesData, ...res]);
          }
          setIsComplete(true);
          setPageLoading(false);
        } else {
          setIsComplete(true);
          pageOffset == 0 && setDishesData();
        }
      });
    } else {
      setDishesData();
    }
  };

  const footerRender = () => {
    return (
      <>
        {isComplete && (
          <Text style={CmnStyles.noMoreResText2}>
            No more record to explore
          </Text>
        )}
        {pageLoading && (
          <ActivityIndicator
            size={'small'}
            style={CmnStyles.paginationLoader2}
            color={Colors.red}
          />
        )}
      </>
    );
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  return (
    <View style={[CmnStyles.bodyBg, otherStyle]}>
      {isSearch && (
        <SearchBox setSearchText={setSearchText} searchText={searchText} />
      )}
      {isSearch && <SelectButton active={active} setActive={setActive} />}
      <View style={isSearch && {marginTop: Responsive.widthPx(20)}}>
        {dishesData?.length > 0 ? (
          <View
            style={{
              height: isSearch
                ? Responsive.heightPx(72)
                : Responsive.heightPx(83),
            }}>
            <FlatList
              data={dishesData}
              renderItem={(item, index) => {
                return (
                  <FavDishesCard
                    item={item}
                    index={index}
                    isSearch={isSearch}
                  />
                );
              }}
              onScroll={({nativeEvent}) => {
                if (isSearch) {
                  if (isCloseToBottom(nativeEvent)) {
                    if (!isComplete) {
                      if (!pageLoading) {
                        setOffset(offset + 10);
                        setPageLoading(true);
                        console.log(
                          'getSearchDataApiCallHandler isCloseToBottom',
                        );
                        getSearchDataApiCallHandler(offset + 10);
                      }
                    }
                  } else {
                    setPageLoading(false);
                  }
                }
              }}
              ListFooterComponent={() => {
                return <>{footerRender()}</>;
              }}
              keyExtractor={(item, index) =>
                'Fav Screen FavRestaurants' + index
              }
              key={(item, index) => 'Fav Screen FavRestaurants' + index}
            />
          </View>
        ) : (
          <>
            {!isSearch ? (
              <NoDataAvailable text={'No Favorite Dishes Found'} />
            ) : (
              <>
                {searchText?.length > 1 && (
                  <NoDataAvailable text={'No Dishes Found'} />
                )}
              </>
            )}
          </>
        )}
      </View>
    </View>
  );
};

export default FavDishes;
