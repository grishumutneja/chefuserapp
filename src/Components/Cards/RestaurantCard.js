import {View, Text, Image, TouchableOpacity} from 'react-native';
import React, {memo} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import FavButton from '../Buttons/FavButton';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import StarReview from '../Star/StarReview';
import {useDispatch, useSelector} from 'react-redux';
import {updateFavResExplore} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantToExploreSlice';
import {updateFavFoodForYouRestaurant} from '../../Core/Redux/Slices/FoodForYouSlice';
import {guestAccountHandler} from '../../Core/Config/ApiCalls';
import {updateFavToRatedRes} from '../../Core/Redux/Slices/ResHomeDataSlice';
import RestaurantsSlider2 from '../Slider/RestaurantsSlider2';

const RestaurantCard = ({item, index, isResList}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {getUserInfoData, isGuestAccount} = useSelector(
    state => state.userInfo,
  );

  const updateFavHandler = () => {
    const body = {
      vendor_id: item?.vendor_id,
      is_like: item?.is_like == 0 ? 1 : 0,
      user_id: getUserInfoData?.id,
      item: item,
      dispatch: dispatch,
    };
    dispatch(updateFavResExplore(body));
    isResList && dispatch(updateFavFoodForYouRestaurant(body));
    dispatch(updateFavToRatedRes(body));
  };

  return (
    <TouchableOpacity
      style={CmnStyles.restaurantCardCont}
      onPress={() => {
        navigation.navigate(Screens.RestaurantDetail, {
          resData: item,
        });
      }}>
      <View style={CmnStyles.restaurantImgBox}>
        <RestaurantsSlider2 data={item.banner_image} />
        <View style={CmnStyles.restaurantCardAbsBox}>
          {item?.isClosed == 0 && (
            <Image
              source={Images.open_now}
              style={CmnStyles.restaurantCardAbsImg}
              resizeMode="contain"
            />
          )}
          {item?.vendor_food_type == 1 && (
            <Image
              source={Images.rest_pure_veg}
              style={CmnStyles.restaurantCardAbsImg}
              resizeMode="contain"
            />
          )}
        </View>
      </View>
      <View style={{flexDirection: 'row'}}>
        <View style={CmnStyles.restaurantCardAbsTextBox}>
          <Text style={CmnStyles.restaurantResName}>{item?.name}</Text>
          <Text style={CmnStyles.restaurantText2}>
            {item.cuisines?.join(', ')}
          </Text>
          <StarReview
            reviewCount={item?.review_count}
            vendorRatings={item?.vendor_ratings}
            venderId={item?.vendor_id}
          />
        </View>
        <View style={{flex: 1}}>
          <View style={CmnStyles.restaurantsFooterBox}>
            <Image
              source={Images.scooter}
              style={CmnStyles.foodiesFavCardScooterImg}
              resizeMode="contain"
            />
            <Text style={{...TextStyles.gray_S_10_700}}>
              {item.distance > 1
                ? `${item.distance} KMS`
                : `${item.distance} KM`}
            </Text>
          </View>
        </View>
      </View>
      <FavButton
        is_like={item?.is_like}
        onPress={() => {
          if (isGuestAccount) {
            guestAccountHandler({navigation});
          } else {
            updateFavHandler();
          }
        }}
      />
    </TouchableOpacity>
  );
};

export default memo(RestaurantCard);
