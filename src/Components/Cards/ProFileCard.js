import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import {useSelector} from 'react-redux';

const ProFileCard = () => {
  const navigation = useNavigation();
  const {getUserInfoData, loginData} = useSelector(state => state.userInfo);

  return (
    <View style={CmnStyles.proFileCardCont}>
      <Image
        source={{uri: getUserInfoData?.image}}
        style={CmnStyles.proFileCardImg}
        resizeMode="contain"
      />
      <View style={{width: Responsive.widthPx(65)}}>
        <Text style={TextStyles.darkGray_S_18_700}>
          {getUserInfoData?.name} {getUserInfoData?.surname}
        </Text>
        <View style={CmnStyles.proFileCardTextBox}>
          <Text style={TextStyles.darkGray_S_14_500}>
            +91 {loginData?.mobile}
          </Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate(Screens.Profile);
            }}>
            <Text style={TextStyles.blue_S_13_400}>View Profile</Text>
          </TouchableOpacity>
        </View>
        <Text style={CmnStyles.proFileCardText}>{loginData?.email}</Text>
      </View>
    </View>
  );
};

export default ProFileCard;
