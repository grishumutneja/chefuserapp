import {Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import Colors from '../../Constants/Colors';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';

const DeliveryInstruction = ({item, onClick, selected}) => {
  return (
    <TouchableOpacity
      onPress={onClick}
      activeOpacity={0.8}
      style={{
        ...CmnStyles.delInsCont,
        backgroundColor: selected ? Colors.red_primary : Colors.grey_e7e7e7,
      }}>
      <Image
        source={item?.icon}
        style={[
          CmnStyles.deliveryInsIcon,
          {tintColor: selected ? Colors.white : Colors.darkGray},
        ]}
        resizeMode="contain"
      />
      <Text
        style={
          selected ? TextStyles.white_S_12_400 : TextStyles.darkGray_S_12_500
        }>
        {item?.title}
      </Text>
    </TouchableOpacity>
  );
};

export default DeliveryInstruction;
