import {View, Text} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import LottieView from 'lottie-react-native';
import Colors from '../../Constants/Colors';
import Animations from '../../Constants/Animations';
import SpStyles from '../../Styles/SpStyles';

const OfferPriceCard = () => {
  return (
    <View style={CmnStyles.orderCardCont}>
      <View style={[CmnStyles.cartScreenDelInsCont, {paddingHorizontal: 0}]}>
        {/* <Text style={TextStyles.darkGray_S_15_400}>Tips :</Text> */}
        <View style={[SpStyles.FDR_ALC]}>
          <LottieView
            source={Animations.saving_money}
            // source={Animations.process_failed}
            style={CmnStyles.saving_money_loti}
            autoPlay={true}
            loop={true}
          />
          <Text style={CmnStyles.offerText}>
            You saved <Text style={{color: Colors.greenPureVeg}}> ₹300 </Text>{' '}
            in this order{' '}
          </Text>
          <LottieView
            source={Animations.saving_money}
            // source={Animations.process_failed}
            style={CmnStyles.saving_money_loti}
            autoPlay={true}
            loop={true}
          />
        </View>
      </View>
    </View>
  );
};

export default OfferPriceCard;
