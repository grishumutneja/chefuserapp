import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  LayoutAnimation,
} from 'react-native';
import React, {memo, useEffect, useState} from 'react';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';
import SpStyles from '../../Styles/SpStyles';
import Colors from '../../Constants/Colors';
import TextStyles from '../../Styles/TextStyles';
import DishesCard from './DishesCard';

const DishMenuCard = ({
  menuItem,
  menuIndex,
  setSelectedItem,
  setDishModalVisible,
  setDishMenuIndex,
  isClosed,
  moveIndex,
  searchApply,
  currentIndex,
}) => {
  const hide = menuIndex < 2 ? true : false;
  const [isActive, setIsActive] = useState(hide);

  useEffect(() => {
    moveIndex == menuIndex && setIsActive(true);
  }, [moveIndex]);

  useEffect(() => {
    if (currentIndex) {
      if (menuIndex > currentIndex + 3) {
        setIsActive(false);
      } else {
        setIsActive(true);
      }
    }
  }, [currentIndex]);

  currentIndex && console.log('currentIndex', currentIndex);

  useEffect(() => {
    if (searchApply > 0) {
      moveIndex == menuIndex ? setIsActive(true) : setIsActive(false);
    } else {
      setIsActive(hide);
    }
  }, [searchApply]);

  const renderDishes = () => {
    return (
      <View>
        <FlatList
          data={menuItem?.products}
          renderItem={({item, index}) => {
            return (
              <DishesCard
                dishItem={item}
                dishIndex={index}
                setSelectedItem={setSelectedItem}
                setDishModalVisible={setDishModalVisible}
                menuIndex={menuIndex}
                setDishMenuIndex={setDishMenuIndex}
                isClosed={isClosed}
              />
            );
          }}
          // scrollEnabled={false}
          // removeClippedSubviews={true} // Unmount components when outside of window
          // initialNumToRender={2} // Reduce initial render amount
          // maxToRenderPerBatch={1} // Reduce number in each render batch
          // updateCellsBatchingPeriod={100} // Increase time between renders
          // windowSize={7}
        />
      </View>
    );
  };

  return (
    <View>
      <DishMenuTitle
        text={menuItem?.menuName}
        onPress={() => {
          setIsActive(!isActive);
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        }}
        isActive={isActive}
      />
      {isActive && renderDishes()}
    </View>
  );
};

export default memo(DishMenuCard);

const DishMenuTitle = ({text, onPress, isActive}) => {
  return (
    <TouchableOpacity
      style={{
        ...SpStyles.FDR_ALC_JCS,
        paddingHorizontal: Responsive.widthPx(4),
        backgroundColor: Colors.lightGray,
        marginVertical: Responsive.widthPx(1),
        marginTop: Responsive.widthPx(4),
        paddingVertical: Responsive.widthPx(2),
      }}
      onPress={onPress}>
      <Text style={TextStyles.black_S_18_700}>{text}</Text>
      <TouchableOpacity>
        <Image
          source={Images.back}
          style={{
            width: Responsive.widthPx(4),
            height: Responsive.widthPx(4),
            transform: [{rotate: isActive ? '90deg' : '270deg'}],
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};
