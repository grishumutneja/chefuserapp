import {View, Text, Image} from 'react-native';
import React, {memo} from 'react';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';
import CmnStyles from '../../Styles/CmnStyles';
import SpStyles from '../../Styles/SpStyles';
import Images from '../../Constants/Images';
import StarReview from '../Star/StarReview';
import TimeConvert from '../../Constants/TimeConvert';
import {Grayscale} from 'react-native-color-matrix-image-filters';

const ResDetailTitleCard = ({otherStyle, resData, isClosed}) => {
  const openTime = resData?.start_time ? TimeConvert(resData?.start_time) : '';
  const closeTime = resData?.end_time ? TimeConvert(resData?.end_time) : '';

  const tomorrow = new Date() + 1;
  const tomorrowDay = tomorrow.day();

  return (
    <View style={[CmnStyles.resDetailTitleCardCont, otherStyle]}>
      <View style={CmnStyles.resDetailTitleCardDishImg}>
        {isClosed == 0 ? (
          <Image
            source={{uri: resData?.image}}
            style={CmnStyles.resDetailTitleCardDishImg}
            resizeMode="cover"
          />
        ) : (
          <Grayscale>
            <Image
              source={{uri: resData?.image}}
              style={CmnStyles.resDetailTitleCardDishImg}
              resizeMode="cover"
            />
          </Grayscale>
        )}
        {resData?.vendor_food_type == 1 && (
          <Image
            source={Images.rest_pure_veg}
            style={CmnStyles.resDetailTitleCardVegImg}
            resizeMode="contain"
          />
        )}
      </View>
      <View style={{width: Responsive.widthPx(65)}}>
        <Text style={TextStyles.black_S_16_700}>
          {resData?.name ? resData?.name : resData?.restaurantName}
        </Text>
        <Text
          style={{
            ...TextStyles.gray_S_12_400,
            marginTop: Responsive.widthPx(0.5),
          }}>
          {resData?.cuisines.join(', ')}
        </Text>
        <StarReview
          vendorRatings={resData?.vendor_ratings}
          reviewCount={resData?.review_count}
          isClosed={isClosed == 0 ? false : true}
          venderId={resData?.vendor_id}
        />
        <View style={{...SpStyles.FDR_ALC, marginTop: Responsive.widthPx(1)}}>
          <Text
            style={
              isClosed == 0 ? TextStyles.blue_S_12_400 : TextStyles.red_12_700
            }>
            {isClosed == 0 ? 'Open' : 'Closed'}
          </Text>
          <View style={CmnStyles.resDetailDotStyle} />
          <Text style={{...TextStyles.gray_S_12_400}}>
            {isClosed == 0 ? `Closes at ${closeTime}` : `Opens ${openTime}`}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default memo(ResDetailTitleCard);
