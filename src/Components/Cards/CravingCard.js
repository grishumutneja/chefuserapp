import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import React, {useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';

const CravingCard = ({item, index, type}) => {
  const navigation = useNavigation();
  const [activityIndicator, setActivityIndicator] = useState();

  return (
    <TouchableOpacity
      style={CmnStyles.cravingCardCont}
      onPress={() => {
        navigation.navigate(Screens.RestaurantList, {
          restaurantData: item,
          type: type,
        });
      }}>
      <View style={CmnStyles.cravingCardImg}>
        {activityIndicator && (
          <View style={CmnStyles.cravingCardImgLoader}>
            <ActivityIndicator size={'small'} color={'red'} />
          </View>
        )}
        <Image
          source={{uri: item?.image}}
          style={CmnStyles.cravingCardImg}
          resizeMode="cover"
          onLoadStart={() => {
            setActivityIndicator(true);
          }}
          onLoadEnd={() => {
            setActivityIndicator(false);
          }}
        />
      </View>
      <Text style={CmnStyles.cravingCardText}>{item?.name}</Text>
    </TouchableOpacity>
  );
};

export default CravingCard;
