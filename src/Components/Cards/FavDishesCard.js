import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import Responsive from '../../Constants/Responsive';
import FavButton from '../Buttons/FavButton';
import {
  getRestDetailApiCall,
  removeFavDishesApiCall,
} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';
import StarReviewDish from '../Star/StarReviewDish';
import Screens from '../../Core/Stack/Screens';
import {useNavigation} from '@react-navigation/native';

const FavDishesCard = ({item, index, isSearch}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {locationData} = useSelector(state => state.location);

  let offerAvail = item?.item?.offer_id != 0 ? true : false;

  const onPressDish = () => {
    const body = {
      vendor_id: item?.item?.vendor_id,
      lat: locationData?.lat,
      lng: locationData?.lng,
    };
    getRestDetailApiCall({body}).then(res => {
      navigation.navigate(Screens.RestaurantDetail, {
        resData: res,
        dishFromSearch: item?.item,
        isFromSearched: true,
      });
    });
  };

  return (
    <TouchableOpacity
      style={CmnStyles.favRestaurantCardCont}
      onPress={() => {
        onPressDish();
      }}>
      <View style={{width: Responsive.widthPx(60)}}>
        <View style={[SpStyles.FDR_ALC_JCS, {width: Responsive.widthPx(56)}]}>
          <Text style={CmnStyles.favDishesMainTitle2} numberOfLines={2}>
            {item?.item?.product_name}
          </Text>
          {(item?.item?.vendor_food_type == 1 ||
            item?.item?.vendor_food_type == 3) && (
            <Image
              source={Images.pure_veg}
              style={CmnStyles.pure_vegFavDishesCard}
              resizeMode="contain"
            />
          )}
        </View>
        {item?.item?.restaurantName ? (
          <Text style={CmnStyles.favDishesTitle} numberOfLines={1}>
            {item?.item?.restaurantName}
          </Text>
        ) : null}
        {item?.item?.dis && (
          <Text style={CmnStyles.favDishesText} numberOfLines={2}>
            {item?.item?.dis}
          </Text>
        )}
        {offerAvail ? (
          <>
            <View style={SpStyles.FDR_ALC}>
              <Text style={CmnStyles.favDishesPriceDark}>
                ₹ {parseFloat(item?.item?.after_offer_price)?.toFixed(2)}
              </Text>
              <Text style={CmnStyles.favDishesPriceText}>
                ₹ {parseFloat(item?.item?.product_price)?.toFixed(2)}
              </Text>
            </View>
            <Text style={CmnStyles.offerPerText}>
              Flat {item?.item?.offer_persentage}% OFF
            </Text>
          </>
        ) : (
          <Text style={CmnStyles.favDishesText2}>
            ₹ {item?.item?.product_price}
          </Text>
        )}
        <StarReviewDish vendorRatings={item?.item?.product_rating} />
      </View>
      <View style={CmnStyles.favDishesCardImg}>
        <Image
          source={{uri: item?.item?.image}}
          style={CmnStyles.favDishesCardImg2}
          resizeMode="cover"
        />
        {!isSearch && (
          <FavButton
            otherStyle={{
              right: Responsive.widthPx(1),
              top: Responsive.widthPx(1),
            }}
            is_like={item?.item?.is_like == 1}
            onPress={() => {
              let body = {
                user_id: 98,
                product_id: item?.item?.product_id,
              };
              removeFavDishesApiCall({
                body: body,
                dispatch: dispatch,
                productId: item?.item?.product_id,
              });
            }}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

export default FavDishesCard;
