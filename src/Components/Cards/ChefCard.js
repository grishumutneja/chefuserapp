import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import TextStyles from '../../Styles/TextStyles';
import SpStyles from '../../Styles/SpStyles';
import Responsive from '../../Constants/Responsive';
import FavButton from '../Buttons/FavButton';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';

const chefUri =
  'https://nationaltoday.com/wp-content/uploads/2021/07/shutterstock_1518533924-min.jpg';

const ChefCard = ({specialVisible}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(Screens.Chef);
      }}
      style={{
        width: Responsive.widthPx(66),
        marginRight: Responsive.widthPx(8),
        minHeight: specialVisible
          ? Responsive.widthPx(48)
          : Responsive.widthPx(40),
        paddingTop: Responsive.widthPx(2.5),
        marginBottom: specialVisible
          ? Responsive.widthPx(-4)
          : Responsive.widthPx(1),
      }}>
      <View style={CmnStyles.chefCardCont}>
        <View style={CmnStyles.chefCardTextBox}>
          <Text
            style={[
              TextStyles.black_S_16_700,
              {
                marginRight: Responsive.widthPx(4),
              },
            ]}
            numberOfLines={1}>
            Chef Ankur Bajaj
          </Text>
          <Text
            style={[
              TextStyles.darkGray_S_12_500,
              {marginTop: Responsive.widthPx(0.7)},
            ]}>
            Age - 31 Yrs, Cooking Exp - 2 Yrs
          </Text>
          <View
            style={[SpStyles.FDR_ALC_JCS, {marginTop: Responsive.widthPx(1)}]}>
            <Image
              source={Images.star}
              style={CmnStyles.chefCardStarImg}
              resizeMode="contain"
            />
            <Text style={TextStyles.black_S_12_700}>4.5</Text>
            <TouchableOpacity style={{marginLeft: 6}}>
              <Text style={TextStyles.blue_S_11_400}>(15 Reviews)</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={CmnStyles.chefCardBottomTextCont}>
          <Text style={TextStyles.darkGray_S_12_500}>Orders Served - 100</Text>
          <View style={SpStyles.FDR_ALC}>
            <Image
              source={Images.scooter}
              style={CmnStyles.chefCardScooterImg}
              resizeMode="contain"
            />
            <Text style={TextStyles.darkGray_S_12_500}>2.5KM</Text>
          </View>
        </View>
        {specialVisible && (
          <View style={CmnStyles.chefCardBottomCont}>
            <Text style={TextStyles.white_S_12_700}>
              Speciality - North Indian
            </Text>
          </View>
        )}
        <FavButton
          otherStyle={{
            top: Responsive.widthPx(1),
            marginRight: Responsive.widthPx(-2),
          }}
        />
      </View>
      <View style={CmnStyles.chefCardChefImgBox}>
        <Image
          source={{uri: chefUri}}
          style={CmnStyles.chefCardChefImg}
          resizeMode="cover"
        />
      </View>
    </TouchableOpacity>
  );
};

export default ChefCard;
