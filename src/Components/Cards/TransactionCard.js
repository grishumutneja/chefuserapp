import {Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import {TransactionDetailModal} from '../Modals/TransactionDetailModal';

const TransactionCard = ({item, index}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedTransaction, setSelectedTransaction] = useState();
  return (
    <TouchableOpacity
      style={CmnStyles.transCardCont}
      onPress={() => {
        setSelectedTransaction(item);
        setModalVisible(true);
      }}>
      <Text style={CmnStyles.transCardOrderType}>{item?.narration}</Text>
      <Text style={CmnStyles.transCardDateType}>{item?.date}</Text>
      <Text
        style={[
          CmnStyles.transCardDateType,
          {color: item?.transaction_type == 1 ? 'green' : 'red'},
        ]}>
        ₹{item?.amount}
      </Text>
      <TransactionDetailModal
        item={item}
        modalVisible={modalVisible}
        selectedTransaction={selectedTransaction}
        onPressCancel={() => {
          setModalVisible(false);
        }}
      />
    </TouchableOpacity>
  );
};

export default TransactionCard;
