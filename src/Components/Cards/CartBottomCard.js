import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Colors from '../../Constants/Colors';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';
import SpStyles from '../../Styles/SpStyles';
import Images from '../../Constants/Images';
import Animations from '../../Constants/Animations';
import LottieView from 'lottie-react-native';
import {useSelector} from 'react-redux';

const CartBottomCard = ({
  onPressPlaceOrder,
  payAmount,
  onPressAddress,
  address,
  discountAmount,
  deliverySaveAmount,
  deliveryChargeSave,
}) => {
  const {offerValue, cartListData} = useSelector(state => state.cart);

  const saveAmount = discountAmount
    ? parseFloat(discountAmount) + offerValue + deliverySaveAmount
    : offerValue + deliverySaveAmount;

  let finalSaveAmount;

  if (saveAmount <= deliveryChargeSave) {
    finalSaveAmount = saveAmount;
  } else {
    finalSaveAmount = saveAmount - deliveryChargeSave;
  }
  console.log('cartListDatacartListData', cartListData);
  console.log('finalSaveAmountfinalSaveAmount', finalSaveAmount);
  return (
    <View style={styles.container}>
      {cartListData?.free_delivery == 1 && (
        <Text
          style={{
            ...styles.freeDelText,
            marginBottom: Responsive.widthPx(saveAmount == 0 ? 3 : -3),
          }}>
          Free delivery upto {cartListData.free_delivery_criteria} km's. Rs.
          {cartListData.charge_after_criteria + ' '}
          per km applied after that
        </Text>
      )}

      {offerValue + deliverySaveAmount > 0 ? (
        <View style={styles.shavingCont}>
          <Text style={CmnStyles.offerText}>
            You saved{' '}
            <Text style={{color: Colors.greenPureVeg}}>
              {' '}
              ₹{finalSaveAmount.toFixed(2)}{' '}
            </Text>{' '}
            in this order{' '}
          </Text>
          <LottieView
            source={Animations.saving_money}
            // source={Animations.process_failed}
            style={CmnStyles.saving_money_loti}
            autoPlay={true}
            loop={true}
          />
        </View>
      ) : (
        <View style={{height: Responsive.widthPx(2.4)}} />
      )}
      <View style={SpStyles.FDR_ALC}>
        <TouchableOpacity onPress={onPressAddress}>
          <View style={SpStyles.FDR_ALC}>
            <Image
              source={Images.location}
              style={styles.location}
              resizeMode="contain"
            />
            <View style={styles.delTextCont}>
              <Text style={TextStyles.gray_S_9_700}>DELIVERY ADDRESS</Text>
              <Image
                source={Images.back}
                style={styles.upIcon}
                resizeMode="contain"
              />
            </View>
          </View>
          <Text style={styles.selectText} numberOfLines={1}>
            {address ? address : 'Select/Change Address'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.orderPlacedBtnCont}
          onPress={onPressPlaceOrder}>
          <View>
            <Text style={TextStyles.white_S_16_700}>₹ {payAmount}</Text>
            <Text style={TextStyles.white_S_14_400}>To Pay</Text>
          </View>
          <View style={SpStyles.FDR_ALC}>
            <Text style={TextStyles.white_S_18_700}>Place Order</Text>
            <Image
              source={Images.back}
              style={styles.frdIcon}
              resizeMode="contain"
            />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CartBottomCard;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: Colors.redLight,
    // height: Responsive.widthPx(47),
    width: Responsive.widthPx(100),
    borderTopLeftRadius: Responsive.widthPx(5),
    borderTopRightRadius: Responsive.widthPx(5),
    borderWidth: 0.3,
    borderColor: Colors.red_primary,
    ...SpStyles.shadowLight,
    paddingBottom:
      Platform.OS == 'android' ? Responsive.widthPx(5) : Responsive.widthPx(12),
    paddingTop: Responsive.widthPx(1),
  },
  orderPlacedBtnCont: {
    backgroundColor: Colors.red_primary,
    width: Responsive.widthPx(58),
    paddingVertical: Responsive.widthPx(3.6),
    ...SpStyles.FDR_ALC_JCS,
    paddingHorizontal: Responsive.widthPx(4),
    borderRadius: Responsive.widthPx(2),
    marginRight: Responsive.widthPx(4),
  },
  frdIcon: {
    transform: [{rotate: '180deg'}],
    width: 9,
    height: 9,
    tintColor: Colors.white,
    marginLeft: 3,
    marginTop: 2,
  },
  upIcon: {
    transform: [{rotate: '90deg'}],
    width: 9,
    height: 9,
    tintColor: Colors.gray,
    marginLeft: 3,
  },
  location: {
    width: 14,
    height: 18,
    marginLeft: Responsive.widthPx(3),
  },
  delTextCont: {
    ...SpStyles.FDR_ALC,
    paddingHorizontal: Responsive.widthPx(1.5),
  },
  selectText: {
    ...TextStyles.black_S_12_400,
    // paddingHorizontal: Responsive.widthPx(3),
    marginTop: 3,
    width: Responsive.widthPx(39),
    paddingLeft: Responsive.widthPx(3.5),
    paddingRight: Responsive.widthPx(2),
  },
  shavingCont: {
    ...SpStyles.FDR_ALC,
    alignSelf: 'center',
    marginVertical: Responsive.widthPx(3),
  },
  freeDelText: {
    ...TextStyles.gray_S_14_500,
    textAlign: 'center',
    marginTop: Responsive.widthPx(3),
    width: Responsive.widthPx(80),
    alignSelf: 'center',
  },
});
