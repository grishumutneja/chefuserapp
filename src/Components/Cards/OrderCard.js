import {View, Text, Image} from 'react-native';
import React, {memo, useEffect, useState} from 'react';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';
import OnePlusItemBox from '../Buttons/OnePlusItemBox';
import {
  addItemToCart,
  minusItemToCart,
} from '../../Core/Redux/Slices/RestaurantsSlices/CartSlice';
import {useDispatch, useSelector} from 'react-redux';

const OrderCard = ({
  item,
  index,
  setDiscountAmount,
  setWalletAmountApply,
  setAppliedCode,
  isTrackScreen,
  locationDataClear,
}) => {
  const dispatch = useDispatch();
  const [variant, setVariant] = useState();
  const [addons, setAddons] = useState([]);
  const [addonPrice, setAddonPrice] = useState(0);
  const {cartListData} = useSelector(state => state.cart);
  const {getUserInfoData} = useSelector(state => state.userInfo);
  const [variantPrice, setVariantPrice] = useState();
  const [variantPriceWithOffer, setVariantPriceWithOffer] = useState();

  useEffect(() => {
    if (item?.customizable == 'true') {
      if (isTrackScreen) {
        setVariant(item?.variant);
      } else {
        item?.variants?.map((itm, index) => {
          if (itm?.added == true) {
            setVariant(itm);
            setVariantPrice(itm?.variant_price);
            setVariantPriceWithOffer(itm?.after_offer_price);
          }
        });
      }
      let tempAddon = [];
      let tempPrice = 0;
      item?.addons?.map((itm, ind) => {
        if (itm?.added == true || isTrackScreen) {
          tempAddon.push(itm);
          tempPrice = tempPrice + parseFloat(itm?.addon_price);
        }
      });
      setAddons(tempAddon);
      setAddonPrice(tempPrice);
    }
  }, []);

  const pPrice = parseFloat(item?.product_price);
  const pPriceOffer = parseFloat(item?.after_offer_price);
  const proPrice = variantPrice ? variantPrice : pPrice;
  const proPriceOffer = variantPriceWithOffer
    ? variantPriceWithOffer
    : pPriceOffer;
  const totalProductPrice =
    parseFloat(item?.product_qty) * proPrice + parseFloat(addonPrice);
  const totalProductOfferPrice =
    parseFloat(item?.product_qty) * proPriceOffer + parseFloat(addonPrice);

  const isOfferApply = item?.offer_id && item?.offer_id != 0;
  return (
    <View style={CmnStyles.orderCardCont}>
      <Image
        source={Images.pure_veg}
        style={CmnStyles.orderCardPureVegImg}
        resizeMode="contain"
      />
      <View style={CmnStyles.orderCardPureContent}>
        <Text style={TextStyles.black_S_16_700}>{item?.product_name}</Text>
        {variant && (
          <View style={CmnStyles.orderCardItemName} key={'variants' + index}>
            <Text style={CmnStyles.orderCardVarText}>
              {variant?.variant_name}
            </Text>
            <View style={CmnStyles.orderCardVarPriceBox}>
              <Text style={CmnStyles.orderCardVarPriceText}>
                {variant?.offer_id && variant?.offer_id != 0
                  ? variant?.variant_price
                  : null}
              </Text>
              <Text style={[TextStyles.black_S_14_400, {marginLeft: 6}]}>
                ₹{' '}
                {variant?.offer_id
                  ? variant?.offer_id != 0
                    ? variant?.after_offer_price.toFixed(2)
                    : parseFloat(variant?.variant_price).toFixed(2)
                  : parseFloat(variant?.variant_price).toFixed(2)}
              </Text>
            </View>
          </View>
        )}
        {addons?.length > 0 && (
          <View>
            <Text style={CmnStyles.orderCardAddonName}>Addons</Text>
            {addons?.map((itm, ind) => {
              return (
                <View key={'addons?.map AddonCartCard' + ind}>
                  <AddonCartCard item={itm} index={ind} />
                </View>
              );
            })}
          </View>
        )}
        <View style={CmnStyles.orderCardPriceCont}>
          {isTrackScreen ? (
            <>
              {/* <View style={CmnStyles.orderCardQtyCont}>
                <Text style={TextStyles.black_S_16_700}>
                  X {item?.product_qty}
                </Text>
              </View> */}
            </>
          ) : (
            <OnePlusItemBox
              productQty={item?.product_qty}
              item={item}
              setDiscountAmount={setDiscountAmount}
              setAppliedCode={setAppliedCode}
              setWalletAmountApply={setWalletAmountApply}
              onPressMinus={() => {
                const body = {
                  cart_id: cartListData?.cart_id,
                  user_id: getUserInfoData?.id,
                  vendor_id: cartListData?.vendor?.vendor_id,
                  products: item,
                  dispatch: dispatch,
                  variant: variant,
                  addons: addons,
                };
                dispatch(minusItemToCart(body));
                setDiscountAmount();
                setWalletAmountApply();
                setAppliedCode();
                locationDataClear();
              }}
              onPressAdd={() => {
                const body = {
                  cart_id: cartListData?.cart_id,
                  user_id: getUserInfoData?.id,
                  vendor_id: cartListData?.vendor?.vendor_id,
                  products: item,
                  dispatch: dispatch,
                  variant: variant,
                  addons: addons,
                };
                dispatch(addItemToCart(body));
                setDiscountAmount();
                setWalletAmountApply();
                setAppliedCode();
                locationDataClear();
              }}
            />
          )}
          <View
            style={
              isOfferApply
                ? CmnStyles.orderCardTotalPriceBox
                : CmnStyles.orderCardTotalPriceBox2
            }>
            <Text
              style={isOfferApply ? CmnStyles.orderCardTotalPriceLine : null}>
              {isOfferApply ? totalProductPrice?.toFixed(2) : null}
            </Text>
            <Text style={CmnStyles.orderCardPrice}>
              ₹{' '}
              {item?.offer_id
                ? item?.offer_id != 0
                  ? totalProductOfferPrice.toFixed(2)
                  : totalProductPrice.toFixed(2)
                : totalProductPrice.toFixed(2)}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default memo(OrderCard);

const AddonCartCard = ({item, index}) => {
  return (
    <View style={CmnStyles.addonCartCardCont} key={'AddonCartCard' + index}>
      <Text style={TextStyles.darkGray_S_14_500}>{item?.addon_name}</Text>
      <Text style={TextStyles.black_S_14_400}>₹{item?.addon_price}</Text>
    </View>
  );
};
