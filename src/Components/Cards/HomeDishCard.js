import {View, Text, Image, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';
import CmnStyles from '../../Styles/CmnStyles';
import RedBorderButton from '../Buttons/RedBorderButton';
import SpStyles from '../../Styles/SpStyles';
import StarTopDishes from '../Star/StarTopDishes';
import FavButton from '../Buttons/FavButton';
import OnePlusItemBox from '../Buttons/OnePlusItemBox';

const tempImg =
  'https://media.architecturaldigest.com/photos/590cc6c3b3064307ffee59a4/master/w_3000,h_2000,c_limit/Tallest%20Restaurants%20in%20the%20World%207.jpg';

const HomeDishCard = ({item, index}) => {
  const [count, setCount] = useState(0);

  return (
    <TouchableOpacity style={CmnStyles.homeDishCardCont}>
      <View style={CmnStyles.homeDishCardTopImgBox}>
        <Image
          source={{uri: tempImg}}
          style={CmnStyles.homeDishCardTopImg}
          resizeMode="cover"
        />
      </View>
      <Text style={CmnStyles.homeDishCardResName}>
        Foodies Fav Card (Foodies Fav)
      </Text>
      <Text style={CmnStyles.homeDishCardResText}>
        ` Foodies Fav Card (Foodies Fav)`
      </Text>
      <Text style={CmnStyles.homeDishCardResText2}>
        Foodies Fav Card (Foodies Fav)
      </Text>
      <StarTopDishes
        scale={Responsive.widthPx(2.5)}
        otherStyle={{alignSelf: 'center'}}
      />
      <View style={CmnStyles.homeDishCardBottomBox}>
        <View style={{...SpStyles.FDR_ALC_JCS}}>
          <View
            style={{
              alignItems: 'center',
              marginRight: Responsive.widthPx(2.4),
            }}>
            <Text style={{...TextStyles.blue_S_9_400}}>Customizable</Text>
            {count > 0 ? (
              <RedBorderButton
                text={'Add'}
                otherStyle={CmnStyles.homeDishCardAddBtn}
              />
            ) : (
              <OnePlusItemBox otherStyle={CmnStyles.onePlusItemBoxCont2} />
            )}
          </View>
          <Text style={{...TextStyles.black_S_16_700}}>₹126.00</Text>
        </View>
      </View>
      <FavButton />
    </TouchableOpacity>
  );
};

export default HomeDishCard;
