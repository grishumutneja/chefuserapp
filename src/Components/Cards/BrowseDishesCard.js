import {View, Text, Image} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Responsive from '../../Constants/Responsive';
import FavButton from '../Buttons/FavButton';
import TextStyles from '../../Styles/TextStyles';
import SpStyles from '../../Styles/SpStyles';
import Images from '../../Constants/Images';
import StarReviewDish from '../Star/StarReviewDish';

const imgUri =
  'https://sandinmysuitcase.com/wp-content/uploads/2020/04/Popular-Indian-Cuisine.jpg';

const BrowseDishesCard = () => {
  return (
    <View style={CmnStyles.browseDishCardCont}>
      <View style={CmnStyles.browseDishCardImg}>
        <Image
          source={{uri: imgUri}}
          style={CmnStyles.browseDishCardImg}
          resizeMode="contain"
        />
        <Image
          source={Images.rest_pure_veg}
          style={CmnStyles.browseDishPureVeg}
          resizeMode="contain"
        />
      </View>
      <View>
        <View style={{width: Responsive.widthPx(56)}}>
          <Text style={TextStyles.black_S_16_700}>Chef Ankur Bajaj</Text>
          <Text style={TextStyles.darkGray_S_12_500}>
            Age - 31 Yrs, Cooking Exp - 2
          </Text>
          <StarReviewDish />
        </View>
        <View style={CmnStyles.chefCardBottomTextCont2}>
          <Text style={TextStyles.darkGray_S_12_500}>Orders Served - 100</Text>
          <View style={SpStyles.FDR_ALC}>
            <Image
              source={Images.scooter}
              style={CmnStyles.chefCardScooterImg}
              resizeMode="contain"
            />
            <Text style={TextStyles.darkGray_S_12_500}>2.5KM</Text>
          </View>
        </View>
      </View>
      <View style={CmnStyles.browseDishesCardBottomCont}>
        <Text style={TextStyles.white_S_12_700}>Speciality - North Indian</Text>
      </View>
      <FavButton />
    </View>
  );
};

export default BrowseDishesCard;
