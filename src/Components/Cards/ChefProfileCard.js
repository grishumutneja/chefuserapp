import {View, Text, Image} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import BioCard from './BioCard';
import SpStyles from '../../Styles/SpStyles';

const chefUri =
  'https://nationaltoday.com/wp-content/uploads/2021/07/shutterstock_1518533924-min.jpg';

const ChefProfileCard = () => {
  return (
    <View style={CmnStyles.chefProfileCardCont}>
      <View style={SpStyles.FDR_ALC}>
        <View style={CmnStyles.chefProfileCardImgBox}>
          <Image source={{uri: chefUri}} style={CmnStyles.chefProfileCardImg} />
        </View>
        <View style={CmnStyles.chefProfileCardTextCont}>
          <Text style={TextStyles.black_S_16_700}>Name - Chef Ankur Bajaj</Text>
          <Text style={TextStyles.black_S_13_400}>Age - 31 Yrs</Text>
          <Text style={TextStyles.black_S_13_400}>Cooking Exp - 2 Yrs</Text>
          <Text style={TextStyles.black_S_13_400}>Origin - Chennai</Text>
          <Text style={TextStyles.black_S_13_400}>
            Speciality - South Indian
          </Text>
          <View style={SpStyles.FDR_ALC}>
            <Text style={TextStyles.black_S_13_400}>Ratings - 4.5 </Text>
            <Text style={TextStyles.blue_S_11_400}>(125 Reviews)</Text>
          </View>
          <Text style={TextStyles.black_S_13_400}>
            Fssai License No. - 1234567890101
          </Text>
        </View>
      </View>
      <BioCard />
    </View>
  );
};

export default ChefProfileCard;
