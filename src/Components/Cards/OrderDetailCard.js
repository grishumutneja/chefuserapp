import {View, Text, FlatList} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import OrderCard from './OrderCard';
import TextStyles from '../../Styles/TextStyles';
import TextPriceCard from './TextPriceCard';
import Colors from '../../Constants/Colors';
import DashedPart from './DashedPart';

const OrderDetailCard = ({orderData}) => {
  return (
    <View>
      <FlatList
        data={orderData?.products}
        renderItem={({item, index}) => {
          return (
            <OrderCard
              item={item}
              index={index}
              addNotVisible={true}
              isTrackScreen={true}
            />
          );
        }}
        scrollEnabled={false}
      />
      <View style={CmnStyles.orderCardCont}>
        <View style={CmnStyles.cartScreenAddMoreCont}>
          <Text style={TextStyles.darkGray_S_16_700}>Grand Total</Text>
          <Text style={TextStyles.black_S_16_700}>
            ₹ {orderData?.total_amount}
          </Text>
        </View>
      </View>
      <View style={CmnStyles.orderCardCont}>
        <View style={CmnStyles.cartScreenDelInsCont}>
          <TextPriceCard
            item={'Item Total'}
            price={orderData?.total_amount.toFixed(2)}
          />
          {orderData?.discount_amount > 0 && (
            <TextPriceCard
              item={`Promo Code Applied (${orderData?.coupon_name})`}
              price={orderData?.discount_amount.toFixed(2)}
            />
          )}
          <TextPriceCard
            item={'Taxes and Fee ⓘ'}
            price={(orderData?.tex + orderData?.platform_charges).toFixed(2)}
          />
          <TextPriceCard
            item={'Delivery Charges'}
            price={orderData?.delivery_charge.toFixed(2)}
          />
          <TextPriceCard
            item={'Total Amount'}
            price={orderData?.gross_amount}
            otherTextStyle={{color: Colors.blue_0638ff}}
          />
          {orderData?.wallet_apply === '1' && (
            <TextPriceCard
              item={'Wallet amount used'}
              price={orderData?.wallet_cut.toFixed(2)}
              minusVisible={true}
            />
          )}
          <DashedPart />
          <TextPriceCard
            item={orderData?.payment_status}
            price={orderData?.net_amount.toFixed(2)}
            otherTextStyle={[
              TextStyles.darkGray_S_18_700,
              {textTransform: 'capitalize'},
            ]}
          />
        </View>
      </View>
    </View>
  );
};

export default OrderDetailCard;
