import {View, Text, Image, TouchableOpacity, Share} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';

const ReferCardEarnCard = ({walletData, refMessage}) => {
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Hey !!
        Download ChefLab, India's first and only complete food solution platform by using my Refer Code "${walletData?.referralCode}" to get exciting offers.

        Download Now : https://play.google.com/store/apps/details?id=com.cheflab`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <View style={CmnStyles.referCardEarnCardCont}>
      <Text style={{...TextStyles.darkGray_S_18_700}}>Refer And Earn</Text>
      <TouchableOpacity
        style={CmnStyles.referCardEarnCardBox}
        onPress={() => {
          onShare();
        }}>
        <Image
          source={Images.refer}
          style={CmnStyles.referCardEarnCardImg}
          resizeMode="contain"
        />
        <View style={CmnStyles.referCardEarnCardTextBox}>
          <Text style={{...TextStyles.black_S_12_400}}>
            Click here to share this app with your friends and family.
          </Text>
          <Text
            style={{
              ...TextStyles.red_17_700,
              marginTop: Responsive.widthPx(1),
            }}>
            {walletData.referralCode}
          </Text>
          <Text
            style={{
              ...TextStyles.black_S_12_700,
              marginTop: Responsive.widthPx(1),
            }}>
            {refMessage}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default ReferCardEarnCard;
