import {View, TouchableOpacity, Image, FlatList} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import Images from '../../Constants/Images';
import OrderStatusSubCard from './OrderStatusSubCard';
import CmnStyles from '../../Styles/CmnStyles';

const OrderStatusCard = ({
  setOrderStatusDataVisible,
  orderStatusData,
  rateCardVisible,
}) => {
  const ref = useRef();
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    setTimeout(() => {
      if (orderStatusData?.length == currentIndex + 1) {
        setCurrentIndex(0);
        ref.current?.scrollToIndex({index: 0, animated: true});
      } else {
        setCurrentIndex(currentIndex + 1);
        ref.current?.scrollToIndex({index: currentIndex + 1, animated: true});
      }
    }, 3000);
  }, [currentIndex]);

  return (
    <View
      style={[CmnStyles.orderStatusCardCont, !rateCardVisible && {bottom: 20}]}>
      <TouchableOpacity
        style={{marginBottom: 5}}
        onPress={() => {
          setOrderStatusDataVisible(false);
        }}>
        <Image
          source={Images.cancel}
          style={CmnStyles.orderStatusCardCancel}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <FlatList
        ref={ref}
        horizontal
        pagingEnabled
        data={orderStatusData}
        renderItem={({item, index}) => (
          <OrderStatusSubCard item={item} index={index} />
        )}
      />
      <View style={CmnStyles.orderStatusCardPageCont}>
        <FlatList
          horizontal
          data={orderStatusData}
          renderItem={({item, index}) => {
            return (
              <View
                style={
                  index == currentIndex
                    ? CmnStyles.orderStatusCardPage
                    : CmnStyles.orderStatusCardPage2
                }
              />
            );
          }}
          ItemSeparatorComponent={<View style={{width: 7}} />}
        />
      </View>
    </View>
  );
};

export default OrderStatusCard;
