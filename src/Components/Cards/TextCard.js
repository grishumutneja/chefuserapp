import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';
import SpStyles from '../../Styles/SpStyles';
import TextStyles from '../../Styles/TextStyles';
import Colors from '../../Constants/Colors';

const TextCard = ({text, onPress}) => {
  return (
    <TouchableOpacity
      style={{
        ...SpStyles.FDR_ALC_JCS,
        paddingHorizontal: Responsive.widthPx(7),
        paddingVertical: Responsive.widthPx(2),
        marginVertical: Responsive.widthPx(1.8),
      }}
      onPress={onPress}>
      <Text style={TextStyles.darkGray_S_16_700}>{text}</Text>
      <Image
        source={Images.back}
        style={{
          width: Responsive.widthPx(5),
          height: Responsive.widthPx(5),
          tintColor: Colors.darkGray,
          transform: [{rotate: '180deg'}],
        }}
        resizeMode="contain"
      />
    </TouchableOpacity>
  );
};

export default TextCard;
