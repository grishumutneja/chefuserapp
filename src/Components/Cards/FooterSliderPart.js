import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Alert,
  AppState,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import BottomSheet from 'react-native-simple-bottom-sheet';
import Responsive from '../../Constants/Responsive';
import RestaurantsSlider from '../Slider/RestaurantsSlider';
import Colors from '../../Constants/Colors';
import SpStyles from '../../Styles/SpStyles';
import Images from '../../Constants/Images';
import Animations from '../../Constants/Animations';
import LottieView from 'lottie-react-native';
import Open from '../../Constants/Open';
import {cancelOrderApiCall} from '../../Core/Config/ApiCalls';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import Utility from '../../Constants/Utility';
import TrackRestaurantsSlider from '../Slider/TrackRestaurantsSlider';

const FooterSliderPart = ({
  data,
  orderStatus,
  driverData,
  isFromPlaced,
  setRemainTime,
  remainTime,
  setOrderStatus,
}) => {
  // const [orderStatus, setOrderStatus] = useState(0);
  const navigation = useNavigation();
  const appState = useRef(AppState.currentState);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      )
        appState.current = nextAppState;
      if (appState.current == 'active') {
        setRemainTime(0);
      } else if (
        appState.current == 'background' ||
        appState.current == 'inactive'
      ) {
        setRemainTime(0);
      }
    });
    return () => {
      subscription.remove();
    };
  }, []);

  const [isCancelApply, setIsCancelApply] = useState(false);

  useEffect(() => {
    if (isCancelApply) {
      cancelOrderHandler(remainTime);
    }
  }, [isCancelApply, remainTime]);

  const cancelOrderHandler = time => {
    if (time <= 1) {
      Utility.showToast('Opps..! Time up for order cancellation');
    } else {
      let body = {
        order_id: driverData?.id,
        isCancelledWithin30Second: 1,
      };
      setIsCancelApply(false);
      console.warn('cancelOrderHandler Call', time);
      cancelOrderApiCall({body}).then(res => {
        navigation.replace(Screens.OrderCancel);
      });
    }
  };

  useEffect(() => {
    if (isFromPlaced) {
      if (remainTime > 0) {
        setTimeout(() => {
          setRemainTime(remainTime - 1);
        }, 1000);
      } else {
        // setOrderStatus(3);
        driverData?.order_status == 'pending' && setOrderStatus('confirmed');
      }
    } else {
      setRemainTime(0);
    }
  }, [remainTime]);

  const renderContent = drag => (
    <TouchableOpacity
      activeOpacity={1}
      style={SpStyles.bgColorWhite}
      onPress={() => {}}>
      {orderStatus == 'pending' ? (
        remainTime ? (
          <CanCancel
            remainTime={remainTime}
            setIsCancelApply={setIsCancelApply}
          />
        ) : null
      ) : orderStatus == 'dispatched' ? (
        <DriverDetailTrackCard orderData={driverData} />
      ) : (
        orderStatus == 'preparing' && <FoodPrePairingCard />
      )}
      <View
        nestedScrollEnabled={true}
        onScrollEndDrag={drag}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={SpStyles.flexGrow1}
        scrollEnabled={true}>
        <Text style={CmnStyles.footerSliderPartText}>
          While you wait for your order, check out ours special offers!
        </Text>
        <View style={CmnStyles.restaurantsSliderTrackCont}>
          {/* <RestaurantsSlider
            data={data}
            otherStyle={CmnStyles.trackBannerCont}
            otherStyleDots={{marginTop: Responsive.widthPx(3)}}
            activeColor={Colors.black}
            isTrack={true}
            isFullWidth={Responsive.widthPx(88)}
          /> */}
          <TrackRestaurantsSlider data={data} />
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <BottomSheet
      sliderMinHeight={Responsive.heightPx(23)}
      isOpen={false}
      lineStyle={CmnStyles.bottomSheetLineStyle}
      wrapperStyle={{padding: 0}}
      lineContainerStyle={SpStyles.height0}>
      {onScrollEndDrag => renderContent(onScrollEndDrag)}
    </BottomSheet>
  );
};

export default FooterSliderPart;

const CanCancel = ({remainTime, setIsCancelApply}) => {
  const dot = offSec => {
    return (
      <View
        style={
          remainTime > offSec
            ? CmnStyles.trackDotActive
            : CmnStyles.trackDotInActive
        }
      />
    );
  };
  return (
    <View style={CmnStyles.canCancelCont}>
      <View style={SpStyles.FDR_ALC}>
        {dot(0)}
        {dot(5)}
        {dot(10)}
        {dot(15)}
        {dot(20)}
        {dot(25)}
      </View>
      <TouchableOpacity
        onPress={() => {
          Alert.alert('Hold On...!', 'Do you want to cancel this order?', [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {text: 'OK', onPress: () => setIsCancelApply(true)},
          ]);
        }}>
        <Text style={CmnStyles.canCancelText}>Cancel Order</Text>
      </TouchableOpacity>
    </View>
  );
};

const DriverDetailTrackCard = ({orderData}) => {
  return (
    <View style={CmnStyles.driverDetailTrackCardCont}>
      <Image
        source={{uri: orderData?.image}}
        style={CmnStyles.driverDetailTrackCardProImg}
        resizeMode="cover"
      />
      <Text style={CmnStyles.driverDetailTrackCardText}>
        {orderData?.driver_name} is on the way to deliver your order.
      </Text>
      <TouchableOpacity
        onPress={() => {
          const mobile = parseInt(orderData?.mobile);
          Open.phoneCall(mobile);
        }}>
        <Image
          source={Images.telephone}
          style={CmnStyles.driverDetailTrackCardIcon}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
};

const FoodPrePairingCard = () => {
  return (
    <View style={CmnStyles.foodPrePairingCardCont}>
      <LottieView
        source={Animations.random}
        style={CmnStyles.foodPrePairingCardLottie}
        autoPlay={true}
        loop={true}
      />
      <Text style={CmnStyles.foodPrePairingCardText}>
        Food is preparing {'\n'}
        Your delivery partner will be assigned soon!
      </Text>
    </View>
  );
};
