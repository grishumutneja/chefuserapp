import {View, Text, TouchableOpacity} from 'react-native';
import React, {memo, useEffect, useState} from 'react';
import Responsive from '../../Constants/Responsive';
import FavButton from '../Buttons/FavButton';
import SpStyles from '../../Styles/SpStyles';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import RedBorderButton from '../Buttons/RedBorderButton';
import TextStyles from '../../Styles/TextStyles';
import OnePlusItemBox from '../Buttons/OnePlusItemBox';
import FastImage from 'react-native-fast-image';
import {useDispatch, useSelector} from 'react-redux';
import {
  addItemToCartRest,
  addItemToCartSearchDish,
  minusItemToCartRest,
  minusItemToCartSearchDish,
  updateFavDishInResDetail,
} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantDetailsSlice';
import {Grayscale} from 'react-native-color-matrix-image-filters';
import StarReviewDish from '../Star/StarReviewDish';

const DishesCard = ({
  dishItem,
  dishIndex,
  setSelectedItem,
  setDishModalVisible,
  menuIndex,
  setDishMenuIndex,
  isClosed,
  setSearchModalVisible,
  menuRef,
  isSearch,
  data,
  setMoveIndex,
  setSearchApply,
  searchApply,
  isSearchScreen,
}) => {
  const dispatch = useDispatch();
  const {getUserInfoData, isGuestAccount} = useSelector(
    state => state.userInfo,
  );
  const [products, setAddProducts] = useState();
  const {cartListData, cartTotalItem, cartItems} = useSelector(
    state => state.cart,
  );
  const {locationData} = useSelector(state => state.location);

  const offerPrice = dishItem?.after_offer_price;
  const offerAvail = dishItem?.offer_id != 0;

  useEffect(() => {
    if (cartTotalItem > 0) {
      let cartData = [];
      cartItems?.map((item, index) => {
        let newItem = Object.assign({});
        let addonPrice = 0;
        newItem.product_id = item?.product_id;
        newItem.product_qty = item?.product_qty;
        if (item?.customizable) {
          let variant = [];
          item.variants?.map((itm, ind) => {
            const newItm = Object.assign({});
            if (itm?.added) {
              newItm.variant_id = itm?.variant_id;
              newItm.variant_qty = itm?.qty;
              variant.push(newItm);
            }
          });
          let addon = [];
          item.addons?.map((itm, ind) => {
            const newItm = Object.assign({});
            if (itm?.added) {
              newItm.addon_id = itm?.id;
              newItm.addon_qty = itm?.qty;
              addonPrice = addonPrice + parseFloat(itm?.price);
              addon.push(newItm);
            }
          });
          if (variant?.length > 0) {
            newItem.variant = variant;
          }
          if (addon?.length > 0) {
            newItem.addons = addon;
          }
        }
        newItem.product_price = parseFloat(item?.product_price);
        newItem.addonPrice = parseFloat(addonPrice);
        cartData.push(newItem);
      });
      setAddProducts(cartData);
      // AsyncStorage.setItem(AsKey.cartData, JSON.stringify(cartData));
    }
  }, []);

  const favHandler = () => {
    let body = {
      user_id: getUserInfoData?.id,
      product_id: dishItem?.product_id,
      menuIndex: menuIndex,
      is_like: dishItem?.is_like == 1 ? 0 : 1,
      lat: locationData?.lat,
      lng: locationData?.lng,
    };
    dispatch(updateFavDishInResDetail(body));
  };

  const body = {
    cart_id: cartListData?.cart_id,
    user_id: getUserInfoData?.id,
    vendor_id: dishItem?.vendor_id,
    products: dishItem,
    menuIndex: menuIndex,
    dispatch: dispatch,
    dishIndex: dishIndex,
  };

  const addPart = () => {
    return (
      <View
        style={
          dishItem?.image
            ? CmnStyles.homeDetailDishAddCont2
            : CmnStyles.homeDetailDishAddCont3
        }>
        {dishItem?.cart_qty > 0 ? (
          <OnePlusItemBox
            otherStyle={[
              CmnStyles.redBtnCont2,
              CmnStyles.homeDishCardAddBtn2,
              SpStyles.FDR_ALC_JCS,
            ]}
            otherTextStyle={TextStyles.red_20_700}
            productQty={dishItem?.cart_qty}
            onPressAdd={() => {
              if (isSearchScreen) {
                dispatch(addItemToCartSearchDish(body));
              } else {
                dispatch(addItemToCartRest(body));
              }
            }}
            onPressMinus={() => {
              if (isSearchScreen) {
                dispatch(minusItemToCartSearchDish(body));
              } else {
                dispatch(minusItemToCartRest(body));
              }
            }}
          />
        ) : (
          <RedBorderButton
            text={'Add'}
            otherStyle={CmnStyles.homeDishCardAddBtn2}
            otherTextStyle={TextStyles.red_20_700}
            onPress={() => {
              if (isSearchScreen) {
                setDishModalVisible(true);
                setSelectedItem(dishItem);
                setDishMenuIndex(menuIndex);
                setSearchModalVisible && setSearchModalVisible(false);
              } else {
                setDishModalVisible(true);
                setSelectedItem(dishItem);
                setDishMenuIndex(menuIndex);
                setSearchModalVisible && setSearchModalVisible(false);
              }
            }}
          />
        )}
        {dishItem?.customizable == 'true' ? (
          <Text style={{...TextStyles.blue_S_9_400}}>Customizable</Text>
        ) : (
          <Text style={{...TextStyles.blue_S_9_400}}> </Text>
        )}
      </View>
    );
  };

  return (
    <TouchableOpacity
      style={CmnStyles.dishCardCont}
      disabled={!isSearch}
      onPress={() => {
        // navigation.navigate(Screens.Dish);
        let offIndex = 0;
        let myIndex = data?.length;
        let mnIndex = 0;
        setSearchApply(searchApply + 1);
        data?.map((itm, ind) => {
          if (itm?.menu_id == dishItem?.menu_id) {
            setMoveIndex(ind);
            itm?.products?.map((proItem, proIndex) => {
              if (proItem?.product_id == dishItem?.product_id) {
                // offIndex = proIndex + offIndex;
                offIndex = proIndex + offIndex;
                myIndex = ind;
              }
            });
          } else {
            if (ind < myIndex) {
              // offIndex = itm?.products?.length + offIndex;
              mnIndex = mnIndex + 1;
            }
          }
        });
        const offset =
          mnIndex * Responsive.widthPx(20) + Responsive.widthPx(50) * offIndex;
        menuRef?.scrollToOffset({
          animated: true,
          offset: offset,
        });
        setSearchModalVisible(false);
      }}>
      <View>
        <View style={SpStyles.FDR_ALC}>
          <View>
            <View style={{width: Responsive.widthPx(60)}}>
              {isClosed == 0 ? (
                <FastImage
                  source={
                    dishItem?.type == 'non_veg'
                      ? Images.non_veg_icon
                      : Images.pure_veg
                  }
                  style={CmnStyles.pure_vegResDetDishesCard}
                  resizeMode="contain"
                />
              ) : (
                <Grayscale>
                  <FastImage
                    source={Images.pure_veg}
                    style={CmnStyles.pure_vegResDetDishesCard}
                    resizeMode="contain"
                  />
                </Grayscale>
              )}
              <Text style={CmnStyles.favDishesMainTitle} numberOfLines={2}>
                {dishItem?.product_name}
              </Text>
              {dishItem?.dis && (
                <Text style={CmnStyles.resDetailDishText} numberOfLines={2}>
                  {dishItem?.dis}
                </Text>
              )}
              <View style={{width: Responsive.widthPx(42)}}>
                <View style={SpStyles.FDR_ALC}>
                  {offerAvail && (
                    <Text style={CmnStyles.offerPriceText}>
                      ₹{offerPrice?.toFixed(2)}
                    </Text>
                  )}
                  <Text
                    style={
                      offerAvail
                        ? CmnStyles.cancelPriceText
                        : CmnStyles.priceText
                    }>
                    ₹{dishItem?.product_price}
                  </Text>
                </View>
                {offerAvail && (
                  <Text style={CmnStyles.offerPricePerText}>
                    {dishItem?.offer_persentage}% OFF
                  </Text>
                )}
                <StarReviewDish
                  vendorRatings={dishItem?.product_rating}
                  isClosed={isClosed == 0 ? false : true}
                />
              </View>
            </View>
          </View>
          {isClosed == 1 ? (
            <Grayscale>
              <View style={CmnStyles.dishCardImgCont}>
                <FastImage
                  source={{uri: dishItem?.image}}
                  style={CmnStyles.dishCardImg}
                  resizeMode="cover"
                />
                {!isSearch && !isGuestAccount && (
                  <FavButton
                    otherStyle={CmnStyles.dishCardFavBtn}
                    is_like={dishItem?.is_like == 1}
                    onPress={() => {
                      favHandler();
                    }}
                  />
                )}
              </View>
            </Grayscale>
          ) : (
            <View style={CmnStyles.dishCardImgCont}>
              {dishItem?.image !== null ? (
                <>
                  <FastImage
                    source={{uri: dishItem?.image}}
                    style={CmnStyles.dishCardImg}
                    resizeMode="cover"
                  />
                  {!isSearch && !isGuestAccount && (
                    <FavButton
                      otherStyle={CmnStyles.dishCardFavBtn}
                      is_like={dishItem?.is_like == 1}
                      onPress={() => {
                        favHandler();
                      }}
                    />
                  )}
                  {/* {!isGuestAccount && addPart()} */}
                  {!isSearch && !isGuestAccount && addPart()}
                </>
              ) : (
                <View
                  style={{
                    // backgroundColor: Colors.redLight,
                    width: Responsive.widthPx(32),
                    height: Responsive.widthPx(32),
                    borderRadius: Responsive.widthPx(3),
                  }}>
                  {!isSearch && !isGuestAccount && (
                    <FavButton
                      otherStyle={CmnStyles.dishCardFavBtn}
                      is_like={dishItem?.is_like == 1}
                      onPress={() => {
                        favHandler();
                      }}
                    />
                  )}
                  {!isGuestAccount && addPart()}
                </View>
              )}
            </View>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default memo(DishesCard);
