import {Text, TouchableOpacity} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import {orderStatusHandler} from '../../Constants/orderStatusHandler';

const OrderStatusSubCard = ({item, index}) => {
  const navigation = useNavigation();

  const status = orderStatusHandler(item.order_status);

  return (
    <TouchableOpacity
      style={CmnStyles.orderStatusSubCardCont}
      onPress={() => {
        navigation.navigate(Screens.OrderHistory);
      }}>
      <Text style={CmnStyles.orderStatusSubCardTitle}>Live Order Status</Text>
      <Text style={CmnStyles.orderStatusSubCardStatus}>{status}</Text>
      <Text style={CmnStyles.orderStatusSubCardText}>{item.vendor_name}</Text>
      <Text style={CmnStyles.orderStatusSubCardOTPText}>
        OTP : {item.deliver_otp}
      </Text>
    </TouchableOpacity>
  );
};

export default OrderStatusSubCard;
