import {View, Text, Image} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';

const BioCard = () => {
  return (
    <View style={CmnStyles.bioCardCont}>
      <Text style={TextStyles.black_S_16_700}>Bio</Text>
      <Image
        source={Images.quote}
        style={CmnStyles.quoteImg}
        resizeMode="contain"
      />
      <Text style={CmnStyles.bioCardText}>
        I am here to entice your taste-buds! #enticeyourtastebuds Boring recipes
        can make you sad, so always try to make some interesting cuisine.{'\n'}
        We will feel ill if we spend too much time out of the kitchen. Chefs
        know that cooking is not their job but the calling of life. A chef has
        the common drive of spreading happiness.
      </Text>
      <Image
        source={Images.quote}
        style={CmnStyles.quoteImg2}
        resizeMode="contain"
      />
    </View>
  );
};

export default BioCard;
