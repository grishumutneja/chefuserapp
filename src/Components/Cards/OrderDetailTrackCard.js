import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import SpStyles from '../../Styles/SpStyles';
import {orderStatusHandler} from '../../Constants/orderStatusHandler';

const OrderDetailTrackCard = ({
  isDelivered,
  orderData,
  orderStatus,
  setViewOrderVisible,
}) => {
  const status = orderStatusHandler(orderStatus);

  return (
    <View style={CmnStyles.orderDetailTrackCardCont}>
      <View style={SpStyles.FDR_ALC_JCS}>
        <Image
          source={{uri: orderData?.image}}
          style={CmnStyles.orderDetailTrackCardResImd}
        />
        <View style={CmnStyles.orderDetailTrackCardResBox}>
          <Text style={TextStyles.darkGray_S_16_700} numberOfLines={2}>
            {orderData?.vendor_name}
          </Text>
          <Text style={TextStyles.darkGray_S_14_500}>
            Order id: #{orderData?.id}
          </Text>
          <Text style={TextStyles.darkGray_S_12_500}>
            {orderData?.order_date}
          </Text>
        </View>
      </View>
      {!isDelivered && (
        <View style={CmnStyles.orderDetailTrackCardResSubBox}>
          <TouchableOpacity
            style={CmnStyles.orderDetailTrackCardResTextBox}
            onPress={() => {
              setViewOrderVisible(true);
            }}>
            <Text style={TextStyles.red_14_700}>View Order Details</Text>
          </TouchableOpacity>
          <View style={CmnStyles.orderDetailTrackCardStatusBg}>
            <Text style={TextStyles.white_S_12_400}>{status}</Text>
          </View>
        </View>
      )}
      <View style={CmnStyles.orderDetailTrackCardResSubBox2}>
        <Text style={TextStyles.darkGray_S_18_400}>Grand Total</Text>
        <View style={SpStyles.FDR_ALC_JCS}>
          <Text style={CmnStyles.orderDetailTrackCardTypeText}>
            {orderData?.payment_type}
          </Text>
          <Text style={TextStyles.darkGray_S_18_700}>
            ₹ {orderData?.gross_amount}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default OrderDetailTrackCard;
