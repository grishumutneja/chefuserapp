import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import TextStyles from '../../Styles/TextStyles';
import SpStyles from '../../Styles/SpStyles';
import Responsive from '../../Constants/Responsive';

const TextPriceCard = ({
  price,
  item,
  otherTextStyle,
  removeVisible,
  onPress,
  onPressRemove,
  minusVisible,
  isFree,
}) => {
  return (
    <View
      style={[
        SpStyles.FDR_ALC_JCS_W100,
        {marginBottom: Responsive.widthPx(2)},
      ]}>
      <View style={SpStyles.FDR_ALC}>
        <TouchableOpacity onPress={onPress}>
          <Text style={[TextStyles.darkGray_S_14_500, otherTextStyle]}>
            {item}
          </Text>
        </TouchableOpacity>
        {removeVisible && (
          <TouchableOpacity onPress={onPressRemove}>
            <Text
              style={{
                ...TextStyles.red_10_400,
                marginBottom: Responsive.widthPx(0.5),
                marginLeft: Responsive.widthPx(1.2),
              }}>
              Remove
            </Text>
          </TouchableOpacity>
        )}
      </View>
      {isFree ? (
        <Text style={[TextStyles.green_S_14_500, otherTextStyle]}>FREE</Text>
      ) : (
        <Text style={[TextStyles.darkGray_S_14_500, otherTextStyle]}>
          {minusVisible ? `- ₹ ${price}` : `₹ ${price}`}
        </Text>
      )}
    </View>
  );
};

export default TextPriceCard;
