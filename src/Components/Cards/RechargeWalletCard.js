import {View, Text} from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import WhiteButton from '../Buttons/WhiteButton';
import {useSelector} from 'react-redux';
import {guestAccountHandler} from '../../Core/Config/ApiCalls';
import {useNavigation} from '@react-navigation/native';

const RechargeWalletCard = ({setRechargeModalVisible, walletData}) => {
  const navigation = useNavigation();
  const {isGuestAccount} = useSelector(state => state.userInfo);

  return (
    <LinearGradient
      colors={['#007b08', '#03fc13']}
      style={CmnStyles.walletCardCont}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 1}}>
      <View>
        <Text style={TextStyles.white_S_18_700} numberOfLines={1}>
          Total Balance
        </Text>
        <Text style={TextStyles.white_S_25_700} numberOfLines={1}>
          ₹{' '}
          {walletData?.wallet_amount
            ? walletData?.wallet_amount?.toFixed(2)
            : 0}
        </Text>
      </View>
      <WhiteButton
        text={'Recharge'}
        onPress={() => {
          if (isGuestAccount) {
            guestAccountHandler({navigation});
          } else {
            setRechargeModalVisible(true);
          }
        }}
      />
    </LinearGradient>
  );
};

export default RechargeWalletCard;
