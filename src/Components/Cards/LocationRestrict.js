import {View, Text, Image} from 'react-native';
import React from 'react';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';
import AppRightsCard from './AppRightsCard';
import Responsive from '../../Constants/Responsive';

const LocationRestrict = () => {
  return (
    <View>
      <Image
        source={Images.unhappy}
        style={CmnStyles.unHappyImg}
        resizeMode="contain"
      />
      <Text style={CmnStyles.locationRestrictText}>
        Sorry, online food ordering is not available at your location yet. We'll
        be there soon - hang tight!
      </Text>
      <AppRightsCard otherStyle={{marginTop: Responsive.widthPx(12)}} />
    </View>
  );
};

export default LocationRestrict;
