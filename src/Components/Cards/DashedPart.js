import {View, StyleSheet} from 'react-native';
import React from 'react';
import Colors from '../../Constants/Colors';

const DashedPart = () => {
  return (
    <View style={styles.cont}>
      {[...Array(21)].map((e, i) => (
        <View style={styles.cutter} />
      ))}
    </View>
  );
};

export default DashedPart;

const styles = StyleSheet.create({
  cont: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },
  cutter: {
    height: 2,
    width: 10,
    backgroundColor: Colors.darkGray,
    borderRadius: 10,
    marginBottom: 10,
    marginTop: 10,
  },
});
