import {View, FlatList} from 'react-native';
import React, {useCallback, useRef, useState} from 'react';
import ChefCard from './ChefCard';
import Responsive from '../../Constants/Responsive';
import CmnStyles from '../../Styles/CmnStyles';

const ChefCardBox = ({data, specialVisible}) => {
  const ref = useRef();

  const [currentIndex, setCurrentIndex] = useState(0);

  const onScroll = useCallback(event => {
    const slideSize =
      event.nativeEvent.layoutMeasurement.width - Responsive.widthPx(30);
    const index = event.nativeEvent.contentOffset.x / slideSize;
    const roundIndex = Math.round(index);
    setCurrentIndex(roundIndex);
  }, []);

  return (
    <View>
      <FlatList
        horizontal
        data={data}
        ref={ref}
        onScroll={onScroll}
        keyExtractor={(item, index) => `key-ChefCard${index}`}
        renderItem={({item, index}) => {
          return (
            <ChefCard
              item={item}
              index={index}
              specialVisible={specialVisible}
            />
          );
        }}
        contentContainerStyle={{
          marginHorizontal: Responsive.widthPx(4),
          marginTop: Responsive.widthPx(2.5),
        }}
        showsHorizontalScrollIndicator={false}
      />
      <View style={CmnStyles.paginationBoxChef}>
        <FlatList
          data={data}
          initialScrollIndex={currentIndex}
          renderItem={({item, index}) => {
            return (
              <View
                style={
                  index === currentIndex
                    ? CmnStyles.activeDotChef
                    : CmnStyles.inActiveDotChef
                }
              />
            );
          }}
          keyExtractor={(item, index) => `category+${index}`}
          showsVerticalScrollIndicator={false}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    </View>
  );
};

export default ChefCardBox;
