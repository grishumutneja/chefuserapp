import {View, Text, Image} from 'react-native';
import React, {memo} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import TextStyles from '../../Styles/TextStyles';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';

const AppRightsCard = ({otherStyle, licenseNumber}) => {
  return (
    <View
      style={[
        {
          height: licenseNumber
            ? Responsive.widthPx(70)
            : Responsive.widthPx(50),
          width: Responsive.widthPx(100),
        },
        otherStyle,
      ]}
      key={licenseNumber}>
      {licenseNumber && (
        <View
          style={[
            {
              width: Responsive.widthPx(100),
              height: Responsive.widthPx(26),
              paddingHorizontal: Responsive.widthPx(6),
              marginBottom: Responsive.widthPx(0),
              backgroundColor: Colors.lightGray2,
              ...SpStyles.FDR_ALC_JCS,
            },
          ]}>
          <View>
            <Text style={TextStyles.darkGray_S_12_500}>License Number</Text>
            <Text style={TextStyles.darkGray_S_14_500}>{licenseNumber}</Text>
          </View>
          <Image
            source={Images.fssai_logo}
            style={CmnStyles.fssaiImg}
            resizeMode="contain"
          />
        </View>
      )}
      <View style={CmnStyles.app_logoImgBox}>
        <Image
          style={CmnStyles.app_logoImg}
          source={Images.app_logo}
          resizeMode="cover"
        />
        <Text style={CmnStyles.app_logoImgText}>
          © 2023, ChefLab.All Rights Reserved
        </Text>
      </View>
    </View>
  );
};

export default memo(AppRightsCard);
