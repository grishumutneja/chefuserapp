import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';
import SpStyles from '../../Styles/SpStyles';
import TextStyles from '../../Styles/TextStyles';
import CmnStyles from '../../Styles/CmnStyles';
import StarReview from '../Star/StarReview';
import FavButton from '../Buttons/FavButton';
import {
  getAllFavDishesApiCall,
  removeFavRestaurantsApiCall,
  restaurantToExploreApiCall,
} from '../../Core/Config/ApiCalls';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';

const FavRestaurantCard = ({item, index, isSearch}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {locationData} = useSelector(state => state.location);

  return (
    <TouchableOpacity
      style={
        item?.isClosed == 1 && isSearch
          ? CmnStyles.favRestaurantCardContDark
          : CmnStyles.favRestaurantCardCont
      }
      onPress={() => {
        navigation.navigate(Screens.RestaurantDetail, {resData: item});
      }}>
      <Image
        source={{uri: item?.image}}
        style={CmnStyles.favRestaurantCardImg}
        resizeMode="cover"
      />
      <View style={{width: Responsive.widthPx(66)}}>
        <View
          style={[
            SpStyles.FDR_ALC_JCS,
            {
              width: isSearch ? Responsive.widthPx(64) : Responsive.widthPx(58),
            },
          ]}>
          <Text
            style={[
              TextStyles.black_S_16_700,
              isSearch && {
                width: Responsive.widthPx(60),
              },
            ]}>
            {item?.name}
          </Text>
          {item?.vendor_food_type == 1 ? (
            <Image
              source={Images.pure_veg}
              style={CmnStyles.img10}
              resizeMode="contain"
            />
          ) : (
            <Image
              source={Images.non_veg_icon}
              style={CmnStyles.img10}
              resizeMode="contain"
            />
          )}
        </View>
        <Text style={TextStyles.darkGray_S_14_500}>
          {item?.categories?.join(', ')}
        </Text>
        <StarReview vendorRatings={item?.vendor_ratings} />
      </View>
      {!isSearch && (
        <FavButton
          is_like={item?.is_like == 1}
          onPress={() => {
            let body = {
              user_id: 98,
              vendor_id: item?.vendor_id,
            };
            let locationBody = {
              lat: locationData.lat,
              lng: locationData.lng,
              offset: 0,
              limit: 10,
            };
            const favBody = {
              lat: locationData?.lat,
              lng: locationData?.lng,
            };
            getAllFavDishesApiCall({dispatch: dispatch, body: favBody});
            removeFavRestaurantsApiCall({
              body: body,
              dispatch: dispatch,
              vendorId: item?.vendor_id,
            });
            restaurantToExploreApiCall({
              dispatch: dispatch,
              body: locationBody,
            });
          }}
        />
      )}
    </TouchableOpacity>
  );
};

export default FavRestaurantCard;
