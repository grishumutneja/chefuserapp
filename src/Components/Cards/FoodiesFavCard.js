import {View, Text, Image, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';
import FavButton from '../Buttons/FavButton';
import Screens from '../../Core/Stack/Screens';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {
  getRestDetailApiCall,
  guestAccountHandler,
} from '../../Core/Config/ApiCalls';
import {updateFavResExplore} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantToExploreSlice';
import {updateFavFoodForYouRestaurant} from '../../Core/Redux/Slices/FoodForYouSlice';
import {updateFavToRatedRes} from '../../Core/Redux/Slices/ResHomeDataSlice';

const FoodiesFavCard = ({item, index, isOnesMoreRes, isFavHide}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [hover, setHover] = useState();
  const {isGuestAccount, getUserInfoData} = useSelector(
    state => state.userInfo,
  );
  const {locationData} = useSelector(state => state.location);

  const onPressCard = () => {
    const body = {
      vendor_id: item?.vendor_id,
      lat: locationData?.lat,
      lng: locationData?.lng,
    };
    getRestDetailApiCall({body}).then(res => {
      navigation.navigate(Screens.RestaurantDetail, {resData: res});
    });
  };

  const updateFavHandler = () => {
    const body = {
      vendor_id: item?.vendor_id,
      is_like: item?.is_like == 0 ? 1 : 0,
      user_id: getUserInfoData?.id,
      item: item,
      dispatch: dispatch,
    };
    dispatch(updateFavResExplore(body));
    dispatch(updateFavFoodForYouRestaurant(body));
    dispatch(updateFavToRatedRes(body));
  };

  return (
    <TouchableOpacity
      style={
        hover ? CmnStyles.foodiesFavCardCont2 : CmnStyles.foodiesFavCardCont
      }
      onPressIn={() => {
        setHover(true);
      }}
      onPressOut={() => {
        setHover(false);
      }}
      onPress={() => {
        if (isOnesMoreRes) {
          onPressCard();
        } else {
          navigation.navigate(Screens.RestaurantDetail, {
            resData: item,
          });
        }
      }}>
      <View
        style={[
          hover
            ? CmnStyles.foodiesFavCardTopImgBox2
            : CmnStyles.foodiesFavCardTopImgBox,
        ]}>
        <Image
          source={{uri: item?.image}}
          style={CmnStyles.foodiesFavCardTopImg}
          resizeMode="cover"
        />
        <View style={CmnStyles.foodiesFavCardTopStarBox}>
          <Image
            source={Images.star}
            style={CmnStyles.foodiesFavCardTopStarImg}
            resizeMode="contain"
          />
          <Text style={TextStyles.white_S_8_700}>{item?.vendor_ratings}</Text>
        </View>
      </View>
      <Text style={CmnStyles.foodiesFavCardResName} numberOfLines={2}>
        {item?.name || item?.vendor_name}
      </Text>
      <View style={CmnStyles.foodiesFavCardFooterBox}>
        <Image
          source={Images.scooter}
          style={CmnStyles.foodiesFavCardScooterImg}
          resizeMode="contain"
        />
        <Text style={{...TextStyles.gray_S_10_700}}>{item?.distance} KM</Text>
      </View>
      {!isOnesMoreRes && !isFavHide && (
        <FavButton
          is_like={item?.is_like}
          onPress={() => {
            if (isGuestAccount) {
              guestAccountHandler({navigation});
            } else {
              updateFavHandler();
            }
          }}
        />
      )}
    </TouchableOpacity>
  );
};

export default FoodiesFavCard;
