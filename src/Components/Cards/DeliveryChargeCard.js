import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import TextStyles from '../../Styles/TextStyles';
import SpStyles from '../../Styles/SpStyles';
import Responsive from '../../Constants/Responsive';

const DeliveryChargeCard = ({
  price,
  item,
  otherTextStyle,
  removeVisible,
  onPress,
  onPressRemove,
  isFree,
  deliveryCharges,
  actualDeliveryCharge,
  minimum_order_amount,

  mimumDiscount,
  minimumTOtamAMount,
  minimumAmountForCharges,
}) => {
  return (
    <View
      style={[
        SpStyles.FDR_ALC_JCS_W100,
        {marginBottom: Responsive.widthPx(2)},
      ]}>
      <View style={SpStyles.FDR_ALC}>
        <TouchableOpacity onPress={onPress}>
          <Text style={[TextStyles.darkGray_S_14_500, otherTextStyle]}>
            {item}
          </Text>
          {isFree && minimum_order_amount > 0 && (
            <Text style={[TextStyles.gray_S_12_400, otherTextStyle]}>
              Add item worth ₹{parseFloat(minimum_order_amount)?.toFixed(2)} for
              a free delivery
            </Text>
          )}
        </TouchableOpacity>
        {removeVisible && (
          <TouchableOpacity onPress={onPressRemove}>
            <Text
              style={{
                ...TextStyles.red_10_400,
                marginBottom: Responsive.widthPx(0.5),
                marginLeft: Responsive.widthPx(1.2),
              }}>
              Remove
            </Text>
          </TouchableOpacity>
        )}
      </View>
      {isFree && minimum_order_amount <= 0 ? (
        <View style={SpStyles.FDR_ALC}>
          <Text
            style={[
              TextStyles.darkGray_S_14_500,
              {textAlign: 'right', textDecorationLine: 'line-through'},
            ]}>
            {minimumTOtamAMount - mimumDiscount >= minimumAmountForCharges
              ? '₹' + actualDeliveryCharge + ' '
              : ''}
          </Text>
          {deliveryCharges == 0 ? (
            <Text
              style={[
                TextStyles.green_S_14_700,
                {width: 42, textAlign: 'right'},
              ]}>
              FREE
            </Text>
          ) : (
            <Text
              style={[
                TextStyles.darkGray_S_14_500,
                {width: 42, textAlign: 'right'},
              ]}>
              ₹{deliveryCharges}
            </Text>
          )}
        </View>
      ) : (
        <Text style={[TextStyles.darkGray_S_14_500, otherTextStyle]}>
          {price == 0 && minimum_order_amount > 0
            ? `₹ ${actualDeliveryCharge}`
            : `₹ ${price}`}
        </Text>
      )}
    </View>
  );
};

export default DeliveryChargeCard;
