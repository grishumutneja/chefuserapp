import {View, Text} from 'react-native';
import React from 'react';
import IconButton from '../Buttons/IconButton';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';
import {useSelector} from 'react-redux';
import Open from '../../Constants/Open';

const FollowOnCard = ({otherStyle}) => {
  const {socialMediaData} = useSelector(state => state.socialMedia);

  return (
    <View style={otherStyle}>
      <Text style={CmnStyles.followOnCardText}>Follow us on</Text>
      <View style={CmnStyles.followOnCardBox}>
        <IconButton
          imgSource={Images.facebook}
          onPress={() => {
            Open.anyUrl(socialMediaData[0].facebook_link);
          }}
        />
        <IconButton
          imgSource={Images.instagram}
          onPress={() => {
            Open.anyUrl(socialMediaData[0].instagram_link);
          }}
        />
        <IconButton
          imgSource={Images.youtube}
          onPress={() => {
            Open.anyUrl(socialMediaData[0].youtube_link);
          }}
        />
      </View>
    </View>
  );
};

export default FollowOnCard;
