import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import SpStyles from '../../Styles/SpStyles';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';
import {useDispatch, useSelector} from 'react-redux';
import {deleteDeliveryAddressCall} from '../../Core/Config/ApiCalls';
import {getLocationByLatLong} from '../../Constants/GetLocationByLatLong';

const AddressCard = ({
  item,
  index,
  setUpdateModalVisible,
  setUpdateAddress,
  UpdateAddress,
}) => {
  const {loginData} = useSelector(state => state.userInfo);
  const dispatch = useDispatch();

  const addressType =
    item?.address_type == '1'
      ? 'Home'
      : item?.address_type == '2'
      ? 'Work'
      : item.address_type == '3'
      ? 'Others'
      : null;

  return (
    <View style={CmnStyles.addressCardCont}>
      <View style={SpStyles.FDR_ALC_JCS}>
        <Text style={{...TextStyles.red_14_700}}>{addressType}</Text>
        <View style={SpStyles.FDR_ALC_JCS}>
          <TouchableOpacity
            onPress={() => {
              const locationData = {lat: item.lat, lng: item.long};
              setUpdateModalVisible(true);
              getLocationByLatLong({
                setLocationName: UpdateAddress.setLocationName,
                position: locationData,
              });
              UpdateAddress.setFullAddress(item.house_no);
              UpdateAddress.setLandmark(item.reach);
              UpdateAddress.setContact(item.contact_no);
              UpdateAddress.setDefaultType(item.address_type);
              UpdateAddress.setIsPrimary(item.primary_key);
              UpdateAddress.setLocation(locationData);
              UpdateAddress.setAddressId(item?.id);
            }}>
            <Image
              source={Images.editPen}
              style={CmnStyles.editPenImg}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              deleteDeliveryAddressCall({
                dispatch: dispatch,
                userId: loginData?.user_id,
                id: item?.id,
              });
            }}>
            <Image
              source={Images.delete}
              style={CmnStyles.deleteAddressImg}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
      <Text style={CmnStyles.addressCardText2}>{item.house_no}</Text>
      <Text style={CmnStyles.addressCardText}>
        Nearest landmark: {item?.reach}
      </Text>
      <Text style={CmnStyles.addressCardText}>
        Alternate Mobile: {item?.contact_no}
      </Text>
    </View>
  );
};

export default AddressCard;
