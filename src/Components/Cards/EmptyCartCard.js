import {View, Text} from 'react-native';
import React from 'react';
import RedButton from '../Buttons/RedButton';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import {useNavigation} from '@react-navigation/native';

const EmptyCartCard = () => {
  const navigation = useNavigation();

  return (
    <View style={CmnStyles.emptyCartCardCont}>
      <Text style={TextStyles.gray_S_22_400}>Empty Stomach - Empty Cart</Text>
      <Text style={TextStyles.darkGray_S_24_700}>Not Fair</Text>
      <Text style={CmnStyles.emptyCartCardEmoji}>😒</Text>
      <RedButton
        text={"Let's Get Something"}
        otherStyle={CmnStyles.emptyCartCardBtn}
        otherTextStyle={TextStyles.white_S_20_700}
        onPress={() => {
          navigation.goBack();
        }}
      />
    </View>
  );
};

export default EmptyCartCard;
