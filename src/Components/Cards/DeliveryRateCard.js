import {View, Text, Image, TextInput, FlatList, ScrollView} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import TextStyles from '../../Styles/TextStyles';
import StarRating from 'react-native-star-rating-widget';
import Colors from '../../Constants/Colors';
import Responsive from '../../Constants/Responsive';
import RedButton from '../Buttons/RedButton';
import SpStyles from '../../Styles/SpStyles';

const DeliveryRateCard = ({
  text,
  name,
  suggestion,
  setSuggestion,
  starRate,
  setStarRate,
  isDriverRate,
  onPress,
  imgUri,
  products,
  setProductData,
}) => {
  const starReview =
    starRate == 0
      ? 'Share Your Rating'
      : starRate == 1
      ? 'Bad'
      : starRate == 2
      ? 'Average'
      : starRate == 3
      ? 'Good'
      : starRate == 4
      ? 'Very Good'
      : starRate == 5 && 'Excellent';

  const renderDishes = ({item, index}) => {
    return (
      <View style={CmnStyles.renderDishesRateCont}>
        <View style={SpStyles.FDR_ALC}>
          <Image
            source={item?.type == 'veg' ? Images.pure_veg : Images.non_veg_icon}
            style={CmnStyles.nonVegRate}
            resizeMode="contain"
          />
          <Text style={CmnStyles.renderDishesRateText}>
            {item?.product_name}
          </Text>
        </View>
        <View style={CmnStyles.renderDishesRateStar}>
          <StarRating
            starSize={Responsive.widthPx(8)}
            starStyle={{marginRight: -5}}
            onChange={value => {
              let Temp = [];
              products?.map((itm, ind) => {
                if (index == ind) {
                  itm.ratting = value;
                }
                Temp.push(itm);
              });
              setProductData(Temp);
            }}
            rating={item?.ratting}
            enableHalfStar={false}
          />
        </View>
      </View>
    );
  };

  return (
    <ScrollView>
      <View style={CmnStyles.driverRattingScreenCont}>
        <Image
          source={imgUri ? {uri: imgUri} : Images.user_profile}
          style={CmnStyles.driverRattingScreenImg}
          resizeMode="contain"
        />
        <Text style={TextStyles.black_S_16_400}>{text}</Text>
        <View style={CmnStyles.deliveryRateCardLine} />
        <Text style={TextStyles.black_S_20_700}>{name}</Text>
        <StarRating
          rating={starRate}
          emptyColor={Colors.gray}
          onChange={num => setStarRate(num)}
          starSize={45}
          starStyle={{marginVertical: Responsive.widthPx(4)}}
          enableHalfStar={false}
        />
        <Text style={TextStyles.black_S_20_700}>{starReview}</Text>
        <View style={CmnStyles.deliveryRateCardSmallLine}>
          <View
            style={{
              ...CmnStyles.deliveryRateCardSmallLine2,
              width: Responsive.widthPx(11) * starRate,
            }}
          />
        </View>
        <Text style={TextStyles.black_S_16_700}>Any Suggestions/Reviews?</Text>
        <View style={CmnStyles.deliveryRateCardTextInputBox}>
          <TextInput
            value={suggestion}
            onChangeText={txt => {
              setSuggestion(txt);
            }}
            placeholder="Type your Suggestion/Review here"
            style={CmnStyles.deliveryRateCardTextInput}
            placeholderTextColor={Colors.darkGray}
            multiline
          />
        </View>
        {!isDriverRate && (
          <>
            <Text style={CmnStyles.deliveryRateCardDishesText}>
              How was the Dishes?
            </Text>
            <FlatList
              data={products}
              scrollEnabled={false}
              renderItem={renderDishes}
            />
          </>
        )}
        <RedButton
          text={'Submit'}
          otherStyle={CmnStyles.deliveryRateCardBtn}
          onPress={onPress}
        />
      </View>
    </ScrollView>
  );
};

export default DeliveryRateCard;
