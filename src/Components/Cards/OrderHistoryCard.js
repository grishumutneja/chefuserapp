import {View, Text, Image, TouchableOpacity, FlatList} from 'react-native';
import React, {useEffect, useState} from 'react';
import SpStyles from '../../Styles/SpStyles';
import Images from '../../Constants/Images';
import TextStyles from '../../Styles/TextStyles';
import CmnStyles from '../../Styles/CmnStyles';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import Colors from '../../Constants/Colors';
import {orderStatusHandler} from '../../Constants/orderStatusHandler';

const OrderHistoryCard = ({item, index}) => {
  const isFocused = useIsFocused();
  const navigation = useNavigation();
  const [status, setStatus] = useState();

  const isOrderCancel =
    item?.order_status == 'cancelled_by_customer_before_confirmed' ||
    item?.order_status == 'cancelled_by_customer_after_confirmed' ||
    item?.order_status == 'cancelled_by_customer_during_prepare' ||
    item?.order_status == 'cancelled_by_customer_after_disptch' ||
    item?.order_status == 'cancelled_by_vendor' ||
    item?.order_status == 'Order cancelled by vendor'
      ? true
      : false;

  useEffect(() => {
    const orderStatus = orderStatusHandler(item?.order_status);
    setStatus(orderStatus);
  }, [isFocused]);

  const renderProducts = ({item, index}) => {
    return (
      <View style={CmnStyles.orderHistoryCardBox3}>
        <Image
          source={Images.pure_veg}
          style={CmnStyles.orderHistoryCardIcon}
          resizeMode="contain"
        />
        <Text style={CmnStyles.orderHistoryCardOrderText}>
          {item?.product_qty} x {item?.product_name}
        </Text>
      </View>
    );
  };

  return (
    <TouchableOpacity
      style={[
        CmnStyles.orderHistoryCardCont,
        isOrderCancel && {backgroundColor: Colors.grey_e7e7e7},
      ]}
      disabled={isOrderCancel}
      onPress={() => {
        navigation.navigate(Screens.TrackOrder, {
          orderId: item?.order_id,
        });
      }}>
      <View style={SpStyles.FDR_ALC}>
        <View style={CmnStyles.orderHistoryCardImgBox}>
          <Image
            source={{uri: item?.image}}
            style={CmnStyles.orderHistoryCardImg}
            resizeMode="contain"
          />
        </View>
        <View style={CmnStyles.orderHistoryCardBox}>
          <Text style={TextStyles.black_S_16_700}>{item?.vendor_name}</Text>
          <View style={SpStyles.FDR_ALC}>
            <View style={CmnStyles.orderHistoryCardSubBox}>
              <Text style={CmnStyles.orderHistoryCardText}>
                Order id: #{item?.order_id}
              </Text>
              <Text style={CmnStyles.orderHistoryCardText2}>
                {item?.order_date}
              </Text>
            </View>
            <View style={CmnStyles.orderHistoryCardSubBox2}>
              <View style={CmnStyles.orderHistoryCardStatusBox}>
                <Text style={CmnStyles.orderHistoryCardStatusText}>
                  {status}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View style={CmnStyles.orderHistoryCardBox2}>
        <FlatList data={item?.products} renderItem={renderProducts} />
      </View>
      <View style={CmnStyles.orderHistoryCardPriceBox}>
        <Text style={{...TextStyles.darkGray_S_18_400}}>Grand Total</Text>
        <View style={{...SpStyles.FDR_ALC}}>
          <Text style={{...TextStyles.darkGray_S_14_500}}>
            {item?.payment_type} :
          </Text>
          <Text style={{...TextStyles.darkGray_S_18_700}}>
            {' '}
            ₹{item?.net_amount}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default OrderHistoryCard;
