import {View, Text, TouchableOpacity, Image} from 'react-native';
import React, {useState} from 'react';
import Images from '../../Constants/Images';
import Colors from '../../Constants/Colors';
import SpStyles from '../../Styles/SpStyles';
import TextStyles from '../../Styles/TextStyles';
import CmnStyles from '../../Styles/CmnStyles';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {updateFavResExplore} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantToExploreSlice';
import {
  guestAccountHandler,
  updateCartApiCall,
} from '../../Core/Config/ApiCalls';
import Screens from '../../Core/Stack/Screens';

const ResDetailTopButtons = ({
  otherStyle,
  isClosed,
  resData,
  setSearchModalVisible,
  cartVendorId,
  cart_id,
  itemsInCart,
  isFreeDelivery,
  minimum_order_amount,
}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {getUserInfoData, isGuestAccount} = useSelector(
    state => state.userInfo,
  );
  const [favDish, setFavDish] = useState(resData?.is_like == 1);

  const updateCartHandler = () => {
    const body = {
      cart_id: cart_id,
      user_id: getUserInfoData?.id,
      vendor_id: resData?.vendor_id,
      products: itemsInCart,
    };
    if (cart_id) {
      updateCartApiCall({body, dispatch});
    }
  };

  const backHandler = () => {
    if (resData?.vendor_id == cartVendorId) {
      updateCartHandler();
      navigation.goBack();
    } else {
      navigation.goBack();
    }
  };

  const updateFavHandler = () => {
    const body = {
      vendor_id: resData?.vendor_id,
      is_like: favDish ? 0 : 1,
      user_id: getUserInfoData?.id,
      item: resData,
      dispatch: dispatch,
    };
    dispatch(updateFavResExplore(body));
  };

  return (
    <View style={[CmnStyles.resDetailTopButtonsCont, otherStyle]}>
      <TouchableOpacity
        style={CmnStyles.resDetailTopBackBtnBox}
        onPress={() => {
          backHandler();
        }}>
        <Image
          source={Images.back}
          style={CmnStyles.resDetailTopBackBtnImg}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View style={SpStyles.FDR_ALC}>
        <TouchableOpacity
          style={CmnStyles.resDetailTopSearchBtnBox}
          onPress={() => {
            navigation.navigate(Screens.ResDishSearch, {
              vendorId: resData?.vendor_id,
              isClosed: isClosed,
              minimum_order_amount: minimum_order_amount,
              isFreeDelivery: isFreeDelivery,
            });
            if (resData?.vendor_id == cartVendorId) {
              updateCartHandler();
              navigation.navigate(Screens.ResDishSearch, {
                vendorId: resData?.vendor_id,
                isClosed: isClosed,
                minimum_order_amount: minimum_order_amount,
                isFreeDelivery: isFreeDelivery,
              });
            } else {
              navigation.navigate(Screens.ResDishSearch, {
                vendorId: resData?.vendor_id,
                isClosed: isClosed,
                minimum_order_amount: minimum_order_amount,
                isFreeDelivery: isFreeDelivery,
              });
            }
          }}>
          <Image
            source={Images.search}
            style={
              isClosed == 0
                ? CmnStyles.resDetailTopSearchBtnImg
                : CmnStyles.resDetailTopSearchBtnImgGray
            }
            resizeMode="contain"
          />
          <Text style={TextStyles.darkGray_S_12_700}>Screen Dishes</Text>
        </TouchableOpacity>
        {/* <TouchableOpacity
          style={CmnStyles.resDetailTopSearchBtnBox}
          onPress={() => {
            setSearchModalVisible(true);
          }}>
          <Image
            source={Images.search}
            style={
              isClosed == 0
                ? CmnStyles.resDetailTopSearchBtnImg
                : CmnStyles.resDetailTopSearchBtnImgGray
            }
            resizeMode="contain"
          />
          <Text style={TextStyles.darkGray_S_12_700}>Search Dishes</Text>
        </TouchableOpacity> */}
        <TouchableOpacity
          style={CmnStyles.resDetailFavImgBox}
          onPress={() => {
            if (isGuestAccount) {
              guestAccountHandler({navigation});
            } else {
              setFavDish(!favDish);
              updateFavHandler();
            }
          }}>
          <Image
            source={Images.favorite}
            style={{
              ...CmnStyles.resDetailFavImg,
              tintColor: favDish ? Colors.red : Colors.gray,
            }}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ResDetailTopButtons;
