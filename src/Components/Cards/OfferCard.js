import {View, Text, Image} from 'react-native';
import React from 'react';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';
import {Grayscale} from 'react-native-color-matrix-image-filters';

const OfferCard = ({item, index, isClosed, otherStyle, textColor, isRed}) => {
  return (
    <View
      style={[
        isClosed == 0 ? CmnStyles.offerCardCont : CmnStyles.offerCardContGray,
        otherStyle,
      ]}>
      {isClosed == 0 ? (
        <Image
          source={isRed ? Images.discount : Images.blue_offer}
          style={CmnStyles.offerCardImg}
          resizeMode="contain"
        />
      ) : (
        <Grayscale>
          <Image
            source={Images.blue_offer}
            style={CmnStyles.offerCardImg}
            resizeMode="contain"
          />
        </Grayscale>
      )}

      <View>
        <Text style={[CmnStyles.offerCardTitle, textColor]}>{item?.name}</Text>
        <Text style={[CmnStyles.offerCardText, textColor]}>{item?.code}</Text>
      </View>
    </View>
  );
};

export default OfferCard;
