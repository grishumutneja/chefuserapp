import {View, Text, ActivityIndicator} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Colors from '../../Constants/Colors';

const FooterRender = ({isComplete, pageLoading}) => {
  return (
    <View>
      {isComplete && (
        <Text style={CmnStyles.noMoreResText2}>No more record to explore</Text>
      )}
      {pageLoading && (
        <ActivityIndicator
          size={'small'}
          style={CmnStyles.paginationLoader2}
          color={Colors.red_primary}
        />
      )}
    </View>
  );
};

export default FooterRender;
