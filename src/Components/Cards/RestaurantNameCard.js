import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import React, {memo, useState} from 'react';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import Colors from '../../Constants/Colors';
import SpStyles from '../../Styles/SpStyles';
import TimeConvert from '../../Constants/TimeConvert';
import {ReviewModal} from '../Modals/ReviewModal';

const RestaurantNameCard = ({resData, isClosed, closeRemainTime}) => {
  const [reviewModalVisible, setReviewModalVisible] = useState(false);
  const openTime = resData?.next_available
    ? TimeConvert(resData?.next_available)
    : '';
  const closeTime = resData?.end_time ? TimeConvert(resData?.end_time) : '';
  const venderId = resData?.vendor_id;

  return (
    <View style={styles.container}>
      {closeRemainTime && (
        <View style={styles.closeSoonCont}>
          <Text style={styles.closeSoon}>Closes Soon</Text>
          <Text style={styles.remainTime}>{closeRemainTime} min</Text>
        </View>
      )}
      <Text style={styles.title}>
        {resData?.name || resData?.restaurantName}
      </Text>
      <View style={styles.vegCont}>
        {resData?.vendor_food_type == 1 && (
          <>
            <Image
              source={Images.leaf}
              style={isClosed == 1 ? styles.vegIconGray : styles.vegIcon}
              resizeMode="contain"
            />
            <Text style={styles.cuisines}>Pure Veg • </Text>
          </>
        )}
        <Text style={styles.cuisines}>{resData?.cuisines?.join(' • ')}</Text>
      </View>
      <View style={styles.vegCont2}>
        <View style={isClosed ? styles.starBoxGray : styles.starBox}>
          <Text style={TextStyles.white_S_12_700}>
            {resData?.vendor_ratings?.toFixed(1)}
          </Text>
          <Image
            source={Images.star}
            style={styles.star}
            resizeMode="contain"
          />
        </View>
        <TouchableOpacity
          style={styles.reviewBox}
          onPress={() => {
            setReviewModalVisible(true);
          }}>
          <Text style={styles.review}>{resData?.review_count} Reviews</Text>
          <UnderLine length={resData?.review_count?.toString()?.length} />
        </TouchableOpacity>
      </View>
      <View style={isClosed ? styles.timeContGray : styles.timeCont}>
        <Image
          source={Images.stopwatch}
          style={styles.stopwatch}
          resizeMode="contain"
        />
        <Text style={isClosed ? styles.closed : styles.time}>
          {isClosed == 0 ? 'Open' : 'Closed'}
        </Text>
        <View style={styles.line} />
        <Text style={TextStyles.black_S_13_400}>
          {isClosed == 0
            ? `Closes at ${closeTime}`
            : `Opens  ${resData?.next_available}`}
        </Text>
      </View>
      {reviewModalVisible && (
        <ReviewModal
          modalVisible={reviewModalVisible}
          onPressCancel={() => {
            setReviewModalVisible(false);
          }}
          // data={reviewData}
          venderId={venderId}
          isClosed={isClosed}
        />
      )}
    </View>
  );
};

export default memo(RestaurantNameCard);

const styles = StyleSheet.create({
  container: {
    marginTop: Responsive.widthPx(24),
    alignItems: 'center',
    paddingHorizontal: Responsive.widthPx(10),
  },
  title: {
    ...TextStyles.black_S_22_700,
    textAlign: 'center',
  },
  vegIconGray: {
    width: Responsive.widthPx(4),
    height: Responsive.widthPx(4),
    marginRight: 2,
    tintColor: Colors.gray,
  },
  vegIcon: {
    width: Responsive.widthPx(4),
    height: Responsive.widthPx(4),
    marginRight: 2,
    tintColor: Colors.greenPureVeg,
  },
  dot: {
    width: 4,
    height: 4,
    borderRadius: 4,
    backgroundColor: Colors.gray,
    marginHorizontal: 5,
  },
  cuisines: {
    ...TextStyles.darkGray_S_15_400,
  },
  vegCont: {
    ...SpStyles.FDR_ALC,
    marginTop: Responsive.widthPx(3),
    height: Responsive.widthPx(6),
    paddingHorizontal: Responsive.widthPx(10),
  },
  vegCont2: {
    ...SpStyles.FDR_ALC,
    marginTop: Responsive.widthPx(3),
  },
  star: {
    width: 10,
    height: 10,
    tintColor: Colors.white,
    marginLeft: 4,
  },
  stopwatch: {
    width: 16,
    height: 16,
    marginRight: 6,
  },
  starBox: {
    paddingHorizontal: 8,
    paddingVertical: 3,
    backgroundColor: Colors.greenPureVeg,
    ...SpStyles.FDR_ALC,
    borderRadius: 6,
  },
  starBoxGray: {
    paddingHorizontal: 8,
    paddingVertical: 3,
    backgroundColor: Colors.gray,
    ...SpStyles.FDR_ALC,
    borderRadius: 6,
  },
  review: {
    ...TextStyles.black_S_14_700,
  },
  reviewBox: {
    marginLeft: 10,
  },
  underLine: {
    height: 2,
    backgroundColor: 'gray',
    width: 4,
    marginRight: 4,
  },
  timeCont: {
    ...SpStyles.FDR_ALC,
    marginTop: Responsive.widthPx(4.5),
    backgroundColor: Colors.blue_light,
    paddingHorizontal: Responsive.widthPx(3),
    paddingVertical: Responsive.widthPx(2),
    borderRadius: Responsive.widthPx(4),
  },
  timeContGray: {
    ...SpStyles.FDR_ALC,
    marginTop: Responsive.widthPx(4.5),
    backgroundColor: Colors.lightGray2,
    paddingHorizontal: Responsive.widthPx(3),
    paddingVertical: Responsive.widthPx(2),
    borderRadius: Responsive.widthPx(4),
  },
  line: {
    height: 14,
    borderRightWidth: 1,
    marginHorizontal: 12,
    borderRightColor: Colors.gray,
  },
  time: {
    ...TextStyles.black_S_13_700,
  },
  closed: {
    ...TextStyles.red_13_700,
  },
  closeSoonCont: {
    backgroundColor: Colors.yellow,
    paddingHorizontal: Responsive.widthPx(4),
    borderRadius: Responsive.widthPx(3),
    paddingVertical: Responsive.widthPx(1.5),
    marginBottom: Responsive.widthPx(2),
    marginTop: Responsive.widthPx(-4),
  },
  closeSoon: {
    ...TextStyles.black_S_15_700,
    textAlign: 'center',
  },
  remainTime: {
    ...TextStyles.black_S_12_400,
    textAlign: 'center',
    marginTop: 2,
  },
});

const UnderLine = ({length}) => {
  return (
    <View style={SpStyles.FDR_ALC}>
      <View style={styles.underLine} />
      <View style={styles.underLine} />
      <View style={styles.underLine} />
      <View style={styles.underLine} />
      <View style={styles.underLine} />
      <View style={styles.underLine} />
      <View style={styles.underLine} />
      <View style={styles.underLine} />
      <View style={styles.underLine} />
      {length == 2 && <View style={styles.underLine} />}
      {length == 3 && <View style={styles.underLine} />}
    </View>
  );
};
