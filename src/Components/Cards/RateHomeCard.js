import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../Constants/Colors';
import CmnStyles from '../../Styles/CmnStyles';
import StarRating from 'react-native-star-rating-widget';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import TextStyles from '../../Styles/TextStyles';
import {useNavigation} from '@react-navigation/native';

const RateHomeCard = ({text, setVisible, onPress}) => {
  const navigation = useNavigation();

  return (
    <View style={CmnStyles.rateHomeCardCont}>
      <TouchableOpacity onPress={onPress}>
        <Text style={TextStyles.black_S_15_400}>{text}</Text>
      </TouchableOpacity>
      <View style={SpStyles.FDR_ALC}>
        <StarRating
          starSize={20}
          color={Colors.red_primary}
          starStyle={{marginRight: -5}}
          onChange={onPress}
        />
        <TouchableOpacity onPress={() => setVisible(false)}>
          <Image
            source={Images.cancel}
            style={{width: 18, height: 18, marginLeft: 12}}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default RateHomeCard;
