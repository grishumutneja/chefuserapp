import {View, Text, StyleSheet, Platform} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import SpStyles from '../../Styles/SpStyles';

const CartSkelton = ({visible}) => {
  const repeatBox = () => {
    return <View style={styles.boxBox} />;
  };

  const repeatLine = () => {
    return (
      <>
        <View style={styles.lineFull2} />
        <View style={styles.lineFull3} />
        <View style={styles.lineFull4} />
      </>
    );
  };

  const lineSkelton = () => {
    return (
      <View style={styles.catSkeltonCont}>
        <View style={styles.bigLineFull} />
        <View style={styles.lineFull} />
        {repeatLine()}
        {repeatLine()}
        {repeatLine()}
        {repeatLine()}
        <View style={{marginTop: 10}}>
          <View style={styles.bigLineFull} />
          <View style={styles.bigLineFull} />
          <View style={styles.bigLineFull} />
          <View style={styles.bigLineFull} />
          <View style={styles.bigLineFull} />
        </View>
        <View style={styles.lineFull5} />
        <View style={SpStyles.FDR_ALC}>
          {repeatBox()}
          {repeatBox()}
          {repeatBox()}
          {repeatBox()}
        </View>
        {repeatLine()}
        <View style={styles.bigLineFull} />
        <View style={styles.bigLineFull} />
      </View>
    );
  };

  return (
    <>
      {visible ? (
        <View
          style={{
            width: Responsive.widthPx(100),
            height: Responsive.heightPx(100),
            position: 'absolute',
            bottom: 0,
            backgroundColor: 'white',
          }}>
          <SkeletonPlaceholder backgroundColor="#e7e7e7">
            {lineSkelton()}
          </SkeletonPlaceholder>
        </View>
      ) : null}
    </>
  );
};

const styles = StyleSheet.create({
  bigLineFull: {
    height: 20,
    width: Responsive.widthPx(94),
    borderRadius: 6,
    marginTop: 8,
    marginBottom: 4,
  },
  lineFull: {
    height: 14,
    width: Responsive.widthPx(94),
    borderRadius: 4,
    marginTop: 10,
  },
  lineFull2: {
    height: 14,
    width: Responsive.widthPx(72),
    borderRadius: 4,
    marginTop: 14,
  },
  lineFull3: {
    height: 14,
    width: Responsive.widthPx(84),
    borderRadius: 4,
    marginTop: 6,
  },
  lineFull4: {
    height: 14,
    width: Responsive.widthPx(78),
    borderRadius: 4,
    marginTop: 6,
  },
  lineFull5: {
    height: 14,
    width: Responsive.widthPx(64),
    borderRadius: 4,
    marginTop: 6,
  },
  catSkeltonCont: {
    marginTop:
      Platform.OS == 'android'
        ? Responsive.widthPx(20)
        : Responsive.widthPx(12),
    marginLeft: Responsive.widthPx(3),
  },
  boxBox: {
    width: Responsive.widthPx(22),
    height: Responsive.widthPx(22),
    borderRadius: Responsive.widthPx(2),
    marginRight: Responsive.widthPx(2),
    marginTop: Responsive.widthPx(4),
  },
});

export default CartSkelton;
