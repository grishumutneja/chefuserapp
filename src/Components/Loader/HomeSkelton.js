import {View, Text, StyleSheet, ScrollView} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const HomeSkelton = () => {
  const box = () => {
    return (
      <View style={styles.boxBox}>
        <View style={styles.round} />
        <View style={styles.line} />
      </View>
    );
  };

  const catSkelton = () => {
    return (
      <View style={styles.catSkeltonCont}>
        <View style={styles.lineFull} />
        <View style={styles.boxCont}>
          {box()}
          {box()}
          {box()}
          {box()}
          {box()}
        </View>
      </View>
    );
  };

  const dishSkelton = () => {
    return (
      <View style={styles.dishSkeltonCont}>
        <View style={styles.title} />
        <View style={styles.line2} />
        <View style={styles.boxCont}>
          <View style={styles.dishBox} />
          <View style={styles.dishBox} />
        </View>
      </View>
    );
  };

  return (
    <ScrollView>
      <SkeletonPlaceholder backgroundColor="#e7e7e7">
        <View style={styles.searchBox} />
        <View style={styles.bannerBox} />
        {catSkelton()}
        {catSkelton()}
        {dishSkelton()}
      </SkeletonPlaceholder>
    </ScrollView>
  );
};

export default HomeSkelton;

const styles = StyleSheet.create({
  lineFull: {
    height: 14,
    width: Responsive.widthPx(94),
    borderRadius: 4,
    alignSelf: 'center',
  },
  dishSkeltonCont: {
    width: Responsive.widthPx(94),
    alignSelf: 'center',
    marginTop: Responsive.widthPx(12),
  },
  bannerBox: {
    width: Responsive.widthPx(100),
    height: Responsive.widthPx(45),
    marginTop: Responsive.widthPx(5),
  },
  searchBox: {
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(13),
    marginTop: Responsive.widthPx(5),
    alignSelf: 'center',
    borderRadius: Responsive.widthPx(3),
  },
  boxCont: {
    width: Responsive.widthPx(94),
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  round: {
    width: Responsive.widthPx(17),
    height: Responsive.widthPx(17),
    borderRadius: Responsive.widthPx(8.5),
  },
  line: {
    width: Responsive.widthPx(17),
    height: 14,
    borderRadius: 8,
    marginTop: 8,
  },
  title: {
    width: Responsive.widthPx(28),
    height: 10,
    borderRadius: 4,
  },
  line2: {
    width: Responsive.widthPx(94),
    height: 28,
    borderRadius: 6,
    marginTop: Responsive.widthPx(2),
  },
  boxBox: {
    marginRight: Responsive.widthPx(2),
  },
  catSkeltonCont: {
    marginTop: Responsive.widthPx(12),
  },
  dishBox: {
    width: Responsive.widthPx(50),
    height: Responsive.widthPx(60),
    borderRadius: Responsive.widthPx(3),
    marginRight: Responsive.widthPx(4),
  },
});
