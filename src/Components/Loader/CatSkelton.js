import {View, Text, ScrollView, StyleSheet} from 'react-native';
import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';

const CatSkelton = () => {
  return (
    <View style={styles.container}>
      <SkeletonPlaceholder backgroundColor="#e7e7e7">
        <View style={styles.bannerBox} />
        <View style={styles.bannerBox} />
        <View style={styles.bannerBox} />
      </SkeletonPlaceholder>
    </View>
  );
};

export default CatSkelton;

const styles = StyleSheet.create({
  container: {
    width: Responsive.widthPx(100),
    position: 'absolute',
    backgroundColor: Colors.white,
    height: Responsive.heightPx(100),
    marginTop: Responsive.widthPx(58),
  },
  bannerBox: {
    width: Responsive.widthPx(94),
    height: Responsive.widthPx(50),
    borderRadius: Responsive.widthPx(2),
    marginTop: Responsive.widthPx(3),
    alignSelf: 'center',
  },
});
