import {View, StyleSheet, Platform, ScrollView} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import SpStyles from '../../Styles/SpStyles';

const TrackSkelton = ({visible}) => {
  const lineSkelton = () => {
    return (
      <>
        <View style={SpStyles.FDR_ALC}>
          <View style={styles.boxBox} />
          <View>
            <View style={styles.lineFull2} />
            <View style={styles.lineFull3} />
            <View style={styles.lineFull4} />
            <View style={styles.lineFull5} />
          </View>
        </View>
        <View style={styles.lineFull} />
        <View style={styles.bigLineFull} />
      </>
    );
  };

  return (
    <>
      {visible ? (
        <View style={styles.container}>
          <ScrollView>
            <SkeletonPlaceholder backgroundColor="#e7e7e7">
              <View style={styles.catSkeltonCont}>
                {lineSkelton()}
                {lineSkelton()}
                {lineSkelton()}
                {lineSkelton()}
                {lineSkelton()}
              </View>
            </SkeletonPlaceholder>
          </ScrollView>
        </View>
      ) : null}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Responsive.widthPx(100),
    height: Responsive.heightPx(100),
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'white',
  },
  bigLineFull: {
    height: 20,
    width: Responsive.widthPx(94),
    borderRadius: 6,
    marginTop: 6,
    marginBottom: 20,
  },
  lineFull: {
    height: 14,
    width: Responsive.widthPx(94),
    borderRadius: 4,
    marginTop: 10,
  },
  lineFull2: {
    height: 18,
    width: Responsive.widthPx(62),
    borderRadius: 4,
    marginTop: -2,
  },
  lineFull3: {
    height: 10,
    width: Responsive.widthPx(52),
    borderRadius: 3,
    marginTop: 4,
  },
  lineFull4: {
    height: 10,
    width: Responsive.widthPx(58),
    borderRadius: 3,
    marginTop: 4,
  },
  lineFull5: {
    height: 10,
    width: Responsive.widthPx(58),
    borderRadius: 3,
    marginTop: 8,
  },
  catSkeltonCont: {
    marginTop:
      Platform.OS == 'android'
        ? Responsive.widthPx(20)
        : Responsive.widthPx(12),
    marginLeft: Responsive.widthPx(3),
  },
  boxBox: {
    width: Responsive.widthPx(22),
    height: Responsive.widthPx(22),
    borderRadius: Responsive.widthPx(2),
    marginRight: Responsive.widthPx(3),
  },
});

export default TrackSkelton;
