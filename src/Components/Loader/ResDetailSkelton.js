import {View, StyleSheet, Platform, ScrollView} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import SpStyles from '../../Styles/SpStyles';

const ResDetailSkelton = ({visible}) => {
  const lineSkelton = () => {
    return (
      <>
        <View
          style={[SpStyles.FDR_ALC_JCS, {marginBottom: Responsive.widthPx(4)}]}>
          <View>
            <View style={styles.lineFull2} />
            <View style={styles.lineFull3} />
            <View style={styles.lineFull4} />
            <View style={styles.lineFull5} />
          </View>
          <View style={styles.boxBox} />
        </View>
      </>
    );
  };

  return (
    <>
      {visible ? (
        <View style={styles.container}>
          <View>
            <SkeletonPlaceholder backgroundColor="#e7e7e7">
              <View style={styles.title} />
              <View style={styles.catSkeltonCont}>
                {lineSkelton()}
                {lineSkelton()}
                {lineSkelton()}
                {lineSkelton()}
                {lineSkelton()}
              </View>
            </SkeletonPlaceholder>
          </View>
        </View>
      ) : null}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bigLineFull: {
    height: 20,
    width: Responsive.widthPx(92),
    borderRadius: 6,
    marginTop: 6,
    marginBottom: 20,
  },
  lineFull: {
    height: 14,
    width: Responsive.widthPx(92),
    borderRadius: 4,
    marginTop: 20,
    marginBottom: Responsive.widthPx(5),
  },
  lineFull2: {
    height: 18,
    width: Responsive.widthPx(42),
    borderRadius: 4,
    marginTop: -2,
  },
  lineFull3: {
    height: 10,
    width: Responsive.widthPx(32),
    borderRadius: 3,
    marginTop: 8,
  },
  lineFull4: {
    height: 10,
    width: Responsive.widthPx(38),
    borderRadius: 3,
    marginTop: 8,
  },
  lineFull5: {
    height: 10,
    width: Responsive.widthPx(38),
    borderRadius: 3,
    marginTop: 8,
  },
  catSkeltonCont: {
    marginTop: Responsive.widthPx(5),
    marginLeft: Responsive.widthPx(4),
  },
  boxBox: {
    width: Responsive.widthPx(32),
    height: Responsive.widthPx(32),
    borderRadius: Responsive.widthPx(2),
    marginRight: Responsive.widthPx(4),
  },
  title: {
    width: Responsive.widthPx(100),
    height: Responsive.widthPx(10),
    marginTop: Responsive.widthPx(10),
  },
});

export default ResDetailSkelton;
