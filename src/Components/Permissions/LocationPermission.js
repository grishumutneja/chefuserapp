import {Platform, PermissionsAndroid} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AsKey from '../../Constants/AsKey';
import {locationCall} from '../../Core/Config/ApiCalls';
import {locationPermissionStatus} from '../../Core/Redux/Slices/CommonSlices/LocationSlice';

const getCurrentLocation = async dispatch => {
  if (Platform.OS === 'ios') {
    await Geolocation.requestAuthorization('always').then(res => {
      Geolocation.getCurrentPosition(
        position => {
          if (position) {
            const location = {
              lat: position?.coords?.latitude,
              lng: position?.coords?.longitude,
            };
            AsyncStorage.getItem(AsKey.userLocation)
              .then(JSON.parse)
              .then(value => {
                if (!value) {
                  console.log('location ===>', location);
                  dispatch && locationCall({dispatch, location});
                  AsyncStorage.setItem(AsKey.userLocation);
                } else {
                  dispatch &&
                    locationCall({dispatch: dispatch, location: value});
                }
              });
          }
        },
        error => {
          console.warn('map error: ', error);
          console.warn(error.code, error.message);
          Platform.OS === 'ios' && Geolocation.requestAuthorization('always');
        },
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
      );
    });
  }

  Geolocation.getCurrentPosition(
    position => {
      if (position) {
        const location = {
          lat: position?.coords?.latitude,
          lng: position?.coords?.longitude,
        };
        AsyncStorage.getItem(AsKey.userLocation)
          .then(JSON.parse)
          .then(res => {
            if (!res) {
              console.log('location ===>', location);
              dispatch && locationCall({dispatch, location});
              AsyncStorage.setItem(AsKey.userLocation);
            } else {
              dispatch && locationCall({dispatch: dispatch, location: res});
            }
          });
      }
    },
    error => {
      console.warn('map error: ', error);
      console.warn(error.code, error.message);
      Platform.OS === 'ios' && Geolocation.requestAuthorization('always');

      //   if (error.code == 2) {
      //     Alert.alert(
      //       'Hold On',
      //       'You need to turn on location for use features in Application',
      //       [
      //         // {
      //         //   text: 'Ask me later',
      //         //   onPress: () => console.warn('Ask me later pressed'),
      //         // },
      //         // {
      //         //   text: 'Cancel',
      //         //   onPress: () => console.warn('Cancel Pressed'),
      //         //   style: 'cancel',
      //         // },
      //         {
      //           text: 'OK',
      //           onPress: () => {
      //             console.warn('OK Pressed');
      //             Linking.openURL('app-settings:');
      //           },
      //         },
      //       ],
      //     );
      //   } else {
      //     Platform.OS == 'ios' &&
      //       Alert.alert(
      //         'Hold On',
      //         'You need to Give Location Access for Better Experience',
      //         [
      //           // {
      //           //   text: 'Ask me later',
      //           //   onPress: () => console.warn('Ask me later pressed'),
      //           // },
      //           // {
      //           //   text: 'Cancel',
      //           //   onPress: () => console.warn('Cancel Pressed'),
      //           //   style: 'cancel',
      //           // },
      //           {
      //             text: 'OK',
      //             onPress: () => {
      //               console.warn('OK Pressed');
      //               //   Linking.openURL('app-settings:');
      //             },
      //           },
      //         ],
      //       );
      //   }
      //   Geolocation.requestAuthorization('always');
    },
    {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
  );
};

export const requestLocationPermission = async dispatch => {
  if (Platform.OS === 'ios') {
    getCurrentLocation(dispatch);
  } else {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Device current location permission',
          message: 'Allow app to get your current location',
          // buttonNeutral: 'Ask Me Later',
          // buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      ).then(res => {
        if (res == 'granted') {
          getCurrentLocation(dispatch);
          console.warn('Location permission given Android', true);
          dispatch(locationPermissionStatus(true));
        } else {
          console.warn('Location permission denied Android', false);
          dispatch(locationPermissionStatus(false));
          // Alert.alert('Permission Required', '', [
          //   {
          //     text: 'Okay',
          //     onPress: () => {
          //       Platform.OS === 'android'
          //         ? Linking.openSettings()
          //         : Linking.openURL('app-settings:');
          //     },
          //   },
          // ]);
        }
      });
    } catch (err) {
      console.warn(err);
    }
  }
};
