import {View, Text, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import StarTopDishes from './StarTopDishes';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import Responsive from '../../Constants/Responsive';
import SpStyles from '../../Styles/SpStyles';
import {ReviewModal} from '../Modals/ReviewModal';

const StarReviewDish = ({reviewCount, vendorRatings, otherStyle, isClosed}) => {
  const [reviewData, setReviewData] = useState();
  const [reviewModalVisible, setReviewModalVisible] = useState(false);

  return (
    <View style={[SpStyles.FDR_ALC, otherStyle]}>
      <StarTopDishes
        scale={Responsive.widthPx(3.6)}
        otherStyle={CmnStyles.restaurantCardStarBox}
        vendorRatings={vendorRatings}
        isClosed={isClosed}
      />
      <TouchableOpacity
        onPress={() => {
          // reviewHandler();
        }}>
        <Text
          style={
            isClosed ? TextStyles.darkGray_S_9_400 : TextStyles.blue_S_9_400
          }>
          {reviewCount > 1
            ? `(${reviewCount} Reviews)`
            : reviewCount == 1 && `(${reviewCount} Review)`}
        </Text>
      </TouchableOpacity>
      {reviewModalVisible && (
        <ReviewModal
          modalVisible={reviewModalVisible}
          onPressCancel={() => {
            setReviewModalVisible(false);
          }}
          data={reviewData}
        />
      )}
    </View>
  );
};

export default StarReviewDish;
