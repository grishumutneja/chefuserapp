import {View, Image, FlatList} from 'react-native';
import React from 'react';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';

const starData = [1, 2, 3, 4, 5];

const StarDisplay = ({scale, otherStyle}) => {
  return (
    <View style={otherStyle}>
      <FlatList
        horizontal
        data={starData}
        renderItem={({item, index}) => {
          return (
            <Image
              source={Images.star}
              style={{
                width: scale,
                height: scale,
                marginRight: scale / 2,
                marginTop: Responsive.widthPx(1.5),
              }}
              resizeMode="contain"
            />
          );
        }}
      />
    </View>
  );
};

export default StarDisplay;
