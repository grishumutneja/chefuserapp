import {View, Text} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';
import StarRating from 'react-native-star-rating-widget';

const StarTopDishes = ({scale, otherStyle, vendorRatings, isClosed}) => {
  return (
    <View
      style={[
        {
          flexDirection: 'row',
          paddingHorizontal: Responsive.widthPx(4),
          alignItems: 'center',
          paddingLeft: Responsive.widthPx(4) + 4,
        },
        otherStyle,
      ]}>
      <StarRating
        rating={vendorRatings}
        onChange={() => {}}
        starSize={scale}
        starStyle={{marginLeft: -4}}
        color={isClosed ? '#bbb' : '#fdd835'}
      />
      <Text style={TextStyles.black_S_12_700}>
        {vendorRatings > 0 && vendorRatings}
      </Text>
    </View>
  );
};
export default StarTopDishes;
