import {
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Responsive from '../../Constants/Responsive';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import {getRestDetailApiCall} from '../../Core/Config/ApiCalls';
import Screens from '../../Core/Stack/Screens';

const TrackRestaurantsSlider = ({data}) => {
  const [imgRef, setImgRef] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(0);
  const navigation = useNavigation();
  const {locationData} = useSelector(state => state.location);

  const onPressBanner = item => {
    const body = {
      vendor_id: item?.vendor_id,
      lat: locationData?.lat,
      lng: locationData?.lng,
    };

    getRestDetailApiCall({body}).then(res => {
      navigation.navigate(Screens.RestaurantDetail, {resData: res});
    });
  };

  useEffect(() => {
    const interval = setInterval(() => {
      if (currentIndex == data?.length - 1) {
        setCurrentIndex(0);
        imgRef?.scrollToIndex({animated: true, index: 0});
      } else {
        imgRef?.scrollToIndex({animated: true, index: 1 + currentIndex});
        setCurrentIndex(1 + currentIndex);
      }
    }, 3000);

    return () => clearInterval(interval);
  }, [currentIndex, data]);

  const renderImg = ({item, index}) => {
    return (
      <TouchableOpacity
        style={styles.imgCont}
        onPress={() => {
          onPressBanner(item);
        }}>
        <Image
          source={{uri: item?.image}}
          style={styles.img}
          resizeMode="stretch"
        />
      </TouchableOpacity>
    );
  };

  const renderDots = ({item, index}) => {
    return (
      <View
        style={index == currentIndex ? styles.dotActive : styles.dotInActive}
      />
    );
  };

  return (
    <View>
      <FlatList
        ref={ref => {
          setImgRef(ref);
        }}
        horizontal
        data={data}
        renderItem={renderImg}
        showsHorizontalScrollIndicator={false}
      />
      <FlatList
        horizontal
        data={data}
        renderItem={renderDots}
        style={styles.dotCont}
      />
    </View>
  );
};

export default TrackRestaurantsSlider;

const styles = StyleSheet.create({
  imgCont: {
    width: Responsive.widthPx(90),
    borderRadius: Responsive.widthPx(4),
    height: Responsive.widthPx(40),
  },
  img: {
    width: Responsive.widthPx(90),
    height: Responsive.widthPx(40),
    borderRadius: Responsive.widthPx(4),
  },
  dotActive: {
    width: 10,
    height: 10,
    marginRight: 10,
    borderRadius: 5,
    backgroundColor: '#666',
    transform: [{scale: 0.9}],
  },
  dotInActive: {
    width: 10,
    height: 10,
    marginRight: 10,
    borderRadius: 5,
    backgroundColor: '#999',
    transform: [{scale: 0.7}],
  },
  dotCont: {
    position: 'absolute',
    bottom: 10,
    alignSelf: 'center',
  },
});
