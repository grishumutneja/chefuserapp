import {Image, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import CmnStyles from '../../Styles/CmnStyles';
import {Grayscale} from 'react-native-color-matrix-image-filters';
import SwiperFlatList from 'react-native-swiper-flatlist';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import {useSelector} from 'react-redux';
import {getRestDetailApiCall} from '../../Core/Config/ApiCalls';

const RestaurantsSlider = ({
  data,
  otherStyle,
  isClosed,
  isFullWidth,
  isTrack,
}) => {
  return (
    <View style={{marginBottom: Responsive.widthPx(0)}}>
      <SwiperFlatList
        data={data}
        autoplay
        autoplayDelay={3}
        autoplayLoop
        autoplayLoopKeepAnimation
        showPagination
        paginationStyle={{bottom: 30}}
        paginationStyleItem={CmnStyles.sliderDot}
        renderItem={({item, index}) => (
          <Preview
            item={item}
            index={index}
            isClosed={isClosed}
            otherStyle={otherStyle}
            isTrack={isTrack}
          />
        )}
        width={isFullWidth ? isFullWidth : Responsive.widthPx(92)}
      />
    </View>
  );
};

export default RestaurantsSlider;

const Preview = ({item, index, isClosed, otherStyle, isTrack}) => {
  const navigation = useNavigation();
  const {locationData} = useSelector(state => state.location);

  const onPressBanner = () => {
    const body = {
      vendor_id: item?.vendor_id,
      lat: locationData?.lat,
      lng: locationData?.lng,
    };

    getRestDetailApiCall({body}).then(res => {
      navigation.navigate(Screens.RestaurantDetail, {resData: res});
    });
  };
  return (
    <>
      {isTrack ? (
        <TouchableOpacity
          style={[CmnStyles.restaurantImg, otherStyle]}
          onPress={() => {
            isTrack && onPressBanner();
          }}>
          {isClosed ? (
            <Grayscale>
              <Image
                source={{uri: item?.image}}
                style={[CmnStyles.restaurantImg, otherStyle]}
                resizeMode="stretch"
              />
            </Grayscale>
          ) : (
            <Image
              source={{uri: item?.image}}
              style={[CmnStyles.restaurantImg, otherStyle]}
              resizeMode="stretch"
            />
          )}
        </TouchableOpacity>
      ) : (
        <View style={[CmnStyles.restaurantImg, otherStyle]}>
          {isClosed ? (
            <Grayscale>
              <Image
                source={{uri: item}}
                style={[CmnStyles.restaurantImg, otherStyle]}
                resizeMode="stretch"
              />
            </Grayscale>
          ) : (
            <Image
              source={{uri: isTrack ? item?.image : item}}
              style={[CmnStyles.restaurantImg, otherStyle]}
              resizeMode="stretch"
            />
          )}
        </View>
      )}
    </>
  );
};
