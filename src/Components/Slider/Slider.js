import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, {useRef, useState} from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Responsive from '../../Constants/Responsive';
import Colors from '../../Constants/Colors';
import {getRestDetailApiCall} from '../../Core/Config/ApiCalls';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Utility from '../../Constants/Utility';

const Slider = ({data, otherStyle}) => {
  const ref = useRef();
  const [currentIndex, setCurrentIndex] = useState(0);

  setTimeout(() => {
    if (currentIndex <= data?.length) {
      if (currentIndex === data?.length - 1) {
        setCurrentIndex(0);
        ref?.current?.scrollToIndex({index: 0, animated: true});
      } else {
        setCurrentIndex(currentIndex + 1);
        ref?.current?.scrollToIndex({index: currentIndex + 1, animated: true});
      }
    }
  }, 3000);

  return (
    <View style={otherStyle}>
      {data?.length <= 0 ? (
        <View style={styles.imageViewContainer}>
          <SkeletonPlaceholder borderRadius={4} backgroundColor="#E1E9EE">
            <View style={styles.imageViewContainer} />
          </SkeletonPlaceholder>
        </View>
      ) : (
        <View style={{marginBottom: Responsive.widthPx(16)}}>
          <SwiperFlatList
            data={data}
            autoplay
            autoplayDelay={3}
            autoplayLoop
            autoplayLoopKeepAnimation
            showPagination
            paginationStyle={{position: 'absolute', bottom: -30}}
            paginationStyleItem={styles.dot}
            paginationActiveColor={Colors.red_primary}
            renderItem={({item, index}) => (
              <Preview item={item} index={index} />
            )}
          />
        </View>
      )}
    </View>
  );
};

export default Slider;

const Preview = ({item, index}) => {
  const navigation = useNavigation();
  const {locationData} = useSelector(state => state.location);

  const onPressBanner = () => {
    const body = {
      vendor_id: item?.vendor_id,
      lat: locationData?.lat,
      lng: locationData?.lng,
    };
    if (locationData) {
      getRestDetailApiCall({body}).then(res => {
        navigation.navigate(Screens.RestaurantDetail, {resData: res});
      });
    } else {
      Utility.showToast('Please Add Delivery Address');
    }
  };

  return (
    <TouchableOpacity
      style={styles.imageViewContainer}
      onPress={() => {
        onPressBanner();
      }}>
      <Image
        source={{uri: item?.image}}
        style={styles.imageView}
        resizeMode="stretch"
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  imageView: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    // borderRadius: Responsive.widthPx(2),
  },
  imageViewContainer: {
    width: Responsive.widthPx(100),
    height: Responsive.widthPx(48),
    alignSelf: 'center',
    marginBottom: Responsive.widthPx(2),
    marginTop: 8,
    // marginRight: 10,
  },
  dot: {
    height: 7,
    width: 7,
    borderRadius: 5,
    marginRight: 3,
  },
});
