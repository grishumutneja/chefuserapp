import {Text, TouchableOpacity} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';
import TextStyles from '../../Styles/TextStyles';

const TextButton = ({text, onPress}) => {
  return (
    <TouchableOpacity
      style={{
        alignItems: 'center',
        width: Responsive.widthPx(94),
        alignSelf: 'center',
        marginTop: Responsive.widthPx(3),
      }}
      onPress={onPress}>
      <Text
        style={{
          ...TextStyles.black_S_14_700,
          textDecorationLine: 'underline',
        }}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

export default TextButton;
