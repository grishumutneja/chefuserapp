import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';

const TextCheckButton = ({text, selected, onPress}) => {
  return (
    <TouchableOpacity style={CmnStyles.textCheckBtnCont} onPress={onPress}>
      <Text style={TextStyles.black_S_16_400}>{text}</Text>
      <Image
        source={selected ? Images.checked : Images.unchecked}
        style={CmnStyles.textCheckBtnImg}
        resizeMode="contain"
      />
    </TouchableOpacity>
  );
};

export default TextCheckButton;
