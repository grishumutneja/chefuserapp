import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../Constants/Colors';
import Responsive from '../../Constants/Responsive';
import Images from '../../Constants/Images';

const AddMinusButton = ({imgSource}) => {
  return (
    <TouchableOpacity
      style={{
        backgroundColor: Colors.white,
        borderWidth: 1.2,
        borderColor: Colors.red_primary,
        width: Responsive.widthPx(5),
        height: Responsive.widthPx(5),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: Responsive.widthPx(1),
      }}>
      <Image
        source={imgSource}
        style={{
          width: Responsive.widthPx(2),
          height: Responsive.widthPx(2),
          tintColor: Colors.red_primary,
        }}
        resizeMode="contain"
      />
    </TouchableOpacity>
  );
};

export default AddMinusButton;
