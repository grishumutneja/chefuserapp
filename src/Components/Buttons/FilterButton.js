import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';

const FilterButton = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Image
        source={Images.filter}
        style={CmnStyles.filterImg}
        resizeMode="contain"
      />
    </TouchableOpacity>
  );
};

export default FilterButton;
