import {Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';
import CmnStyles from '../../Styles/CmnStyles';
import {useSelector} from 'react-redux';

const MenuButton = ({onPress}) => {
  const {totalItem} = useSelector(state => state.restaurantDetails);
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        CmnStyles.menuBtnCont,
        totalItem > 0 && {bottom: Responsive.widthPx(30)},
      ]}>
      <Image
        style={{
          height: Responsive.widthPx(4.5),
          width: Responsive.widthPx(4.5),
        }}
        source={Images.dishIcon}
        resizeMode="contain"
      />
      <Text style={CmnStyles.menuBtnText}>Menu</Text>
    </TouchableOpacity>
  );
};

export default MenuButton;
