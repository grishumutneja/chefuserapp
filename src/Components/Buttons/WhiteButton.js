import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import TextStyles from '../../Styles/TextStyles';
import CmnStyles from '../../Styles/CmnStyles';

const WhiteButton = ({text, onPress, otherStyle, otherTextStyle}) => {
  return (
    <TouchableOpacity
      style={[CmnStyles.whiteBtnCont, otherStyle]}
      onPress={onPress}>
      <Text style={[TextStyles.black_S_15_400, otherTextStyle]}>{text}</Text>
    </TouchableOpacity>
  );
};

export default WhiteButton;
