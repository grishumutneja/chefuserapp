import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';

const RedButton = ({text, otherStyle, onPress, otherTextStyle}) => {
  return (
    <TouchableOpacity
      style={[CmnStyles.redButtonCont, otherStyle]}
      onPress={onPress}>
      <Text style={[CmnStyles.redButtonText, otherTextStyle]}>{text}</Text>
    </TouchableOpacity>
  );
};

export default RedButton;
