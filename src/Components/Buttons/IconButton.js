import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import Responsive from '../../Constants/Responsive';

const IconButton = ({imgSource, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Image
        source={imgSource}
        style={{
          width: Responsive.widthPx(10),
          height: Responsive.widthPx(10),
        }}
        resizeMode="contain"
      />
    </TouchableOpacity>
  );
};

export default IconButton;
