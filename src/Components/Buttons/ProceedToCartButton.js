import {View, Text, Image, TouchableOpacity} from 'react-native';
import React, {memo, useEffect} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import Images from '../../Constants/Images';
import SpStyles from '../../Styles/SpStyles';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import {useDispatch, useSelector} from 'react-redux';
import {
  getCartListApiCall,
  updateCartApiCall,
} from '../../Core/Config/ApiCalls';
import {updateCheckOut} from '../../Core/Redux/Slices/RestaurantsSlices/RestaurantDetailsSlice';

const ProceedToCartButton = ({
  vendorId,
  addProducts,
  isFreeDelivery,
  setAddProducts,
  minimum_order_amount,
}) => {
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  const {cartTotalItem, cartVendorId, cartItems} = useSelector(
    state => state.cart,
  );
  const {itemsInCart, totalAmount, totalItem, cart_id, vendor_id} = useSelector(
    state => state.restaurantDetails,
  );
  const {getUserInfoData, proceedCartRefreshCount} = useSelector(
    state => state.userInfo,
  );
  // const [totalAmount, setTotalAmount] = useState();
  // const [totalItem, setTotalItem] = useState(0);

  console.log('itemsInCar ===>', JSON.stringify(itemsInCart));

  useEffect(() => {
    const body = {
      user_id: getUserInfoData?.id,
    };
    if (!cartTotalItem) {
      getCartListApiCall({body, dispatch});
    }
  }, []);

  useEffect(() => {
    if (cartTotalItem > 0) {
      if (cartVendorId == vendorId) {
        let cartData = [];
        cartItems?.map((item, index) => {
          let newItem = Object.assign({});
          let addonPrice = 0;
          let variantPrice = null;
          newItem.product_id = item?.product_id;
          newItem.product_qty = item?.product_qty;
          if (item?.customizable) {
            let variant = [];
            item.variants?.map((itm, ind) => {
              const newItm = Object.assign({});
              if (itm?.added) {
                newItm.variant_id = itm?.variant_id;
                newItm.variant_qty = itm?.qty;
                variant.push(newItm);
                variantPrice =
                  itm?.offer_id != 0
                    ? itm?.after_offer_price
                    : itm?.variant_price;
              }
            });
            let addon = [];
            item.addons?.map((itm, ind) => {
              const newItm = Object.assign({});
              if (itm?.added) {
                newItm.addon_id = itm?.addon_id;
                newItm.addon_qty = itm?.addon_qty;
                addonPrice = addonPrice + parseFloat(itm?.addon_price);
                addon.push(newItm);
              }
            });
            if (variant?.length > 0) {
              newItem.variant = variant;
            }
            if (addon?.length > 0) {
              newItem.addons = addon;
            }
          }
          const proPri =
            item?.offer_id == 0
              ? parseFloat(item?.product_price)
              : parseFloat(item?.after_offer_price);
          const proPrice = variantPrice ? parseFloat(variantPrice) : proPri;
          newItem.product_price = proPrice.toFixed(2);
          newItem.addonPrice = parseFloat(addonPrice);
          cartData.push(newItem);
        });
        setAddProducts(cartData);
        dispatch(updateCheckOut(cartData));
        // AsyncStorage.setItem(AsKey.cartData, JSON.stringify(cartData));
      }
    }
  }, [isFocused]);

  const navigation = useNavigation();

  return (
    <View key={itemsInCart}>
      {cartTotalItem > 0 && cartVendorId == vendorId && totalItem > 0 ? (
        <View style={CmnStyles.proceedToCartBtnCont}>
          {isFreeDelivery && minimum_order_amount - totalAmount > 0 ? (
            <View style={CmnStyles.proceedToCartBtnSubCont1}>
              <Text style={TextStyles.white_S_15_700}>
                Add item worth ₹
                {parseFloat(minimum_order_amount - totalAmount).toFixed(2)} to
                get FREE delivery
              </Text>
            </View>
          ) : (
            isFreeDelivery && (
              <View style={CmnStyles.proceedToCartBtnSubCont1}>
                <Text style={TextStyles.white_S_15_700}>
                  {`Hurray..!  You will get FREE delivery.`}
                </Text>
              </View>
            )
          )}
          <View style={CmnStyles.proceedToCartBtnSubCont}>
            <View>
              <Text style={TextStyles.white_S_14_700}>{totalItem} item</Text>
              <Text style={TextStyles.white_S_14_700}>
                ₹ {totalAmount?.toFixed(2)} plus taxes
              </Text>
            </View>
            <TouchableOpacity
              style={SpStyles.FDR_ALC}
              onPress={() => {
                const body = {
                  cart_id: cart_id,
                  user_id: getUserInfoData?.id,
                  vendor_id: vendor_id,
                  products: itemsInCart,
                };
                if (vendor_id && cart_id) {
                  console.log('ksdjkkdjcjasdjskljdkjsjlk');
                  updateCartApiCall({body, dispatch});
                }

                navigation.navigate(Screens.Cart, {
                  isFromResDetail: true,
                });
              }}
              activeOpacity={0.5}>
              <Text style={TextStyles.white_S_16_700}>Proceed to checkout</Text>
              <Image
                source={Images.back}
                style={CmnStyles.proceedToCartBtn}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
      ) : null}
    </View>
  );
};

export default memo(ProceedToCartButton);
