import {View, Text, TouchableOpacity, Image} from 'react-native';
import React, {memo} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import TextStyles from '../../Styles/TextStyles';

const OnePlusItemBox = ({
  otherStyle,
  productQty,
  otherTextStyle,
  onPressAdd,
  onPressMinus,
}) => {
  return (
    <View style={[CmnStyles.onePlusItemBoxCont, otherStyle]}>
      <TouchableOpacity
        onPress={onPressMinus}
        style={CmnStyles.onePlusItemBoxIconBox}>
        <Image source={Images.minus} style={CmnStyles.onePlusItemBoxIcon} />
      </TouchableOpacity>
      <Text style={[TextStyles.red_16_700, otherTextStyle]}>{productQty}</Text>
      <TouchableOpacity
        onPress={onPressAdd}
        style={CmnStyles.onePlusItemBoxIconBox}>
        <Image source={Images.plusIcon} style={CmnStyles.onePlusItemBoxIcon} />
      </TouchableOpacity>
    </View>
  );
};

export default memo(OnePlusItemBox);
