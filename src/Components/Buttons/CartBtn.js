import {View, Text, TouchableOpacity, Image} from 'react-native';
import React, {useEffect} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Screens from '../../Core/Stack/Screens';
import {useNavigation} from '@react-navigation/native';
import Images from '../../Constants/Images';
import TextStyles from '../../Styles/TextStyles';
import {useDispatch, useSelector} from 'react-redux';
import {getCartListApiCall} from '../../Core/Config/ApiCalls';

const CartBtn = ({otherStyle}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {cartTotalItem} = useSelector(state => state.cart);
  const {getUserInfoData} = useSelector(state => state.userInfo);

  useEffect(() => {
    const body = {
      user_id: getUserInfoData?.id,
    };
    !cartTotalItem && getCartListApiCall({body, dispatch});
  }, []);

  return (
    <TouchableOpacity
      style={[CmnStyles.cartHeaderImg, otherStyle]}
      onPress={() => {
        navigation.navigate(Screens.Cart);
      }}>
      <Image
        source={Images.cart}
        style={CmnStyles.cartHeaderImg}
        resizeMode="contain"
      />
      {cartTotalItem > 0 && (
        <View style={CmnStyles.cartHeaderValBox}>
          <Text style={TextStyles.white_S_11_700}>{cartTotalItem}</Text>
        </View>
      )}
    </TouchableOpacity>
  );
};

export default CartBtn;
