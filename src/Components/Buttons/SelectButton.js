import {View, TouchableOpacity, Animated} from 'react-native';
import React from 'react';
import SpStyles from '../../Styles/SpStyles';
import Responsive from '../../Constants/Responsive';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import Colors from '../../Constants/Colors';

const SelectButton = ({active, setActive}) => {
  return (
    <View
      style={[
        SpStyles.FDR_ALC_JCS,
        {position: 'absolute', top: Responsive.widthPx(10)},
      ]}>
      <BarButton
        text={'Restaurants'}
        isFocused={active === 'Restaurants'}
        onPress={() => {
          setActive('Restaurants');
        }}
      />
      <BarButton
        text={'Dishes'}
        isFocused={active === 'Dishes'}
        onPress={() => {
          setActive('Dishes');
        }}
      />
    </View>
  );
};

export default SelectButton;

const BarButton = ({isFocused, text, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        backgroundColor: isFocused ? Colors.red_primary : Colors.lightGray,
        ...CmnStyles.topTapBarBox2,
      }}>
      <Animated.Text style={TextStyles.white_S_15_700}>{text}</Animated.Text>
    </TouchableOpacity>
  );
};
