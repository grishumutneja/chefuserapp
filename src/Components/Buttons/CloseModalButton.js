import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';

const CloseModalButton = ({onPress, otherStyle}) => {
  return (
    <TouchableOpacity
      style={[CmnStyles.closeModalButtonCont, otherStyle]}
      onPress={onPress}>
      <Image source={Images.cancel} style={CmnStyles.closeModalButtonImg} />
    </TouchableOpacity>
  );
};

export default CloseModalButton;
