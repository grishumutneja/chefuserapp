import {Text, TouchableOpacity} from 'react-native';
import React from 'react';
import TextStyles from '../../Styles/TextStyles';
import CmnStyles from '../../Styles/CmnStyles';

const RedBorderButton = ({text, onPress, otherStyle, otherTextStyle}) => {
  return (
    <TouchableOpacity
      style={[CmnStyles.redBtnCont, otherStyle]}
      onPress={onPress}>
      <Text style={[TextStyles.red_14_700, otherTextStyle]}>{text}</Text>
    </TouchableOpacity>
  );
};

export default RedBorderButton;
