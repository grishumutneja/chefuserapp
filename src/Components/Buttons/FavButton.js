import {View, Text, TouchableOpacity, Image} from 'react-native';
import React, {useState} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Colors from '../../Constants/Colors';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';

const FavButton = ({otherStyle, imgOtherStyle, is_like, onPress}) => {
  return (
    <TouchableOpacity
      style={[
        CmnStyles.foodiesFavCardFavImgBox,
        {backgroundColor: is_like ? Colors.lightGray2 : Colors.lightGray2},
        otherStyle,
      ]}
      onPress={onPress}>
      <Image
        source={Images.favorite}
        style={[
          {
            width: Responsive.widthPx(6),
            height: Responsive.widthPx(6),
            tintColor: is_like ? Colors.red : Colors.darkGray,
          },
          imgOtherStyle,
        ]}
        resizeMode="contain"
      />
    </TouchableOpacity>
  );
};

export default FavButton;
