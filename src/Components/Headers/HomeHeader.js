import {View, Text, Image, TouchableOpacity, Alert} from 'react-native';
import React, {useEffect, useState} from 'react';
import Images from '../../Constants/Images';
import CmnStyles from '../../Styles/CmnStyles';
import TextStyles from '../../Styles/TextStyles';
import SpStyles from '../../Styles/SpStyles';
import Responsive from '../../Constants/Responsive';
import {useNavigation} from '@react-navigation/native';
import Screens from '../../Core/Stack/Screens';
import {SelectDeliveryModal} from '../Modals/SelectDeliveryModal';
import {requestLocationPermission} from '../Permissions/LocationPermission';
import {AppSettingAlert} from '../Alerts/AppSettingAlert';
import CartBtn from '../Buttons/CartBtn';
import {useDispatch, useSelector} from 'react-redux';
import {getLocationByLatLong} from '../../Constants/GetLocationByLatLong';
import AsKey from '../../Constants/AsKey';
import {guestAccountHandler} from '../../Core/Config/ApiCalls';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Utility from '../../Constants/Utility';

const HomeHeader = ({backVisible}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [selectDelModalVisible, setSelectDelModalVisible] = useState(false);
  const {locationData} = useSelector(state => state.location);
  const [locationName, setLocationName] = useState();
  const [cityName, setCityName] = useState();
  const {foodForYouData, foodCategoriesData} = useSelector(
    state => state.foodForYou,
  );
  const {isGuestAccount} = useSelector(state => state.userInfo);
  const [isCartVisible, setIsCartVisible] = useState(false);

  useEffect(() => {
    if (!locationData) {
      requestLocationPermission(dispatch);
    }
  }, []);

  useEffect(() => {
    if (isGuestAccount) {
      setIsCartVisible(false);
    } else {
      if (foodForYouData?.length > 0 && foodCategoriesData?.length > 0) {
        setIsCartVisible(true);
      } else {
        setIsCartVisible(false);
      }
    }
  }, [foodForYouData, foodCategoriesData, isGuestAccount]);

  useEffect(() => {
    AsyncStorage.getItem(AsKey.userLocation)
      .then(JSON.parse)
      .then(res => {
        getLocationByLatLong({
          setLocationName: setLocationName,
          position: res,
          setCityName: setCityName,
        });
      });
  }, [locationData]);

  return (
    <View style={CmnStyles.homeHeaderCont}>
      <View style={SpStyles.FDR_ALC_JCS}>
        {backVisible && (
          <TouchableOpacity
            style={{
              width: Responsive.widthPx(10),
              height: Responsive.widthPx(10),
              alignItems: 'center',
              justifyContent: 'center',
              // marginRight: Responsive.widthPx(1),
              marginLeft: Responsive.widthPx(-3),
            }}
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              source={Images.back}
              style={{
                width: Responsive.widthPx(4.5),
                height: Responsive.widthPx(4.5),
              }}
              resizeMode="contain"
            />
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={SpStyles.FDR_ALC}
          onPress={() => {
            const result = requestLocationPermission();
            if (result) {
              setSelectDelModalVisible(true);
            } else {
              AppSettingAlert();
            }
          }}>
          <Image
            source={Images.location}
            style={CmnStyles.locationHeaderImg}
            resizeMode="contain"
          />
          <View>
            <View style={SpStyles.FDR_ALC}>
              <Text style={CmnStyles.homeHeaderTitle} numberOfLines={1}>
                {locationData?.address_type == '1'
                  ? 'Home'
                  : locationData?.address_type == '2'
                  ? 'Work'
                  : locationData?.address_type == 3
                  ? 'Others'
                  : locationName?.split(',')[1]}
              </Text>
              <Image
                source={Images.back}
                style={CmnStyles.homeHeaderDownImg}
                resizeMode="contain"
              />
            </View>
            <Text style={TextStyles.black_S_12_400}>
              {locationData?.address_type
                ? locationData?.fullAddress
                : cityName
                ? `  ${cityName?.split(',')[0]}`
                : ''}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {isCartVisible && <CartBtn />}
      <SelectDeliveryModal
        modalVisible={selectDelModalVisible}
        onPressCancel={() => {
          setSelectDelModalVisible(false);
        }}
        onPressAddAddress={() => {
          if (isGuestAccount) {
            setSelectDelModalVisible(false);
            guestAccountHandler({navigation});
          } else {
            if (!locationData) {
              Utility.showToast('Please Enable Location and Restart App');
            } else {
              navigation.navigate(Screens.Maps);
              setSelectDelModalVisible(false);
            }
          }
        }}
        setSelectDelModalVisible={setSelectDelModalVisible}
      />
    </View>
  );
};

export default HomeHeader;
