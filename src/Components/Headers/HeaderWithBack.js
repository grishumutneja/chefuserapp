import {View, Text, Image, TouchableOpacity, BackHandler} from 'react-native';
import React, {useEffect} from 'react';
import CmnStyles from '../../Styles/CmnStyles';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';
import {useNavigation} from '@react-navigation/native';
import SpStyles from '../../Styles/SpStyles';
import TextStyles from '../../Styles/TextStyles';
import Screens from '../../Core/Stack/Screens';
import CartBtn from '../Buttons/CartBtn';

const HeaderWithBack = ({
  text,
  backVisible,
  onPressRightImg,
  rightImg,
  rightImgOtherStyle,
  downText,
  cartVisible,
  onPressBack,
  rightText,
  onPressRightText,
}) => {
  const navigation = useNavigation();

  return (
    <View style={CmnStyles.authHeaderCont}>
      <View style={SpStyles.FDR_ALC}>
        {backVisible && (
          <TouchableOpacity
            style={CmnStyles.backImgHeaderBox}
            onPress={
              onPressBack
                ? onPressBack
                : () => {
                    navigation.goBack();
                  }
            }>
            <Image
              source={Images.back}
              style={CmnStyles.backImg}
              resizeMode="contain"
            />
          </TouchableOpacity>
        )}
        <View>
          <Text style={CmnStyles.authHeaderText}>{text}</Text>
          {downText && (
            <Text
              style={[
                TextStyles.red_12_700,
                {maxWidth: Responsive.widthPx(75)},
              ]}
              numberOfLines={1}>
              {downText}
            </Text>
          )}
        </View>
      </View>
      {rightImg && (
        <TouchableOpacity
          style={CmnStyles.headerRightImgBox}
          onPress={onPressRightImg}>
          <Image
            source={rightImg}
            style={[CmnStyles.rightImg, rightImgOtherStyle]}
            resizeMode="contain"
          />
        </TouchableOpacity>
      )}
      {rightText && (
        <TouchableOpacity
          style={CmnStyles.headerRightTextBox}
          onPress={onPressRightText}>
          <Text style={TextStyles.darkGray_S_16_700}>{rightText}</Text>
          <Image
            source={Images.back}
            style={CmnStyles.next}
            resizeMode="contain"
          />
        </TouchableOpacity>
      )}
      {cartVisible && (
        <CartBtn otherStyle={{marginRight: Responsive.widthPx(6)}} />
      )}
    </View>
  );
};

export default HeaderWithBack;
