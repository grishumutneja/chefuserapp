import {View, Pressable, Image, StyleSheet} from 'react-native';
import React from 'react';
import {MotiView} from 'moti';
import {MotiTransitionProp} from '@motify/core';
import {Easing} from 'react-native-reanimated';
import Colors from '../../Constants/Colors';
import SpStyles from '../../Styles/SpStyles';
import Responsive from '../../Constants/Responsive';

const AnimatedSwitch = ({isActive, size, onPress, imgBgColor, imgSource}) => {
  const transition: MotiTransitionProp = {
    type: 'timing',
    duration: 100,
    easing: Easing.inOut(Easing.ease),
  };

  const trackWidth = React.useMemo(() => {
    return size * 2;
  }, [size]);
  const trackHeight = React.useMemo(() => {
    return size * 1.2;
  }, [size]);

  const knobSize = React.useMemo(() => {
    return size * 0.6;
  }, [size]);

  const _colors = {active: imgBgColor, inactive: Colors.lightGray};

  return (
    <Pressable onPress={onPress}>
      <View style={[styles.cont, {marginLeft: size}]}>
        <MotiView
          transition={transition}
          style={{
            position: 'absolute',
            width: trackWidth * 1.2,
            height: trackHeight,
            borderRadius: trackHeight / 2,
            backgroundColor: _colors.inactive,
          }}
        />
        <MotiView
          transition={transition}
          animate={{
            translateX: isActive ? trackWidth / 3 : -(trackHeight / 2),
          }}
          style={{
            borderBottomColor: isActive ? imgBgColor : Colors.darkGray,
            borderLeftWidth: size,
            borderRightWidth: size,
            borderBottomWidth: size * 2,
            ...styles.triangle,
            width: size * 1.7,
            height: size * 1.7,
          }}>
          <MotiView
            transition={transition}
            from={{opacity: isActive ? 0 : 1}}
            animate={{opacity: isActive ? 1 : 0}}
          />
        </MotiView>
      </View>
    </Pressable>
  );
};

export default AnimatedSwitch;

const styles = StyleSheet.create({
  triangle: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    marginBottom: Responsive.widthPx(0.3),
  },
  cont: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
