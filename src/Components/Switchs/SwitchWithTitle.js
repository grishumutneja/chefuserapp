import {View, Text} from 'react-native';
import React from 'react';
import AnimatedSwitch from './AnimatedSwitch';
import TextStyles from '../../Styles/TextStyles';
import Colors from '../../Constants/Colors';
import Images from '../../Constants/Images';
import Responsive from '../../Constants/Responsive';
import SpStyles from '../../Styles/SpStyles';

const SwitchWithTitle = ({onPress, value, imgBgColor, imgSource, text}) => {
  return (
    <View style={{...SpStyles.FDR_ALC, marginRight: Responsive.widthPx(3)}}>
      <AnimatedSwitch
        size={Responsive.widthPx(2.5)}
        onPress={onPress}
        isActive={value}
        imgBgColor={imgBgColor}
        imgSource={imgSource}
      />
      <Text
        style={{
          ...TextStyles.darkGray_S_14_700,
          marginLeft: Responsive.widthPx(2.5),
        }}>
        {text}
      </Text>
    </View>
  );
};

export default SwitchWithTitle;
