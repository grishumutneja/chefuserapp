import {Alert, Platform, Linking} from 'react-native';

export const AppSettingAlert = () => {
  Alert.alert('Alert Title', 'My Alert Msg', [
    {
      text: 'OK',
      onPress: () => {
        Platform.OS === 'android'
          ? Linking.openSettings()
          : Linking.openURL('app-settings:');
      },
    },
  ]);
};
