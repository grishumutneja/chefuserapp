import React, {useEffect, useState} from 'react';
import Route from './src/Core/Route';
import {Provider} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import messaging from '@react-native-firebase/messaging';
import {store} from './src/Core/Redux/Store';
import NoInternetScreen from './src/Screens/OtherScreen/NoInternetScreen';
import {requestUserPermission} from './src/Core/fireBase/notificationService';
import {AndroidImportance} from '@notifee/react-native';
import notifee from '@notifee/react-native';
import Toast from 'react-native-toast-message';
import CustomToast from './src/Components/Toast/CustomToast';
import {LogBox, Platform} from 'react-native';
import AppContext from './src/Core/Redux/AppContext';
import {requestTrackingPermission} from 'react-native-tracking-transparency';
import {DefaultTheme, PaperProvider} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DeviceInfo from 'react-native-device-info';
import RNFS from 'react-native-fs';
import RNRestart from 'react-native-restart'; // Import package from node modules

LogBox.ignoreLogs([
  'VirtualizedLists should never be nested inside plain ScrollViews with the same orientation because it can break windowing and other functionality - use another VirtualizedList-backed container instead.',
]);

const App = () => {
  const [isNoInternet, setIsNoInternet] = useState(false);
  const [notificationData, setNotificationData] = useState();
  const [toastActive, setToastActive] = useState();

  useEffect(() => {
    if (Platform.OS == 'ios') {
      requestTrackingPermission();
    }
  }, []);

  useEffect(() => {
    checkVersion();
    NetInfo.addEventListener(state => {
      if (state.isConnected) {
        setIsNoInternet(false);
      } else {
        setIsNoInternet(true);
      }
    });
  }, []);

  useEffect(() => {
    messaging()
      .subscribeToTopic('ChefLab_User_Testing')
      .then(() => {
        // console.log('Subscribed to topic!');
      });
  }, []);

  const userSettings = {
    notificationData: notificationData,
  };

  // useEffect(() => {
  //   messaging()
  //     .subscribeToTopic('ChefLab_User')
  //     .then(() => console.log('Subscribed to topic!'));
  // }, []);

  useEffect(() => {
    requestUserPermission();
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      let dataP = JSON.parse(remoteMessage.data?.data);
      // setNotificationData(data);r
      if (Platform.OS == 'android') {
        console.log('remoteMessage onMessage android', remoteMessage);
      } else {
        console.log('remoteMessage onMessage IOS', remoteMessage);
      }
      if (
        remoteMessage.data?.data != null &&
        remoteMessage.data?.data != undefined
      ) {
        let data = JSON.parse(remoteMessage.data?.data);
        // setNotificationData(data);r
        setNotificationData(remoteMessage);
        DisplayNotification(remoteMessage);
      }
    });
    DisplayNotification(notificationData);
    return unsubscribe;
  }, []);

  // Function to clear the cache directory
  const clearAppCache = async currentVersion => {
    console.log('currentVersion clearAppCache', currentVersion);
    const keys = await AsyncStorage.getAllKeys();
    await AsyncStorage.multiRemove(keys);
    AsyncStorage.setItem('savedVersion', currentVersion);
    console.log('Cache cleared successfully.');
    RNRestart.restart();
    await RNFS.unlink(RNFS.CachesDirectoryPath);
    await RNFS.unlink(RNFS.TemporaryDirectoryPath);
  };

  const checkVersion = async () => {
    const currentVersion = DeviceInfo.getVersion();
    console.log('currentVersion', currentVersion);
    const savedVersion = await AsyncStorage.getItem('savedVersion')
      .then(JSON.parse)
      .then(async val => {
        console.log('val currentVersion', val);
        console.log('val == currentVersion', val == currentVersion);
        if (!val || val != currentVersion) {
          clearAppCache(currentVersion);
          // RNRestart.restart();
        }
      }); // this will return null, because the previous version didn't set this value
  };

  messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('remoteMessage backGround ', remoteMessage);
    if (
      remoteMessage.data?.data != null &&
      remoteMessage.data?.data != undefined
    ) {
      let data = JSON.parse(remoteMessage.data?.data);
      console.log('unsubscribe onMessage -> ');
      setNotificationData(data);
      setNotificationData(remoteMessage);
      DisplayNotification(remoteMessage);
    }
  });

  const DisplayNotification = async remoteMessage => {
    const channelId = await notifee.createChannel({
      id: 'ChefLab',
      name: 'ChefLab_Channel',
      importance: AndroidImportance.HIGH,
    });
    remoteMessage &&
      (await notifee.displayNotification({
        title: remoteMessage?.notification.title,
        body: remoteMessage?.notification.body,
        android: {
          channelId: channelId,
          loopSound: false,
        },
      }));
  };

  const toastConfig = {
    tomatoToast: ({text1, props}) => (
      <CustomToast text1={text1} props={props} />
    ),
  };

  useEffect(() => {
    setTimeout(() => {
      setToastActive();
    }, 3000);
  }, [toastActive]);

  const theme = {
    ...DefaultTheme,
    // Specify custom property
    myOwnProperty: true,
    // Specify custom property in nested object
    colors: {
      // to make it 'transparent, you can make it same color as your background
      secondaryContainer: 'transparent',
    },
  };

  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <AppContext.Provider value={userSettings}>
          {isNoInternet ? <NoInternetScreen /> : <Route />}
          <Toast config={toastConfig} />
        </AppContext.Provider>
      </PaperProvider>
    </Provider>
  );
};

export default App;
